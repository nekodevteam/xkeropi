/**
 * @file	midiout.h
 * @brief	MIDI OUT クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

struct IMidiOut;
typedef struct IMidiOut *MIDIOUTHDL;

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void MidiOut_Init(void);
LPCTSTR MidiOut_GetModuleName(unsigned int num);
MIDIOUTHDL MidiOut_Create(void);
void MidiOut_Destroy(MIDIOUTHDL hdl);
void MidiOut_SendShort(MIDIOUTHDL hdl, uint32_t msg);
void MidiOut_SendLong(MIDIOUTHDL hdl, const uint8_t *excv, uint32_t length);
void MidiOut_Process16(MIDIOUTHDL hdl, int16_t* buffer, uint32_t length);
void MidiOut_Update(void);

#ifdef __cplusplus
}
#endif	// __cplusplus
