// ---------------------------------------------------------------------------------------
//  WINDRAW.C - DirectDrawによる画面描画
//              ビデオコントローラによるプライオリティ処理も含んでいます
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	<ddraw.h>
#include	"resource.h"
#include	"windraw.h"
#include	"winui.h"
#include	"status.h"
#include	"prop.h"
#include	"winx68k.h"
#include	"mouse.h"
#include	"../x68k/crtc.h"
#include	"../x68k/palette.h"
#include	"../x68k/tvram.h"

#define SUPPORT_32BPP

#define BMPSIZEX 64
#define BMPSIZEY 64
#define BMPID IDB_KEROPI

	WORD	ScrBuf[FULLSCREEN_WIDTH*FULLSCREEN_HEIGHT];
//	WORD*	ScrBuf = 0;

	int	FullScreenFlag = 0;
	BYTE	Draw_DrawFlag = 1;

static	LPDIRECTDRAW4		dd = NULL;
static	LPDIRECTDRAWSURFACE4	prsurf = NULL;
static	LPDIRECTDRAWSURFACE4	bksurf = NULL;
static	LPDIRECTDRAWCLIPPER	clipper = NULL;
static	LPDIRECTDRAWSURFACE4	splashsurf = NULL;

	int	winx=0, winy=0;
	int	winh=0, winw=0;
	WORD	FrameCount = 0;
	int	SplashFlag = 0;

	WORD	WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;

	int	WindowX = 0;
	int	WindowY = 0;

void WinDraw_InitWindowSize(WORD width, WORD height)
{
	RECT	rectWindow, rectClient;
	int	scx, scy;

	GetWindowRect(hWndMain, &rectWindow);
	GetClientRect(hWndMain, &rectClient);
	winw = width + (rectWindow.right - rectWindow.left)
					- (rectClient.right - rectClient.left);
	winh = height + (rectWindow.bottom - rectWindow.top)
					- (rectClient.bottom - rectClient.top);

	scx = GetSystemMetrics(SM_CXSCREEN);
	scy = GetSystemMetrics(SM_CYSCREEN);

	if (scx < winw)
		winx = (scx - winw) / 2;
	else if (winx < 0)
		winx = 0;
	else if ((winx + winw) > scx)
		winx = scx - winw;
	if (scy < winh)
		winy = (scy - winh) / 2;
	else if (winy < 0)
		winy = 0;
	else if ((winy + winh) > scy)
		winy = scy - winh;

	if (hWndStat) winh += heightStat;
}


void WinDraw_ChangeSize(void)
{
	DDBLTFX	ddbf;
	RECT	rct = {0, 0, 800, 600};
	int oldx=WindowX, oldy=WindowY, dif;
	TextDotX = min(TextDotX, (FULLSCREEN_WIDTH - 16));
	TextDotY = min(TextDotY, (FULLSCREEN_HEIGHT - 16));
	Mouse_ChangePos();
	switch(Config.WinStrech)
	{
	case 0:		// No Stretch
		WindowX = TextDotX;
		WindowY = TextDotY;
		break;
	case 1:		// Fixed Stretch
		WindowX = 768;
		WindowY = 512;
		break;
	case 2:		// Auto Stretch
		if (TextDotX<=384) WindowX=TextDotX*2; else WindowX=TextDotX;
		if (TextDotY<=256) WindowY=TextDotY*2; else WindowY=TextDotY;
		break;
	case 3:		// Auto X68k Size
		if (TextDotX<=384) WindowX=TextDotX*2; else WindowX=TextDotX;
		if (TextDotY<=256) WindowY=TextDotY*2; else WindowY=TextDotY;
		dif = WindowX-WindowY;
		if ((dif>(-32))&&(dif<32)) WindowX = (int)(WindowX*1.25);	// 正方形に近い画面なら、としておこう
		break;
	}
	if ((WindowX>768)||(WindowX<=0)) {
		if (oldx)
			WindowX=oldx;
		else
			WindowX = oldx = 768;
	}
	if ((WindowY>512)||(WindowY<=0)) {
		if (oldy)
			WindowY=oldy;
		else
			WindowY = oldy = 512;
	}

	if ((oldx==WindowX)&&(oldy==WindowY)) return;

	if (FullScreenFlag)
	{
		ZeroMemory(&ddbf, sizeof(ddbf));
		ddbf.dwSize = sizeof(ddbf);
		ddbf.dwFillColor = 0;
		IDirectDrawSurface4_Blt(prsurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
		StatBar_Show(Config.FullScrFDDStat);
	}
	else
	{
		WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
		WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
		StatBar_Show(Config.WindowFDDStat);
		Mouse_ChangePos();
	}
}

#ifdef SUPPORT_32BPP
static uint32_t* pPal15To32 = NULL;
BYTE ScrDirtyLine[1024];
#else	// SUPPORT_32BPP
static int dispflag = 0;
static DEVMODE oldDisp, newDisp;
#endif	// SUPPORT_32BPP

void WinDraw_StartupScreen(void)
{
	LPDIRECTDRAW	dd1;
	dd = NULL;

#ifndef SUPPORT_32BPP
	if ( EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &oldDisp) ) {
		if ( oldDisp.dmBitsPerPel!=16 ) {
			memcpy(&newDisp, &oldDisp, sizeof(DEVMODE));
			newDisp.dmBitsPerPel = 16;
			newDisp.dmFields = DM_BITSPERPEL|DM_DISPLAYFREQUENCY;
			if ( ChangeDisplaySettings(&newDisp, 0)==DISP_CHANGE_SUCCESSFUL ) {
				dispflag = 1;
			}
		}
	}
#endif	// !SUPPORT_32BPP

	if (DirectDrawCreate(NULL, &dd1, NULL) != DD_OK) {
		return/* FALSE*/;
	}
	// 環境によっては失敗する？ ので安全策〜
#ifdef __cplusplus
	if (IDirectDraw_QueryInterface(dd1, IID_IDirectDraw4, reinterpret_cast<LPVOID*>(&dd)) != DD_OK)
#else	// __cplusplus
	if (IDirectDraw_QueryInterface(dd1, &IID_IDirectDraw4, (void **)&dd) != DD_OK)
#endif	// __cplusplus
	{
		dd = 0;
		return/* FALSE*/;
	}

	IDirectDraw_Release(dd1);
	dd1=0;
}


void WinDraw_CleanupScreen(void)
{
	if (dd     ) IDirectDraw4_Release(dd);
	dd=0;

#ifdef SUPPORT_32BPP
	if (pPal15To32)
		free(pPal15To32);
	pPal15To32 = NULL;
#else	// SUPPORT_32BPP
	if ( dispflag )
		ChangeDisplaySettings(&oldDisp, 0);
#endif	// SUPPORT_32BPP
}


void WinDraw_ChangeMode(int flag) {
	DWORD	winstyle, winstyleex;
	//HMENU	hMenu = GetMenu(hWndMain);

	FullScreenFlag = flag;
	winstyle = GetWindowLong(hWndMain, GWL_STYLE);
	winstyleex = GetWindowLong(hWndMain, GWL_EXSTYLE);
	if (flag)
	{
		RECT rect;
		GetWindowRect(hWndMain, &rect);
		winx = rect.left;
		winy = rect.top;

		winstyle = (winstyle | WS_POPUP)
						& (~(WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU));
		winstyleex |= WS_EX_TOPMOST;
		SetWindowLong(hWndMain, GWL_STYLE, winstyle);
		SetWindowLong(hWndMain, GWL_EXSTYLE, winstyleex);
		CheckMenuItem(hMenu, IDM_TOGGLEFULLSCREEN, MF_CHECKED);
		CheckMenuItem(hMenu, IDM_MOUSE, MF_CHECKED);
		EnableMenuItem(hMenu, IDM_MOUSE, MF_GRAYED);
	}
	else
	{
		winstyle = (winstyle | WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU)
						& ~WS_POPUP;
		winstyleex &= (~WS_EX_TOPMOST);
		SetWindowLong(hWndMain, GWL_STYLE, winstyle);
		SetWindowLong(hWndMain, GWL_EXSTYLE, winstyleex);

		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);

		CheckMenuItem(hMenu, IDM_TOGGLEFULLSCREEN, MF_UNCHECKED);
		CheckMenuItem(hMenu, IDM_MOUSE, MF_UNCHECKED);
		EnableMenuItem(hMenu, IDM_MOUSE, MF_ENABLED);
	}
}


static void WinDraw_ShowSplash(void)
{
	POINT	pt;
	RECT	rectDst;
	RECT	rectSrc;

	pt.x = (768-BMPSIZEX+16);
	pt.y = (512-BMPSIZEY+16);
#ifdef SUPPORT_32BPP
	pt.x -= 16;
	pt.y -= 16;
	ClientToScreen(hWndMain, &pt);
#endif	// SUPPORT_32BPP
	SetRect(&rectDst, pt.x, pt.y, pt.x + BMPSIZEX, pt.y + BMPSIZEY);
	SetRect(&rectSrc, 0, 0, BMPSIZEX, BMPSIZEY);
#ifdef SUPPORT_32BPP
	IDirectDrawSurface4_Blt(prsurf, &rectDst, splashsurf, &rectSrc, DDBLT_WAIT/*|DDBLT_KEYSRC*/, NULL);
#else	// SUPPORT_32BPP
	IDirectDrawSurface4_Blt(bksurf, &rectDst, splashsurf, &rectSrc, DDBLT_WAIT/*|DDBLT_KEYSRC*/, NULL);
#endif	// SUPPORT_32BPP
}


void WinDraw_HideSplash(void)
{
	DDBLTFX	ddbf;
	RECT	rct = {768-BMPSIZEX+16, 512-BMPSIZEY+16, 768+16, 512+16};

//	IDirectDrawSurface4_SetClipper(prsurf, 0);
	ZeroMemory(&ddbf, sizeof(ddbf));
	ddbf.dwSize = sizeof(ddbf);
	ddbf.dwFillColor = 0;
	IDirectDrawSurface4_Blt(bksurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
	Draw_DrawFlag = 1;
}


int WinDraw_Init(void)
{
	DDSURFACEDESC2	ddsd;
//	DDSURFACEDESC	ddsd00;
	DDPIXELFORMAT	ddpf;

	HDC hdcs, hdcd;
	HANDLE hbmp,hbmpold;
	const int caps[] = {
//		DDSCAPS_VIDEOMEMORY | DDSCAPS_LOCALVIDMEM | DDSCAPS_PRIMARYSURFACE,
		DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE | DDSCAPS_OWNDC,
		DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE,
		DDSCAPS_PRIMARYSURFACE,
		0
	};
	int i;

	prsurf = NULL;
	bksurf = NULL;
	clipper = NULL;

	WindowX = 768;
	WindowY = 512;

	if (!dd) return FALSE;			// これも安全策の一環…

	if (FullScreenFlag)
	{
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain,
					DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWREBOOT);
		if (IDirectDraw4_SetDisplayMode(dd, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT,
								 16, 0, 0) != DD_OK) {
#ifdef SUPPORT_32BPP
			if (IDirectDraw4_SetDisplayMode(dd, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT,
								 32, 0, 0) != DD_OK)
#endif	// SUPPORT_32BPP
			return FALSE;
		}
		IDirectDraw4_CreateClipper(dd, 0, &clipper, NULL);
		IDirectDrawClipper_SetHWnd(clipper, 0, hWndMain);

		for (i=0; caps[i]; i++) {
			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_CAPS;
			ddsd.ddsCaps.dwCaps = caps[i];
			if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)==DD_OK ) break;
		}
		if ( !caps[i] ) return FALSE;

		ZeroMemory(&ddpf, sizeof(ddpf));
		ddpf.dwSize = sizeof(DDPIXELFORMAT);
		if (DD_OK != IDirectDrawSurface4_GetPixelFormat(prsurf, &ddpf)) {
			return FALSE;
		}

		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY;
		ddsd.dwWidth = FULLSCREEN_WIDTH;
		ddsd.dwHeight = FULLSCREEN_HEIGHT;
		if (IDirectDraw4_CreateSurface(dd, &ddsd, &bksurf, NULL) != DD_OK) {
			return FALSE;
		}

		if (ddpf.dwRGBBitCount==16) {
			WinDraw_Pal16B = (WORD)ddpf.dwBBitMask;
			WinDraw_Pal16R = (WORD)ddpf.dwRBitMask;
			WinDraw_Pal16G = (WORD)ddpf.dwGBitMask;
			Pal_SetColor();

			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_LPSURFACE;
			ddsd.lpSurface = ScrBuf;
			if (DD_OK != IDirectDrawSurface4_SetSurfaceDesc(bksurf, &ddsd, 0)) return FALSE;
		}
#ifdef SUPPORT_32BPP
		else if (ddpf.dwRGBBitCount==32) {
			unsigned int i = 0;
			pPal15To32 = (uint32_t *)malloc(0x8000 * sizeof(uint32_t));
			for (i = 0; i < 0x8000; i++) {
				const uint8_t r5 = (i >> 10) & 0x1f;
				const uint8_t g5 = (i >>  5) & 0x1f;
				const uint8_t b5 = (i >>  0) & 0x1f;
				const uint8_t r8 = (r5 << 3) + (r5 >> 2);
				const uint8_t g8 = (g5 << 3) + (g5 >> 2);
				const uint8_t b8 = (b5 << 3) + (b5 >> 2);
				pPal15To32[i] = (r8 << 16) + (g8 << 8) + b8;
			}
			WinDraw_Pal16B = 0x001f;
			WinDraw_Pal16G = 0x03e0;
			WinDraw_Pal16R = 0x7c00;
			Pal_SetColor();
		}
#endif	// SUPPORT_32BPP
		else {
			return FALSE;
		}
	}
	else
	{
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain, DDSCL_NORMAL);
		// Primary Surface
#ifdef SUPPORT_32BPP
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
		if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)!=DD_OK ) return FALSE;
#else	// SUPPORT_32BPP
		for (i=0; caps[i]; i++) {
			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_CAPS;
			ddsd.ddsCaps.dwCaps = caps[i];
			if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)==DD_OK ) break;
		}
		if ( !caps[i] ) return FALSE;
#endif	// SUPPORT_32BPP

		IDirectDraw4_CreateClipper(dd, 0, &clipper, NULL);
		IDirectDrawClipper_SetHWnd(clipper, 0, hWndMain);
		IDirectDrawSurface4_SetClipper(prsurf, clipper);

		ZeroMemory(&ddpf, sizeof(ddpf));
		ddpf.dwSize = sizeof(DDPIXELFORMAT);
		if (DD_OK != IDirectDrawSurface4_GetPixelFormat(prsurf, &ddpf)) {
			return FALSE;
		}
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY;
//		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE | DDSCAPS_OWNDC;
//		ddsd.ddsCaps.dwCaps = DDSCAPS_VIDEOMEMORY | DDSCAPS_NONLOCALVIDMEM;
		ddsd.dwWidth = FULLSCREEN_WIDTH;
		ddsd.dwHeight = FULLSCREEN_HEIGHT;

		if (IDirectDraw4_CreateSurface(dd, &ddsd, &bksurf, NULL) != DD_OK) {
			return FALSE;
		}
		if (!(ddpf.dwFlags&DDPF_RGB)) {
			return FALSE;
		}
		if (ddpf.dwRGBBitCount==16) {
			WinDraw_Pal16B = (WORD)ddpf.dwBBitMask;
			WinDraw_Pal16R = (WORD)ddpf.dwRBitMask;
			WinDraw_Pal16G = (WORD)ddpf.dwGBitMask;
			Pal_SetColor();

			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_LPSURFACE;
			ddsd.lpSurface = ScrBuf;
			if (DD_OK != IDirectDrawSurface4_SetSurfaceDesc(bksurf, &ddsd, 0)) {
				return FALSE;
			}
		}
#ifdef SUPPORT_32BPP
		else if (ddpf.dwRGBBitCount==32) {
			unsigned int i = 0;
			pPal15To32 = (uint32_t *)malloc(0x8000 * sizeof(uint32_t));
			for (i = 0; i < 0x8000; i++) {
				const uint8_t r5 = (i >> 10) & 0x1f;
				const uint8_t g5 = (i >>  5) & 0x1f;
				const uint8_t b5 = (i >>  0) & 0x1f;
				const uint8_t r8 = (r5 << 3) + (r5 >> 2);
				const uint8_t g8 = (g5 << 3) + (g5 >> 2);
				const uint8_t b8 = (b5 << 3) + (b5 >> 2);
				pPal15To32[i] = (r8 << 16) + (g8 << 8) + b8;
			}
			WinDraw_Pal16B = 0x001f;
			WinDraw_Pal16G = 0x03e0;
			WinDraw_Pal16R = 0x7c00;
			Pal_SetColor();
		}
#endif	// SUPPORT_32BPP
		else {
			return FALSE;
		}
/*
		if (IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL) == DD_OK) {
			ScrBuf = ddsd.lpSurface;
		} else {
			return FALSE;
		}
*/
	}

							// けろぴーすぷらっしゅ用意
	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof(ddsd);
	ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
	ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
	ddsd.dwWidth = BMPSIZEX;
	ddsd.dwHeight = BMPSIZEY;
	if (IDirectDraw4_CreateSurface(dd, &ddsd, &splashsurf, NULL) != DD_OK) return FALSE;
							// BITMAPを読み込む
	hbmp=LoadBitmap(hInst,MAKEINTRESOURCE(BMPID));
							// サーフェイスへ転送
	hdcs=CreateCompatibleDC(NULL);
	hbmpold=SelectObject(hdcs, hbmp);
	IDirectDrawSurface4_GetDC(splashsurf, &hdcd);
	BitBlt(hdcd, 0, 0, BMPSIZEX, BMPSIZEY, hdcs, 0, 0,SRCCOPY);
	IDirectDrawSurface4_ReleaseDC(splashsurf, hdcd);
	SelectObject(hdcs, hbmpold);
	DeleteDC(hdcs);
	DeleteObject(hbmp);

	return TRUE;
}


void WinDraw_Cleanup(void) {

	if ((FullScreenFlag) && (dd)) {
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain, DDSCL_NORMAL);
#ifndef SUPPORT_32BPP
		if ( dispflag )
			IDirectDraw4_SetDisplayMode(dd, newDisp.dmPelsWidth, newDisp.dmPelsHeight,
				 newDisp.dmBitsPerPel, newDisp.dmDisplayFrequency, 0);
//		IDirectDraw4_RestoreDisplayMode(dd);
#endif	// !SUPPORT_32BPP
	}
	if (splashsurf ) IDirectDrawSurface4_Release(splashsurf);
	if (bksurf ) IDirectDrawSurface4_Release(bksurf);
	if (clipper) IDirectDrawClipper_Release(clipper);
	if (prsurf ) IDirectDrawSurface4_Release(prsurf);
	prsurf=0;
	bksurf=0;
	splashsurf=0;
	clipper=0;
}


void WinDraw_Redraw(void)
{
	Draw_DrawFlag = 1;
	TVRAM_SetAllDirty();
}


void WinDraw_ShowMenu(int flag)
{
	//DDBLTFX	ddbf;
	//RECT	rct = {0, 0, FULLSCREEN_WIDTH, FULLSCREEN_POSY};

	if (!FullScreenFlag) return;
	if (flag)
	{
		IDirectDrawSurface4_SetClipper(prsurf, clipper);
		//SetMenu(hWndMain, hMenu);
		DrawMenuBar(hWndMain);
	}
	else
	{
		IDirectDrawSurface4_SetClipper(prsurf, 0);
		//ZeroMemory(&ddbf, sizeof(ddbf));
		//ddbf.dwSize = sizeof(ddbf);
		//ddbf.dwFillColor = 0;
		//IDirectDrawSurface4_Blt(prsurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
		//SetMenu(hWndMain, NULL);
		StatBar_Redraw();
	}
#ifdef SUPPORT_32BPP
	WinDraw_Redraw();
#endif	// SUPPORT_32BPP
}


void FASTCALL WinDraw_Draw(void)
{
	POINT	pt;
	RECT	rectDst;
	RECT	rectSrc;
//	DDSURFACEDESC2	LockedSurface;
	int sx, sy;

	FrameCount++;

	if ((!prsurf)||(!bksurf)) return;

#ifdef SUPPORT_32BPP
	if (SplashFlag)
		WinDraw_ShowSplash();
#endif	// SUPPORT_32BPP

	if (!Draw_DrawFlag) return;
	Draw_DrawFlag = 0;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
		CRTC.AllFlash = 0;
		_TVRAM_SetAllDirty();
	}
#endif	// USES_ALLFLASH

#ifndef SUPPORT_32BPP
	if (SplashFlag)
		WinDraw_ShowSplash();
#endif	// !SUPPORT_32BPP

	if (TextDotX>SCREEN_WIDTH)  sx = (TextDotX-SCREEN_WIDTH)/2;  else sx = 0;
	if (TextDotY>SCREEN_HEIGHT) sy = (TextDotY-SCREEN_HEIGHT)/2; else sy = 0;

	if (FullScreenFlag)
	{
		pt.x = (800-WindowX)/2;
		pt.y = (600-WindowY)/2;
		ClientToScreen(hWndMain, &pt);
		SetRect(&rectDst, pt.x, pt.y, pt.x+WindowX, pt.y+WindowY);
		SetRect(&rectSrc, 16+sx, 16+sy, TextDotX+16-sx, TextDotY+16-sy);
	}
	else
	{
		pt.x = 0;
		pt.y = 0;
		ClientToScreen(hWndMain, &pt);
		SetRect(&rectDst, pt.x, pt.y, pt.x + WindowX, pt.y + WindowY);
		SetRect(&rectSrc, 16+sx, 16+sy, TextDotX+16-sx, TextDotY+16-sy);
	}

#ifdef SUPPORT_32BPP
	if (pPal15To32) {
		HRESULT r;
		DDSURFACEDESC2 ddsd;
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		r = IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL);
		if (r == DDERR_SURFACELOST) {
			IDirectDrawSurface4_Restore(bksurf);
			r = IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL);
		}
		if (r == DD_OK) {
			int x, y;
			uint16_t* p = ScrBuf + (rectSrc.top * FULLSCREEN_WIDTH);
			intptr_t q = (intptr_t)ddsd.lpSurface + (rectSrc.top * ddsd.lPitch);
			for (y = rectSrc.top; y < rectSrc.bottom; y++) {
				if (ScrDirtyLine[y - 16]) {
					ScrDirtyLine[y - 16] = 0;
					for (x = rectSrc.left; x < rectSrc.right; x++) {
						((uint32_t *)q)[x] = pPal15To32[p[x] & 0x7fff];
					}
				}
				p += FULLSCREEN_WIDTH;
				q += ddsd.lPitch;
			}
			IDirectDrawSurface4_Unlock(bksurf, NULL);
		}
	}
#endif	// SUPPORT_32BPP

	if (IDirectDrawSurface4_Blt(prsurf, &rectDst, bksurf, &rectSrc, DDBLT_WAIT, NULL)
		== DDERR_SURFACELOST)
	{
		IDirectDrawSurface4_Restore(prsurf);
		WinDraw_Redraw();
	}
}

void WinDraw_DrawLine(uint32_t v)
{
	if (v >= _countof(TextDirtyLine)) return;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
	}
	else
#endif	// USES_ALLFLASH
	if (!TextDirtyLine[v]) return;
	TextDirtyLine[v] = 0;

	CRTC_DrawLine(&ScrBuf[((v + 16) * FULLSCREEN_WIDTH) + 16], v);

	Draw_DrawFlag = 1;
#ifdef SUPPORT_32BPP
	ScrDirtyLine[v] = 1;
#endif	// SUPPORT_32BPP
}
