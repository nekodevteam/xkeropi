#ifndef _winx68k_config
#define _winx68k_config

typedef struct
{
	DWORD SampleRate;
	DWORD BufferSize;
	int WinPosX;
	int WinPosY;
	int OPM_VOL;
	int PCM_VOL;
	int MCR_VOL;
	int JOY_BTN[2][8];
	int MouseSpeed;
	int WindowFDDStat;
	int FullScrFDDStat;
	int DSAlert;
	TCHAR SoundDevice[MAX_PATH];
	int MIDI_SW;
	int MIDI_Type;
	int MIDI_Reset;
	TCHAR MIDI_Module[MAX_PATH];
	int JoyKey;
	int JoyKeyReverse;
	int JoyKeyJoy2;
	int SRAMWarning;
	TCHAR HDImage[16][MAX_PATH];
	int ToneMap;
	TCHAR ToneMapFile[MAX_PATH];
	int XVIMode;
	int JoySwap;
	int LongFileName;
	int WinDrvFD;
	int WinStrech;
	int DSMixing;
	int CDROM_ASPI;
	int CDROM_ASPI_Drive;
	int CDROM_IOCTRL_Drive;
	int CDROM_SCSIID;
	int CDROM_Enable;
	int SSTP_Enable;
	int SSTP_Port;
	int Sound_LPF;
#ifdef USE_ROMEO
	int SoundROMEO;
#endif	// USE_ROMEO
	int MIDIDelay;
	int MIDIAutoDelay;
} Win68Conf;

EXTERNC Win68Conf Config;

void LoadConfig(void);
void SaveConfig(void);
void PropPage_Init(void);

#endif //_winx68k_config
