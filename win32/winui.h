#ifndef _winx68k_winui_h
#define _winx68k_winui_h

extern	BYTE	FrameRate;
extern	TCHAR	filepath[MAX_PATH];
extern	DWORD	LastClock[4];

void WinUI_Init(void);
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

#endif //winx68k_winui_h
