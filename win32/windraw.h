#ifndef _winx68k_windraw_h
#define _winx68k_windraw_h

extern BYTE Draw_DrawFlag;
extern int winx, winy;
extern int winh, winw;
extern int FullScreenFlag;
extern WORD FrameCount;
extern int SplashFlag;
extern WORD WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;

extern	int	WindowX;
extern	int	WindowY;

void WinDraw_InitWindowSize(WORD width, WORD height);
void WinDraw_ChangeMode(int flag);
int WinDraw_Init(void);
void WinDraw_Cleanup(void);
void WinDraw_Redraw(void);
void FASTCALL WinDraw_Draw(void);
void WinDraw_ShowMenu(int flag);
void WinDraw_DrawLine(uint32_t v);
void WinDraw_HideSplash(void);
void WinDraw_ChangeSize(void);

void WinDraw_StartupScreen(void);
void WinDraw_CleanupScreen(void);

#endif //winx68k_windraw_h
