#ifndef _winx68k_mouse
#define _winx68k_mouse

extern	int	MousePosX;
extern	int	MousePosY;
extern	BYTE	MouseStat;
extern	BYTE	MouseSW;

void Mouse_Init(void);
void Mouse_Event(WPARAM wparam, LPARAM lparam);
void Mouse_SetData(void);
void Mouse_StartCapture(int flag);
void Mouse_ChangePos(void);

#endif //_winx68k_mouse
