#ifndef KEROPI_SSTP_H_
#define	KEROPI_SSTP_H_

void SSTP_Init(void);
void SSTP_Cleanup(void);

void SSTP_Send(char* str);
void SSTP_Communicate(char* str);
void SSTP_Execute(char* cmd);
void SSTP_GiveDoc(char* str);
void SSTP_GiveSong(char* str);

void SSTP_SendMes(int id);


// 使ってないのもあります
#define SSTPMES_OPEN		1
#define SSTPMES_ABOUT		2
#define SSTPMES_ERROR		3
#define SSTPMES_EJECT0		4
#define SSTPMES_EJECT1		5
#define SSTPMES_MOUSEHELP	6
#define SSTPMES_FULLSCRHELP	7
#define SSTPMES_RESET		8
#define SSTPMES_NMI		9
#define SSTPMES_AUTOSTRETCH	10
#define SSTPMES_NONSTRETCH	11
#define SSTPMES_FIXEDSTRETCH	12
#define SSTPMES_DROPFD0		13
#define SSTPMES_DROPFD1		14
#define SSTPMES_MAKEFONT	15
#define SSTPMES_WAITFD		16
#define SSTPMES_DUALBOOT	17
#define SSTPMES_X68STRETCH	18

#define SSTPMES_NP2		100
#define SSTPMES_EX68		101
#define SSTPMES_XMIL		102
#define SSTPMES_WINX1		103
#define SSTPMES_XM7		104
#define SSTPMES_MZ700WIN	105
#define SSTPMES_IP6		106
#define SSTPMES_ANEX86		107
#define SSTPMES_T98		108
#define SSTPMES_T98NEXT		109
#define SSTPMES_XXX		110
#define SSTPMES_TOOMANY		130

#endif // KEROPI_SSTP_H_
