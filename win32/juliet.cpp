#include	"common.h"

#include	"romeo.h"
#include	"juliet.h"


enum {
	ROMEO_AVAIL			= 0x01,
	ROMEO_YMF288		= 0x02,			// 必ず存在する筈？
	ROMEO_YM2151		= 0x04
};

typedef struct {
	HMODULE			mod;

	PCIFINDDEV		finddev;
	PCICFGREAD32	read32;
	PCIMEMWR8		out8;
	PCIMEMWR16		out16;
	PCIMEMWR32		out32;
	PCIMEMRD8		in8;
	PCIMEMRD16		in16;
	PCIMEMRD32		in32;

	ULONG			addr;
	ULONG			irq;
	ULONG			avail;
	ULONG			snoopcount;
} _ROMEO;


#define	ROMEO_TPTR(member)	(intptr_t)&(((_ROMEO *)NULL)->member)


typedef struct {
	const char	*symbol;
	intptr_t		addr;
} DLLPROCESS;


static const DLLPROCESS	dllproc[] = {
				{FN_PCIFINDDEV,		ROMEO_TPTR(finddev)},
				{FN_PCICFGREAD32,	ROMEO_TPTR(read32)},
				{FN_PCIMEMWR8,		ROMEO_TPTR(out8)},
				{FN_PCIMEMWR16,		ROMEO_TPTR(out16)},
				{FN_PCIMEMWR32,		ROMEO_TPTR(out32)},
				{FN_PCIMEMRD8,		ROMEO_TPTR(in8)},
				{FN_PCIMEMRD16,		ROMEO_TPTR(in16)},
				{FN_PCIMEMRD32,		ROMEO_TPTR(in32)}};


static	_ROMEO		romeo = {NULL};


BOOL juliet_load(void) {

		int			i;
const	DLLPROCESS	*dp;
		BOOL		r = SUCCESS;

	juliet_unload();

	romeo.mod = LoadLibrary(TEXT(PCIDEBUG_DLL));
	if (romeo.mod == NULL) {
		return(FAILURE);
	}
	for (i=0, dp=dllproc; i<sizeof(dllproc)/sizeof(DLLPROCESS); i++, dp++) {
		FARPROC proc;
		proc = GetProcAddress(romeo.mod, dp->symbol);
		if (proc == NULL) {
			r = FAILURE;
			break;
		}
		*(FARPROC *)(((BYTE *)&romeo) + (dp->addr)) = proc;
	}
	if (r) {
		juliet_unload();
	}
	return(r);
}


void juliet_unload(void) {

	if (romeo.mod) {
		FreeLibrary(romeo.mod);
	}
	ZeroMemory(&romeo, sizeof(romeo));
}


// ----

// pciFindPciDevice使うと、OS起動後一発目に見つけられないことが多いので、自前で検索する（矢野さん方式）
static ULONG searchRomeo(void)
{
	int bus, dev, func;
	for (bus=0; bus<256; bus++) {
		for (dev=0; dev<32; dev++) {
			for (func=0; func<8; func++) {
				ULONG addr = pciBusDevFunc(bus, dev, func);
				ULONG dev_vend = romeo.read32(addr, 0x0000);
				if ( (dev_vend&0xffff)!=ROMEO_VENDORID ) continue;
				dev_vend >>= 16;
				if ( (dev_vend==ROMEO_DEVICEID)||(dev_vend==ROMEO_DEVICEID2) ) return addr;
			}
		}
	}
	return ((ULONG)0xffffffff);
}


BOOL juliet_prepare(void) {

	ULONG	pciaddr;

	if (romeo.mod == NULL) {
		return(FAILURE);
	}

	pciaddr = searchRomeo();
	if ( pciaddr!=(ULONG)0xffffffff ) {
		romeo.addr = romeo.read32(pciaddr, ROMEO_BASEADDRESS1);
		romeo.irq  = romeo.read32(pciaddr, ROMEO_PCIINTERRUPT) & 0xff;
		if (romeo.addr) {
			romeo.avail = ROMEO_AVAIL | ROMEO_YMF288;
			juliet_YM2151Reset();
			juliet_YMF288Reset();
		}
		return (SUCCESS);
	}
	return (FAILURE);
}


// ---- YM2151部
// リセットと同時に、OPMチップの有無も確認
void juliet_YM2151Reset(void)
{
	BYTE flag;

	if (romeo.avail & ROMEO_AVAIL) {
		romeo.out32(romeo.addr + ROMEO_YM2151CTRL, 0x00);
		Sleep(10);					// 44.1kHz x 192 clk = 4.35ms 以上ないと、DACのリセットかからない
		flag = romeo.in8(romeo.addr + ROMEO_YM2151DATA) + 1;
		romeo.out32(romeo.addr + ROMEO_YM2151CTRL, 0x80);
		Sleep(10);					// リセット解除後、一応安定するまでちょっと待つ
		flag |= romeo.in8(romeo.addr + ROMEO_YM2151DATA);
		if ( !flag ) {					// flag!=0 だと OPM チップがない
			romeo.avail |= ROMEO_YM2151;
			// Busy検出用にSnoopカウンタを使う
			romeo.out32(romeo.addr + ROMEO_SNOOPCTRL, (unsigned int)0x80000000);
			romeo.snoopcount = 0xffffffff;
		}
	}
}

int juliet_YM2151IsEnable(void)
{
	return (( romeo.avail&ROMEO_YM2151 )?TRUE:FALSE);
}

int juliet_YM2151IsBusy(void)
{
	int ret = FALSE;
	if ( romeo.avail&ROMEO_YM2151 ) {
		if ( ((romeo.snoopcount==romeo.in32(romeo.addr + ROMEO_SNOOPCTRL)) ||
		      (romeo.in8(romeo.addr + ROMEO_YM2151DATA)&0x80 )) ) ret = TRUE;
	}
	return ret;
}


void juliet_YM2151W(BYTE reg, BYTE data)
{
	if ( romeo.avail&ROMEO_YM2151) {

		// 書き込み直後だと、ROMEOチップでの遅延のため、まだ書き込みが起こっていない（＝Busyが
		// 立っていない）可能性がある。ので、Snoopカウンタで書き込み発生を見張る
		while ( romeo.snoopcount==romeo.in32(romeo.addr + ROMEO_SNOOPCTRL) ) Sleep(0);
		romeo.snoopcount = romeo.in32(romeo.addr + ROMEO_SNOOPCTRL);

		// カウンタ増えた時点ではまだBusyの可能性があるので、OPMのBusyも見張る
		while ( romeo.in8(romeo.addr + ROMEO_YM2151DATA)&0x80 ) Sleep(0);

		romeo.out8(romeo.addr + ROMEO_YM2151ADDR, reg);
		romeo.out8(romeo.addr + ROMEO_YM2151DATA, data);
	}
}

// ---- YMF288部

void juliet_YMF288Reset(void) {

	if (romeo.avail & ROMEO_YMF288) {
		romeo.out32(romeo.addr + ROMEO_YMF288CTRL, 0x00);
		Sleep(150);
		romeo.out32(romeo.addr + ROMEO_YMF288CTRL, 0x80);
		Sleep(150);
	}
}

int juliet_YM288IsEnable(void) {

	return(TRUE);
}

int juliet_YM288IsBusy(void) {

	return((!(romeo.avail&ROMEO_YMF288)) ||
			((romeo.in8(romeo.addr + ROMEO_YMF288ADDR1) & 0x80) != 0));
}

void juliet_YMF288A(BYTE addr, BYTE data) {

	if (romeo.avail & ROMEO_YMF288) {
		while(romeo.in8(romeo.addr + ROMEO_YMF288ADDR1) & 0x80) {
			Sleep(0);
		}
		romeo.out8(romeo.addr + ROMEO_YMF288ADDR1, addr);
		while(romeo.in8(romeo.addr + ROMEO_YMF288ADDR1) & 0x80) {
			Sleep(0);
		}
		romeo.out8(romeo.addr + ROMEO_YMF288DATA1, data);
	}
}

void juliet_YMF288B(BYTE addr, BYTE data) {

	if (romeo.avail & ROMEO_YMF288) {
		while(romeo.in8(romeo.addr + ROMEO_YMF288ADDR1) & 0x80) {
			Sleep(0);
		}
		romeo.out8(romeo.addr + ROMEO_YMF288ADDR2, addr);
		while(romeo.in8(romeo.addr + ROMEO_YMF288ADDR1) & 0x80) {
			Sleep(0);
		}
		romeo.out8(romeo.addr + ROMEO_YMF288DATA2, data);
	}
}

void juliet_YMF288W(BYTE addr, BYTE data)
{
	static const int Adrs[4] = {ROMEO_YMF288ADDR1, ROMEO_YMF288DATA1, ROMEO_YMF288ADDR2, ROMEO_YMF288DATA2};
	if ( (addr<=3)&&(romeo.avail&ROMEO_YMF288) ) {
		romeo.out8(romeo.addr+Adrs[addr], data);
	}
}

BYTE juliet_YMF288R(BYTE addr)
{
	static const int Adrs[4] = {ROMEO_YMF288ADDR1, ROMEO_YMF288DATA1, ROMEO_YMF288ADDR2, ROMEO_YMF288DATA2};
	BYTE ret = 0;
	if ( (addr<=3)&&(romeo.avail&ROMEO_YMF288) ) {
		ret = romeo.in8(romeo.addr+Adrs[addr]);
	}
	return ret;
}
