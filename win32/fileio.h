#ifndef winx68k_fileio_h
#define winx68k_fileio_h

#define	FILEH		HANDLE

#define	FSEEK_SET	0
#define	FSEEK_CUR	1
#define	FSEEK_END	2

LPTSTR	getFileName(LPCTSTR filename);

FILEH	File_Open(LPCTSTR filename);
FILEH	File_Create(LPCTSTR filename);
DWORD	File_Seek(FILEH handle, long pointer, short mode);
DWORD	File_Read(FILEH handle, void *data, DWORD length);
DWORD	File_Write(FILEH handle, const void *data, DWORD length);
short	File_Close(FILEH handle);
short	File_Attr(LPCTSTR filename);

void	File_SetCurDir(LPCTSTR exename);
FILEH	File_OpenCurDir(LPCTSTR filename);
FILEH	File_CreateCurDir(LPCTSTR filename);
short	File_AttrCurDir(LPCTSTR filename);

#endif
