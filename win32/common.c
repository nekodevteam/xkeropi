// ---------------------------------------------------------------------------------------
//  COMMON - 標準ヘッダ群（COMMON.H）とエラーダイアログ表示とか
// ---------------------------------------------------------------------------------------
#include	"common.h"
#include	"winx68k.h"
#include	"sstp.h"
#include	"winx68k.h"

void Error(const char* s)
{
	char title[80];
#ifdef _UNICODE
	sprintf(title, "%S エラー", PrgTitle);
#else	// _UNICODE
	sprintf(title, "%s エラー", PrgTitle);
#endif	// _UNICODE

	SSTP_SendMes(SSTPMES_ERROR);

	MessageBoxA(hWndMain, s, title, MB_ICONERROR | MB_OK);
}

