// ---------------------------------------------------------------------------------------
//  CD-ROM drive access routine by IOCTRL
//    NT系Only（Win95/98不可）
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "cdrom.h"
#include "cdctrl.h"
#include "prop.h"

#include "msfiles\winioctl.h"
#include "msfiles\ntddcdrm.h"

#define RAW_SECTOR_SIZE     2352
#define COOKED_SECTOR_SIZE  2048

static HANDLE	hCD        = NULL;
static LPBYTE	lpSector   = NULL;
static int	CDCTRL_Error    = 0;
static long	LastSector = -1;

static TOC	tocbuf;

//------------------------------------------------------
// 1ブロック読み込み
//------------------------------------------------------
int CDCTRL_Read(long block, BYTE* buf)
{
	DWORD dwNotUsed;

	if (block != LastSector)
	{
		SetFilePointer(hCD, block*2048, NULL, FILE_BEGIN);
		LastSector = block;
	}
	LastSector++;
	ReadFile(hCD, buf, 2048, &dwNotUsed, NULL);
	return TRUE;
}


//------------------------------------------------------
// TOC読み込み
//------------------------------------------------------
int CDCTRL_ReadTOC(void* buf)
{
	if (hCD)
	{
		memcpy(buf, &tocbuf, sizeof(tocbuf));
		return TRUE;
	}
	return FALSE;
}


//------------------------------------------------------
// Wait（意味無し）
//------------------------------------------------------
int CDCTRL_Wait(void)
{
    if (CDCTRL_Error)
    {
        CDCTRL_Error = 0;
        return FALSE;
    }

    return TRUE;
}



//------------------------------------------------------
// CDアクセス準備（Open成功=True）
//------------------------------------------------------
int CDCTRL_Open(void)
{
	TCHAR drv[8]=TEXT("\\\\.\\?:");
	DWORD dwNotUsed;

	drv[4] = (TCHAR)(0x41+Config.CDROM_IOCTRL_Drive);

	hCD = CreateFile (drv, GENERIC_READ,
			FILE_SHARE_READ|FILE_SHARE_WRITE,
			NULL, OPEN_EXISTING,
			FILE_FLAG_NO_BUFFERING,
			NULL);

	if (hCD == INVALID_HANDLE_VALUE) return FALSE;

	lpSector = (LPBYTE)VirtualAlloc (NULL, 4096,
			MEM_COMMIT|MEM_RESERVE,
			PAGE_READWRITE);

	CDCTRL_Error = 0;
	ZeroMemory(&tocbuf, sizeof(tocbuf));
	if (DeviceIoControl(hCD, IOCTL_CDROM_READ_TOC,
			NULL, 0, &tocbuf, CDROM_TOC_SIZE,
			&dwNotUsed, NULL))
	{
		return TRUE;
	}
	else
	{
		if (lpSector)
			VirtualFree (lpSector, 0, MEM_RELEASE);
		CloseHandle (hCD);
		hCD = 0;
		lpSector = NULL;
		return FALSE;
	}
}


//------------------------------------------------------
// Close
//------------------------------------------------------
void CDCTRL_Close(void)
{
	CDCTRL_Wait();

	if (lpSector)
		VirtualFree (lpSector, 0, MEM_RELEASE);
	lpSector = NULL;
	LastSector = -1;

	CloseHandle (hCD);
	hCD = 0;
}

int CDCTRL_IsOpen(void)
{
	if (hCD)
		return TRUE;
	else
		return FALSE;
}
