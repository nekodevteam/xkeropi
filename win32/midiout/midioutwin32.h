/**
 * @file	midioutwin32.h
 * @brief	MIDI OUT win32 クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <vector>
#include "imidiout.h"

#if !defined(__GNUC__)
#pragma comment(lib, "winmm.lib")
#endif	// !defined(__GNUC__)

static const TCHAR szMidiMapper[] = TEXT("MIDI MAPPER");

/**
 * @brief MIDI OUT win32 クラス
 */
class MidiOutWin32 : public IMidiOut
{
public:
	/**
	 * インスタンスを作成
	 * @param[in] lpMidiOut デバイス名
	 * @return インスタンス
	 */
	static MidiOutWin32* CreateInstance(LPCTSTR lpMidiOut)
	{
		UINT nId;
		if (!GetId(lpMidiOut, &nId))
		{
			return NULL;
		}

		HMIDIOUT hMidiOut = NULL;
		if (::midiOutOpen(&hMidiOut, nId, 0, 0, CALLBACK_NULL) != MMSYSERR_NOERROR)
		{
			return NULL;
		}
		return new MidiOutWin32(hMidiOut);
	}

	/**
	 * コンストラクタ
	 * @param[in] hMidiOut ハンドル
	 */
	MidiOutWin32(HMIDIOUT hMidiOut)
		: m_hMidiOut(hMidiOut)
	{
		ZeroMemory(&m_midihdr, sizeof(m_midihdr));
		::midiOutReset(m_hMidiOut);
	}

	/**
	 * デストラクタ
	 */
	virtual ~MidiOutWin32()
	{
		WaitSentExclusive();
		::midiOutReset(m_hMidiOut);
		::midiOutClose(m_hMidiOut);
	}

	/**
	 * ショート メッセージ
	 * @param[in] nMessage メッセージ
	 */
	virtual void Short(uint32_t nMessage)
	{
		WaitSentExclusive();
		::midiOutShortMsg(m_hMidiOut, nMessage);
	}

	/**
	 * ロング メッセージ
	 * @param[in] lpMessage メッセージ ポインタ
	 * @param[in] cbMessage メッセージ サイズ
	 */
	virtual void Long(const uint8_t* lpMessage, uint32_t cbMessage)
	{
		if (cbMessage == 0)
		{
			return;
		}

		WaitSentExclusive();

		m_excvbuf.resize(cbMessage);
		CopyMemory(&m_excvbuf[0], lpMessage, cbMessage);

		m_midihdr.lpData = &m_excvbuf[0];
		m_midihdr.dwFlags = 0;
		m_midihdr.dwBufferLength = cbMessage;
		::midiOutPrepareHeader(m_hMidiOut, &m_midihdr, sizeof(m_midihdr));
		::midiOutLongMsg(m_hMidiOut, &m_midihdr, sizeof(m_midihdr));
	}

	/**
	 * ID を得る
	 * @param[in] lpMidiOut デバイス名
	 * @param[out] pId ID
	 * @retval true 成功
	 * @retval false 失敗
	 */
	static bool GetId(LPCTSTR lpMidiOut, UINT* pId)
	{
		const UINT nNum = ::midiOutGetNumDevs();
		for (UINT i = 0; i < nNum; i++)
		{
			MIDIOUTCAPS moc;
			if (midiOutGetDevCaps(i, &moc, sizeof(moc)) != MMSYSERR_NOERROR)
			{
				continue;
			}
			if (!lstrcmp(lpMidiOut, moc.szPname))
			{
				*pId = i;
				return true;
			}
		}

		if (!lstrcmp(lpMidiOut, szMidiMapper))
		{
			*pId = MIDI_MAPPER;
			return true;
		}
		return false;
	}

private:
	HMIDIOUT m_hMidiOut;					/*!< MIDIOUT ハンドル */
	MIDIHDR m_midihdr;						/*!< MIDIHDR */
	std::vector<char> m_excvbuf;			/*!< エクスクルーシヴ バッファ */

	/**
	 * エクスクルーシブ送信完了を待つ
	 */
	void WaitSentExclusive()
	{
		if (!m_excvbuf.empty())
		{
			while (midiOutUnprepareHeader(m_hMidiOut, &m_midihdr, sizeof(m_midihdr)) == MIDIERR_STILLPLAYING)
			{
			}
			m_excvbuf.clear();
		}
	}
};
