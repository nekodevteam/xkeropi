/**
 * @file	imidiout.h
 * @brief	MIDI OUT 基底クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief MIDI OUT 基底クラス
 */
struct IMidiOut
{
	/**
	 * デストラクタ
	 */
	virtual ~IMidiOut()
	{
	}

	/**
	 * ショート メッセージ
	 * @param[in] nMessage メッセージ
	 */
	virtual void Short(uint32_t nMessage) = 0;

	/**
	 * ロング メッセージ
	 * @param[in] lpMessage メッセージ ポインタ
	 * @param[in] cbMessage メッセージ サイズ
	 */
	virtual void Long(const uint8_t* lpMessage, uint32_t cbMessage) = 0;

	/**
	 * プロセス (16bit)
	 * @param[out] lpBuffer バッファ
	 * @param[in] nBufferCount サンプル数
	 */
	virtual void Process16(int16_t* lpBuffer, uint32_t nBufferCount)
	{
	}
};
