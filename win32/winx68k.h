#ifndef winx68k_wincore_h
#define winx68k_wincore_h

#define		SCREEN_WIDTH		768
#define		SCREEN_HEIGHT		512

#define		FULLSCREEN_WIDTH	800
#define		FULLSCREEN_HEIGHT	600
#define		FULLSCREEN_POSX		((FULLSCREEN_WIDTH - SCREEN_WIDTH) / 2)
#define		FULLSCREEN_POSY		((FULLSCREEN_HEIGHT - SCREEN_HEIGHT) / 2)

extern	const TCHAR	PrgName[];
extern	const TCHAR	PrgTitle[];

extern	TCHAR	winx68k_dir[MAX_PATH];
extern	TCHAR	winx68k_ini[MAX_PATH];

EXTERNC	HINSTANCE	hInst;
EXTERNC	HWND		hWndMain;
extern	HMENU		hMenu;

extern	int	NoWaitMode;

extern	UINT_PTR	hTimerID;
extern	DWORD	TimerICount;

#ifdef WIN68DEBUG
extern	BYTE	traceflag;
#endif	// WIN68DEBUG

int WinX68k_Reset(void);

#endif //winx68k_wincore_h
