# けろぴー (WinX68k)

## WinX68k とは?
けんじょさん謹製 Windows向け X68000 エミュレータです。

## 本レポジトリについて
WinX68k が Windows98 や 2000 当時のソースコードであるため、Windows 10 や 11 では動作はしますが、やや難があります。<br>
このレポジトリでは、これらの不具合を最小限で修正します。

## オリジナルのソースコードについて
オリジナルのソースコードは うささんの [うさの倉庫](http://retropc.net/usalin/) から入手可能です。

## オリジナル クレジット
~~~
Keropi(WinX68k)
SHARP X680x0 series emulator
Version 0.65 w/ SSTP1.0
Copyright (C) 2000-02 Kenjo
Using "FM Sound Generator" (C) cisc
~~~

## x64版
x64版は以下のソースコードを流用しています
- xkeropi / NONAKA Kimihiro (埜中公博)
- px68k / ひっそりぃ(hissorii)
- px68k-libretro / r-type (not6) 

## 改変履歴
### Version 0.65a - 2022/12/24
- ディスクイメージを開く時に毎回警告が表示されるのを抑制
- 今どきの 32bpp 表示に対応
- マルチモニタを跨げないのを修正

### Version 0.65b - 2023/1/16
- メモリを破壊する不具合の修正
- x64 をビルド

### Version 0.65c - 2023/2/14
- フルスクリーンできなかったのを修正
- サウンドデバイスを選択できるようにした
  - ASIO に対応
  - WASAPI に対応
- MIDI デバイスを選択できるようにした
  - VSTi に対応
