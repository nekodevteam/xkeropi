#ifndef winx68k_common_h
#define winx68k_common_h

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <commdlg.h>
#include <commctrl.h>

#if (_MSC_VER >= 1600 /*Visual Studio 2010*/)
#include <stdint.h>
#else	// (_MSC_VER >= 1600 /*Visual Studio 2010*/)
typedef signed char		int8_t;		/*!< signed 8bit */
typedef unsigned char		uint8_t;	/*!< unsigned 8bit */
typedef signed short		int16_t;	/*!< signed 16bit */
typedef unsigned short		uint16_t;	/*!< unsigned 16bit */
typedef signed int		int32_t;	/*!< signed 32bit */
typedef unsigned int		uint32_t;	/*!< unsigned 32bit */
typedef signed __int64		int64_t;	/*!< signed 64bit */
typedef unsigned __int64	uint64_t;	/*!< unsigned 64bit */
#define uint_fast8_t		uint8_t
#define uint_fast16_t		uint16_t
#endif	// (_MSC_VER >= 1600 /*Visual Studio 2010*/)

#if (_MSC_VER < 1300 /*Visual Studio 2002*/)
#define intptr_t	INT_PTR		/*!< INT_PTR */
#define DWORD_PTR	DWORD		/*!< DWORD_PTR */
#endif	// (_MSC_VER < 1300 /*Visual Studio 2002*/)

#define	TRUE		1
#define	FALSE		0
#define	SUCCESS		0
#define	FAILURE		1

//#define WIN68DEBUG						// -DEBUG オプション用

#define FASTCALL	__fastcall

#ifndef INLINE
#define	INLINE		__inline
#endif

#ifndef LABEL
#ifdef USE_ASM
								// C++のオープニング・エピローグを消す
#define	LABEL		__declspec(naked)
#else	// USE_ASM
#define	LABEL
#endif	// USE_ASM
#endif

#ifdef __cplusplus
#define EXTERNC extern "C"
#else	// __cplusplus
#define EXTERNC extern
#endif	// __cplusplus

#ifndef _countof
//! countof
#define _countof(x)		(sizeof((x)) / sizeof((x)[0]))
#endif	// _countof

#define __attribute(n)
#define __attribute__(n)

typedef union {
	struct {
		BYTE l;
		BYTE h;
	} b;
	WORD w;
} PAIR;

#ifdef __cplusplus
extern "C" {
#endif
void Error(const char* s);
#ifdef __cplusplus
}
#endif

#endif //winx68k_common_h
