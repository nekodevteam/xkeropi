#ifndef keropi_cmdline_h
#define keropi_cmdline_h

extern TCHAR macrofile[MAX_PATH];

void CheckCmdLine(LPCTSTR s);
void SetCmdLineFD(void);

#endif //keropi_cmdline_h
