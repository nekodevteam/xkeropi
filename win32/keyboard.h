#ifndef _winx68k_keyboard
#define _winx68k_keyboard

extern BYTE KeyTable[512];
extern const BYTE KeyTableMaster[512];

void Keyboard_Init(void);
void Keyboard_KeyDown(WORD vkcode, DWORD keystate);
void Keyboard_KeyUp(WORD vkcode, DWORD keystate);
uint_fast8_t Keyboard_BufferEmpty(void);
uint_fast8_t Keyboard_Read(void);
void Keyboard_Write(uint_fast8_t nData);

#endif //_winx68k_keyboard
