/**
 * @file	sounddevice.h
 * @brief	サウンド デバイス基底クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief サウンド データ取得インタフェイス
 */
class ISoundData
{
public:
	/**
	 * ストリーム データを得る
	 * @param[out] lpBuffer バッファ
	 * @param[in] nBufferCount バッファ カウント
	 * @return サンプル数
	 */
	virtual UINT Get16(int16_t* lpBuffer, UINT nBufferCount) = 0;
};

/**
 * @brief サウンド デバイス基底クラス
 */
class ISoundDevice
{
public:
	/**
	 * コンストラクタ
	 */
	ISoundDevice()
		: m_pSoundData(NULL)
	{
	}

	/**
	 * デストラクタ
	 */
	virtual ~ISoundDevice()
	{
	}

	/**
	 * ストリーム データの設定
	 * @param[in] pSoundData サウンド データ
	 */
	void SetStreamData(ISoundData* pSoundData)
	{
		m_pSoundData = pSoundData;
	}

	/**
	 * オープン
	 * @param[in] lpDevice デバイス名
	 * @param[in] hWnd ウィンドウ ハンドル
	 * @retval true 成功
	 * @retval false 失敗
	 */
	virtual bool Open(LPCTSTR lpDevice = NULL, HWND hWnd = NULL) = 0;

	/**
	 * クローズ
	 */
	virtual void Close() = 0;

	/**
	 * ストリームの作成
	 * @param[in] nSamplingRate サンプリング レート
	 * @param[in] nChannels チャネル数
	 * @param[in] nBufferSize バッファ サイズ
	 * @return バッファ サイズ
	 */
	virtual UINT CreateStream(UINT nSamplingRate, UINT nChannels, UINT nBufferSize = 0) = 0;

	/**
	 * ストリームを破棄
	 */
	virtual void DestroyStream() = 0;

	/**
	 * ストリームをリセット
	 */
	virtual void ResetStream()
	{
	}

	/**
	 * ストリームの再生
	 * @retval true 成功
	 * @retval false 失敗
	 */
	virtual bool PlayStream() = 0;

	/**
	 * ストリームの停止
	 */
	virtual void StopStream() = 0;

protected:
	ISoundData* m_pSoundData;		/*!< サウンド データ インスタンス */
};
