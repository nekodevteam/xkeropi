﻿// ---------------------------------------------------------------------------------------
//  BG.C - BGとスプライト
//  ToDo：透明色の処理チェック（特に対Text間）
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "bg.h"
#include "crtc.h"
#include "tvram.h"
#include "palette.h"
#include "windraw.h"
#include "x68k.inl"

	struct tagBG	BG;

	BYTE	BG_Ram[0x8000];
	BYTE	Sprite_Regs[0x800];

	BYTE	BGCHR8[8*8*256];
	BYTE	BGCHR16[16*16*256];

#define BG_CHREND	BG.CHREND
#define BG_BG0END	BG.BG0END
#define BG_BG1END	BG.BG1END

#define	BG0ScrollX		*(uint16_t *)(&BG_Regs[0x00])
#define	BG0ScrollY		*(uint16_t *)(&BG_Regs[0x02])
#define	BG1ScrollX		*(uint16_t *)(&BG_Regs[0x04])
#define	BG1ScrollY		*(uint16_t *)(&BG_Regs[0x06])

struct SPRITECTRLTBL {
	uint16_t posx;
	uint16_t posy;
	uint8_t num;
	uint8_t ctrl;
	uint8_t ply;
	uint8_t dummy;
} __attribute__ ((packed));
typedef struct SPRITECTRLTBL SPRITECTRLTBL_T;

// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void BG_Init(void)
{
	DWORD i;
	ZeroMemory(Sprite_Regs, 0x800);
	ZeroMemory(BG_Ram, 0x8000);
	ZeroMemory(BGCHR8, 8*8*256);
	ZeroMemory(BGCHR16, 16*16*256);
	ZeroMemory(BG_LineBuf, sizeof(BG_LineBuf));
	for (i=0; i<0x12; i++)
		BG_Write(0xeb0800+i, 0);
	BG_CHREND = 0x8000;

	BG_UpdateShift();
}


// -----------------------------------------------------------------------
//   I/O Read
// -----------------------------------------------------------------------
IOBUS8 FASTCALL BG_Read(DWORD adr)
{
	adr -= 0xeb0000;
	if (adr < 0x400)
	{
		return Sprite_Regs[ADRSWAPW(adr)];
	}

	adr -= 0x800;
	if (adr < 0x12)
	{
		return BG_Regs[ADRSWAPW(adr)];
	}

	return 0xff;
}

IOBUS16 FASTCALL BG16_Read(DWORD adr)
{
	adr -= 0xeb0000;
	if (adr < 0x400)
	{
		return *(uint16_t *)(&Sprite_Regs[adr]);
	}

	adr -= 0x800;
	if (adr < 0x12)
	{
		return *(uint16_t *)(&BG_Regs[adr]);
	}

	return ~1;
}

IOBUS8 FASTCALL BGRam_Read(DWORD adr)
{
	return BG_Ram[ADRSWAPW(adr) - 0xeb8000];
}

IOBUS16 FASTCALL BGRam16_Read(DWORD adr)
{
	return *(uint16_t *)(&BG_Ram[adr - 0xeb8000]);
}

// -----------------------------------------------------------------------
//   I/O Write
// -----------------------------------------------------------------------
INLINE void UpdateCtlH(uint_fast8_t data)
{
	if (data & 0x08)
	{
		if (data & 0x30)
		{
			BG_BG1TOP = 0x6000;
			BG_BG1END = 0x8000;
		}
		else
		{
			BG_BG1TOP = 0x4000;
			BG_BG1END = 0x6000;
		}
	}
	else
	{
		BG_BG1TOP = BG_BG1END = 0;
	}

	if (data & 0x01)
	{
		if (data & 0x06)
		{
			BG_BG0TOP = 0x6000;
			BG_BG0END = 0x8000;
		}
		else
		{
			BG_BG0TOP = 0x4000;
			BG_BG0END = 0x6000;
		}
	}
	else
	{
		BG_BG0TOP = BG_BG0END = 0;
	}
}

INLINE void UpdateChrEnd()
{
	if (SM_RESO & 3)
	{
		if ((BG_BG0TOP == 0x4000) || (BG_BG1TOP == 0x4000))
		{
			BG_CHREND = 0x4000;
		}
		else if ((BG_BG0TOP == 0x6000) || (BG_BG1TOP == 0x6000))
		{
			BG_CHREND = 0x6000;
		}
		else
		{
			BG_CHREND = 0x8000;
		}
	}
	else
	{
		BG_CHREND = 0x2000;
	}
}

void FASTCALL BG_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);
	adr -= 0xeb0000;
	if (adr < 0x400)
	{
/*{
FILE* fp = fopen("_sprite.txt", "a");
if (fp) fprintf(fp, "Adr:$%08X  Data:$%02X @ $%08X\n", adr, data, M68000_GETPC);
fclose(fp);
}*/
		adr = ADRSWAPW(adr);
		if (Sprite_Regs[adr] != data)
		{
#ifdef USES_ALLFLASH
			Sprite_Regs[adr] = data;
			TVRAM_SetAllDirty();
#else	// USES_ALLFLASH
			volatile SPRITECTRLTBL_T* sp = (SPRITECTRLTBL_T *)(Sprite_Regs + (adr & (~7)));
			int32_t y0, y1;
			const uint_fast8_t h = 4 << BG.Shift;
			const int32_t y = ((BG.VDiff << BG.Shift) >> 2) - h;

			y0 = y + sp->posy;
			SetTextDirtyLines(y0, h);

			Sprite_Regs[adr] = data;

			y1 = y + sp->posy;
			if (y0 != y1)
			{
				SetTextDirtyLines(y1, h);
			}
#endif	// USES_ALLFLASH
		}
		return;
	}

	adr -= 0x800;
	if (adr < 0x12)
	{
		if (BG_Regs[ADRSWAPW(adr)] == data) return;	// データに変化が無ければ帰る
		BG_Regs[ADRSWAPW(adr)] = data;
		switch(adr)
		{
		case 0x00:
		case 0x01:
		case 0x02:
		case 0x03:
		case 0x04:
		case 0x05:
		case 0x06:
		case 0x07:
			TVRAM_SetAllDirty();
			break;

		case 0x08:		// BG On/Off Changed
			TVRAM_SetAllDirty();
			break;
		case 0x09:		// BG Plane Cfg Changed
			TVRAM_SetAllDirty();
			UpdateCtlH(data);
			UpdateChrEnd();
			break;

		case 0x0d:
			BG_HAdjust = ((int32_t)SM_HDSP-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			TVRAM_SetAllDirty();
			break;
		case 0x0f:
			BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分
			BG_UpdateVAdjust();
			TVRAM_SetAllDirty();
			break;

		case 0x11:		// BG ScreenRes Changed
			UpdateChrEnd();
			BG_HAdjust = ((int32_t)SM_HDSP-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分
			BG_UpdateShift();
			TVRAM_SetAllDirty();
			break;
		}
	}
}

void FASTCALL BG16_Write(DWORD adr, IOBUS16 data)
{
	assert(data < 0x10000);
	adr -= 0xeb0000;
	if (adr < 0x400)
	{
		if (*(uint16_t *)(&Sprite_Regs[adr]) != data)
		{
#ifdef USES_ALLFLASH
			*(uint16_t *)(&Sprite_Regs[adr]) = data;
			TVRAM_SetAllDirty();
#else	// USES_ALLFLASH
			volatile SPRITECTRLTBL_T* sp = (SPRITECTRLTBL_T *)(Sprite_Regs + (adr & (~7)));
			int32_t y0, y1;
			const uint_fast8_t h = 4 << BG.Shift;
			const int32_t y = ((BG.VDiff << BG.Shift) >> 2) - h;

			y0 = y + sp->posy;
			SetTextDirtyLines(y0, h);

			*(uint16_t *)(&Sprite_Regs[adr]) = data;

			y1 = y + sp->posy;
			if (y0 != y1)
			{
				SetTextDirtyLines(y1, h);
			}
#endif	// USES_ALLFLASH
		}
		return;
	}

	adr -= 0x800;
	if (adr < 0x12)
	{
		if (*(uint16_t *)(&BG_Regs[adr]) == data) return;	// データに変化が無ければ帰る
		*(uint16_t *)(&BG_Regs[adr]) = data;
		switch(adr)
		{
		case 0x00:
			TVRAM_SetAllDirty();
			break;
		case 0x02:
			TVRAM_SetAllDirty();
			break;
		case 0x04:
			TVRAM_SetAllDirty();
			break;
		case 0x06:
			TVRAM_SetAllDirty();
			break;

		case 0x08:		// BG On/Off Plane Cfg Changed
			TVRAM_SetAllDirty();
			UpdateCtlH(data);
			UpdateChrEnd();
			break;

		case 0x0c:
			BG_HAdjust = ((int32_t)SM_HDSP-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			TVRAM_SetAllDirty();
			break;
		case 0x0e:
			BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分
			BG_UpdateVAdjust();
			TVRAM_SetAllDirty();
			break;

		case 0x10:		// BG ScreenRes Changed
			UpdateChrEnd();
			BG_HAdjust = ((int32_t)SM_HDSP-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分
			BG_UpdateShift();
			TVRAM_SetAllDirty();
			break;
		}
	}
}

void FASTCALL BGRam_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);

	if (BG_Ram[ADRSWAPW(adr) - 0xeb8000] == data) return;			// データに変化が無ければ帰る
	BG_Ram[ADRSWAPW(adr) - 0xeb8000] = data;

	adr &= 0x7fff;
	if (adr<0x2000)
	{
		BGCHR8[adr*2]   = data>>4;
		BGCHR8[adr*2+1] = data&15;
	}
	{
		const DWORD bg16chr = ((adr&0x3c)*4)+((adr&0x40)>>3)+((adr&(~0x7c))*2);
		BGCHR16[bg16chr]   = data>>4;
		BGCHR16[bg16chr+1] = data&15;
	}

	if (adr<BG_CHREND)				// パターンエリア
	{
		TVRAM_SetAllDirty();
	}
	else if ((adr>=BG_BG1TOP)&&(adr<BG_BG1END))	// BG1 MAPエリア
	{
		TVRAM_SetAllDirty();
	}
	else if ((adr>=BG_BG0TOP)&&(adr<BG_BG0END))	// BG0 MAPエリア
	{
		TVRAM_SetAllDirty();
	}
}

void FASTCALL BGRam16_Write(DWORD adr, IOBUS16 data)
{
	if (*(uint16_t *)(&BG_Ram[adr - 0xeb8000]) == data) return;			// データに変化が無ければ帰る
	*(uint16_t *)(&BG_Ram[adr - 0xeb8000]) = data;

	adr &= 0x7fff;
	if (adr < 0x2000)
	{
		BGCHR8[adr*2  ] = (data >> 12) & 15;
		BGCHR8[adr*2+1] = (data >>  8) & 15;
		BGCHR8[adr*2+2] = (data >>  4) & 15;
		BGCHR8[adr*2+3] =  data        & 15;
	}
	{
		const DWORD bg16chr = ((adr&0x3c)*4)+((adr&0x40)>>3)+((adr&(~0x7c))*2);
		BGCHR16[bg16chr  ] = (data >> 12) & 15;
		BGCHR16[bg16chr+1] = (data >>  8) & 15;
		BGCHR16[bg16chr+2] = (data >>  4) & 15;
		BGCHR16[bg16chr+3] =  data        & 15;
	}

	if (adr<BG_CHREND)				// パターンエリア
	{
		TVRAM_SetAllDirty();
	}
	else if ((adr>=BG_BG1TOP)&&(adr<BG_BG1END))	// BG1 MAPエリア
	{
		TVRAM_SetAllDirty();
	}
	else if ((adr>=BG_BG0TOP)&&(adr<BG_BG0END))	// BG0 MAPエリア
	{
		TVRAM_SetAllDirty();
	}
}

// -----------------------------------------------------------------------
//   1ライン分の描画
// -----------------------------------------------------------------------
uint32_t BG_CalcLine(uint32_t v)
{
	v <<= 2;
	v >>= BG.Shift;
	v -= BG.VDiff;
	return v;
}

#ifndef USE_ASM
static void
Sprite_DrawLineMcr(uint32_t v, uint_fast8_t pri)
{
	const SPRITECTRLTBL_T *sct = (SPRITECTRLTBL_T *)Sprite_Regs;
	const SPRITECTRLTBL_T *sctp = sct + 128;
	DWORD y;
	DWORD t;

	while (sct < sctp)
	{
		--sctp;
		if ((uint_fast8_t)(sctp->ply & 3) != pri)
		{
			continue;
		}

		t = (sctp->posx + BG_HAdjust) & 0x3ff;
		if (t >= TextDotX + 16)
		{
			continue;
		}

		y = v + 16 - (sctp->posy & 0x3ff);
		if (y < 16)
		{
			const uint16_t *pal = &TextPal[(sctp->ctrl & 15) << 4];
			const uint8_t *p;
			int d = 1;
			unsigned int i;
			const uint_fast16_t n = (uint_fast16_t)((intptr_t)sctp - (intptr_t)sct);

			if (sctp->ctrl & 0x80)
			{
				y = 15 - y;
			}
			p = &BGCHR16[(sctp->num * 256) + (y * 16)];

			if (sctp->ctrl & 0x40)
			{
				p += 15;
				d = -1;
			}

			for (i = 0; i < 16; i++)
			{
				const uint_fast8_t dat = *p;
				p += d;
				if (dat)
				{
					const unsigned int x = t + i;
					if (BG_PriBuf[x] >= n)
					{
						BG_LineBuf[x] = pal[dat];
						Text_TrFlag[x] |= 2;
						BG_PriBuf[x] = (uint16_t)n;
					}
				}
			}
		}
	}
}

#define BG_DRAWLINE_LOOPY(cnt) \
{ \
	unsigned int j;							\
	const uint_fast8_t n = bl & 15;					\
	if (n == 0) {							\
		for (j = 0; j < cnt; j++, edi++) {			\
			const uint_fast8_t dat = *esi;			\
			esi += d;					\
			if (dat == 0)					\
				continue;				\
			if (!(Text_TrFlag[edi] & 2)) {			\
				BG_LineBuf[edi] = TextPal[dat];		\
				Text_TrFlag[edi] |= 2;			\
			}						\
		}							\
	} else {							\
		const uint16_t* pal = &TextPal[n << 4];			\
		for (j = 0; j < cnt; j++, edi++) {			\
			const uint_fast8_t dat = *esi;			\
			esi += d;					\
			if ((dat) || !(Text_TrFlag[edi] & 2)) {		\
				BG_LineBuf[edi] = pal[dat];		\
				Text_TrFlag[edi] |= 2;			\
			}						\
		}							\
	}								\
}

#define BG_DRAWLINE_LOOPY_NG(cnt) \
{ \
	unsigned int j;						\
	const uint16_t* pal = &TextPal[(bl & 15) << 4];		\
	for (j = 0; j < cnt; j++, edi++) {			\
		const uint_fast8_t dat = *esi;			\
		esi += d;					\
		if (dat) {					\
			BG_LineBuf[edi] = pal[dat];		\
			Text_TrFlag[edi] |= 2;			\
		}						\
	}							\
}

INLINE void
bg_drawline_loopx8(uint_fast16_t BGTOP, uint32_t h, uint32_t v, uint_fast8_t ng)
{
	int i;
	const uint_fast8_t ebp = (v & 7) << 3;
	const uint32_t edx = BGTOP + ((v & 0x1f8) << 4);
	const uint8_t *pBGLine = &BG_Ram[edx];
	uint32_t edi = 16 - (h & 7);
	uint_fast8_t ecx = h >> 2;

	for (i = TextDotX >> 3; i >= 0; i--) {
		const uint8_t *pBG = &pBGLine[ecx & 0x7e];
		const uint_fast8_t bl = pBG[ADRSWAPW(0)];
		uint_fast16_t si = (pBG[ADRSWAPW(1)] << 6) + ebp;
		const uint8_t *esi;
		int d = 1;
		if (bl & 0x80) {
			si ^= 0x38;
		}
		esi = &BGCHR8[si];
		if (bl & 0x40) {
			esi += 7;
			d = -1;
		}
		if (ng) {
			BG_DRAWLINE_LOOPY_NG(8);
		} else {
			BG_DRAWLINE_LOOPY(8);
		}
		ecx += 2;
	}
}

INLINE void
bg_drawline_loopx16(uint_fast16_t BGTOP, uint32_t h, uint32_t v, uint_fast8_t ng)
{
	int i;
	const uint_fast8_t ebp = (v & 15) << 4;
	const uint32_t edx = BGTOP + ((v & 0x3f0) << 3);
	const uint8_t *pBGLine = &BG_Ram[edx];
	uint32_t edi = 16 - (h & 15);
	uint_fast8_t ecx = h >> 3;

	for (i = TextDotX >> 4; i >= 0; i--) {
		const uint8_t *pBG = &pBGLine[ecx & 0x7e];
		const uint_fast8_t bl = pBG[ADRSWAPW(0)];
		uint_fast16_t si = (pBG[ADRSWAPW(1)] << 8) + ebp;
		const uint8_t *esi;
		int d = 1;
		if (bl & 0x80) {
			si ^= 0xf0;
		}
		esi = &BGCHR16[si];
		if (bl & 0x40) {
			esi += 15;
			d = -1;
		}
		if (ng) {
			BG_DRAWLINE_LOOPY_NG(16);
		} else {
			BG_DRAWLINE_LOOPY(16);
		}
		ecx += 2;
	}
}

static void
BG_DrawLineMcr8(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx8(BGTOP, BGScrollX - BG_HAdjust, v, 0);
}

static void
BG_DrawLineMcr16(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx16(BGTOP, BGScrollX - BG_HAdjust, v, 0);
}

static void
BG_DrawLineMcr8_ng(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx8(BGTOP, BGScrollX - BG_HAdjust, v, 1);
}

static void
BG_DrawLineMcr16_ng(uint_fast16_t BGTOP, uint32_t BGScrollX, uint32_t v)
{
	bg_drawline_loopx16(BGTOP, BGScrollX, v, 1);
}

INLINE void
FillMem16(uint16_t* p, uint_fast16_t v, uint32_t size8)
{
	uint32_t i;
	const uint32_t v2 = v + (v << 16);
	for (i = 0; i < size8; i += 8)
	{
		*(uint32_t *)(&p[i + 0]) = v2;
		*(uint32_t *)(&p[i + 2]) = v2;
		*(uint32_t *)(&p[i + 4]) = v2;
		*(uint32_t *)(&p[i + 6]) = v2;
	}
}

void
BG_DrawLine(uint32_t v, uint_fast8_t opaq, uint_fast8_t gd)
{
	if (opaq) {
		FillMem16(&BG_LineBuf[16], TextPal[0], TextDotX);
	}
	FillMem16(&BG_PriBuf[16], 0xffff, TextDotX);

	v <<= 2;
	v >>= BG.Shift;
	v -= BG.VDiff;

	Sprite_DrawLineMcr(v, 1);
	if ((BS_CTLL & 8) && ((SM_RESO & 3) == 0)) { // BG1 on
		void (*func8)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr8 : BG_DrawLineMcr8_ng;
		(*func8)(BG_BG1TOP, BG1ScrollX, BG1ScrollY + v);
	}
	Sprite_DrawLineMcr(v, 2);
	if (BS_CTLL & 1) { // BG0 on
		if ((SM_RESO & 3) == 0) {
			void (*func8)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr8 : BG_DrawLineMcr8_ng;
			(*func8)(BG_BG0TOP, BG0ScrollX, BG0ScrollY + v);
		} else {
			void (*func16)(uint_fast16_t, uint32_t, uint32_t) = (gd) ? BG_DrawLineMcr16 : BG_DrawLineMcr16_ng;
			(*func16)(BG_BG0TOP, BG0ScrollX, BG0ScrollY + v);
		}
	}
	Sprite_DrawLineMcr(v, 3);
}
#endif /* !USE_ASM */
