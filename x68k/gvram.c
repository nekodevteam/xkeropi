﻿// ---------------------------------------------------------------------------------------
//  GVRAM.C - Graphic VRAM
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "gvram.h"
#include "crtc.h"
#include "memory.h"
#include "palette.h"
#include "tvram.h"
#include <assert.h>

	WORD	Pal16Adr[256];			// 16bit color パレットアドレス計算用


// -----------------------------------------------------------------------
//   初期化～
// -----------------------------------------------------------------------
void GVRAM_Init(void)
{
	int i;

	ZeroMemory(GVRAM, 0x80000);
	for (i=0; i<128; i++)			// 16bit color パレットアドレス計算用
	{
		Pal16Adr[i*2] = (i * 4) + ADRSWAPW(0);
		Pal16Adr[i*2+1] = (i * 4) + ADRSWAPW(1);
	}
}


// -----------------------------------------------------------------------------------
//  高速クリア用ルーチン
// -----------------------------------------------------------------------------------
#ifndef USE_ASM
void GVRAM_FastClear(void)
{
	const uint32_t v = (CRTC_R20L & 4) ? 512 : 256;
	const uint32_t h = (CRTC_R20L & 3) ? 512 : 256;
	// やっぱちゃんと範囲指定しないと変になるものもある（ダイナマイトデュークとか）

	const uint32_t offx = GrphScroll[0].X;
	const uint32_t offy = GrphScroll[0].Y;
	uint32_t y;
	for (y = 0; y < v; ++y) {
		WORD *p = (WORD *)(GVRAM + (((offy + y) & 0x1ff) << 10));
		uint32_t x;
		for (x = 0; x < h; ++x) {
			p[(offx + x) & 0x1ff] &= CRTC_FastClrMask;
		}
	}
}
#endif /* !USE_ASM */


// -----------------------------------------------------------------------
//   VRAM Read
// -----------------------------------------------------------------------
IOBUS8 FASTCALL GVRAM_Read(DWORD adr)
{
	IOBUS8 ret = 0;

	adr -= 0xc00000;

	if (CRTC_R20H & 8)				// 読み込み側も65536モードのVRAM配置（苦胃頭捕物帳）
	{
		if (adr < 0x80000)
		{
			ret = GVRAM[ADRSWAPW(adr)];
		}
	}
	else
	{
		switch (CRTC_R20H & 3)
		{
		case 0:					// 16 colors
			if (adr & 1)
			{
				if (CRTC_R20H & 4)		// 1024dot
				{
					const int offset = (adr & (1 << 20)) ? ADRSWAPW(0) : ADRSWAPW(1);
					ret = GVRAM[((adr & 0xff800) >> 1) + (adr & 0x3ff) - 1 + offset];
					if (adr & (1 << 10))
					{
						ret >>= 4;
					}
					ret &= 15;
				}
				else
				{
					const int offset = (adr & (1 << 20)) ? ADRSWAPW(0) : ADRSWAPW(1);
					ret = GVRAM[(adr & 0x7ffff) - 1 + offset];
					if (adr & (1 << 19))
					{
						ret >>= 4;
					}
					ret &= 15;
				}
			}
			break;
		case 1:					// 256
		case 2:					// Unknown
			if (adr < 0x100000)
			{
				if (adr & 1)
				{
					const int offset = (adr & (1 << 19)) ? ADRSWAPW(0) : ADRSWAPW(1);
					ret = GVRAM[(adr & 0x7ffff) - 1 + offset];
				}
			}
//			else
//				BusErrFlag = 1;
			break;
		case 3:					// 65536
			if (adr < 0x80000)
			{
				ret = GVRAM[ADRSWAPW(adr)];
			}
//			else
//				BusErrFlag = 1;
			break;
		}
	}
	return ret;
}

IOBUS16 FASTCALL GVRAM16_Read(DWORD adr)
{
	if ((CRTC_R20H & 8) || ((CRTC_R20H & 3) == 3))
	{
		if (adr < 0xc80000)
		{
			return *(uint16_t *)(&GVRAM[adr - 0xc00000]);
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return GVRAM_Read(adr + 1);
	}
}


// -----------------------------------------------------------------------
//   VRAM Write
// -----------------------------------------------------------------------
void FASTCALL GVRAM_Write(DWORD adr, IOBUS8 data)
{
	assert(data <= 0x100);

	adr -= 0xc00000;

	if (CRTC_R20H & 8)				// 65536モードのVRAM配置？（Nemesis）
	{
		if (adr < 0x80000)
		{
			const unsigned line = (adr >> 10) - GrphScroll[0].Y;
			GVRAM[ADRSWAPW(adr)] = data;
			SetBGDirtyLine(line);
		}
	}
	else
	{
		switch (CRTC_R20H & 3)
		{
		case 0:					// 16 colors
			if ((adr & 1) == 0) break;
			if (CRTC_R20H & 4)		// 1024dot
			{
				const unsigned line = (adr >> 11) - GrphScroll[0].Y;
				const int offset = (adr & (1 << 20)) ? ADRSWAPW(0) : ADRSWAPW(1);
				uint8_t *p = &GVRAM[((adr & 0xff800) >> 1) + (adr & 0x3ff) - 1 + offset];
				const uint_fast8_t v = *p;
				if (adr & (1 << 10))
				{
					*p = (v & 0x0f) + (data << 4);
				}
				else
				{
					*p = (v & 0xf0) + (data & 0x0f);
				}
				SetTextDirtyLine(line);
			}
			else
			{
				const unsigned line = (adr >> 10) - GrphScroll[adr >> 19].Y;
				const int offset = (adr & (1 << 20)) ? ADRSWAPW(0) : ADRSWAPW(1);
				uint8_t *p = &GVRAM[(adr & 0x7ffff) - 1 + offset];
				const uint_fast8_t v = *p;
				if (adr & (1 << 19))
				{
					*p = (v & 0x0f) + (data << 4);
				}
				else
				{
					*p = (v & 0xf0) + (data & 0x0f);
				}
				SetBGDirtyLine(line);
			}
			break;
		case 1:					// 256 colors
		case 2:					// Unknown
			if (adr < 0x100000)
			{
				if (adr & 1)
				{
					const unsigned y = adr >> 10;
					const unsigned p = (adr & (1 << 19)) ? ADRSWAPW(0) : ADRSWAPW(1);
					const unsigned line0 = y - GrphScroll[(p * 2) + 0].Y;
					const unsigned line1 = y - GrphScroll[(p * 2) + 1].Y;
					GVRAM[(adr & 0x7ffff) - 1 + p] = data;
					SetBGDirtyLine(line0);			// 32色4面みたいな使用方法時
					SetBGDirtyLine(line1);
				}
			}
//			else
//			{
//				BusErrFlag = 1;
//				return;
//			}
			break;
		case 3:					// 65536 colors
			if (adr < 0x80000)
			{
				const unsigned line = (adr >> 10) - GrphScroll[0].Y;
				GVRAM[ADRSWAPW(adr)] = data;
				SetBGDirtyLine(line);
/*{
FILE* fp = fopen("_gvram.txt", "a");
fprintf(fp, "Adr:$%08X Dat:$%02X @ $%08X\n", adr+0xc00000, data, M68000_GETPC);
fclose(fp);
}*/
			}
//			else
//			{
//				BusErrFlag = 1;
//				return;
//			}
			break;
		}
	}
}

void FASTCALL GVRAM16_Write(DWORD adr, IOBUS16 data)
{
	if ((CRTC_R20H & 8) || ((CRTC_R20H & 3) == 3))
	{
		if (adr < 0xc80000)
		{
			const unsigned line = (adr >> 10) - GrphScroll[0].Y;
			*(uint16_t *)(&GVRAM[adr - 0xc00000]) = data;
			SetBGDirtyLine(line);
		}
	}
	else
	{
		GVRAM_Write(adr + 1, data & 0xff);
	}
}


// -----------------------------------------------------------------------
//   こっから後はライン単位での画面展開部
// -----------------------------------------------------------------------
#ifndef USE_ASM
void Grp_DrawLine16(uint32_t v)
{
	const WORD *srcp;
	WORD *destp;
	DWORD x, y;
	DWORD i;

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = ((GrphScroll[0].Y + v) & 0x1ff) << 10;

	x = GrphScroll[0].X & 0x1ff;
	srcp = (WORD *)(GVRAM + y + x * 2);
	destp = Grp_LineBuf;

	x = 512 - x;

	i = 0;
	if (x < TextDotX) {
		for (; i < x; ++i) {
			uint_fast16_t dat = *srcp++;
			if (dat != 0) {
				const uint_fast8_t dat0 = (dat >> 8) & 0xff;
				dat &= 0x00ff;

				dat = Pal_Regs[Pal16Adr[dat]];
				dat += Pal_Regs[Pal16Adr[dat0] + 2] << 8;
				dat = PAL16(dat);
			}
			*destp++ = (uint16_t)dat;
		}
		srcp -= 0x200;
	}

	for (; i < TextDotX; ++i) {
		uint_fast16_t dat = *srcp++;
		if (dat != 0) {
			const uint_fast8_t dat0 = (dat >> 8) & 0xff;
			dat &= 0x00ff;

			dat = Pal_Regs[Pal16Adr[dat]];
			dat += Pal_Regs[Pal16Adr[dat0] + 2] << 8;
			dat = PAL16(dat);
		}
		*destp++ = (uint16_t)dat;
	}
}


void Grp_DrawLine8(uint32_t v, uint32_t page, uint_fast8_t opaq)
{
	const WORD *srcp;
	WORD *destp;
	DWORD x, x0;
	DWORD y, y0;
	DWORD off;
	DWORD i;

	assert(page < 2);

	if ((CRTC_R20L & 0x1c) == 0x1c) {
		v *= 2;
	}
	y = (((GrphScroll[page * 2].Y + v) & 0x1ff) << 10) + page;
	y0 = (((GrphScroll[page * 2 + 1].Y + v) & 0x1ff) << 10) + page;

	x = GrphScroll[page * 2].X & 0x1ff;
	x0 = GrphScroll[page * 2 + 1].X & 0x1ff;

	off = y0 + x0 * 2;
	srcp = (WORD *)(GVRAM + y + x * 2);
	destp = Grp_LineBuf;

	x = 512 - x;

	i = 0;

	if (opaq) {
		if (x < TextDotX) {
			for (; i < x; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast16_t dat = GrphPal[(GVRAM[off] & 0xf0) + (s & 0x0f)];
				srcp++;
				*destp++ = (uint16_t)dat;

				off += 2;
				if ((off & 0x3fe) == 0x000)
					off -= 0x400;
			}
			srcp -= 0x200;
		}

		for (; i < TextDotX; ++i) {
			const uint_fast8_t s = *(const uint8_t *)srcp;
			const uint_fast16_t dat = GrphPal[(GVRAM[off] & 0xf0) + (s & 0x0f)];
			srcp++;
			*destp++ = (uint16_t)dat;

			off += 2;
			if ((off & 0x3fe) == 0x000)
				off -= 0x400;
		}
	} else {
		if (x < TextDotX) {
			for (; i < x; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast8_t dat = (GVRAM[off] & 0xf0) + (s & 0x0f);
				if (dat != 0x00)
					*destp = GrphPal[dat];
				srcp++;
				destp++;

				off += 2;
				if ((off & 0x3fe) == 0x000)
					off -= 0x400;
			}
			srcp -= 0x200;
		}

		for (; i < TextDotX; ++i) {
			const uint_fast8_t s = *(const uint8_t *)srcp;
			const uint_fast8_t dat = (GVRAM[off] & 0xf0) + (s & 0x0f);
			if (dat != 0x00)
				*destp = GrphPal[dat];
			srcp++;
			destp++;

			off += 2;
			if ((off & 0x3fe) == 0x000)
				off -= 0x400;
		}
	}
}

				// Manhattan Requiem Opening 7.0→7.5MHz
void Grp_DrawLine4(uint32_t v, uint32_t page, uint_fast8_t opaq)
{
	const WORD *srcp;
	WORD *destp;	// XXX: ALIGN
	DWORD x, y;
	DWORD off;
	DWORD i;

	assert(page < 4);

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = ((GrphScroll[page].Y + v) & 0x1ff) << 10;

	x = GrphScroll[page].X & 0x1ff;
	off = y + x * 2;

	x = 512 - x;

	srcp = (WORD *)(GVRAM + off + (page >> 1));
	destp = Grp_LineBuf;

	i = 0;

	if (page & 1) {
		if (opaq) {
			if (x < TextDotX) {
				for (; i < x; ++i) {
					const uint_fast8_t s = *(const uint8_t *)srcp;
					const uint_fast16_t dat = GrphPal[s >> 4];
					srcp++;
					*destp++ = (uint16_t)dat;
				}
				srcp -= 0x200;
			}
			for (; i < TextDotX; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast16_t dat = GrphPal[s >> 4];
				srcp++;
				*destp++ = (uint16_t)dat;
			}
		} else {
			if (x < TextDotX) {
				for (; i < x; ++i) {
					const uint_fast8_t s = *(const uint8_t *)srcp;
					const uint_fast8_t dat = s >> 4;
					if (dat != 0x00)
						*destp = GrphPal[dat];
					srcp++;
					destp++;
				}
				srcp -= 0x200;
			}
			for (; i < TextDotX; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast8_t dat = s >> 4;
				if (dat != 0x00)
					*destp = GrphPal[dat];
				srcp++;
				destp++;
			}
		}
	} else {
		if (opaq) {
			if (x < TextDotX) {
				for (; i < x; ++i) {
					const uint_fast8_t s = *(const uint8_t *)srcp;
					const uint_fast16_t dat = GrphPal[s & 0x0f];
					srcp++;
					*destp++ = (uint16_t)dat;
				}
				srcp -= 0x200;
			}
			for (; i < TextDotX; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast16_t dat = GrphPal[s & 0x0f];
				srcp++;
				*destp++ = (uint16_t)dat;
			}
		} else {
			if (x < TextDotX) {
				for (; i < x; ++i) {
					const uint_fast8_t s = *(const uint8_t *)srcp;
					const uint_fast8_t dat = s & 0x0f;
					if (dat != 0x00)
						*destp = GrphPal[dat];
					srcp++;
					destp++;
				}
				srcp -= 0x200;
			}
			for (; i < TextDotX; ++i) {
				const uint_fast8_t s = *(const uint8_t *)srcp;
				const uint_fast8_t dat = s & 0x0f;
				if (dat != 0x00)
					*destp = GrphPal[dat];
				srcp++;
				destp++;
			}
		}
	}
}

					// この画面モードは勘弁して下さい…
void Grp_DrawLine4h(uint32_t v)
{
	const WORD *srcp;
	WORD *destp;
	DWORD x, y;
	DWORD i;
	int bits;

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = (GrphScroll[0].Y + v) & 0x3ff;

	if ((y & 0x200) == 0x000) {
		y <<= 10;
		bits = (GrphScroll[0].X & 0x200) ? 4 : 0;
	} else {
		y = (y & 0x1ff) << 10;
		bits = (GrphScroll[0].X & 0x200) ? 12 : 8;
	}

	x = GrphScroll[0].X & 0x1ff;

	srcp = (WORD *)(GVRAM + y + x * 2);
	destp = Grp_LineBuf;

	x = 512 - x;

	for (i = 0; i < TextDotX; ++i) {
		const uint_fast16_t dat = *srcp++;
		*destp++ = GrphPal[(dat >> bits) & 0x0f];

		if (--x == 0) {
			srcp -= 0x200;
			bits ^= 4;
			x = 512;
		}
	}
}


// -------------------------------------------------
// --- 半透明／特殊Priのベースとなるページの描画 ---
// -------------------------------------------------
void Grp_DrawLine16SP(uint32_t v)
{
	DWORD x, y;
	DWORD off;
	DWORD i;

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = ((GrphScroll[0].Y + v) & 0x1ff) << 10;

	x = GrphScroll[0].X & 0x1ff;
	off = y + x * 2;
	x = 512 - x;

	for (i = 0; i < TextDotX; ++i) {
		const uint_fast16_t dat = (Pal_Regs[(GVRAM[off+1] * 2) + ADRSWAPW(0)] << 8) + Pal_Regs[(GVRAM[off] * 2) + ADRSWAPW(1)];
		if ((GVRAM[off] & 1) == 0) {
			Grp_LineBufSP[i] = 0;
			Grp_LineBufSP2[i] = PAL16(dat & (~1));
		} else {
			Grp_LineBufSP[i] = PAL16(dat & (~1));
			Grp_LineBufSP2[i] = 0;
		}

		off += 2;
		if (--x == 0)
			off -= 0x400;
	}
}


void Grp_DrawLine8SP(uint32_t v, uint32_t page)
{
	DWORD x, x0;
	DWORD y, y0;
	DWORD off, off0;
	DWORD i;

	assert(page < 2);

	if ((CRTC_R20L & 0x1c) == 0x1c) {
		v *= 2;
	}
	y = ((GrphScroll[page * 2].Y + v) & 0x1ff) << 10;
	y0 = ((GrphScroll[page * 2 + 1].Y + v) & 0x1ff) << 10;

	x = GrphScroll[page * 2].X & 0x1ff;
	x0 = GrphScroll[page * 2 + 1].X & 0x1ff;

	off = y + (x * 2) + page;
	off0 = y0 + (x0 * 2) + page;

	x = 512 - x;

	for (i = 0; i < TextDotX; ++i) {
		uint_fast16_t dat = (GVRAM[off] & 0x0f) + (GVRAM[off0] & 0xf0);
		if ((dat & 1) == 0) {
			dat &= 0xfe;
			if (dat != 0x00)
				dat = GrphPal[dat];
			Grp_LineBufSP[i] = 0;
			Grp_LineBufSP2[i] = (uint16_t)dat;
		} else {
			dat &= 0xfe;
			if (dat != 0x00)
				dat = GrphPal[dat] | Ibit;
			Grp_LineBufSP[i] = (uint16_t)dat;
			Grp_LineBufSP2[i] = 0;
		}

		off += 2;
		off0 += 2;
		if ((off0 & 0x3fe) == 0)
			off0 -= 0x400;
		if (--x == 0)
			off -= 0x400;
	}
}


void Grp_DrawLine4SP(uint32_t v, uint32_t page/*, uint_fast8_t opaq*/)
{
	DWORD x, y;
	DWORD off;
	DWORD i;

	assert(page < 4);

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = ((GrphScroll[page].Y + v) & 0x1ff) << 10;

	x = GrphScroll[page].X & 0x1ff;
	off = y + x * 2 + (page >> 1);
	x = 512 - x;

	if (page & 1) {
		for (i = 0; i < TextDotX; ++i) {
			const uint_fast8_t dat = GVRAM[off] >> 4;
			if ((dat & 1) == 0) {
				Grp_LineBufSP[i] = 0;
				Grp_LineBufSP2[i] = GrphPal[dat & 0x0e];
			} else {
				Grp_LineBufSP[i] = GrphPal[dat & 0x0e];
				Grp_LineBufSP2[i] = 0;
			}

			off += 2;
			if (--x == 0)
				off -= 0x400;
		}
	} else {
		for (i = 0; i < TextDotX; ++i) {
			const uint_fast8_t dat = GVRAM[off];
			if ((dat & 1) == 0) {
				Grp_LineBufSP[i] = 0;
				Grp_LineBufSP2[i] = GrphPal[dat & 0x0e];
			} else {
				Grp_LineBufSP[i] = GrphPal[dat & 0x0e];
				Grp_LineBufSP2[i] = 0;
			}

			off += 2;
			if (--x == 0)
				off -= 0x400;
		}
	}
}


void Grp_DrawLine4hSP(uint32_t v)
{
	const WORD *srcp;
	DWORD x, y;
	DWORD i;
	int bits;

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = (GrphScroll[0].Y + v) & 0x3ff;

	if ((y & 0x200) == 0x000) {
		y <<= 10;
		bits = (GrphScroll[0].X & 0x200) ? 4 : 0;
	} else {
		y = (y & 0x1ff) << 10;
		bits = (GrphScroll[0].X & 0x200) ? 12 : 8;
	}

	x = GrphScroll[0].X & 0x1ff;
	srcp = (WORD *)(GVRAM + y + x * 2);
	x = 512 - x;

	for (i = 0; i < TextDotX; ++i) {
		const uint_fast16_t dat = *srcp++ >> bits;
		if ((dat & 1) == 0) {
			Grp_LineBufSP[i] = 0;
			Grp_LineBufSP2[i] = GrphPal[dat & 0x0e];
		} else {
			Grp_LineBufSP[i] = GrphPal[dat & 0x0e];
			Grp_LineBufSP2[i] = 0;
		}

		if (--x == 0)
			srcp -= 0x400;
	}
}



// -------------------------------------------------
// --- 半透明の対象となるページの描画 --------------
// 2ページ以上あるグラフィックモードのみなので、
// 256色2面 or 16色4面のモードのみ。
// 256色時は、Opaqueでない方のモードはいらないかも…
// （必ずOpaqueモードの筈）
// -------------------------------------------------
// ここはまだ32色x4面モードの実装をしてないれす…
// （れじすた足りないよぅ…）
// -------------------------------------------------
							// やけにすっきり
void Grp_DrawLine8TR(uint32_t v, uint32_t page, uint_fast8_t opaq) {

	if (opaq) {
		DWORD x, y;
		DWORD i;

		assert(page < 2);

		if ((CRTC_R20L & 0x1c) == 0x1c)
			v *= 2;
		y = (((GrphScroll[page * 2].Y + v) & 0x1ff) << 10) + page;
		x = GrphScroll[page * 2].X & 0x1ff;

		for (i = 0; i < TextDotX; ++i, x = (x + 1) & 0x1ff) {
			uint32_t dat0 = Grp_LineBufSP[i];
			uint32_t dat = GVRAM[y + x * 2];

			if (dat0 != 0) {
				if (dat != 0) {
					dat = GrphPal[dat];
					if (dat != 0) {
						dat0 &= Pal_HalfMask;
						if (dat & Ibit)
							dat0 |= Pal_Ix2;
						dat &= Pal_HalfMask;
						dat += dat0;
						dat >>= 1;
					}
				}
			} else
				dat = GrphPal[dat];
			Grp_LineBuf[i] = (uint16_t)dat;
		}
	}
}

void Grp_DrawLine4TR(uint32_t v, uint32_t page, uint_fast8_t opaq) {

	DWORD x, y;
	DWORD i;

	assert(page < 4);

	if ((CRTC_R20L & 0x1c) == 0x1c)
		v *= 2;
	y = ((GrphScroll[page].Y + v) & 0x1ff) << 10;
	x = GrphScroll[page].X & 0x1ff;

	y += (page >> 1);
	if (page & 1) {
		if (opaq) {
			for (i = 0; i < TextDotX; ++i, x = (x + 1) & 0x1ff) {
				uint32_t dat0 = Grp_LineBufSP[i];
				uint32_t dat = GVRAM[y + x * 2] >> 4;

				if (dat0 != 0) {
					if (dat != 0) {
						dat = GrphPal[dat];
						if (dat != 0) {
							dat0 &= Pal_HalfMask;
							if (dat & Ibit)
								dat0 |= Pal_Ix2;
							dat &= Pal_HalfMask;
							dat += dat0;
							dat >>= 1;
						}
					}
				} else
					dat = GrphPal[dat];
				Grp_LineBuf[i] = (uint16_t)dat;
			}
		} else {
			for (i = 0; i < TextDotX; ++i, x = (x + 1) & 0x1ff) {
				uint32_t dat0 = Grp_LineBufSP[i];
				uint32_t dat = GVRAM[y + x * 2] >> 4;

				if (dat0 == 0) {
					if (dat != 0)
						Grp_LineBuf[i] = GrphPal[dat];
				} else {
					if (dat != 0) {
						dat = GrphPal[dat];
						if (dat != 0) {
							dat0 &= Pal_HalfMask;
							if (dat & Ibit)
								dat0 |= Pal_Ix2;
							dat &= Pal_HalfMask;
							dat += dat0;
							dat = GrphPal[dat >> 1];
							Grp_LineBuf[i] = (uint16_t)dat;
						}
					} else
						Grp_LineBuf[i] = (uint16_t)dat;
				}
			}
		}
	} else {
		if (opaq) {
			for (i = 0; i < TextDotX; ++i, x = (x + 1) & 0x1ff) {
				uint32_t dat = GVRAM[y + x * 2] & 0x0f;
				uint32_t dat0 = Grp_LineBufSP[i];

				if (dat0 != 0) {
					if (dat != 0) {
						dat = GrphPal[dat];
						if (dat != 0) {
							dat0 &= Pal_HalfMask;
							if (dat & Ibit)
								dat0 |= Pal_Ix2;
							dat &= Pal_HalfMask;
							dat += dat0;
							dat >>= 1;
						}
					}
				} else
					dat = GrphPal[dat];
				Grp_LineBuf[i] = (uint16_t)dat;
			}
		} else {
			for (i = 0; i < TextDotX; ++i, x = (x + 1) & 0x1ff) {
				uint32_t dat = GVRAM[y + x * 2] & 0x0f;
				uint32_t dat0 = Grp_LineBufSP[i];

				if (dat0 != 0) {
					if (dat != 0) {
						dat = GrphPal[dat];
						if (dat != 0) {
							dat0 &= Pal_HalfMask;
							if (dat & Ibit)
								dat0 |= Pal_Ix2;
							dat &= Pal_HalfMask;
							dat += dat0;
							dat >>= 1;
							Grp_LineBuf[i] = (uint16_t)dat;
						}
					} else
						Grp_LineBuf[i] = (uint16_t)dat;
				} else if (dat != 0)
					Grp_LineBuf[i] = GrphPal[dat];
			}
		}
	}
}
#endif /* !USE_ASM */
