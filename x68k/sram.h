#ifndef _winx68k_sram
#define _winx68k_sram

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void SRAM_Init(void);
void SRAM_Cleanup(void);
void SRAM_VirusCheck(void);

IOBUS8 FASTCALL SRAM_Read(DWORD adr);
IOBUS16 FASTCALL SRAM16_Read(DWORD adr);
void FASTCALL SRAM_Write(DWORD adr, IOBUS8 data);
void FASTCALL SRAM16_Write(DWORD adr, IOBUS16 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_sram
