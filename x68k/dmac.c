﻿// ---------------------------------------------------------------------------------------
//  DMAC.C - DMAコントローラ（HD63450）
//  ToDo : もっと奇麗に ^^;
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "dmac.h"
#include "adpcm.h"
#include "fdc.h"
#include "irqh.h"
#include "memory.h"
#include "mercury.h"
#include "sasi.h"

struct DmaCtrl DMA;

#ifdef WIN68DEBUG
uint8_t dmatrace = 0;
#endif	// WIN68DEBUG

#define DMAINT(dch)			do {								\
								if ((dch)->r.CCR & 0x08) {		\
									DMA.IntCH |= (dch)->bit;	\
									IRQH_Int(3, &DMA_Int);		\
								}								\
							} while (0 /*CONSTCOND*/)
#define DMAERR(dch, err)	do {						\
								(dch)->r.CER  = err;	\
								(dch)->r.CSR |= 0x10;	\
								(dch)->r.CSR &= 0xf7;	\
								(dch)->r.CCR &= 0x7f;	\
								(dch)->ready = 0;		\
								DMAINT(dch);			\
							} while (0 /*CONSTCOND*/)


static int DMA_DummyIsReady(void)
{
	return 0;
}


static void DMA_SetReadyCB(unsigned ch, int (*func)(void))
{
	if (ch < 4) DMA.ch[ch].IsDeviceReady = func;
}

INLINE void DMA_UpdateReady(DMACH *dmach)
{
	dmach->ready = ((dmach->r.CSR & 0x88) == 0x08) && (!(dmach->r.CCR & 0x20)) && (dmach->r.MTC);
}

// -----------------------------------------------------------------------
//   割り込みベクタを返す
// -----------------------------------------------------------------------
static int32_t FASTCALL DMA_Int(uint_fast8_t irq)
{
	int32_t ret = -1;
	IRQH_IRQCallBack(irq);
	if ( irq==3 ) {
		int i = DMA.LastInt;
		do {
			const DMACH *dmach = &DMA.ch[i];
			const uint_fast8_t bit = 1 << i;
			if ( DMA.IntCH&bit ) {
				if ( (dmach->r.CSR)&0x10 )
					ret = dmach->r.EIV;
				else
					ret = dmach->r.NIV;
				DMA.IntCH &= ~bit;
				break;
			}
			i = (i+1)&3;
		} while ( i!=DMA.LastInt );
		DMA.LastInt = i;
	}
	if ( DMA.IntCH ) IRQH_Int(3, &DMA_Int);
	return ret;
}


// -----------------------------------------------------------------------
//   I/O Read
// -----------------------------------------------------------------------
IOBUS8 FASTCALL DMA_Read(DWORD adr)
{
	unsigned ch;
	DMACH *dmach;
	unsigned off;

	if (adr & 0x1f00) return 0;		// ばすえらー？

	ch = (adr >> 6) & 3;
	dmach = &DMA.ch[ch];

	off = adr & 0x3f;
	switch ( off ) {
#ifndef	NO_MERCURY
	case 0x00:
		if ( (ch==2)&&(off==0) ) {
			dmach->r.CSR = (dmach->r.CSR&0xfe)|(Mcry_LRTiming&1);
			Mcry_LRTiming ^= 1;
		}
		break;
#endif
	case 0x0a: case 0x0b: case 0x1a: case 0x1b:
		off = ADRSWAPW(off);
		break;
	case 0x0c: case 0x0d: case 0x0e: case 0x0f:
	case 0x14: case 0x15: case 0x16: case 0x17:
	case 0x1c: case 0x1d: case 0x1e: case 0x1f:
		off = ADRSWAPD(off);
		break;
	default:
		break;
	}
	return ((uint8_t *)(&dmach->r))[off];
}


// -----------------------------------------------------------------------
//   I/O Write
// -----------------------------------------------------------------------
void FASTCALL DMA_Write(DWORD adr, IOBUS8 data)
{
	unsigned ch;
	DMACH *dmach;
	unsigned off;
	uint_fast8_t old;

	if (adr & 0x1f00) return;		// ばすえらー？

	ch = (adr >> 6) & 3;
	dmach = &DMA.ch[ch];

	off = adr & 0x3f;
/*if (ch==3) {
FILE* fp = fopen("_dma.txt", "a");
fprintf(fp, "W $%02X $%02X\n", off, data);
fclose(fp);
}*/

	switch (off) {
	case 0x0a: case 0x0b:
		((uint8_t *)(&dmach->r))[ADRSWAPW(off)] = data;
		DMA_UpdateReady(dmach);
		break;

	case 0x1a: case 0x1b:
		((uint8_t *)(&dmach->r))[ADRSWAPW(off)] = data;
		break;

	case 0x0c: case 0x0d: case 0x0e: case 0x0f:
	case 0x14: case 0x15: case 0x16: case 0x17:
	case 0x1c: case 0x1d: case 0x1e: case 0x1f:
		((uint8_t *)(&dmach->r))[ADRSWAPD(off)] = data;
		break;

	case 0x00:
		dmach->r.CSR &= ((~data)|0x09);
		DMA_UpdateReady(dmach);
		break;

	case 0x01:
		dmach->r.CER &= (~data);
		break;

	case 0x07:
		old = dmach->r.CCR;
		dmach->r.CCR = (data & 0xef) | (dmach->r.CCR & 0x80);		// CCRのSTRは書き込みでは落とせない
		DMA_UpdateReady(dmach);
		if ((data & 0x10) && (dmach->r.CCR & 0x80)) {			// Software Abort
			DMAERR(dmach, 0x11);
			break;
		}
		if (data & 0x20) {						// Halt
//			dmach->r.CSR &= 0xf7;					// 本来は落ちるはず。Nemesis'90で調子悪いので…
			break;
		}
		if (data & 0x80) {						// 動作開始
			if (old & 0x20) {					// Halt解除
				dmach->r.CSR |= 0x08;
				DMA_UpdateReady(dmach);
				DMA_Process(dmach);
			} else {
				if (dmach->r.CSR & 0xf8) {			// タイミングエラー
					DMAERR(dmach, 0x02);
					break;
				}
				dmach->r.CSR |= 0x08;
				if ((dmach->r.OCR & 8)/*&&(!dmach->r.MTC)*/) {	// アレイ／リンクアレイチェイン
					dmach->r.MAR = dma_readmem24_dword(dmach->r.BAR) & 0xffffff;
					dmach->r.MTC = dma_readmem24_word(dmach->r.BAR + 4);
					DMA_UpdateReady(dmach);
					if (dmach->r.OCR & 4) {
						dmach->r.BAR = dma_readmem24_dword(dmach->r.BAR+6);
					} else {
						dmach->r.BAR += 6;
						if (!dmach->r.BTC) {		// これもカウントエラー
						DMAERR(dmach, 0x0f);
							break;
						}
					}
				}
				if (!dmach->r.MTC) {				// カウントエラー
					DMAERR(dmach, 0x0d);
					break;
				}
				dmach->r.CER = 0x00;
				DMA_UpdateReady(dmach);
				DMA_Process(dmach);					// 開始直後にカウンタを見て動作チェックする場合があるので、少しだけ実行しておく
			}
		}
		if ((data&0x40)&&(!dmach->r.MTC)) {				// Continuous Op.
			if (dmach->r.CCR&0x80) {
				if (dmach->r.CCR&0x40) {
					DMAERR(dmach, 0x02);
				} else if (dmach->r.OCR & 8) {			// アレイ／リンクアレイチェイン
					DMAERR(dmach, 0x01);
				} else {
					dmach->r.MAR = dmach->r.BAR;
					dmach->r.MTC = dmach->r.BTC;
					dmach->r.CSR |= 0x08;
					dmach->r.BAR = 0;
					dmach->r.BTC = 0;
					DMA_UpdateReady(dmach);
					if (!dmach->r.MAR) {
						dmach->r.CSR |= 0x40;		// ブロック転送終了ビット／割り込み
						DMAINT(dmach);
						break;
					} else if (!dmach->r.MTC) {
						DMAERR(dmach, 0x0d);
						break;
					}
					dmach->r.CCR &= 0xbf;
					DMA_Process(dmach);
				}
			} else {						// 非Active時のCNTビットは動作タイミングエラー
				DMAERR(dmach, 0x02);
			}
		}
		break;

	case 0x3f:
		if (ch != 3) break;

	default:
		((uint8_t *)(&dmach->r))[off] = data;
		break;
	}
}


// -----------------------------------------------------------------------
//   DMA実行
// -----------------------------------------------------------------------
int FASTCALL DMA_Process(DMACH *dmach)
{
	DWORD *src, *dst;

//	if ( DMA.IntCH & dmach->bit ) return 1;

	if (!dmach->ready)
	{
		return 0;
	}

	if (dmach->r.OCR & 0x80) {		// Device->Memory
		src = &dmach->r.DAR;
		dst = &dmach->r.MAR;
	} else {				// Memory->Device
		src = &dmach->r.MAR;
		dst = &dmach->r.DAR;
	}

	do
	{
		if (((dmach->r.OCR & 3) == 2) && (!dmach->IsDeviceReady()))
		{
			break;
		}
		BusErrFlag = 0;
		switch (((dmach->r.OCR >> 4) & 3) + ((dmach->r.DCR >> 1) & 4)) {
			case 0:
			case 3:
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				break;
			case 1:
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				break;
			case 2:
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				break;
			case 4:
				dma_writemem24(*dst, dma_readmem24(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 1; else if (dmach->r.SCR & 8) dmach->r.MAR -= 1;
				if (dmach->r.SCR & 1) dmach->r.DAR += 1; else if (dmach->r.SCR & 2) dmach->r.DAR -= 1;
				break;
			case 5:
				dma_writemem24_word(*dst, dma_readmem24_word(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 2; else if (dmach->r.SCR & 8) dmach->r.MAR -= 2;
				if (dmach->r.SCR & 1) dmach->r.DAR += 2; else if (dmach->r.SCR & 2) dmach->r.DAR -= 2;
				break;
			case 6:
				dma_writemem24_dword(*dst, dma_readmem24_dword(*src));
				if (dmach->r.SCR & 4) dmach->r.MAR += 4; else if (dmach->r.SCR & 8) dmach->r.MAR -= 4;
				if (dmach->r.SCR & 1) dmach->r.DAR += 4; else if (dmach->r.SCR & 2) dmach->r.DAR -= 4;
				break;
		}

		if ( BusErrFlag ) {
			switch ( BusErrFlag ) {
			case 1:					// BusErr/Read
				DMAERR(dmach, (dmach->r.OCR & 0x80) ? 0x0a : 0x09);
				break;
			case 2:					// BusErr/Write
				DMAERR(dmach, (dmach->r.OCR & 0x80) ? 0x09 : 0x0a);
				break;
			case 3:					// AdrErr/Read
				DMAERR(dmach, (dmach->r.OCR & 0x80) ? 0x06 : 0x05);
				break;
			case 4:					// BusErr/Write
				DMAERR(dmach, (dmach->r.OCR & 0x80) ? 0x05 : 0x06);
				break;
			}
			BusErrFlag = 0;
			break;
		}

		dmach->r.MTC--;
		if (!dmach->r.MTC) {					// 指定分のバイト数転送終了
			dmach->ready = 0;
			if (dmach->r.OCR & 8) {				// チェインモードで動いている場合
				if (dmach->r.OCR & 4) {			// リンクアレイチェイン
					if (dmach->r.BAR) {
						dmach->r.MAR = dma_readmem24_dword(dmach->r.BAR);
						dmach->r.MTC = dma_readmem24_word(dmach->r.BAR + 4);
						dmach->r.BAR = dma_readmem24_dword(dmach->r.BAR + 6);
						if ( BusErrFlag ) {
							DMAERR(dmach, (BusErrFlag == 1) ? 0x0b : 0x07);
							BusErrFlag = 0;
							break;
						} else if (!dmach->r.MTC) {
							DMAERR(dmach, 0x0d);
							break;
						}
					}
				} else {					// アレイチェイン
					dmach->r.BTC--;
					if (dmach->r.BTC) {			// 次のブロックがある
						dmach->r.MAR = dma_readmem24_dword(dmach->r.BAR);
						dmach->r.MTC = dma_readmem24_word(dmach->r.BAR + 4);
						dmach->r.BAR += 6;
						if ( BusErrFlag ) {
							DMAERR(dmach, (BusErrFlag == 1) ? 0x0b : 0x07);
							BusErrFlag = 0;
							break;
						} else if (!dmach->r.MTC) {
							DMAERR(dmach, 0x0d);
							break;
						}
					}
				}
			} else {						// 通常モード（1ブロックのみ）終了
				if (dmach->r.CCR & 0x40) {			// Countinuous動作中
					dmach->r.CSR |= 0x40;			// ブロック転送終了ビット／割り込み
					DMAINT(dmach);
					if (dmach->r.BAR) {
						dmach->r.MAR  = dmach->r.BAR;
						dmach->r.MTC  = dmach->r.BTC;
						dmach->r.CSR |= 0x08;
						dmach->r.BAR  = 0x00;
						dmach->r.BTC  = 0x00;
						if (!dmach->r.MTC)
						{
							DMAERR(dmach, 0x0d);
							break;
						}
						dmach->r.CCR &= 0xbf;
					}
				}
			}
			if (!dmach->r.MTC)
			{
				dmach->r.CSR |= 0x80;
				dmach->r.CSR &= 0xf7;
				DMAINT(dmach);
				break;
			}

			dmach->ready = 1;
		}
		if ((dmach->r.OCR & 3) != 1) break;
	} while (TRUE /*CONSTCOND*/);

	return 0;
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void DMA_Init(void)
{
	int i;
	memset(&DMA, 0, sizeof(DMA));
	for (i=0; i<4; i++) {
		DMACH *dmach = &DMA.ch[i];
//		dmach->r.CSR = 0;
//		dmach->r.CCR = 0;
		dmach->bit = 1 << i;
		dmach->IsDeviceReady = DMA_DummyIsReady;
	}
	DMA_SetReadyCB(0, FDC_IsDataReady);
	DMA_SetReadyCB(1, SASI_IsReady);
#ifndef	NO_MERCURY
	DMA_SetReadyCB(2, Mcry_IsReady);
#endif
	DMA_SetReadyCB(3, ADPCM_IsReady);
}
