﻿// ---------------------------------------------------------------------------------------
//  TVRAM.C - Text VRAM
//  ToDo : 透明色処理とか色々
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "tvram.h"
#include "bg.h"
#include "crtc.h"
#include "memory.h"
#include "palette.h"

	BYTE	TextDrawWork[1024 * 1024];
	BYTE	TextDirtyLine[1024];

static	uint32_t	TextDrawPattern[4][256][2];


// -----------------------------------------------------------------------
//   全部書き換え～
// -----------------------------------------------------------------------
void _TVRAM_SetAllDirty(void)
{
	memset(TextDirtyLine, 1, 1024);
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void TVRAM_Init(void)
{
	int i, j, bit;
	ZeroMemory(TVRAM, 0x80000);
	ZeroMemory(TextDrawWork, 1024*1024);
	TVRAM_SetAllDirty();

	ZeroMemory(TextDrawPattern, sizeof(TextDrawPattern));	// パターンテーブル初期化
	for (i=0; i<256; i++)
	{
		for (j=0, bit=0x80; j<8; j++, bit>>=1)
		{
			if (i & bit)
			{
				((uint8_t *)TextDrawPattern[0][i])[j] = 1;
				((uint8_t *)TextDrawPattern[1][i])[j] = 2;
				((uint8_t *)TextDrawPattern[2][i])[j] = 4;
				((uint8_t *)TextDrawPattern[3][i])[j] = 8;
			}
		}
	}
}


// -----------------------------------------------------------------------
//   撤収
// -----------------------------------------------------------------------
void TVRAM_Cleanup(void)
{
}


// -----------------------------------------------------------------------
//   読むなり
// -----------------------------------------------------------------------
IOBUS8 FASTCALL TVRAM_Read(DWORD adr)
{
	return TVRAM[ADRSWAPW(adr) - 0xe00000];
}

IOBUS16 FASTCALL TVRAM16_Read(DWORD adr)
{
	return *(uint16_t *)(&TVRAM[adr - 0xe00000]);
}

// -----------------------------------------------------------------------
//   書くなり
// -----------------------------------------------------------------------
INLINE void BuildTextPattern(uint32_t *dst, const uint8_t *src)
{
	uint_fast8_t pat = src[0x20000 * 0];
	uint32_t t0 = TextDrawPattern[0][pat][0];
	uint32_t t1 = TextDrawPattern[0][pat][1];

	pat = src[0x20000 * 1];
	t0 += TextDrawPattern[1][pat][0];
	t1 += TextDrawPattern[1][pat][1];

	pat = src[0x20000 * 2];
	t0 += TextDrawPattern[2][pat][0];
	t1 += TextDrawPattern[2][pat][1];

	pat = src[0x20000 * 3];
	t0 += TextDrawPattern[3][pat][0];
	t1 += TextDrawPattern[3][pat][1];

	dst[0] = t0;
	dst[1] = t1;
}

void FASTCALL TVRAM_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);
	adr = ADRSWAPW(adr) - 0xe00000;
	if (CRTC_R21H & 1)			// 同時アクセス
	{
		const uint_fast8_t mask = (CRTC_R21H & 2) ? CRTC_R23[adr & 1] : 0;
		SetTextDirtyLine((adr >> 7) - TextScroll->Y);
		adr = adr & (~(3 << 17));
		if (mask)
		{
			data &= ~mask;
			if (CRTC_R21L & 0x10) TVRAM[adr        ] = (TVRAM[adr        ] & mask) + data;
			if (CRTC_R21L & 0x20) TVRAM[adr+0x20000] = (TVRAM[adr+0x20000] & mask) + data;
			if (CRTC_R21L & 0x40) TVRAM[adr+0x40000] = (TVRAM[adr+0x40000] & mask) + data;
			if (CRTC_R21L & 0x80) TVRAM[adr+0x60000] = (TVRAM[adr+0x60000] & mask) + data;
		}
		else
		{
			if (CRTC_R21L & 0x10) TVRAM[adr        ] = data;
			if (CRTC_R21L & 0x20) TVRAM[adr+0x20000] = data;
			if (CRTC_R21L & 0x40) TVRAM[adr+0x40000] = data;
			if (CRTC_R21L & 0x80) TVRAM[adr+0x60000] = data;
		}
	}
	else					// シングルアクセス
	{
		if (CRTC_R21H & 2)		// Text Mask
		{
			const uint_fast8_t mask = CRTC_R23[adr & 1];
			data = (TVRAM[adr] & mask) + (data & (~mask));
		}
		if (TVRAM[adr] == data)
		{
			return;
		}
		TVRAM[adr] = data;
		SetTextDirtyLine((adr >> 7) - TextScroll->Y);

		adr = adr & (~(3 << 17));
	}

	{
		const uint8_t *src = &TVRAM[adr];
		uint32_t *dst = (uint32_t *)(&TextDrawWork[ADRSWAPW(adr) << 3]);
		BuildTextPattern(dst, src);
	}
}

void FASTCALL TVRAM16_Write(DWORD adr, IOBUS16 data)
{
	adr = adr - 0xe00000;
	if (CRTC_R21H & 1)			// 同時アクセス
	{
		const uint_fast16_t mask = (CRTC_R21H & 2) ? *(uint16_t *)(CRTC_R23) : 0;
		SetTextDirtyLine((adr >> 7) - TextScroll->Y);
		adr = adr & (~(3 << 17));
		if (mask)
		{
			data &= ~mask;
			if (CRTC_R21L & 0x10) *(uint16_t *)(&TVRAM[adr        ]) = (*(uint16_t *)(&TVRAM[adr        ]) & mask) + data;
			if (CRTC_R21L & 0x20) *(uint16_t *)(&TVRAM[adr+0x20000]) = (*(uint16_t *)(&TVRAM[adr+0x20000]) & mask) + data;
			if (CRTC_R21L & 0x40) *(uint16_t *)(&TVRAM[adr+0x40000]) = (*(uint16_t *)(&TVRAM[adr+0x40000]) & mask) + data;
			if (CRTC_R21L & 0x80) *(uint16_t *)(&TVRAM[adr+0x60000]) = (*(uint16_t *)(&TVRAM[adr+0x60000]) & mask) + data;
		}
		else
		{
			if (CRTC_R21L & 0x10) *(uint16_t *)(&TVRAM[adr        ]) = data;
			if (CRTC_R21L & 0x20) *(uint16_t *)(&TVRAM[adr+0x20000]) = data;
			if (CRTC_R21L & 0x40) *(uint16_t *)(&TVRAM[adr+0x40000]) = data;
			if (CRTC_R21L & 0x80) *(uint16_t *)(&TVRAM[adr+0x60000]) = data;
		}
	}
	else					// シングルアクセス
	{
		if (CRTC_R21H & 2)		// Text Mask
		{
			const uint_fast16_t mask = *(uint16_t *)(CRTC_R23);
			data = (TVRAM[adr] & mask) + (data & (~mask));
		}
		if (*(uint16_t *)(&TVRAM[adr]) == data)
		{
			return;
		}
		*(uint16_t *)(&TVRAM[adr]) = data;
		SetTextDirtyLine((adr >> 7) - TextScroll->Y);

		adr = adr & (~(3 << 17));
	}

	{
		const uint8_t *src = &TVRAM[adr];
		uint32_t *dst = (uint32_t *)(&TextDrawWork[adr << 3]);
		BuildTextPattern(dst + 0, src + ADRSWAPW(0));
		BuildTextPattern(dst + 2, src + ADRSWAPW(1));
	}
}

// -----------------------------------------------------------------------
//   らすたこぴー時のあっぷでーと
// -----------------------------------------------------------------------
void FASTCALL TVRAM_RCUpdate(void)
{
	const uint32_t adr = CRTC_R22L << 9;
	const uint8_t* src = &TVRAM[adr];
	uint32_t *dst = (uint32_t *)(&TextDrawWork[adr << 3]);
	unsigned int i;

	for (i = 0; i < 512; i += 2)
	{
		BuildTextPattern(dst + (i * 2) + 0, src + i + ADRSWAPW(0));
		BuildTextPattern(dst + (i * 2) + 2, src + i + ADRSWAPW(1));
	}
}


// -----------------------------------------------------------------------
//   1ライン描画
// -----------------------------------------------------------------------
#ifndef USE_ASM
void Text_DrawLine(uint32_t v, uint_fast8_t opaq)
{
	DWORD addr;
	DWORD x, y;
	const uint32_t off = 16;
	uint32_t i;
	uint32_t limit;

	if ((CRTC_R20L & 0x1c) == 0x1c) {
		v *= 2;
	}
	y = ((TextScroll->Y + v) & 0x3ff) << 10;

	x = TextScroll->X & 0x3ff;
	addr = x + y;

	limit = 1024 - x;
	if (limit > TextDotX) {
		limit = TextDotX;
	}

	if (opaq) {
		for (i = 0; i < limit; i++) {
			const uint8_t t = TextDrawWork[addr + i];
			Text_TrFlag[off + i] = t ? 1 : 0;
			BG_LineBuf[off + i] = TextPal[t];
		}
		for (; i < TextDotX; i++) {
			BG_LineBuf[off + i] = TextPal[0];
			Text_TrFlag[off + i] = 0;
		}
	} else {
		for (i = 0; i < limit; i++) {
			const uint8_t t = TextDrawWork[addr + i];
			if (t) {
				Text_TrFlag[off + i] |= 1;
				BG_LineBuf[off + i] = TextPal[t];
			}
		}
	}
}
#endif	/* !USE_ASM */
