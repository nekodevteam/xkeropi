
%include 'x68k-x86.inc'

%ifidn __OUTPUT_FORMAT__, win32
%define	Text_DrawLine	_Text_DrawLine
%endif

	bits	32
	section	.text

	global	Text_DrawLine
	align	16
Text_DrawLine:
	push	ebx
	push	edi

	mov	al, [CRTC_R20L]
	mov	edx, [esp + 8 + 4]	; v
	mov	ebx, [TextScroll]
	and	al, 1ch
	shl	edx, 16
	cmp	al, 1ch
	jne	short .single
	add	edx, edx
.single:
	add	edx, ebx
	xor	edi, edi
	and	edx, (1023 << 16)
	and	ebx, 1023
	shr	edx, 6
	mov	ecx, [TextDotX]
	add	edx, ebx
	xor	ebx, 1023
	inc	ebx
	cmp	ebx, ecx
	jc	short .main
	mov	ebx, ecx

.main:	cmp	byte [esp + 8 + 8], 0	; ecx = opaq
	jz	short .nolooptextline

.looptextline:
	movzx	eax, byte [TextDrawWork + edx + edi]
	mov	byte [Text_TrFlag + 16 + edi], 0
	and	eax, byte 15
	jz	short .textline_skip
	mov	byte [Text_TrFlag + 16 + edi], 1
.textline_skip:
	mov	ax, [TextPal + eax*2]
	mov	[BG_LineBuf + 32 + edi*2], ax
	inc	edi
	cmp	edi, ebx
	jc	short .looptextline

	cmp	edi, ecx
	jnc	short .finishtextline

	mov	ax, word [TextPal + 0]
.endtextlineloop:
	mov	word [BG_LineBuf + 32 + edi*2], ax
	mov	byte [Text_TrFlag + 16 + edi], 0
	inc	edi
	cmp 	edi, ecx
	jc	short .endtextlineloop
	jmp	short .finishtextline

.nolooptextline:
	movzx	eax, byte [TextDrawWork + edx + edi]
	and	eax, byte 15
	jz	short .notextline_skip
	or	byte [Text_TrFlag + 16 + edi], 1
	mov	ax, [TextPal + eax*2]
	mov	[BG_LineBuf + 32 + edi*2], ax
.notextline_skip:
	inc	edi
	cmp	edi, ebx
	jc	short .nolooptextline

.finishtextline:
	pop	edi
	pop	ebx
	ret
