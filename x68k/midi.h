#ifndef _winx68k_midi
#define _winx68k_midi

#include "midiout.h"
#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern MIDIOUTHDL hMIDIOut;

void MIDI_Init(void);
void MIDI_Cleanup(void);
void MIDI_Reset(void);
IOBUS8 FASTCALL MIDI_Read(DWORD adr);
void FASTCALL MIDI_Write(DWORD adr, IOBUS8 data);
void MIDI_SetModule(void);
void FASTCALL MIDI_Timer(DWORD clk);
int MIDI_SetMimpiMap(LPCTSTR filename);
int MIDI_EnableMimpiDef(int enable);
void MIDI_DelayOut(unsigned int delay);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_midi
