#ifndef _winx68k_ioc
#define _winx68k_ioc

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	IOC_IntStat;
extern	BYTE	IOC_IntVect;

void IOC_Init(void);
IOBUS8 FASTCALL IOC_Read(DWORD adr);
void FASTCALL IOC_Write(DWORD adr, IOBUS8 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_ioc
