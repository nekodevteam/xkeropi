﻿// ---------------------------------------------------------------------------------------
//  ADPCM.C - ADPCM (OKI MSM6258V)
//    な～んか、X68Sound.dllに比べてカシャカシャした音になるんだよなぁ……
//    DSoundのクセってのもあるけど、それだけじゃなさそうな気もする
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "adpcm.h"
#include <math.h>
#include "dmac.h"
#include "prop.h"

#define ADPCM_BufSize      96000
#define ADPCMMAX           2047
#define ADPCMMIN          -2048

union StereoSample
{
	uint32_t d;
	struct
	{
		int16_t l;
		int16_t r;
	} w;
};
typedef union StereoSample STEREOSAMPLE;

static int ADPCM_VolumeShift = 16;
static const int8_t index_shift[8] = { -1, -1, -1, -1, 2, 4, 6, 8 };
static const uint8_t clock_mul[8] = {6, 8, 12, 8, 3, 4, 6, 4};
static int dif_table[49][16];
static STEREOSAMPLE ADPCM_Buf[ADPCM_BufSize];

static int32_t ADPCM_WrPtr = 0;
static int32_t ADPCM_RdPtr = 0;
static DWORD ADPCM_SampleRate = 44100*12;
static DWORD ADPCM_ClockRate = 12625*6;
static DWORD ADPCM_Count = 0;
static int8_t ADPCM_Step = 0;
static int ADPCM_Out = 0;
static BYTE ADPCM_Playing = 0;
static BYTE ADPCM_Clock = 0;
static uint8_t ADPCM_ClockMul = 6;
static int ADPCM_PreCounter = 0;
static int ADPCM_DifBuf = 0;


static int ADPCM_Pan = 0x00;
static STEREOSAMPLE OldRL;
static int Outs[8];
#define ADPCM_RatioShift 10
static unsigned int ADPCM_Ratio = 1 << ADPCM_RatioShift;
static signed int ADPCM_Sample = 0;

int ADPCM_IsReady(void)
{
	return 1;
}


// -----------------------------------------------------------------------
//   てーぶる初期化
// -----------------------------------------------------------------------
static void ADPCM_InitTable(void)
{
	int step, n;

	for (step=0; step<=48; step++) {
		const double val = floor(16.0 * pow (1.1, (double)step));
		for (n=0; n<16; n++) {
			dif_table[step][n] = ((n < 8) ? 1 : -1) * (int)(val / 8 * (((n & 7) * 2) + 1));
		}
	}
}

// -----------------------------------------------------------------------
//   MPUクロック経過分だけバッファにデータを溜めておく
// -----------------------------------------------------------------------
void FASTCALL ADPCM_PreUpdate(DWORD clock)
{
	/*if (!ADPCM_Playing) return;*/
	ADPCM_PreCounter += (ADPCM_ClockMul * clock);
	while ( ADPCM_PreCounter>=(10000000 * 12 / 15625) ) {
		// ↓ データの送りすぎ防止（A-JAX）。200サンプリングくらいまでは許そう…。
		ADPCM_DifBuf -= ( (ADPCM_SampleRate*200)/ADPCM_ClockRate );
		if ( ADPCM_DifBuf<=0 ) {
			ADPCM_DifBuf = 0;
			DMA_Exec(3);
		}
		ADPCM_PreCounter -= (10000000 * 12 / 15625);
	}
}


// -----------------------------------------------------------------------
//   DSoundが指定してくる分だけバッファにデータを書き出す
// -----------------------------------------------------------------------
void FASTCALL ADPCM_Update(signed short *buffer, DWORD length)
{
	signed int outl, outr;

	if ( length<=0 ) return;
	while ( length ) {
		int tmpl, tmpr;
		if ( (ADPCM_WrPtr==ADPCM_RdPtr)&&(!(DMA.ch[3].r.CCR&0x40)) ) DMA_Exec(3);
		if ( ADPCM_WrPtr!=ADPCM_RdPtr ) {
			OldRL = ADPCM_Buf[ADPCM_RdPtr];
			ADPCM_RdPtr++;
			if ( ADPCM_RdPtr>=ADPCM_BufSize ) ADPCM_RdPtr = 0;
		}
		outl = OldRL.w.l;
		outr = OldRL.w.r;

		tmpl = outl * ADPCM_VolumeShift;
		if ( Config.Sound_LPF ) {
			outl = tmpl * 40;
			tmpl = (outl + Outs[3]*2 + Outs[2] + Outs[1]*157 - Outs[0]*61) >> 8;
			Outs[2] = Outs[3];
			Outs[3] = outl;
			Outs[0] = Outs[1];
			Outs[1] = tmpl;
		}

		tmpr = outr * ADPCM_VolumeShift;
		if ( Config.Sound_LPF ) {
			outr = tmpr * 40;
			tmpr = (outr + Outs[7]*2 + Outs[6] + Outs[5]*157 - Outs[4]*61) >> 8;
			Outs[6] = Outs[7];
			Outs[7] = outr;
			Outs[4] = Outs[5];
			Outs[5] = tmpr;
		}

		if ( tmpr>32767 ) tmpr = 32767; else if ( tmpr<(-32768) ) tmpr = -32768;
		if ( tmpl>32767 ) tmpl = 32767; else if ( tmpl<(-32768) ) tmpl = -32768;

		*(buffer++) = (short)tmpl;
		*(buffer++) = (short)tmpr;

		length--;
	}

	ADPCM_DifBuf = ADPCM_WrPtr-ADPCM_RdPtr;
	if ( ADPCM_DifBuf<0 ) ADPCM_DifBuf += ADPCM_BufSize;
}


// -----------------------------------------------------------------------
//   1nibble（4bit）をデコード
// -----------------------------------------------------------------------
INLINE void ADPCM_WriteOne(int val)
{
	unsigned n = ADPCM_Ratio;
	const unsigned m = (1 << ADPCM_RatioShift) - ADPCM_Count;

	ADPCM_Out += dif_table[ADPCM_Step][val];
	if ( ADPCM_Out>ADPCMMAX ) ADPCM_Out = ADPCMMAX; else if ( ADPCM_Out<ADPCMMIN ) ADPCM_Out = ADPCMMIN;
	ADPCM_Step += index_shift[val & 7];
	if ( ADPCM_Step>48 ) ADPCM_Step = 48; else if ( ADPCM_Step<0 ) ADPCM_Step = 0;

	if (n >= m)
	{
		const int nOut = (ADPCM_Sample + (ADPCM_Out * m)) >> ADPCM_RatioShift;
		n -= m;
		ADPCM_Buf[ADPCM_WrPtr].w.r = (!(ADPCM_Pan & 1)) ? (short)nOut : 0;
		ADPCM_Buf[ADPCM_WrPtr].w.l = (!(ADPCM_Pan & 2)) ? (short)nOut : 0;
		ADPCM_WrPtr++;
		if (ADPCM_WrPtr >= ADPCM_BufSize) ADPCM_WrPtr = 0;

		while (n >= (1 << ADPCM_RatioShift))
		{
			n -= (1 << ADPCM_RatioShift);
			ADPCM_Buf[ADPCM_WrPtr].w.r = (!(ADPCM_Pan & 1)) ? (short)ADPCM_Out : 0;
			ADPCM_Buf[ADPCM_WrPtr].w.l = (!(ADPCM_Pan & 2)) ? (short)ADPCM_Out : 0;
			ADPCM_WrPtr++;
			if (ADPCM_WrPtr >= ADPCM_BufSize) ADPCM_WrPtr = 0;
		}
		ADPCM_Count = n;
		ADPCM_Sample = ADPCM_Out * n;
	}
	else
	{
		ADPCM_Count += n;
		ADPCM_Sample += ADPCM_Out * n;
	}
}


// -----------------------------------------------------------------------
//   I/O Write
// -----------------------------------------------------------------------
void FASTCALL ADPCM_Write(DWORD adr, IOBUS8 data)
{
	if ( adr==0xe92001 ) {
		if ( data&1 ) {
			ADPCM_Playing = 0;
		} else if ( data&2 ) {
			if ( !ADPCM_Playing ) {
				ADPCM_Step = 0;
				ADPCM_Out = 0;
				OldRL.d = 0;
				ADPCM_Playing = 1;
			}
		}
	} else if ( adr==0xe92003 ) {
		if ( ADPCM_Playing ) {
			ADPCM_WriteOne(data&15);
			ADPCM_WriteOne((data>>4)&15);
		}
	}
}


// -----------------------------------------------------------------------
//   I/O Read（ステータスチェック）
// -----------------------------------------------------------------------
IOBUS8 FASTCALL ADPCM_Read(DWORD adr)
{
	if ( adr==0xe92001 )
		return ((ADPCM_Playing)?0xc0:0x40);
	else
		return 0x00;
}


// -----------------------------------------------------------------------
//   ぼりゅーむ
// -----------------------------------------------------------------------
void ADPCM_SetVolume(BYTE vol)
{
	if ( vol>16 ) vol=16;
//	if ( vol<0  ) vol=0;

	if ( vol )
		ADPCM_VolumeShift = (int)((double)16/pow(1.189207115, (16-vol)));
	else
		ADPCM_VolumeShift = 0;		// Mute
}


// -----------------------------------------------------------------------
//   Clock
// -----------------------------------------------------------------------
static void UpdateClock(uint_fast8_t n)
{
	if (ADPCM_Clock != n)
	{
//		ADPCM_Count = 0;
		ADPCM_Clock = n;
		ADPCM_ClockMul = clock_mul[n];
		ADPCM_ClockRate = clock_mul[n] * 15625;
		ADPCM_Ratio = (ADPCM_SampleRate << ADPCM_RatioShift) / ADPCM_ClockRate;
	}
}


// -----------------------------------------------------------------------
//   Panning
// -----------------------------------------------------------------------
void ADPCM_SetPan(int n)
{
	ADPCM_Pan = n;
	UpdateClock((ADPCM_Clock & 4) + ((n >> 2) & 3));
}


// -----------------------------------------------------------------------
//   Clock
// -----------------------------------------------------------------------
void ADPCM_SetClock(int n)
{
	UpdateClock(n + (ADPCM_Clock & 3));
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void ADPCM_Init(DWORD samplerate)
{
	ADPCM_WrPtr = 0;
	ADPCM_RdPtr = 0;
	ADPCM_Out = 0;
	ADPCM_Step = 0;
	ADPCM_Playing = 0;
	ADPCM_SampleRate = (samplerate*12);
	ADPCM_PreCounter = 0;
	memset(Outs, 0, sizeof(Outs));
	OldRL.d = 0;

	ADPCM_SetPan(0x0b);
	ADPCM_InitTable();
}
