/**
 * @file	x68k.h
 */

#pragma once

#include <assert.h>

#ifdef _M_ARM
typedef uint32_t IOBUS8;
typedef uint32_t IOBUS16;
#else	// _M_ARM
typedef uint_fast8_t IOBUS8;
typedef uint_fast16_t IOBUS16;
#endif	// _M_ARM

#define ADRSWAPW(a)			((a) ^ 1)
#define ADRSWAPD(a)			((a) ^ 3)
