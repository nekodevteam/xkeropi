﻿// ---------------------------------------------------------------------------------------
//  FDD.C - 内蔵FDD Unit（イメージファイルの管理とFD挿抜割り込みの発生）
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "fdd.h"
#include <ctype.h>
#include <shlwapi.h>
#include <string.h>
#include "disk_d88.h"
#include "disk_dim.h"
#include "disk_xdf.h"
#include "ioc.h"
#include "irqh.h"
#include "fileio.h"
#include "status.h"

#if defined(_MSC_VER)
#pragma comment(lib, "shlwapi.lib")
#endif	// defined(_MSC_VER)

typedef struct {
	int SetDelay[4];
	int Types[4];
	int ROnly[4];
	int EMask[4];
	int Blink[4];
	int Access;
} FDDINFO;

static FDDINFO fdd;
static int (* const SetFD[4])(int, LPCTSTR)                           = { 0, XDF_SetFD,        D88_SetFD,        DIM_SetFD };
static int (* const Eject[4])(int)                                    = { 0, XDF_Eject,        D88_Eject,        DIM_Eject };
static int (* const Seek[4])(int, int, FDCID*)                        = { 0, XDF_Seek,         D88_Seek,         DIM_Seek };
static int (* const ReadID[4])(int, FDCID*)                           = { 0, XDF_ReadID,       D88_ReadID,       DIM_ReadID };
static int (* const WriteID[4])(int, int, const unsigned char*, int)  = { 0, XDF_WriteID,      D88_WriteID,      DIM_WriteID };
static int (* const Read[4])(int, FDCID*, unsigned char*)             = { 0, XDF_Read,         D88_Read,         DIM_Read };
static int (* const ReadDiag[4])(int, FDCID*, FDCID*, unsigned char*) = { 0, XDF_ReadDiag,     D88_ReadDiag,     DIM_ReadDiag };
static int (* const Write[4])(int, FDCID*, const unsigned char*, int) = { 0, XDF_Write,        D88_Write,        DIM_Write };
static int (* const GetCurrentID[4])(int, FDCID*)                     = { 0, XDF_GetCurrentID, D88_GetCurrentID, DIM_GetCurrentID };

// -----------------------------------------------------------------------
//   イメージタイプ判別
// -----------------------------------------------------------------------
static int Cmp(LPCTSTR lpString1, LPCTSTR lpString2)
{
	while (1 /*CONSTCOND*/)
	{
		const int s = toupper(*lpString1++);
		const int t = toupper(*lpString2++);
		if (s != t)
		{
			return t - s;
		}
		if (s == 0)
		{
			return 0;
		}
	}
}

static int GetDiskType(LPCTSTR file)
{
	LPCTSTR p;
	int ret = FD_XDF;
	p = PathFindExtension(file);
	if ( p ) {
		if ( (!Cmp(p, TEXT(".D88")))||(!Cmp(p, TEXT(".88D"))) )
			ret = FD_D88;
		else if ( !Cmp(p, TEXT(".DIM")) )
			ret = FD_DIM;
	}
	return ret;
}



// -----------------------------------------------------------------------
//   挿抜割り込み
// -----------------------------------------------------------------------
static int32_t FASTCALL FDD_Int(uint_fast8_t irq)
{
	IRQH_IRQCallBack(irq);
	if ( irq==1 )
		return (IOC_IntVect+1);
	else
		return -1;
}


// -----------------------------------------------------------------------
//   FDせっと
//     すぐには割り込み上げないです
// -----------------------------------------------------------------------
void FDD_SetFD(int drive, LPCTSTR filename, int readonly)
{
	int type = GetDiskType(filename);
	if ( (drive<0)||(drive>3) ) return;
	FDD_EjectFD(drive);
	if ( SetFD[type] ) {
		if ( SetFD[type](drive, filename) ) {
			fdd.Types[drive]  = type;
			fdd.ROnly[drive] |= readonly;
			fdd.SetDelay[drive] = 3;
			fdd.EMask[drive] = 0;
			fdd.Blink[drive] = 0;
			StatBar_SetFDD(drive, filename);
			StatBar_ParamFDD(drive, (fdd.Types[drive]!=FD_Non)?((fdd.Access==drive)?2:1):0, ((fdd.Types[drive]!=FD_Non)&&(!fdd.EMask[drive]))?1:0, (fdd.Blink[drive])?1:0);
		}
	}
}


// -----------------------------------------------------------------------
//   いじぇくと
// -----------------------------------------------------------------------
void FDD_EjectFD(int drive)
{
	int type;
	if ( (drive<0)||(drive>3) ) return;
	type = fdd.Types[drive];
	if ( Eject[type] ) {
		Eject[type](drive);
		if ( IOC_IntStat&2 ) IRQH_Int(1, &FDD_Int);
	}
	fdd.Types[drive] = FD_Non;
	fdd.ROnly[drive] = 0;
	fdd.EMask[drive] = 0;
	fdd.Blink[drive] = 0;
	StatBar_SetFDD(drive, TEXT(""));
	StatBar_ParamFDD(drive, (fdd.Types[drive]!=FD_Non)?((fdd.Access==drive)?2:1):0, ((fdd.Types[drive]!=FD_Non)&&(!fdd.EMask[drive]))?1:0, (fdd.Blink[drive])?1:0);
}


// -----------------------------------------------------------------------
//   Eject Mask / Blink / AccessDrive
// -----------------------------------------------------------------------
void FDD_SetEMask(int drive, int emask)
{
	if ( (drive<0)||(drive>3) ) return;
	if ( fdd.EMask[drive]==emask ) return;
	fdd.EMask[drive] = emask;
	StatBar_ParamFDD(drive, (fdd.Types[drive]!=FD_Non)?((fdd.Access==drive)?2:1):0, ((fdd.Types[drive]!=FD_Non)&&(!fdd.EMask[drive]))?1:0, (fdd.Blink[drive])?1:0);
}

void FDD_SetAccess(int drive)
{
	if ( fdd.Access==drive ) return;
	fdd.Access = drive;
	StatBar_ParamFDD(0, (fdd.Types[0]!=FD_Non)?((fdd.Access==0)?2:1):0, ((fdd.Types[0]!=FD_Non)&&(!fdd.EMask[0]))?1:0, (fdd.Blink[0])?1:0);
	StatBar_ParamFDD(1, (fdd.Types[1]!=FD_Non)?((fdd.Access==1)?2:1):0, ((fdd.Types[1]!=FD_Non)&&(!fdd.EMask[1]))?1:0, (fdd.Blink[1])?1:0);
}

void FDD_SetBlink(int drive, int blink)
{
	if ( (drive<0)||(drive>3) ) return;
	if ( fdd.Blink[drive]==blink ) return;
	fdd.Blink[drive] = blink;
	StatBar_ParamFDD(drive, (fdd.Types[drive]!=FD_Non)?((fdd.Access==drive)?2:1):0, ((fdd.Types[drive]!=FD_Non)&&(!fdd.EMask[drive]))?1:0, (fdd.Blink[drive])?1:0);
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void FDD_Init(void)
{
	memset(&fdd,0 , sizeof(FDDINFO));
	fdd.Access = -1;
	D88_Init();
	XDF_Init();
	DIM_Init();
}


// -----------------------------------------------------------------------
//   終了
// -----------------------------------------------------------------------
void FDD_Cleanup(void)
{
	D88_Cleanup();
	XDF_Cleanup();
	DIM_Cleanup();
}


// -----------------------------------------------------------------------
//   りせっと
// -----------------------------------------------------------------------
void FDD_Reset(void)
{
	int i;
	FDD_SetAccess(-1);
	for (i=0; i<4; i++) {
		FDD_SetEMask(i, 0);
		FDD_SetBlink(i, 0);
	}
}


// -----------------------------------------------------------------------
//   FD入れ替えが起こっていたら割り込み発生
// -----------------------------------------------------------------------
void FDD_SetFDInt(void)
{
	int i;
	for (i=0; i<4; i++) {
		if ( fdd.SetDelay[i] ) {
			fdd.SetDelay[i]--;
			if ( fdd.SetDelay[i]<=0 ) {
				if ( IOC_IntStat&2 ) IRQH_Int(1, &FDD_Int);
				fdd.SetDelay[i] = 0;
			}
		}
	}
}


int FDD_Seek(int drv, int trk, FDCID* id)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( Seek[type] )
		return Seek[type](drv, trk, id);
	else
		return FALSE;
}

int FDD_ReadID(int drv, FDCID* id)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( ReadID[type] )
		return ReadID[type](drv, id);
	else
		return FALSE;
}

int FDD_WriteID(int drv, int trk, const unsigned char* buf, int num)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( WriteID[type] )
		return WriteID[type](drv, trk, buf, num);
	else
		return FALSE;
}


int FDD_Read(int drv, FDCID* id, unsigned char* buf)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( Read[type] )
		return Read[type](drv, id, buf);
	else
		return FALSE;
}


int FDD_ReadDiag(int drv, FDCID* id, FDCID* retid, unsigned char* buf)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( ReadDiag[type] )
		return ReadDiag[type](drv, id, retid, buf);
	else
		return FALSE;
}


int FDD_Write(int drv, FDCID* id, const unsigned char* buf, int del)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( Write[type] )
		return Write[type](drv, id, buf, del);
	else
		return FALSE;
}


int FDD_GetCurrentID(int drv, FDCID* id)
{
	int type;
	if ( (drv<0)||(drv>3) ) return FALSE;
	type = fdd.Types[drv];
	if ( GetCurrentID[type] )
		return GetCurrentID[type](drv, id);
	else
		return FALSE;
}


int FDD_IsReady(int drv)
{
	if ( (drv<0)||(drv>3) ) return FALSE;
	if ( (fdd.Types[drv]!=FD_Non)&&(!fdd.SetDelay[drv]) )
		return TRUE;
	else
		return FALSE;
}


int FDD_IsReadOnly(int drv)
{
	if ( (drv<0)||(drv>3) ) return FALSE;
	return fdd.ROnly[drv];
}


void FDD_SetReadOnly(int drv)
{
	fdd.ROnly[drv] |= 1;
}
