#ifndef _winx68k_68kem
#define _winx68k_68kem

#include "irqh.h"

typedef struct
{
	DWORD d[8];
	DWORD a[8];

	DWORD isp;

	DWORD sr_high;
	DWORD ccr;
	DWORD x_carry;

	DWORD pc;
	DWORD IRQ_level;

	DWORD sr;

	DMAHANDLER irq_callback;

	DWORD ppc;
	DWORD (*reset_callback)(void);

	DWORD sfc;
	DWORD dfc;
	DWORD usp;
	DWORD vbr;

	DWORD bank;

	DWORD memmin;
	DWORD memmax;

	DWORD cputype;
} m68k_regs;

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	int	m68000_ICount;
extern	int	m68000_ICountBk;
extern	int	ICount;
extern	m68k_regs regs;

void	M68KRUN(void);
void	M68KRESET(void);
void	FASTCALL cpu_setOPbase24(DWORD adr);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_68kem
