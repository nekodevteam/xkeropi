#ifndef _winx68k_adpcm_h
#define _winx68k_adpcm_h

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void FASTCALL ADPCM_PreUpdate(DWORD clock);
void FASTCALL ADPCM_Update(signed short *buffer, DWORD length);

void FASTCALL ADPCM_Write(DWORD adr, IOBUS8 data);
IOBUS8 FASTCALL ADPCM_Read(DWORD adr);

void ADPCM_SetVolume(BYTE vol);
void ADPCM_SetPan(int n);
void ADPCM_SetClock(int n);

void ADPCM_Init(DWORD samplerate);
int ADPCM_IsReady(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_adpcm_h
