﻿#ifndef _winx68k_crtc
#define _winx68k_crtc

#include "x68k.h"

#define	VSYNC_HIGH	180310L
#define	VSYNC_NORM	162707L

#ifndef LINEBUFSIZE
#define LINEBUFSIZE		1008
#endif	// LINEBUFSIZE

struct tagCrtc
{
	uint8_t		Mode;
	uint8_t		FastClr;
	uint8_t		VStep;
	uint8_t		Reseted;

	uint32_t	VLine;
	uint32_t	TextX, TextY;

	uint16_t	FastClrMask;
	uint16_t	IntLine;

	int32_t		HSYNC;
	uint16_t	VTOTAL;
	uint8_t		RCFlag[2];

	uint8_t		Regs[24*2];
	uint8_t		VCReg[3][2];

#ifdef USES_ALLFLASH
	uint8_t		AllFlash;
#endif	// USES_ALLFLASH

	uint16_t IntLineRise;
	uint16_t IntLineFall;
	uint16_t IntLineVDisp;
	uint16_t VDispEvent;
};

/**
 * @brief CRTC バッファ
 */
struct tagCrtcLineBuf
{
	uint16_t	bgbuf[LINEBUFSIZE + 16];	//!< BG_LineBuf
	uint16_t	bgpri[LINEBUFSIZE + 16];	//!< BG_PriBuf
	uint8_t		tr[LINEBUFSIZE + 16];		//!< Text_TrFlag
	uint16_t	grpbuf[LINEBUFSIZE + 16];	//!< Grp_LineBuf
	uint16_t	grpsp1[LINEBUFSIZE + 16];	//!< Grp_LineBufSP 特殊プライオリティ／半透明用バッファ
	uint16_t	grpsp2[LINEBUFSIZE + 16];	//!< Grp_LineBufSP2 半透明ベースプレーン用バッファ（非半透明ビット格納）
};
typedef struct tagCrtcLineBuf CRTCLINEBUF;

typedef struct
{
	uint16_t X;
	uint16_t Y;
} CRTCXY;

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern struct tagCrtc	CRTC;

#define	CRTC_Regs	CRTC.Regs
#define	CRTC_R00L	CRTC_Regs[ 0 * 2 + ADRSWAPW(1)]
#define	CRTC_R02L	CRTC_Regs[ 2 * 2 + ADRSWAPW(1)]
#define	CRTC_R03L	CRTC_Regs[ 3 * 2 + ADRSWAPW(1)]
#define	CRTC_R06L	CRTC_Regs[ 6 * 2 + ADRSWAPW(1)]
#define	CRTC_R07H	CRTC_Regs[ 7 * 2 + ADRSWAPW(0)]
#define	CRTC_R07L	CRTC_Regs[ 7 * 2 + ADRSWAPW(1)]
#define	CRTC_R20H	CRTC_Regs[20 * 2 + ADRSWAPW(0)]
#define	CRTC_R20L	CRTC_Regs[20 * 2 + ADRSWAPW(1)]
#define	CRTC_R21H	CRTC_Regs[21 * 2 + ADRSWAPW(0)]
#define	CRTC_R21L	CRTC_Regs[21 * 2 + ADRSWAPW(1)]
#define	CRTC_R22L	CRTC_Regs[22 * 2 + ADRSWAPW(1)]
#define	CRTC_R23	(&CRTC_Regs[23 * 2])

#define	CRTC_HSTART	(*(uint16_t *)(&CRTC_Regs[ 2 * 2]))
#define	CRTC_HEND	(*(uint16_t *)(&CRTC_Regs[ 3 * 2]))
#define	CRTC_VSTART	(*(uint16_t *)(&CRTC_Regs[ 6 * 2]))
#define	CRTC_VEND	(*(uint16_t *)(&CRTC_Regs[ 7 * 2]))
#define	TextScroll	((const CRTCXY*)(&CRTC_Regs[10 * 2]))
#define	GrphScroll	((const CRTCXY*)(&CRTC_Regs[12 * 2]))

#define	CRTC_Mode		CRTC.Mode
#define	TextDotX		CRTC.TextX
#define	TextDotY		CRTC.TextY
#define	VCReg			CRTC.VCReg
#define	VCReg0			(*(uint16_t *)(VCReg[0]))
#define	VCReg1			(*(uint16_t *)(VCReg[1]))
#define	VCReg2			(*(uint16_t *)(VCReg[2]))
#define	CRTC_IntLine		CRTC.IntLine
#define	CRTC_FastClr		CRTC.FastClr
#define	CRTC_FastClrMask	CRTC.FastClrMask
#define	CRTC_VStep		CRTC.VStep
#define	HSYNC_CLK		CRTC.HSYNC
#define	VLINE_TOTAL		CRTC.VTOTAL
#define	vline			CRTC.VLine

#ifdef WIN68DEBUG
extern	uint8_t	Debug_Text, Debug_Grp, Debug_Sp;
#endif	// WIN68DEBUG

extern CRTCLINEBUF CRTCLineBuf;
#define	BG_LineBuf	CRTCLineBuf.bgbuf
#define	BG_PriBuf	CRTCLineBuf.bgpri
#define	Text_TrFlag	CRTCLineBuf.tr
#define	Grp_LineBuf	CRTCLineBuf.grpbuf
#define	Grp_LineBufSP	CRTCLineBuf.grpsp1
#define	Grp_LineBufSP2	CRTCLineBuf.grpsp2

void CRTC_Init(void);

IOBUS8 FASTCALL CRTC_Read(DWORD adr);
void FASTCALL CRTC_Write(DWORD adr, IOBUS8 data);
void CRTC_DrawLine(uint16_t* dst, uint32_t v);

IOBUS8 FASTCALL VCtrl_Read(DWORD adr);
void FASTCALL VCtrl_Write(DWORD adr, IOBUS8 data);

void FASTCALL CrtVC16_Write(DWORD adr, IOBUS16 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_crtc
