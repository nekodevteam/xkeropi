#ifndef _winx68k_mercury
#define _winx68k_mercury

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern BYTE Mcry_LRTiming;

void FASTCALL Mcry_PreUpdate(DWORD clock);
EXTERNC void FASTCALL Mcry_Update(signed short *buffer, DWORD length);

void FASTCALL Mcry_Write(DWORD adr, IOBUS8 data);
IOBUS8 FASTCALL Mcry_Read(DWORD adr);

void Mcry_SetClock(void);
void Mcry_SetVolume(BYTE vol);

void Mcry_Init(DWORD samplerate, LPCTSTR path);
void Mcry_Cleanup(void);
int Mcry_IsReady(void);

void FASTCALL Mcry_Int(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_mercury
