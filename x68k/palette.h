#ifndef _winx68k_pal
#define _winx68k_pal

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	Pal_Regs[1024];

struct tagGrphTextPal
{
	union
	{
		struct
		{
			uint16_t Grph[256];
			uint16_t Text[256];
		};
		uint16_t Pal[512];
	};
};
extern struct tagGrphTextPal GrphTextPal;
#define	TextPal	GrphTextPal.Text
#define	GrphPal	GrphTextPal.Grph

#ifdef USE_NATIVECOLOR
#define	PAL16(p)	(p)
static const WORD Ibit = 0x0001;
static const WORD Pal_HalfMask = ~((1 << 11) + (1 << 6) + (1 << 1) + (0x0001));
static const WORD Pal_Ix2 = 0x0001 << 1;
#else	// USE_NATIVECOLOR
extern	WORD	Pal16[65536];
extern	WORD	Ibit, Pal_HalfMask, Pal_Ix2;
#define	PAL16(p)	Pal16[(p)]
#endif	// USE_NATIVECOLOR

void Pal_SetColor(void);
void Pal_Init(void);

void Pal_ChangeContrast(int num);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_pal
