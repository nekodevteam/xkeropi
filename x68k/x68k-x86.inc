
%ifidn __OUTPUT_FORMAT__, win32

%ifdef USE_FETCHMEM
%define	FetchMem	_FetchMem
%else
%define	GVRAM		_GVRAM
%endif

%define	BG		_BG
%define	BG_Ram		_BG_Ram
%define	Sprite_Regs	_Sprite_Regs
%define	BG_Regs		_BG_Regs
%define	BG_BG0TOP	_BG_BG0TOP
%define	BG_BG1TOP	_BG_BG1TOP
%define	BG_HAdjust	_BG_HAdjust
%define	BGCHR8		_BGCHR8
%define	BGCHR16		_BGCHR16
%define BG_CalcLine	_BG_CalcLine

%define	CRTC		_CRTC
%define	CRTCLineBuf	_CRTCLineBuf

%define Pal16Adr	_Pal16Adr

%define	Pal_Regs	_Pal_Regs
%define	GrphTextPal	_GrphTextPal
%define	Pal16		_Pal16
%define	Ibit		_Ibit
%define Pal_HalfMask	_Pal_HalfMask
%define	Pal_Ix2		_Pal_Ix2

%define	TextDrawWork	_TextDrawWork
%endif

%ifdef USE_FETCHMEM
extern	FetchMem
%define	GVRAM		(FetchMem + 0xc00000)
%else
extern	GVRAM
%endif

struc	tagBg
	.regs		resb	18
	.chrend		resw	1
	.bg0top		resw	1
	.bg0end		resw	1
	.bg1top		resw	1
	.bg1end		resw	1
	.hadjust	resd	1
	.vadjust	resd	1
endstruc
extern	BG		; tagBg
extern	BG_Ram		; BYTE [0x8000]
extern	Sprite_Regs	; BYTE [0x800]
%define	BG_Regs		(BG + tagBg.regs)
%define BG0ScrollX	(BG_Regs + 0x00)
%define BG0ScrollY	(BG_Regs + 0x02)
%define BG1ScrollX	(BG_Regs + 0x04)
%define BG1ScrollY	(BG_Regs + 0x06)
%define	BS_CTLL		(BG_Regs + 0x08 + 0)
%define	SM_RESO		(BG_Regs + 0x10 + 0)
%define	BG_BG0TOP	(BG + tagBg.bg0top)
%define	BG_BG1TOP	(BG + tagBg.bg1top)
%define	BG_HAdjust	(BG + tagBg.hadjust)
extern	BGCHR8		; BYTE [8*8*256]
extern	BGCHR16		; BYTE [16*16*256]
extern	BG_CalcLine	; DWORD

struc	tagCrtc
	.mode		resb	1
	.fastclr	resb	1
	.vstep		resb	1
	.reseted	resb	1
	.vline		resd	1
	.textx		resd	1
	.texty		resd	1
	.fastclrmask	resw	1
	.intline	resw	1
	.hsync		resd	1
	.vtotal		resw	1
	.rcflag		resb	2
	.regs		resw	24
	.vcreg		resw	3
endstruc
extern	CRTC
%define	CRTC_Regs	(CRTC + tagCrtc.regs)
%define	CRTC_R20L	(CRTC_Regs + (20 * 2) + 0)
%define	TextScroll	(CRTC_Regs + (10 * 2))
%define	GrphScroll	(CRTC_Regs + (12 * 2))
%define	TextDotX	(CRTC + tagCrtc.textx)
%define	CRTC_FastClrMask	(CRTC + tagCrtc.fastclrmask)

struc	tagCrtcLineBuf
	.bgbuf	resw	1024
	.bgpri	resw	1024
	.tr	resb	1024
	.grpbuf	resw	1024
	.grpsp1	resw	1024
	.grpsp2 resw	1024
endstruc
extern	CRTCLineBuf
%define	BG_LineBuf	(CRTCLineBuf + tagCrtcLineBuf.bgbuf)
%define	BG_PriBuf	(CRTCLineBuf + tagCrtcLineBuf.bgpri)
%define	Text_TrFlag	(CRTCLineBuf + tagCrtcLineBuf.tr)
%define	Grp_LineBuf	(CRTCLineBuf + tagCrtcLineBuf.grpbuf)
%define	Grp_LineBufSP	(CRTCLineBuf + tagCrtcLineBuf.grpsp1)
%define	Grp_LineBufSP2	(CRTCLineBuf + tagCrtcLineBuf.grpsp2)

extern	Pal16Adr

extern	Pal_Regs
struc	tagGrphTextPal
	.grph	resw	256
	.text	resw	256
endstruc
extern	GrphTextPal
%define	GrphPal		(GrphTextPal + tagGrphTextPal.grph)
%define	TextPal		(GrphTextPal + tagGrphTextPal.text)
extern	Pal16
extern	Ibit
extern	Pal_HalfMask
extern	Pal_Ix2

extern	TextDrawWork
