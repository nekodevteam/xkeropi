﻿/**
 * @file	x68k.inl
 */

#pragma once

#include "bg.h"
#include "crtc.h"
#include "mfp.h"

INLINE void BG_UpdateVAdjust()
{
	int32_t v;
	BG.Shift = 2 + BG.Shift2 - BG.Shift1;
	v = (int32_t)SM_VDSP - CRTC_VSTART;			// BGとその他がずれてる時の差分
	if (!(SM_RESO & 4))
	{
		v /= 2;
	}
	assert(v == BG_VAdjust);
	if (!(SM_RESO & 16))
	{
		v += (SM_VDSP >> BG.Shift1) - (CRTC_R06L >> BG.Shift2);
	}
	BG.VDiff = v;
}

INLINE void BG_UpdateShift()
{
	BG.Shift1 = ((SM_RESO & 4) ? 2 : 1) - ((SM_RESO & 16) ? 1 : 0);
	BG_UpdateVAdjust();
}

INLINE void CRTC_UpdateShift()
{
	BG.Shift2 = ((CRTC_R20L & 4) ? 2 : 1) - ((CRTC_R20L & 16) ? 1 : 0);
	BG_UpdateVAdjust();
}

INLINE void CRTC_UpdateIntLine()
{
	CRTC.IntLineRise = (!(MFP[MFP_AER] & 0x40)) ? CRTC_IntLine : -1;
	CRTC.IntLineFall = (MFP[MFP_AER] & 0x40) ? CRTC_IntLine : -1;
}

INLINE void CRTC_UpdateVDispEvent()
{
	int line = -1;
	if ((MFP[MFP_TACR] & 15) == 8)
	{
		if (MFP[MFP_AER] & 0x10)
		{
			line = CRTC_VSTART;
		}
		else
		{
			if (CRTC_VEND >= VLINE_TOTAL)
			{
				line = VLINE_TOTAL - 1;
			}
			else
			{
				line = CRTC_VEND;
			}
		}
	}
	CRTC.VDispEvent = line;
}

INLINE void CRTC_UpdateVDisp()
{
	int line;
	if (MFP[MFP_AER] & 0x10)
	{
		line = CRTC_VSTART;
	}
	else
	{
		if (CRTC_VEND >= VLINE_TOTAL)
		{
			line = CRTC_VEND - VLINE_TOTAL;
		}
		else
		{
			line = VLINE_TOTAL - 1;
		}
	}
	CRTC.IntLineVDisp = line;
	CRTC_UpdateVDispEvent();
}
