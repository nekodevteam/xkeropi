#ifndef _winx68k_memory
#define _winx68k_memory

#ifndef LOW16
#define LOW16(a)			((a) & ((1 << 16) - 1))
#endif	// LOW16
#define LOW24(a)			((a) & ((1 << 24) - 1))

#define	Memory_ReadB		dma_readmem24
#define Memory_ReadW		dma_readmem24_word
#define Memory_ReadD		dma_readmem24_dword

#define	Memory_WriteB		dma_writemem24
#define Memory_WriteW		dma_writemem24_word
#define Memory_WriteD		dma_writemem24_dword

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

#ifdef USE_FETCHMEM
extern	uint8_t	FetchMem[0x1000000];
#define	MEM	FetchMem
#define	GVRAM	(FetchMem + 0xc00000)
#define	TVRAM	(FetchMem + 0xe00000)
#define	SCSIIPL	(FetchMem + 0xea0000)
#define	SRAM	(FetchMem + 0xed0000)
#define	FONT	(FetchMem + 0xf00000)
#define	IPL	(FetchMem + 0xfc0000)
#else	// USE_FETCHMEM
extern	BYTE	*MEM;
extern	BYTE	GVRAM[0x80000];
extern	BYTE	TVRAM[0x80000];
extern	BYTE	SCSIIPL[0x2000];
extern	BYTE	SRAM[0x4000];
extern	BYTE	*FONT;
extern	BYTE	*IPL;
#endif	// USE_FETCHMEM

extern	BYTE	BusErrFlag;
extern	DWORD	BusErrAdr;

#ifdef _DEBUG
void FASTCALL Memory_ErrTrace(void);
void FASTCALL Memory_IntErr(int i);
#endif	// _DEBUG

void Memory_Init(void);

#ifdef USE_68KEM
uint8_t FASTCALL dma_readmem24(uint32_t adr);
void FASTCALL dma_writemem24(uint32_t adr, uint8_t data);
uint16_t FASTCALL dma_readmem24_word(uint32_t adr);
void FASTCALL dma_writemem24_word(uint32_t adr, uint16_t data);
#else	// USE_68KEM
uint_fast8_t FASTCALL dma_readmem24(uint32_t adr);
void FASTCALL dma_writemem24(uint32_t adr, uint_fast8_t data);
uint_fast16_t FASTCALL dma_readmem24_word(uint32_t adr);
void FASTCALL dma_writemem24_word(uint32_t adr, uint_fast16_t data);
#endif	// USE_68KEM
uint32_t FASTCALL dma_readmem24_dword(uint32_t adr);
void FASTCALL dma_writemem24_dword(uint32_t adr, uint32_t data);

void FASTCALL Memory_SetSCSIMode(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#define	SetBusErrFlag(t, a)	do { BusErrFlag = t; BusErrAdr = a; } while (0 /*CONSTCOND*/)

#endif //_winx68k_memory
