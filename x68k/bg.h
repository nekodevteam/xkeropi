#ifndef _winx68k_bg
#define _winx68k_bg

#include "x68k.h"

/**
 * @brief BG
 */
struct tagBG
{
	uint8_t Regs[0x12];	// BG_Regs
	uint16_t CHREND;	// BG_CHREND
	uint16_t BG0TOP;	// BG_BG0TOP
	uint16_t BG0END;	// BG_BG0END
	uint16_t BG1TOP;	// BG_BG1TOP
	uint16_t BG1END;	// BG_BG1END
	int32_t HAdjust;	// BG_HAdjust
	int32_t VAdjust;	// BG_VAdjust

	uint8_t Shift;
	uint8_t Shift1;
	uint8_t Shift2;
	uint8_t padding;
	int32_t VDiff;
};

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	struct tagBG	BG;
#define	BG_Regs		BG.Regs
#define	BG_BG0TOP	BG.BG0TOP
#define	BG_BG1TOP	BG.BG1TOP
#define	BG_HAdjust	BG.HAdjust
#define	BG_VAdjust	BG.VAdjust

extern	BYTE	BG_Ram[0x8000];
extern	BYTE	Sprite_Regs[0x800];

extern	BYTE	BGCHR8[8*8*256];
extern	BYTE	BGCHR16[16*16*256];

#define	BS_CTLH		BG_Regs[0x08 + ADRSWAPW(0)]
#define	BS_CTLL		BG_Regs[0x08 + ADRSWAPW(1)]
#define	SM_HTTL		BG_Regs[0x0a + ADRSWAPW(1)]
#define	SM_HDSP		BG_Regs[0x0c + ADRSWAPW(1)]
#define	SM_VDSP		BG_Regs[0x0e + ADRSWAPW(1)]
#define	SM_RESO		BG_Regs[0x10 + ADRSWAPW(1)]

void BG_Init(void);

IOBUS8 FASTCALL BG_Read(DWORD adr);
IOBUS16 FASTCALL BG16_Read(DWORD adr);
IOBUS8 FASTCALL BGRam_Read(DWORD adr);
IOBUS16 FASTCALL BGRam16_Read(DWORD adr);
void FASTCALL BG_Write(DWORD adr, IOBUS8 data);
void FASTCALL BG16_Write(DWORD adr, IOBUS16 data);
void FASTCALL BGRam_Write(DWORD adr, IOBUS8 data);
void FASTCALL BGRam16_Write(DWORD adr, IOBUS16 data);

uint32_t BG_CalcLine(uint32_t v);
void BG_DrawLine(uint32_t v, uint_fast8_t opaq, uint_fast8_t gd);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_bg
