#ifndef _winx68k_tvram
#define _winx68k_tvram

#include "crtc.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	TextDrawWork[1024 * 1024];
extern	BYTE	TextDirtyLine[1024];

void _TVRAM_SetAllDirty(void);

void TVRAM_Init(void);
void TVRAM_Cleanup(void);

IOBUS8 FASTCALL TVRAM_Read(DWORD adr);
IOBUS16 FASTCALL TVRAM16_Read(DWORD adr);
void FASTCALL TVRAM_Write(DWORD adr, IOBUS8 data);
void FASTCALL TVRAM16_Write(DWORD adr, IOBUS16 data);

void FASTCALL TVRAM_RCUpdate(void);
void Text_DrawLine(uint32_t v, uint_fast8_t opaq);

#ifdef USES_ALLFLASH
#define	TVRAM_SetAllDirty()		CRTC.AllFlash = 1
#else	// USES_ALLFLASH
#define	TVRAM_SetAllDirty()		_TVRAM_SetAllDirty()
#endif	// USES_ALLFLASH

#define SetBGDirtyLine(y)		TextDirtyLine[(y) & 511] = 1
#define SetTextDirtyLine(y)		TextDirtyLine[(y) & 1023] = 1
#define SetTextDirtyLines(y, l)		do { unsigned i; for (i = 0; i < l; i++) { SetTextDirtyLine((y) + i); } } while (0 /*CONSTCOND*/)

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_tvram
