#ifndef _winx68k_scsi
#define _winx68k_scsi

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void SCSI_Init(void);
void SCSI_Cleanup(void);

IOBUS8 FASTCALL SCSI_Read(DWORD adr);
void FASTCALL SCSI_Write(DWORD adr, IOBUS8 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_scsi
