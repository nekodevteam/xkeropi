#ifndef _winx68k_irqh
#define _winx68k_irqh

typedef int32_t (FASTCALL * DMAHANDLER)(uint_fast8_t irq);

#ifndef USE_68KEM
#define VECT_BUSERR		255
#define VECT_ADRERR		254
#endif	// !USE_68KEM

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void IRQH_Init(void);
void IRQH_IRQCallBack(uint_fast8_t irq);
void IRQH_Int(uint_fast8_t irq, DMAHANDLER handler);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_irqh
