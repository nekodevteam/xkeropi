#ifndef _winx68k_sasi
#define _winx68k_sasi

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

#ifdef WIN68DEBUG
extern uint8_t hddtrace;
#endif	// WIN68DEBUG

void SASI_Init(void);
IOBUS8 FASTCALL SASI_Read(DWORD adr);
void FASTCALL SASI_Write(DWORD adr, IOBUS8 data);
int SASI_IsReady(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_sasi
