;-----------------------------------------------------------------------------
;   Memory & I/O Access Routine for WinX68k
;-----------------------------------------------------------------------------

	BITS 32

	SECTION .data

%ifidn __OUTPUT_FORMAT__, elf
%define	_BusErrFlag	BusErrFlag
%define	_BusErrAdr	BusErrAdr
%define	_MemByteAccess	MemByteAccess
%define	@dma_readmem24@4	dma_readmem24
%define	@dma_readmem24_word@4	dma_readmem24_word
%define	@dma_readmem24_dword@4	dma_readmem24_dword
%define	@dma_writemem24@8	dma_writemem24
%define	@dma_writemem24_word@8	dma_writemem24_word
%define	@dma_writemem24_dword@8	dma_writemem24_dword
%define	@cpu_setOPbase24@4	cpu_setOPbase24
%define	@Memory_SetSCSIMode@0	Memory_SetSCSIMode

%ifdef USE_FETCHMEM
%define	_FetchMem	FetchMem
%else
%define	_MEM		MEM
%define	_IPL		IPL
%define	_FONT		FONT
%define	_GVRAM		GVRAM
%define	_TVRAM		TVRAM
%define	_SRAM		SRAM
%define	_SCSIIPL	SCSIIPL
%endif
%endif

	extern	_BusErrFlag
	extern	_BusErrAdr
;	extern	_MemByteAccess
	extern	_OP_ROM

%ifdef USE_FETCHMEM
	extern	_FetchMem
%else
	extern	_MEM
	extern	_IPL
	extern	_FONT
	extern	_GVRAM
	extern	_TVRAM
	extern	_SRAM
	extern	_SCSIIPL
%endif

	SECTION .text

;-----------------------------------------------------------------------------

	extern	BusError@68kem
	extern	AdrError@68kem

; 宣言マクロ
%macro	ImportIO	1
%ifidn __OUTPUT_FORMAT__, elf
	%define	@%1_Read@4	%1_Read
	%define	@%1_Write@8	%1_Write
%endif
	extern	@%1_Read@4
	extern	@%1_Write@8
%endmacro

%macro	BindIO	2
	%define	@%1_Read@4	rm_%2
	%define	@%1_Write@8	wm_%2
%endmacro

	ImportIO ADPCM
	ImportIO BG
	ImportIO BGRam
	ImportIO CRTC
	ImportIO DMA
	ImportIO FDC
	ImportIO GVRAM
	ImportIO IOC
	ImportIO MFP
	ImportIO OPM
	ImportIO PIA
	ImportIO RTC
	ImportIO SASI
	ImportIO SCC
	ImportIO SCSI
	ImportIO SRAM
	ImportIO SysPort
	ImportIO TVRAM
	ImportIO VCtrl

%ifdef NO_MERCURY
	BindIO Mcry, buserr
%else	; NO_MERCURY
	ImportIO Mcry
%endif	; NO_MERCURY
%ifdef NO_MIDI
	BindIO MIDI, buserr
%else	; NO_MIDI
	ImportIO MIDI
%endif	; NO_MIDI
%ifdef NO_WINDRV
	BindIO WinDrv, nop
%else	; NO_WINDRV
	ImportIO WinDrv
%endif	; NO_WINDRV

; 公開I/F
	global  @cpu_readmem24@4
	global  @cpu_readmem24_word@4
	global  @cpu_readmem24_dword@4
	global  @cpu_writemem24@8
	global  @cpu_writemem24_word@8
	global  @cpu_writemem24_dword@8

	global  @dma_readmem24@4
	global  @dma_readmem24_word@4
	global  @dma_readmem24_dword@4
	global  @dma_writemem24@8
	global  @dma_writemem24_word@8
	global  @dma_writemem24_dword@8

	global  @cpu_setOPbase24@4

	global  @Memory_SetSCSIMode@0

;-----------------------------------------------------------------------------
;  Memory Write
;  後々のために、BusErrとAdrErrはReadMemとは別ルーチンを用意しておこう…

	ALIGN 4

@dma_writemem24@8:
;	mov	byte [_MemByteAccess], 1	; 神戸恋愛物語
	call	wm_main
;	mov	byte [_MemByteAccess], 0
	ret
@dma_writemem24_word@8:
	test	cl, 1			; 奇数バウンダリはアドレスエラー
	jnz	dmaw_jmpae
	mov	byte [WriteValue], dl	; 下位バイト退避
	shr	dx, 8			; 上位バイト書きこみ
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue]	; 下位バイト書きこみ
	call	wm_main
	ret
@dma_writemem24_dword@8:
	test	cl, 1			; 奇数バウンダリはアドレスエラー
	jnz	dmaw_jmpae
	mov	dword [WriteValue], edx	; 書きこみデータ退避
	shr	edx, 24
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue+2]
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue+1]
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue]
	call	wm_main
	ret
dmaw_jmpae:
	mov	byte [_BusErrFlag], 4
	ret


@cpu_writemem24@8:
;	mov	byte [_MemByteAccess], 1	; ZOUNDS
	call	wm_cnt			; バスエラーチェックはいらない
;	mov	byte [_MemByteAccess], 0
	cmp	byte [_BusErrFlag], 0
	jnz	wm_jmpbe
	ret
@cpu_writemem24_word@8:
	test	cl, 1			; 奇数バウンダリはアドレスエラー
	jnz	wm_jmpae
	mov	byte [WriteValue], dl	; 下位バイト退避
	shr	dx, 8			; 上位バイト書きこみ
	push	ecx
	call	wm_cnt			; バスエラーチェックはいらない
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue]	; 下位バイト書きこみ
	call	wm_main
	cmp	byte [_BusErrFlag], 0
	jnz	wm_jmpbe
	ret
@cpu_writemem24_dword@8:
	test	cl, 1			; 奇数バウンダリはアドレスエラー
	jnz	wm_jmpae
	mov	dword [WriteValue], edx	; 書きこみデータ退避
	shr	edx, 24
	push	ecx
	call	wm_cnt			; バスエラーチェックはいらない
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue+2]
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue+1]
	push	ecx
	call	wm_main
	pop	ecx
	inc	ecx
	mov	dl, byte [WriteValue]
	call	wm_main
	cmp	byte [_BusErrFlag], 0
	jnz	wm_jmpbe
	ret

wm_jmpae:
	jmp	AdrError@68kem
wm_jmpbe:
	mov	byte [_BusErrFlag], 0
	mov	ecx, [_BusErrAdr]
	jmp	BusError@68kem

wm_main:
	cmp	byte [_BusErrFlag], 0
	jz	wm_cnt
	ret				; 既にバスエラーが起こってたら何もしない（いちおー、ね…）
wm_cnt:
	and	edx, 0ffh
	and	ecx, 0ffffffh
	cmp	ecx, 0a00000h		; Main Memory 10Mb
;	cmp	ecx, 0c00000h		; Main Memory 12Mb
	jc	wm_ram
	cmp	ecx, 0c00000h		; 
	jc	wm_buserr
	cmp	ecx, 0e00000h		; Graphic VRAM
	jc	wm_gvram
	mov	eax, ecx
	shr	eax, 11
	and	eax, 03fch
	jmp	[MemWriteTable+eax]

wm_gvram:
	jmp	@GVRAM_Write@8

wm_ram:
	xor	ecx, byte 1
%ifdef USE_FETCHMEM
	mov	byte [_FetchMem + ecx], dl
%else
	mov	eax, dword [_MEM]
	mov	byte [ecx+eax], dl
%endif
	ret

wm_buserr:
	mov	byte [_BusErrFlag], 2
	mov	dword [_BusErrAdr], ecx
	ret

wm_nop:
	ret

;-----------------------------------------------------------------------------
;  Memory Read
;
	ALIGN 4

@dma_readmem24@4:
	call	rm_main
	ret
@dma_readmem24_word@4:
	test	cl, 1
	jnz	dmar_jmpae
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	push	eax
	call	rm_main
	pop	ecx
	mov	ah, cl
	ret
@dma_readmem24_dword@4:
	test	cl, 1
	jnz	dmar_jmpae
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+3], al
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+2], al
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+1], al
	call	rm_main
	mov	byte [RetValue], al
	mov	eax, dword [RetValue]
	ret
dmar_jmpae:
	mov	byte [_BusErrFlag], 3
	ret


@cpu_readmem24@4:
	call	rm_main
	cmp	byte [_BusErrFlag], 0
	jnz	rm_jmpbe
	ret
@cpu_readmem24_word@4:
	test	cl, 1
	jnz	rm_jmpae
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	push	ax
	call	rm_main
	pop	cx
	cmp	byte [_BusErrFlag], 0
	jnz	rm_jmpbe
	mov	ah, cl
	ret
@cpu_readmem24_dword@4:
	test	cl, 1
	jnz	rm_jmpae
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+3], al
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+2], al
	push	ecx
	call	rm_main
	pop	ecx
	inc	ecx
	mov	byte [RetValue+1], al
	call	rm_main
	cmp	byte [_BusErrFlag], 0
	jnz	rm_jmpbe
	mov	byte [RetValue], al
	mov	eax, dword [RetValue]
	ret

rm_jmpae:
	jmp	AdrError@68kem
rm_jmpbe:
	mov	byte [_BusErrFlag], 0
	mov	ecx, [_BusErrAdr]
	jmp	BusError@68kem

rm_main:
	and	ecx, 0ffffffh
	cmp	ecx, 0a00000h		; Main Memory 10Mb
;	cmp	ecx, 0c00000h		; Main Memory 12Mb
	jc	rm_ram
	cmp	ecx, 0c00000h		; 
	jc	rm_buserr
	cmp	ecx, 0e00000h		; Graphic VRAM
	jc	rm_gvram
	mov	eax, ecx
	shr	eax, 11
	and	eax, 03fch
	jmp	[MemReadTable+eax]

rm_gvram:
	jmp	@GVRAM_Read@4

rm_ram:
	xor	ecx, byte 1
%ifdef USE_FETCHMEM
	mov	al, byte [_FetchMem + ecx]
%else
	mov	eax, dword [_MEM]
	mov	al, byte [ecx+eax]
%endif
	ret
rm_font:
	and	ecx, 0fffffh
%ifdef USE_FETCHMEM
	mov	al, byte [_FetchMem + 0xf00000 + ecx]
%else
	mov	eax, dword [_FONT]
	mov	al, byte [ecx+eax]
%endif
	ret
rm_ipl:
	and	ecx, 03ffffh
	xor	ecx, byte 1
%ifdef USE_FETCHMEM
	mov	al, byte [_FetchMem + 0xfc0000 + ecx]
%else
	mov	eax, dword [_IPL]
	mov	al, byte [ecx+eax]
%endif
	ret

rm_nop:
	xor	al, al
;	mov	al, 0ffh		; Genocide2 → SysPortで対処
	ret

rm_buserr:
	mov	byte [_BusErrFlag], 1
	mov	dword [_BusErrAdr], ecx
	ret


;-----------------------------------------------------------------------------
;  Memory Operation Base Set
;  マクロにして68KEMに組み込む方が正解かなぁ…

@cpu_setOPbase24@4:
%ifdef USE_FETCHMEM
	mov	eax, _FetchMem
	mov	dword [_OP_ROM], eax
	ret
%else
	and	ecx, 0ffffffh
	mov	eax, ecx
	shr	eax, 20
	and	eax, 0fh
	jmp	[OPBaseTable+eax*4]
setop_mem:
	mov	eax, dword [_MEM]
	mov	dword [_OP_ROM], eax
	ret
setop_gvr:
	mov	eax, _GVRAM
	sub	eax, 0c00000h
	mov	dword [_OP_ROM], eax
	ret
setop_tvr:
	cmp	ecx, 0e80000h
	jc	setop_tvram
	cmp	ecx, 0ea0000h
	jc	setop_jmpbe
	cmp	ecx, 0ea2000h
	jc	setop_scsi
	cmp	ecx, 0ed0000h
	jc	setop_jmpbe
	cmp	ecx, 0ed4000h
	jnc	setop_jmpbe
	mov	eax, _SRAM
	sub	eax, 0ed0000h
	mov	dword [_OP_ROM], eax
	ret
setop_tvram:
	mov	eax, _TVRAM
	sub	eax, 0e00000h
	mov	dword [_OP_ROM], eax
	ret

setop_scsi:
	mov	eax, _SCSIIPL
	cmp	ecx, 0ea2000h
	jnc	setop_jmpbe
	sub	eax, 0ea0000h
	mov	dword [_OP_ROM], eax
	ret

setop_rom:
	cmp	ecx, 0fc0000h
	jc	setop_jmpbe
	mov	eax, dword [_IPL]
	sub	eax, 0fc0000h
	mov	dword [_OP_ROM], eax
	ret
setop_jmpbe:
	mov	byte [_BusErrFlag], 0
	mov	dword [_BusErrAdr], ecx
	jmp	BusError@68kem

	align	4
OPBaseTable:
		dd	setop_mem, setop_mem, setop_mem, setop_mem, setop_mem, setop_mem, setop_mem, setop_mem
		dd	setop_mem, setop_mem, setop_mem, setop_mem, setop_gvr, setop_gvr, setop_tvr, setop_rom
%endif

@Memory_SetSCSIMode@0:
	mov	eax, rm_buserr
	mov	edx, 380h
.lp:	mov	dword [MemReadTable + edx], eax
	add	edx, byte 4
	cmp	edx, 3c0h
	jc	short .lp
	ret


;-----------------------------------------------------------------------------
;  てーぶる類
;
	SECTION .data

	ALIGN 4

MemReadTable:
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4, @TVRAM_Read@4
		dd	@CRTC_Read@4, @VCtrl_Read@4, @DMA_Read@4, rm_nop, @MFP_Read@4, @RTC_Read@4, rm_nop, @SysPort_Read@4
		dd	@OPM_Read@4, @ADPCM_Read@4, @FDC_Read@4, @SASI_Read@4, @SCC_Read@4, @PIA_Read@4, @IOC_Read@4, @WinDrv_Read@4;rm_nop
		dd	@SCSI_Read@4, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, @MIDI_Read@4
		dd	@BG_Read@4, @BG_Read@4, @BG_Read@4, @BG_Read@4, @BGRam_Read@4, @BGRam_Read@4, @BGRam_Read@4, @BGRam_Read@4
		dd	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, @Mcry_Read@4, rm_buserr
		dd	@SRAM_Read@4, @SRAM_Read@4, rm_nop, rm_nop, rm_nop, rm_nop, rm_nop, rm_nop
		dd	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dd	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr

		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font
		dd	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl
		dd	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl
		dd	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl
		dd	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl


MemWriteTable:
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8, @TVRAM_Write@8
		dd	@CRTC_Write@8, @VCtrl_Write@8, @DMA_Write@8, wm_nop, @MFP_Write@8, @RTC_Write@8, wm_nop, @SysPort_Write@8
		dd	@OPM_Write@8, @ADPCM_Write@8, @FDC_Write@8, @SASI_Write@8, @SCC_Write@8, @PIA_Write@8, @IOC_Write@8, @WinDrv_Write@8;wm_nop
		dd	@SCSI_Write@8, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, @MIDI_Write@8
		dd	@BG_Write@8, wm_nop, wm_nop, wm_nop, @BGRam_Write@8, @BGRam_Write@8, @BGRam_Write@8, @BGRam_Write@8
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, @Mcry_Write@8, wm_buserr
		dd	@SRAM_Write@8, @SRAM_Write@8, wm_nop, wm_nop, wm_nop, wm_nop, wm_nop, wm_nop
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
; ROMエリアへの書きこみは全てバスエラー
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dd	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr


;-----------------------------------------------------------------------------
;  その他のでーた
;
RetValue:	dd	0
WriteValue:	dd	0
