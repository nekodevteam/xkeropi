﻿// ---------------------------------------------------------------------------------------
//  SRAM.C - SRAM (16kb) 領域
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	"sram.h"
#include	"memory.h"
#include	"sysport.h"
#include	"fileio.h"
#include	"prop.h"
#if defined(_WIN32) && !defined(_WIN32_WCE)
#include	"winx68k.h"
#endif	// defined(_WIN32) && !defined(_WIN32_WCE)

	static const TCHAR SRAMFILE[] = TEXT("sram.dat");


// -----------------------------------------------------------------------
//   役に立たないうぃるすチェック
// -----------------------------------------------------------------------
void SRAM_VirusCheck(void)
{
	if (!Config.SRAMWarning) return;				// Warning発生モードでなければ帰る

	if ( (Memory_ReadD(0xed3f60)==0x60000002)
	   &&(Memory_ReadD(0xed0010)==0x00ed3f60) )		// 特定うぃるすにしか効かないよ～
	{
#if defined(_WIN32) && !defined(_WIN32_WCE)
		const int ret = MessageBox(hWndMain,
			TEXT("このSRAMデータはウィルスに感染している可能性があります。\n該当個所のクリーンアップを行いますか？"),
			TEXT("けろぴーからの警告"), MB_ICONWARNING | MB_YESNO);
		if (ret == IDYES)
		{
			int i;
			for (i=0x3c00; i<0x4000; i++)
				SRAM[i] = 0;
			SRAM[0x11] = 0x00;
			SRAM[0x10] = 0xed;
			SRAM[0x13] = 0x01;
			SRAM[0x12] = 0x00;
			SRAM[0x19] = 0x00;
		}
#endif	// defined(_WIN32) && !defined(_WIN32_WCE)
		SRAM_Cleanup();
		SRAM_Init();			// Virusクリーンアップ後のデータを書き込んでおく
	}
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void SRAM_Init(void)
{
	FILEH fp;

	memset(SRAM, 0, 0x4000);

	fp = File_OpenCurDir(SRAMFILE);
	if (fp)
	{
		File_Read(fp, SRAM, 0x4000);
		File_Close(fp);
		if (ADRSWAPW(0) != 0)
		{
			unsigned int i;
			for (i = 0; i < 0x4000; i += 2)
			{
				const uint_fast8_t tmp = SRAM[i];
				SRAM[i] = SRAM[i+1];
				SRAM[i+1] = tmp;
			}
		}
	}
}


// -----------------------------------------------------------------------
//   撤収～
// -----------------------------------------------------------------------
void SRAM_Cleanup(void)
{
	FILEH fp;

	if (ADRSWAPW(0) != 0)
	{
		unsigned int i;
		for (i = 0; i < 0x4000; i += 2)
		{
			const uint_fast8_t tmp = SRAM[i];
			SRAM[i] = SRAM[i+1];
			SRAM[i+1] = tmp;
		}
	}

	fp = File_OpenCurDir(SRAMFILE);
	if (!fp)
	{
#ifdef _WIN32
		fp = File_CreateCurDir(SRAMFILE);
#else	// _WIN32
		fp = File_CreateCurDir(SRAMFILE, FTYPE_SRAM);
#endif	// _WIN32
	}
	if (fp)
	{
		File_Write(fp, SRAM, 0x4000);
		File_Close(fp);
	}
}


// -----------------------------------------------------------------------
//   りーど
// -----------------------------------------------------------------------
IOBUS8 FASTCALL SRAM_Read(DWORD adr)
{
	return SRAM[ADRSWAPW(adr) - 0xed0000];
}

IOBUS16 FASTCALL SRAM16_Read(DWORD adr)
{
	return *(uint16_t *)(&SRAM[adr - 0xed0000]);
}


// -----------------------------------------------------------------------
//   らいと
// -----------------------------------------------------------------------
void FASTCALL SRAM_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);

	if (SysPort[5] != 0x31) return;

	{
#if defined(_WIN32) && !defined(_WIN32_WCE)
		if ((adr == 0xed0018) && (data == 0xb0))	// SRAM起動への切り替え（簡単なウィルス対策）
		{
			if (Config.SRAMWarning)		// Warning発生モード（デフォルト）
			{
				const int ret = MessageBox(hWndMain,
					TEXT("SRAMブートに切り替えようとしています。\nウィルスの危険がない事を確認してください。\nSRAMブートに切り替え、継続しますか？"),
					TEXT("けろぴーからの警告"), MB_ICONWARNING | MB_YESNO);
				if (ret != IDYES)
				{
					data = 0;	// STDブートにする
				}
			}
		}
#endif	// defined(_WIN32) && !defined(_WIN32_WCE)
		SRAM[ADRSWAPW(adr) - 0xed0000] = data;
	}
}

void FASTCALL SRAM16_Write(DWORD adr, IOBUS16 data)
{
	assert(data < 0x10000);

	if (SysPort[5] != 0x31) return;

	{
#if defined(_WIN32) && !defined(_WIN32_WCE)
		if ((adr == 0xed0018) && ((data >> 8) == 0xb0))	// SRAM起動への切り替え（簡単なウィルス対策）
		{
			if (Config.SRAMWarning)		// Warning発生モード（デフォルト）
			{
				const int ret = MessageBox(hWndMain,
					TEXT("SRAMブートに切り替えようとしています。\nウィルスの危険がない事を確認してください。\nSRAMブートに切り替え、継続しますか？"),
					TEXT("けろぴーからの警告"), MB_ICONWARNING | MB_YESNO);
				if (ret != IDYES)
				{
					data = 0;	// STDブートにする
				}
			}
		}
#endif	// defined(_WIN32) && !defined(_WIN32_WCE)
		*(uint16_t *)(&SRAM[adr - 0xed0000]) = data;
	}
}
