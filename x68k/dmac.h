#ifndef _winx68k_dmac
#define _winx68k_dmac

#include "x68k.h"

struct DmaChReg
{
	BYTE CSR;		// 00
	BYTE CER;
	BYTE dmy0[2];
	BYTE DCR;		// 04
	BYTE OCR;
	BYTE SCR;
	BYTE CCR;
	BYTE dmy1[2];	// 08
	WORD MTC;
	DWORD MAR;		// 0C
	BYTE dmy2[4];	// 10
	DWORD DAR;		// 14
	BYTE dmy3[2];	// 18
	WORD BTC;
	DWORD BAR;		// 1C
	BYTE dmy4[5];	// 20
	BYTE NIV;
	BYTE dmy5;
	BYTE EIV;
	BYTE dmy6;		// 28
	BYTE MFC;
	BYTE dmy7[3];
	BYTE CPR;
	BYTE dmy8[3];
	BYTE DFC;
	BYTE dmy9[7];
	BYTE BFC;
	BYTE dmya[5];
	BYTE GCR;
};

struct DmaCh
{
	struct DmaChReg r;
	uint8_t bit;
	uint8_t ready;
	int (*IsDeviceReady)(void);
};
typedef struct DmaCh DMACH;

struct DmaCtrl
{
	int IntCH;
	int LastInt;
	struct DmaCh ch[4];
};

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern struct DmaCtrl DMA;

#ifdef WIN68DEBUG
extern uint8_t dmatrace;
#endif	// WIN68DEBUG

IOBUS8 FASTCALL DMA_Read(DWORD adr);
void FASTCALL DMA_Write(DWORD adr, IOBUS8 data);

int FASTCALL DMA_Process(DMACH *dmach);
void DMA_Init(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#define DMA_Exec(chnum)		DMA_Process(&DMA.ch[(chnum)])

#endif //_winx68k_dmac
