#ifndef _winx68k_pia
#define _winx68k_pia

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void PIA_Init(void);
IOBUS8 FASTCALL PIA_Read(DWORD adr);
void FASTCALL PIA_Write(DWORD adr, IOBUS8 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_pia
