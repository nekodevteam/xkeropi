#ifndef _winx68k_fdc
#define _winx68k_fdc

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void FDC_Init(void);
IOBUS8 FASTCALL FDC_Read(DWORD adr);
void FASTCALL FDC_Write(DWORD adr, IOBUS8 data);
void FDC_SetForceReady(int n);
int FDC_IsDataReady(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_fdc
