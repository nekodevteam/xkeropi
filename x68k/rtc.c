﻿// ---------------------------------------------------------------------------------------
//  RTC.C - RTC (Real Time Clock / RICOH RP5C15)
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "rtc.h"
#ifndef _WIN32
#include <time.h>
#endif	// !_WIN32
#include "mfp.h"

static const uint8_t RTC_Bank = 0;

struct tagRtc
{
	uint32_t Timer1;
	uint32_t Timer16;
	uint8_t Regs[2][16];
};

static struct tagRtc RTC;

// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void RTC_Init(void)
{
	ZeroMemory(&RTC, sizeof(RTC));
//	RTC.Regs[0][13] = 0;
//	RTC.Regs[0][14] = 0;
	RTC.Regs[0][15] = 0x0c;
}


// -----------------------------------------------------------------------
//   とけいのりーど
// -----------------------------------------------------------------------
IOBUS8 FASTCALL RTC_Read(DWORD adr)
{
	if (!(adr&1)) return 0;
	if (RTC_Bank == 0)
	{
		const uint_fast8_t reg = (adr >> 1) & 0x0f;
		if (reg < 13)
		{
#ifdef _WIN32
			SYSTEMTIME t;
			GetLocalTime(&t);
			switch (reg)
			{
				case 0: return (t.wSecond)%10;
				case 1: return (t.wSecond)/10;
				case 2: return (t.wMinute)%10;
				case 3: return (t.wMinute)/10;
				case 4: return (t.wHour)%10;
				case 5: return (t.wHour)/10;
				case 6: return (BYTE)(t.wDayOfWeek);
				case 7: return (t.wDay)%10;
				case 8: return (t.wDay)/10;
				case 9: return (t.wMonth)%10;
				case 10: return (t.wMonth)/10;
				case 11: return ((t.wYear)-1980)%10;
				case 12: return (((t.wYear)-1980)/10)&15;
			}
#else	// WIN32
			time_t t = time(NULL);
			struct tm *tm = localtime(&t);
			switch (reg)
			{
				case 0: return (tm->tm_sec)%10;
				case 1: return (tm->tm_sec)/10;
				case 2: return (tm->tm_min)%10;
				case 3: return (tm->tm_min)/10;
				case 4: return (tm->tm_hour)%10;
				case 5: return (tm->tm_hour)/10;
				case 6: return (BYTE)(tm->tm_wday);
				case 7: return (tm->tm_mday)%10;
				case 8: return (tm->tm_mday)/10;
				case 9: return (tm->tm_mon+1)%10;
				case 10: return (tm->tm_mon+1)/10;
				case 11: return ((tm->tm_year)-80)%10;
				case 12: return (((tm->tm_year)-80)/10)&0xf;
			}
#endif	// _WIN32
		}
		return RTC.Regs[0][reg];
	}
	else
	{
		const uint_fast8_t reg = (adr >> 1) & 0x0f;
		if (reg == 11)
		{
#ifdef _WIN32
			SYSTEMTIME t;
			GetLocalTime(&t);
			return ((t.wYear)-1980)%4;
#else	// _WIN32
			time_t t = time(NULL);
			struct tm *tm = localtime(&t);
			return ((tm->tm_year)-80)%4;
#endif	// _WIN32
		}
		else if (reg == 13)
		{
			return (RTC.Regs[1][13] | 1);
		}
		else
		{
			return RTC.Regs[1][reg];
		}
	}
}


// -----------------------------------------------------------------------
//   らいと
// -----------------------------------------------------------------------
void FASTCALL RTC_Write(DWORD adr, IOBUS8 data)
{
	adr &= 0x1f;
	if ( adr==0x01 ) {
//		RTC.Timer1  = 0;
//		RTC.Timer16 = 0;
	} else if ( adr==0x1b ) {
		RTC.Regs[0][13] = RTC.Regs[1][13] = data&0x0c;		// Alarm/Timer Enable制御
	} else if ( adr==0x1f ) {
		RTC.Regs[0][15] = RTC.Regs[1][15] = data&0x0c;		// Alarm端子出力制御
	}
}


void RTC_Timer(int clock)
{
	RTC.Timer16 += clock;
	if (RTC.Timer16 >= 625000)
	{
		RTC.Timer16 -= 625000;
		RTC.Timer1++;
		if (((RTC.Timer1 & 15) == 0) && (!(RTC.Regs[0][15] & 8)))
		{
			MFP_Int(15);
		}
		if (!(RTC.Regs[0][15]&4))
		{
			MFP_Int(15);
		}
	}
}
