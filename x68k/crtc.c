﻿// ---------------------------------------------------------------------------------------
//  CRTC.C - CRT Controller / Video Controller
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "crtc.h"
#include "bg.h"
#include "gvram.h"
#include "memory.h"
#include "palette.h"
#include "tvram.h"
#include "windraw.h"
#include "x68k.inl"

static const WORD FastClearMask[16] = {
	0xffff, 0xfff0, 0xff0f, 0xff00, 0xf0ff, 0xf0f0, 0xf00f, 0xf000,
	0x0fff, 0x0ff0, 0x0f0f, 0x0f00, 0x00ff, 0x00f0, 0x000f, 0x0000
};

	struct tagCrtc	CRTC;

#if 0
static	DWORD	CRTC_FastClrLine = 0;
#endif	// 0

#ifdef WIN68DEBUG
	uint8_t		Debug_Text = 1, Debug_Grp = 1, Debug_Sp = 1;
#else	// WIN68DEBUG
const	uint8_t		Debug_Text = 1, Debug_Grp = 1, Debug_Sp = 1;
#endif	// WIN68DEBUG

	CRTCLINEBUF CRTCLineBuf;

#define	CRTC_RCFlag	CRTC.RCFlag
#define	CRTC_R04	*(uint16_t *)(&CRTC_Regs[ 4 * 2])
#define	CRTC_R09	*(uint16_t *)(&CRTC_Regs[ 9 * 2])
#define	CRTC_R22H	CRTC_Regs[22 * 2 + ADRSWAPW(0)]

#define	VCReg0H		VCReg[0][ADRSWAPW(0)]
#define	VCReg0L		VCReg[0][ADRSWAPW(1)]
#define	VCReg1H		VCReg[1][ADRSWAPW(0)]
#define	VCReg1L		VCReg[1][ADRSWAPW(1)]
#define	VCReg2H		VCReg[2][ADRSWAPW(0)]
#define	VCReg2L		VCReg[2][ADRSWAPW(1)]

// -----------------------------------------------------------------------
//   らすたーこぴー
// -----------------------------------------------------------------------
static void CRTC_RasterCopy(void)
{
	uint32_t line = CRTC_R22L << 2;
	const uint8_t* src = &TVRAM[CRTC_R22H << 9];
	uint8_t* dst = &TVRAM[CRTC_R22L << 9];
	if (src == dst)
	{
		return;
	}

	if (CRTC_R21L & (1 << 0))
	{
		memcpy(&dst[0x20000 * 0], &src[0x20000 * 0], 512);
	}
	if (CRTC_R21L & (1 << 1))
	{
		memcpy(&dst[0x20000 * 1], &src[0x20000 * 1], 512);
	}
	if (CRTC_R21L & (1 << 2))
	{
		memcpy(&dst[0x20000 * 2], &src[0x20000 * 2], 512);
	}
	if (CRTC_R21L & (1 << 3))
	{
		memcpy(&dst[0x20000 * 3], &src[0x20000 * 3], 512);
	}

	line = line - TextScroll->Y;
	SetTextDirtyLines(line, 4);

	TVRAM_RCUpdate();
}


// -----------------------------------------------------------------------
//   びでおこんとろーるれじすた
// -----------------------------------------------------------------------
// Reg0の色モードは、ぱっと見CRTCと同じだけど役割違うので注意。
// CRTCはGVRAMへのアクセス方法（メモリマップ上での見え方）が変わるのに対し、
// VCtrlは、GVRAM→画面の展開方法を制御する。
// つまり、アクセス方法（CRTC）は16bitモードで、表示は256色モードってな使い
// 方も許されるのれす。
// コットン起動時やYs（電波版）OPなどで使われてまふ。

IOBUS8 FASTCALL VCtrl_Read(DWORD adr)
{
	adr -= 0xe82000;
	if (adr < 0x400)
	{
		return Pal_Regs[ADRSWAPW(adr)];
	}
	else
	{
		const uint_fast8_t offset = ADRSWAPW(adr & 1);
		switch (adr & (~0xff))
		{
			case 0x400:
				return VCReg[0][offset];

			case 0x500:
				return VCReg[1][offset];

			case 0x600:
				return VCReg[2][offset];

			default:
				return 0;
		}
	}
}


void FASTCALL VCtrl_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);

	adr -= 0xe82000;
	if (adr < 0x400)
	{
		adr = ADRSWAPW(adr);
		if (Pal_Regs[adr] != data)
		{
			const unsigned index = adr >> 1;
			uint_fast16_t pal;
			Pal_Regs[adr] = data;
			pal = ((uint16_t *)Pal_Regs)[index];
			GrphTextPal.Pal[index] = PAL16(pal);
			TVRAM_SetAllDirty();
		}
	}
	else
	{
		const uint_fast8_t offset = ADRSWAPW(adr & 1);
		switch (adr & (~0xfe))
		{
			case 0x401:
				if (VCReg[0][offset] != data)
				{
					VCReg[0][offset] = data;
					TVRAM_SetAllDirty();
				}
				break;

			case 0x500:
			case 0x501:
				if (VCReg[1][offset] != data)
				{
					VCReg[1][offset] = data;
					TVRAM_SetAllDirty();
				}
				break;

			case 0x600:
			case 0x601:
				if (VCReg[2][offset] != data)
				{
					VCReg[2][offset] = data;
					TVRAM_SetAllDirty();
				}
				break;
		}
/*{
FILE* fp = fopen("_vreg.txt", "a");
fprintf(fp, "VREG 1=$%04X 2=$%04X 3=$%04X\n", VCReg0, VCReg1, VCReg2);
fclose(fp);
}*/
	}
}

// -----------------------------------------------------------------------
//   CRTCれじすた
// -----------------------------------------------------------------------
// レジスタアクセスのコードが汚い ^^;

void CRTC_Init(void)
{
	if (!CRTC.Reseted)
	{
		CRTC.Reseted = 1;
		TextDotX = 768;
		TextDotY = 512;
		CRTC_VStep = 2;
		HSYNC_CLK = 324;
		VLINE_TOTAL = 567;
	}
	ZeroMemory(&CRTC_Regs, sizeof(CRTC_Regs));

	BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分

	CRTC_UpdateShift();
	CRTC_UpdateIntLine();
	CRTC_UpdateVDisp();
}


IOBUS8 FASTCALL CRTC_Read(DWORD adr)
{
	IOBUS8 ret;
	adr -= 0xe80000;
	if ((adr & (~3)) == 0x28)
	{
		return CRTC_Regs[ADRSWAPW(adr)];
	}
	else if (adr == 0x0481)
	{
// FastClearの注意点：
// FastClrビットに1を書き込むと、その時点ではReadBackしても1は見えない。
// 1書き込み後の最初の垂直帰線期間で1が立ち、消去を開始する。
// 1垂直同期期間で消去がおわり、0に戻る……らしひ（PITAPAT）
		if (CRTC_FastClr)
			ret = CRTC_Mode | 0x02;
		else
			ret = CRTC_Mode & 0xfd;

		return ret;
	}
	else
	{
		return 0x00;
	}
}

INLINE void UpdateTextX()
{
	TextDotX = (CRTC_HEND - CRTC_HSTART) * 8;
	if (TextDotX > LINEBUFSIZE)
	{
		TextDotX = LINEBUFSIZE;
	}
}

INLINE void UpdateTextY()
{
	TextDotY = CRTC_VEND - CRTC_VSTART;
	if ((CRTC_R20L & 0x14) == 0x10)
	{
		TextDotY /= 2;
		CRTC_VStep = 1;
	}
	else if ((CRTC_R20L & 0x14) == 0x04)
	{
		TextDotY *= 2;
		CRTC_VStep = 4;
	}
	else
	{
		CRTC_VStep = 2;
	}
}

INLINE void UpdateReg(DWORD adr)
{
	switch (adr)
	{
		case 0x04:
		case 0x05:
			UpdateTextX();
			BG_HAdjust = ((int32_t)SM_HDSP-(CRTC_HSTART+4))*8;				// 水平方向は解像度による1/2はいらない？（Tetris）
			WinDraw_ChangeSize();
			break;
		case 0x06:
		case 0x07:
			UpdateTextX();
			WinDraw_ChangeSize();
			break;
		case 0x08:
		case 0x09:
			VLINE_TOTAL = CRTC_R04;
			HSYNC_CLK = ((CRTC_R20L & 0x10) ? VSYNC_HIGH : VSYNC_NORM) / VLINE_TOTAL;
			CRTC_UpdateVDisp();
			break;
		case 0x0c:
		case 0x0d:
			BG_VAdjust = ((int32_t)SM_VDSP-CRTC_VSTART)/((SM_RESO&4)?1:2);	// BGとその他がずれてる時の差分
			UpdateTextY();
			BG_UpdateVAdjust();
			CRTC_UpdateVDisp();
			WinDraw_ChangeSize();
			break;
		case 0x0e:
		case 0x0f:
			UpdateTextY();
			CRTC_UpdateVDisp();
			WinDraw_ChangeSize();
			break;
		case 0x12:
		case 0x13:
			CRTC_IntLine = CRTC_R09 & 1023;
			CRTC_UpdateIntLine();
			break;
		case 0x28:
			break;
		case 0x29:
			HSYNC_CLK = ((CRTC_R20L & 0x10) ? VSYNC_HIGH : VSYNC_NORM) / VLINE_TOTAL;
			UpdateTextY();
			CRTC_UpdateShift();
			WinDraw_ChangeSize();
			break;
		case 0x2a:
		case 0x2b:
			break;
		case 0x2c:				// CRTC動作ポートのラスタコピーをONにしておいて（しておいたまま）、
		case 0x2d:				// Src/Dstだけ次々変えていくのも許されるらしい（ドラキュラとか）
			CRTC_RCFlag[adr - 0x2c] = 1;	// Dst変更後に実行される？
			if ((CRTC_Mode&8)&&/*(CRTC_RCFlag[0])&&*/(CRTC_RCFlag[1]))
			{
				CRTC_RasterCopy();
				CRTC_RCFlag[0] = 0;
				CRTC_RCFlag[1] = 0;
			}
			break;
	}
}

INLINE void SetMode(uint_fast8_t data)
{
	CRTC_Mode = (data | (CRTC_Mode&2));
	if (CRTC_Mode & 8)
	{				// Raster Copy
		CRTC_RasterCopy();
		CRTC_RCFlag[0] = 0;
		CRTC_RCFlag[1] = 0;
	}
	if (CRTC_Mode&2)		// 高速クリア
	{
#if 0
		CRTC_FastClrLine = vline;
#endif	// 0
					// この時点のマスクが有効らしい（クォース）
		CRTC_FastClrMask = FastClearMask[CRTC_R21L & 15];
/*{
FILE *fp;
fp=fopen("_crtc.txt", "a");
fprintf(fp, "FastClr\n");
fclose(fp);
}*/
	}
}

void FASTCALL CRTC_Write(DWORD adr, IOBUS8 data)
{
	assert(data < 0x100);
	adr -= 0xe80000;
	if (adr < 0x0030)
	{
		if (CRTC_Regs[ADRSWAPW(adr)] == data) return;
		CRTC_Regs[ADRSWAPW(adr)] = data;
		TVRAM_SetAllDirty();
		UpdateReg(adr);
	}
	else if (adr==0x0481)
	{					// CRTC動作ポート
		SetMode(data);
	}
}

void FASTCALL CrtVC16_Write(DWORD adr, IOBUS16 data)
{
	assert(data < 0x10000);
	adr -= 0xe80000;
	if (adr < 0x2000)
	{
		if (adr < 0x0030)
		{
			if (*(uint16_t *)(&CRTC_Regs[adr]) == data) return;
			*(uint16_t *)(&CRTC_Regs[adr]) = data;
			TVRAM_SetAllDirty();
			UpdateReg(adr + 1);
		}
		else if (adr==0x0480)
		{					// CRTC動作ポート
			SetMode(data & 0xff);
		}
	}
	else
	{
		adr -= 0x2000;
		if (adr < 0x400)
		{
			if (*(uint16_t *)(&Pal_Regs[adr]) != data)
			{
				*(uint16_t *)(&Pal_Regs[adr]) = data;
				GrphTextPal.Pal[adr >> 1] = PAL16(data);
				TVRAM_SetAllDirty();
			}
		}
		else
		{
			const unsigned index = (adr - 0x400) >> 8;
			if (index < 3)
			{
				if (*(uint16_t *)(&VCReg[index]) != data)
				{
					*(uint16_t *)(&VCReg[index]) = data;
					TVRAM_SetAllDirty();
				}
			}
		}
	}
}


#ifdef USE_ASM
EXTERNC void CRTC_DrawGrpLine(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawGrpLineNonSP(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawTextLine(uint16_t* dst, uint_fast8_t opaq, uint_fast8_t td);
EXTERNC void CRTC_DrawTextLineTR(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawTextLineTR2(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawBGLine(uint16_t* dst, uint_fast8_t opaq, uint_fast8_t td);
EXTERNC void CRTC_DrawBGLineTR(uint16_t* dst, uint_fast8_t opaq);
EXTERNC void CRTC_DrawPriLine(uint16_t* dst);
EXTERNC void CRTC_DrawTRLine(uint16_t* dst);
#else	// USE_ASM

INLINE void
CopyMem16(uint16_t* q, const uint16_t* p, uint32_t size8)
{
	uint32_t i;
	for (i = 0; i < size8; i += 8)
	{
		*(uint32_t *)(&q[i + 0]) = *(const uint32_t *)(&p[i + 0]);
		*(uint32_t *)(&q[i + 2]) = *(const uint32_t *)(&p[i + 2]);
		*(uint32_t *)(&q[i + 4]) = *(const uint32_t *)(&p[i + 4]);
		*(uint32_t *)(&q[i + 6]) = *(const uint32_t *)(&p[i + 6]);
	}
}

INLINE void
CopyMem16ne(uint16_t* q, const uint16_t* p, uint32_t size8)
{
	uint32_t i;
	for (i = 0; i < size8; i += 2)
	{
		const uint32_t v = *(const uint32_t *)(&p[i + 0]);
		if ((v & 0xffff) != 0)
		{
			q[i + 0] = (uint16_t)(v & 0xffff);
		}
		if ((v >> 16) != 0)
		{
			q[i + 1] = (uint16_t)(v >> 16);
		}
	}
}


INLINE void CRTC_DrawGrpLine(uint16_t* pLine, uint_fast8_t opaq)
{
	if (opaq)
	{
		CopyMem16(pLine, Grp_LineBuf, TextDotX);
	}
	else
	{
		CopyMem16ne(pLine, Grp_LineBuf, TextDotX);
	}
}

INLINE void CRTC_DrawGrpLineNonSP(uint16_t* pLine, uint_fast8_t opaq)
{
	if (opaq)
	{
		CopyMem16(pLine, Grp_LineBufSP2, TextDotX);
	}
	else
	{
		CopyMem16ne(pLine, Grp_LineBufSP2, TextDotX);
	}
}

INLINE void CRTC_DrawTextLine(uint16_t* pLine, uint_fast8_t opaq, uint_fast8_t td)
{
	unsigned int i;
	if (opaq)
	{
		CopyMem16(pLine, &BG_LineBuf[16], TextDotX);
	}
	else
	{
		if (td)
		{
			for (i = 0; i < TextDotX; i++)
			{
				if (Text_TrFlag[i + 16] & 1)
				{
					const uint16_t w = BG_LineBuf[i + 16];
					if (w != 0)
					{
						pLine[i] = w;
					}
				}
			}
		}
		else
		{
			CopyMem16ne(pLine, &BG_LineBuf[16], TextDotX);
		}
	}
}

INLINE void CRTC_DrawTextLineTR(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = 0;
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				v = BG_LineBuf[i + 16];
				if (v & Ibit)
				{
					w += Pal_Ix2;
				}
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			else if (Text_TrFlag[i + 16] & 1)
			{
				v = BG_LineBuf[i + 16];
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 1)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawTextLineTR2(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = 0;
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				v = BG_LineBuf[i + 16];
				if (v & Ibit)
				{
					w += Pal_Ix2;
				}
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			else if (Text_TrFlag[i + 16] & 1)
			{
				v = BG_LineBuf[i + 16];
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 1)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawBGLine(uint16_t* pLine, uint_fast8_t opaq, uint_fast8_t td)
{
	unsigned int i;
	if (opaq)
	{
		CopyMem16(pLine, &BG_LineBuf[16], TextDotX);
	}
	else
	{
		if (td)
		{
			for (i = 0; i < TextDotX; i++)
			{
				if (Text_TrFlag[i + 16] & 2)
				{
					const uint16_t w = BG_LineBuf[i + 16];
					if (w != 0)
					{
						pLine[i] = w;
					}
				}
			}
		}
		else
		{
			CopyMem16ne(pLine, &BG_LineBuf[16], TextDotX);
		}
	}
}

INLINE void CRTC_DrawBGLineTR(uint16_t* pLine, uint_fast8_t opaq)
{
	unsigned int i;
	if (opaq)
	{
		for (i = 0; i < TextDotX; i++)
		{
			uint32_t v = BG_LineBuf[i + 16];
			uint16_t w = Grp_LineBufSP[i];
			if (w != 0)
			{
				w &= Pal_HalfMask;
				if (v & Ibit)
					w += Pal_Ix2;
				v &= Pal_HalfMask;
				v += w;
				v >>= 1;
			}
			pLine[i] = (uint16_t)v;
		}
	}
	else
	{
		for (i = 0; i < TextDotX; i++)
		{
			if (Text_TrFlag[i + 16] & 2)
			{
				uint32_t v = BG_LineBuf[i + 16];
				if (v != 0)
				{
					uint16_t w = Grp_LineBufSP[i];
					if (w != 0)
					{
						w &= Pal_HalfMask;
						if (v & Ibit)
						{
							w += Pal_Ix2;
						}
						v &= Pal_HalfMask;
						v += w;
						v >>= 1;
					}
					pLine[i] = (uint16_t)v;
				}
			}
		}
	}
}

INLINE void CRTC_DrawPriLine(uint16_t* pLine)
{
	unsigned int i;
	for (i = 0; i < TextDotX; i++)
	{
		const uint16_t w = Grp_LineBufSP[i];
		 if (w != 0)
		{
			pLine[i] = w;
		}
	}
}

INLINE void CRTC_DrawTRLine(uint16_t* pLine)
{
	unsigned int i;
	for (i = 0; i < TextDotX; i++)
	{
		uint16_t w = Grp_LineBufSP[i];
		if (w != 0)
		{
			uint32_t v = pLine[i];
			w &= Pal_HalfMask;
			if (v & Ibit)
			{
				w += Pal_Ix2;
			}
			v &= Pal_HalfMask;
			v += w;
			v >>= 1;
			pLine[i] = (uint16_t)v;
		}
	}
}
#endif	// USE_ASM

INLINE void
ZeroMem8(uint8_t* p, uint32_t size8)
{
	uint32_t i;
	for (i = 0; i < size8; i += 8)
	{
		*(uint32_t *)(&p[i + 0]) = 0;
		*(uint32_t *)(&p[i + 4]) = 0;
	}
}

INLINE void
FillMem16(uint16_t* p, uint_fast16_t v, uint32_t size8)
{
	uint32_t i;
	const uint32_t v2 = v + (v << 16);
	for (i = 0; i < size8; i += 8)
	{
		*(uint32_t *)(&p[i + 0]) = v2;
		*(uint32_t *)(&p[i + 2]) = v2;
		*(uint32_t *)(&p[i + 4]) = v2;
		*(uint32_t *)(&p[i + 6]) = v2;
	}
}

void CRTC_DrawLine(uint16_t* dst, uint32_t v)
{
	uint_fast8_t opaq, ton=0, gon=0, bgon=0, tron=0, pron=0, tdrawed=0/*, gdrawed=0*/;

	if (Debug_Grp)
	{
	switch(VCReg0L&3)
	{
	case 0:					// 16 colors
		if (VCReg0L&4)		// 1024dot
		{
			if (VCReg2L&0x10)
			{
				if ( (VCReg2H&0x14)==0x14 )
				{
					Grp_DrawLine4hSP(v);
					pron = tron = 1;
				}
				else
				{
					Grp_DrawLine4h(v);
					gon=1;
				}
			}
		}
		else				// 512dot
		{
			if ( (VCReg2H&0x10)&&(VCReg2L&1) )
			{
				Grp_DrawLine4SP(v, (VCReg1L   )&3/*, 1*/);			// 半透明の下準備
				pron = tron = 1;
			}
			opaq = 1;
			if (VCReg2L&8)
			{
				Grp_DrawLine4(v, (VCReg1L>>6)&3, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2L&4)
			{
				Grp_DrawLine4(v, (VCReg1L>>4)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2L&2)
			{
				if ( ((VCReg2H&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine4TR(v, (VCReg1L>>2)&3, opaq);
				else
					Grp_DrawLine4(v, (VCReg1L>>2)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2L&1)
			{
//				if ( (VCReg2H&0x1e)==0x1e )
//				{
//					Grp_DrawLine4SP(v, (VCReg1L   )&3, opaq);
//					tron = pron = 1;
//				}
//				else
				if ( (VCReg2H&0x14)!=0x14 )
				{
					Grp_DrawLine4(v, (VCReg1L   )&3, opaq);
					gon=1;
				}
			}
		}
		break;
	case 1:	
	case 2:	
		opaq = 1;		// 256 colors
		if ( (VCReg1L&3) <= ((VCReg1L>>4)&3) )	// 同じ値の時は、GRP0が優先（ドラスピ）
		{
			if ( (VCReg2H&0x10)&&(VCReg2L&1) )
			{
				Grp_DrawLine8SP(v, 0);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2L&4)
			{
				if ( ((VCReg2H&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(v, 1, 1);
				else
					Grp_DrawLine8(v, 1, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2L&1)
			{
				if ( (VCReg2H&0x14)!=0x14 )
				{
					Grp_DrawLine8(v, 0, opaq);
					gon=1;
				}
			}
		}
		else
		{
			if ( (VCReg2H&0x10)&&(VCReg2L&1) )
			{
				Grp_DrawLine8SP(v, 1);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2L&4)
			{
				if ( ((VCReg2H&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(v, 0, 1);
				else
					Grp_DrawLine8(v, 0, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2L&1)
			{
				if ( (VCReg2H&0x14)!=0x14 )
				{
					Grp_DrawLine8(v, 1, opaq);
					gon=1;
				}
			}
		}
		break;
	case 3:					// 65536 colors
		if (VCReg2L&15)
		{
			if ( (VCReg2H&0x14)==0x14 )
			{
				Grp_DrawLine16SP(v);
				tron = pron = 1;
			}
			else
			{
				Grp_DrawLine16(v);
				gon=1;
			}
		}
		break;
	}
	}


//	if ( ( ((VCReg1H&0x30)>>4) < (VCReg1H&0x03) ) && (gon) )
//		gdrawed = 1;				// GrpよりBGの方が上

	if ( ((VCReg1H&0x30)>>2) < (VCReg1H&0x0c) )
	{						// BGの方が上
		if ((VCReg2L&0x20)&&(Debug_Text))
		{
			Text_DrawLine(v, 1);
			ton = 1;
		}
		else
		{
			ZeroMem8(Text_TrFlag, TextDotX+16);
		}

		if ((VCReg2L&0x40)&&(BS_CTLH&2)&&(!(SM_RESO&2))&&(Debug_Sp))
		{
			BG_DrawLine(v, (uint_fast8_t)(ton ^ 1), 0);
			bgon = 1;
		}
	}
	else
	{						// Textの方が上
		if ((VCReg2L&0x40)&&(BS_CTLH&2)&&(!(SM_RESO&2))&&(Debug_Sp))
		{
			ZeroMem8(Text_TrFlag, TextDotX+16);
			BG_DrawLine(v, 1, 1);
			bgon = 1;
		}
		else
		{
			if ((VCReg2L&0x20)&&(Debug_Text))
			{
				FillMem16(&BG_LineBuf[16], TextPal[0], TextDotX);
			} else {		// 20010120 （琥珀色）
				FillMem16(&BG_LineBuf[16], 0, TextDotX);
			}
			ZeroMem8(Text_TrFlag, TextDotX+16);
			bgon = 1;
		}

		if ((VCReg2L&0x20)&&(Debug_Text))
		{
			Text_DrawLine(v, (uint_fast8_t)(bgon ^ 1));
			ton = 1;
		}
	}


	opaq = 1;


#if 0
					// Pri = 3（違反）に設定されている画面を表示
		if ( ((VCReg1H&0x30)==0x30)&&(bgon) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&((VCReg1H&0x03)!=0x03)&&(tron) )
			{
				if ( (VCReg1H&3)<((VCReg1H>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, /*tdrawed*/0);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1H&0x0c)==0x0c)&&(ton) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&((VCReg1H&0x03)!=0x0c)&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, /*tdrawed*/((VCReg1H&0x30)==0x30));
			opaq = 0;
			tdrawed = 1;
		}
#endif
					// Pri = 2 or 3（最下位）に設定されている画面を表示
					// プライオリティが同じ場合は、GRP<SP<TEXT？（ドラスピ、桃伝、YsIII等）

					// GrpよりTextが上にある場合にTextとの半透明を行うと、SPのプライオリティも
					// Textに引きずられる？（つまり、Grpより下にあってもSPが表示される？）

					// KnightArmsとかを見ると、半透明のベースプレーンは一番上になるみたい…。

		if ( (VCReg1H&0x02) )
		{
			if (gon)
			{
				CRTC_DrawGrpLine(dst, opaq);
				opaq = 0;
			}
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
				opaq = 0;
			}
		}
		if ( (VCReg1H&0x20)&&(bgon) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&((VCReg1H&0x03)!=0x02)&&(tron) )
			{
				if ( (VCReg1H&3)<((VCReg1H>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, /*0*/tdrawed);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( (VCReg1H&0x08)&&(ton) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&((VCReg1H&0x03)!=0x02)&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, tdrawed/*((VCReg1H&0x30)>=0x20)*/);
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 1（2番目）に設定されている画面を表示
		if ( ((VCReg1H&0x03)==0x01)&&(gon) )
		{
			CRTC_DrawGrpLine(dst, opaq);
			opaq = 0;
		}
		if ( ((VCReg1H&0x30)==0x10)&&(bgon) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&(!(VCReg1H&0x03))&&(tron) )
			{
				if ( (VCReg1H&3)<((VCReg1H>>2)&3) )
				{
					CRTC_DrawBGLineTR(dst, opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				CRTC_DrawBGLine(dst, opaq, (uint_fast8_t)((VCReg1H&0xc)==0x8));
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1H&0x0c)==0x04) && ((VCReg2H&0x5d)==0x1d) && (VCReg1H&0x03) && (((VCReg1H>>4)&3)>(VCReg1H&3)) && (bgon) && (tron) )
		{
			CRTC_DrawBGLineTR(dst, opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
			}
		}
		else if ( ((VCReg1H&0x03)==0x01)&&(tron)&&(gon)&&(VCReg2H&0x10) )
		{
			CRTC_DrawGrpLineNonSP(dst, opaq);
			opaq = 0;
		}
		if ( ((VCReg1H&0x0c)==0x04)&&(ton) )
		{
			if ( ((VCReg2H&0x5d)==0x1d)&&(!(VCReg1H&0x03))&&(tron) )
				CRTC_DrawTextLineTR(dst, opaq);
			else
				CRTC_DrawTextLine(dst, opaq, (uint_fast8_t)((VCReg1H&0x30)>=0x10));
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 0（最優先）に設定されている画面を表示
		if ( (!(VCReg1H&0x03))&&(gon) )
		{
			CRTC_DrawGrpLine(dst, opaq);
			opaq = 0;
		}
		if ( (!(VCReg1H&0x30))&&(bgon) )
		{
			CRTC_DrawBGLine(dst, opaq, /*tdrawed*/(uint_fast8_t)((VCReg1H&0xc)>=0x4));
			tdrawed = 1;
			opaq = 0;
		}
		if ( (!(VCReg1H&0x0c)) && ((VCReg2H&0x5d)==0x1d) && (((VCReg1H>>4)&3)>(VCReg1H&3)) && (bgon) && (tron) )
		{
			CRTC_DrawBGLineTR(dst, opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				CRTC_DrawGrpLineNonSP(dst, opaq);
			}
		}
		else if ( (!(VCReg1H&0x03))&&(tron)&&(VCReg2H&0x10) )
		{
			CRTC_DrawGrpLineNonSP(dst, opaq);
			opaq = 0;
		}
		if ( (!(VCReg1H&0x0c))&&(ton) )
		{
			CRTC_DrawTextLine(dst, opaq, 1);
			tdrawed = 1;
			opaq = 0;
		}

					// 特殊プライオリティ時のグラフィック
		if ( ((VCReg2H&0x5c)==0x14)&&(pron) )	// 特殊Pri時は、対象プレーンビットは意味が無いらしい（ついんびー）
		{
			CRTC_DrawPriLine(dst);
		}
		else if ( ((VCReg2H&0x5d)==0x1c)&&(tron) )	// 半透明時に全てが透明なドットをハーフカラーで埋める
		{						// （AQUALES）
			unsigned int i;
			for (i = 0; i < TextDotX; ++i) {
				const uint16_t w = Grp_LineBufSP[i];
				if ((w != 0) && (dst[i] == 0))
					dst[i] = (w & Pal_HalfMask) >> 1;
			}
		}

	if (opaq)
	{
		FillMem16(dst, 0, TextDotX);
	}
}
