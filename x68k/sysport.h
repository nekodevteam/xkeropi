#ifndef _winx68k_sysport
#define _winx68k_sysport

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	BYTE	SysPort[7];

void SysPort_Init(void);
IOBUS8 FASTCALL SysPort_Read(DWORD adr);
void FASTCALL SysPort_Write(DWORD adr, IOBUS8 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_sysport
