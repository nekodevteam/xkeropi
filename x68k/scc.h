#ifndef _winx68k_scc
#define _winx68k_scc

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern signed char MouseX;
extern signed char MouseY;
extern BYTE MouseSt;

void SCC_IntCheck(void);
void SCC_Init(void);
IOBUS8 FASTCALL SCC_Read(DWORD adr);
void FASTCALL SCC_Write(DWORD adr, IOBUS8 data);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_scc
