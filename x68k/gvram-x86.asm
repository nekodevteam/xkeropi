
%include 'x68k-x86.inc'

%ifidn __OUTPUT_FORMAT__, win32
%define	GVRAM_FastClear		_GVRAM_FastClear
%define	Grp_DrawLine16		_Grp_DrawLine16
%define	Grp_DrawLine8		_Grp_DrawLine8
%define	Grp_DrawLine4		_Grp_DrawLine4
%define	Grp_DrawLine4h		_Grp_DrawLine4h
%define	Grp_DrawLine16SP	_Grp_DrawLine16SP
%define	Grp_DrawLine8SP		_Grp_DrawLine8SP
%define	Grp_DrawLine4SP		_Grp_DrawLine4SP
%define	Grp_DrawLine4hSP	_Grp_DrawLine4hSP
%define	Grp_DrawLine8TR		_Grp_DrawLine8TR
%define	Grp_DrawLine4TR		_Grp_DrawLine4TR
%endif

	bits	32
	section	.text

	global	GVRAM_FastClear
	align	16
GVRAM_FastClear:
	push	ebx
	push	esi
	mov	al, [CRTC_R20L]
	mov	ecx, 256
	test	al, 4
	je	short .singlex
	shl	ecx, 1
.singlex:
	mov	ebx, 256
	test	al, 3
	je	short .singley
	shl	ebx, 1
.singley:
	movzx	esi, word [GrphScroll + 2]
	shl	esi, 10
	mov	ax, [CRTC_FastClrMask]
.fclp2:
	movzx	edx, word [GrphScroll + 0]
	push	ecx
	and	esi, 07fc00h
.fclp:
	and	edx, 511
	and	[GVRAM + esi + edx*2], ax
	inc	edx
	loop	.fclp
	pop	ecx
	add	esi, 1024
	dec	ebx
	jne	short .fclp2
	pop	esi
	pop	ebx
	ret

	global	Grp_DrawLine16
	align	16
Grp_DrawLine16:
	pushfd
	cld
	push	ebx
	push	esi
	push	edi
	mov	esi, [esp + 16 + 4]
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp16linenotspecial
	add	esi, esi
.gp16linenotspecial:
	add	esi, ebx
	and	ebx, 511
	and	esi, 511 << 16
	mov	ecx, [TextDotX]
	shr	esi, 16 - 10
	xor	edx, edx
	lea	esi, [GVRAM + esi + ebx*2]
	xor	ebx, 511
	inc	ebx
	mov	edi, Grp_LineBuf
	xor	eax, eax
	cmp	ecx, ebx
	jbe	short .gp16linelp_b
	sub	ecx, ebx
.gp16linelp_a:
	lodsw
	or	ax, ax
	je	short .gp16linelp_a_skip
	mov	dl, ah
	mov	ah, 0
	mov	dh, 0
	mov	ax, [Pal16Adr + eax*2]
	mov	dx, [Pal16Adr + edx*2]
	mov	al, [Pal_Regs + eax]
	mov	ah, [Pal_Regs + edx + 2]
	mov	ax, [Pal16 + eax*2]
;or ax, [Ibit]		; 20010120
.gp16linelp_a_skip:
	stosw
	dec	ebx
	jnz	short .gp16linelp_a
	sub	esi, 400h
.gp16linelp_b:
	lodsw
	or	ax, ax
	je	short .gp16linelp_b_skip
	mov	dl, ah
	mov	ah, 0
	mov	dh, 0
	mov	ax, [Pal16Adr + eax*2]
	mov	dx, [Pal16Adr + edx*2]
	mov	al, [Pal_Regs + eax]
	mov	ah, [Pal_Regs + edx + 2]
	mov	ax, [Pal16 + eax*2]
;or ax, [Ibit]		; 20010120
.gp16linelp_b_skip:
	stosw
	loop	.gp16linelp_b
	pop	edi
	pop	esi
	pop	ebx
	popfd
	ret

	global	Grp_DrawLine8
	align	16
Grp_DrawLine8:
	pushfd
	cld
	push	ebx
	push	esi
	push	edi

	mov	esi, [esp + 16 + 4]
	mov	ecx, [esp + 16 + 8]	; ecx = page
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*8]
	mov	edx, [GrphScroll + ecx*8 + 4]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp8linenotspecial
	add	esi, esi
.gp8linenotspecial:
	mov	edi, esi
	add	esi, ebx
	add	edi, edx
	and	esi, 511 << 16
	and	edi, 511 << 16
	shr	esi, 16 - 10
	shr	edi, 16 - 10
	and	ebx, 511
	and	edx, 511
	lea	esi, [GVRAM + esi + ebx*2]
	lea	edi, [edi + edx*2]
	add	esi, ecx
	xor	ebx, 511
	add	edi, ecx
	inc	ebx

	xor	eax, eax
	mov	ecx, [TextDotX]

	cmp	byte [esp + 16 + 12], 0	; dl = opaq
	mov	edx, Grp_LineBuf
	je	short .gp8linelp

	cmp	ecx, ebx
	jbe	short .gp8olinelp_b
	sub	ecx, ebx

.gp8olinelp_a:
	lodsw
	mov	ah, [GVRAM + edi]
	and	ah, 0f0h
	and	al, 0fh
	or	al, ah
	mov	ah, 0
;or al,al
	mov	ax, [GrphPal + eax*2]
;jz gp8noti_a
;or ax, [Ibit]		; 20010120
;gp8noti_a:
	mov	[edx], ax
	add	edx, byte 2
	add	edi, byte 2
	test	di, 03feh
	jnz	short .gp8onotxend1
	sub	edi, 0400h
.gp8onotxend1:
	dec	ebx
	jnz	short .gp8olinelp_a
	sub	esi, 400h
.gp8olinelp_b:
	lodsw
	mov	ah, [GVRAM + edi]
	and	ah, 0f0h
	and	al, 0fh
	or	al, ah
	mov	ah, 0
;or al,al
	mov	ax, [GrphPal + eax*2]
;jz gp8noti_b
;or ax, [Ibit]		; 20010120
;gp8noti_b:
	mov	[edx], ax
	add	edx, byte 2
	add	edi, byte 2
	test	di, 03feh
	jnz	short .gp8onotxend2
	sub	edi, 0400h
.gp8onotxend2:
	loop	.gp8olinelp_b

	pop	edi
	pop	esi
	pop	ebx
	popfd
	ret

.gp8linelp:
	cmp	ecx, ebx
	jbe	short .gp8linelp_b
	sub	ecx, ebx
.gp8linelp_a:
	lodsw
	mov	ah, [GVRAM + edi]
	and	ah, 0f0h
	and	al, 0fh
	or	al, ah
	and	ax, 00ffh
	jz	short .gp8lineskip_a
	mov	ax, [GrphPal + eax*2]
;or ax, [Ibit]		; 20010120
	mov	[edx], ax
.gp8lineskip_a:
	add	edx, byte 2
	add	edi, byte 2
	test	di, 03feh
	jnz	short .gp8notxend1
	sub	edi, 0400h
.gp8notxend1:
	dec	ebx
	jnz	short .gp8linelp_a
	sub	esi, 400h
.gp8linelp_b:
	lodsw
	mov	ah, [GVRAM + edi]
	and	ah, 0f0h
	and	al, 0fh
	or	al, ah
	and	ax, 00ffh
	jz	short .gp8lineskip_b
	mov	ax, [GrphPal + eax*2]
;or ax, [Ibit]		; 20010120
	mov	[edx], ax
.gp8lineskip_b:
	add	edx, byte 2
	add	edi, byte 2
	test	di, 03feh
	jnz	short .gp8notxend2
	sub	edi, 0400h
.gp8notxend2:
	loop	.gp8linelp_b

	pop	edi
	pop	esi
	pop	ebx
	popfd
	ret


	global	Grp_DrawLine4
	align	16
Grp_DrawLine4:
	pushfd
	cld
	push	ebx
	push	esi
	mov	esi, [esp + 12 + 4]
	mov	ecx, [esp + 12 + 8]
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*4]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp4linenotspecial
	add	esi, esi
.gp4linenotspecial:
	add	esi, ebx
	and	ebx, 511
	and	esi, 511 << 16
	xor	eax, eax
	shr	esi, 16 - 10
	lea	esi, [esi + ebx*2]
	xor	ebx, 511
	inc	ebx

	shr	ecx, 1
	lea	esi, [GVRAM + esi + ecx]	; ecx = page/2
	jnc	near .gp4olinepage0		; (shr cl,1) page0or2

	mov	ecx, [TextDotX]
	cmp	byte [esp + 12 + 12], 0
	mov	edx, Grp_LineBuf
	jz	short .gp4linelp2		; opaq == 0

	cmp	ecx, ebx
	jbe	short .gp4olinelp2_b
	sub	ecx, ebx
.gp4olinelp2_a:
	lodsw
	mov	ah, 0
	shr	al, 4
	mov	ax, [GrphPal + eax*2]
	mov	[edx], ax
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4olinelp2_a
	sub	esi, 400h
.gp4olinelp2_b:
	lodsw
	mov	ah, 0
	shr	al, 4
	mov	ax, word [GrphPal + eax*2]
	mov	[edx], ax
	add	edx, byte 2
	loop	.gp4olinelp2_b
	pop	esi
	pop	ebx
	popfd
	ret

.gp4linelp2:
	cmp	ecx, ebx
	jbe	short .gp4linelp2_b
	sub	ecx, ebx
.gp4linelp2_a:
	lodsw
	mov	ah, 0
	shr	al, 4			; shrのZFは確認済
	jz	short .gp4lineskip2_a
	mov	ax, word [GrphPal + eax*2]
	mov	[edx], ax
.gp4lineskip2_a:
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4linelp2_a
	sub	esi, 400h
.gp4linelp2_b:
	lodsw
	mov	ah, 0
	shr	al, 4			; shrのZFは確認済
	jz	short .gp4lineskip2_b
	mov	ax, word [GrphPal + eax*2]
	mov	[edx], ax
.gp4lineskip2_b:
	add	edx, byte 2
	loop	.gp4linelp2_b
	pop	esi
	pop	ebx
	popfd
	ret

.gp4olinepage0:
	mov	ecx, [TextDotX]
	cmp	byte [esp + 12 + 12], 0
	mov	edx, Grp_LineBuf
	jz	short .gp4linelp	; opaq == 0

	cmp	ecx, ebx
	jbe	short .gp4olinelp_b
	sub	ecx, ebx
.gp4olinelp_a:
	lodsw
	and	ax, byte 15
	mov	ax, [GrphPal + eax*2]
	mov	[edx], ax
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4olinelp_a
	sub	esi, 400h
.gp4olinelp_b:
	lodsw
	and	ax, 15
	mov	ax, [GrphPal + eax*2]
	mov	[edx], ax
	add	edx, byte 2
	loop	.gp4olinelp_b
	pop	esi
	pop	ebx
	popfd
	ret

.gp4linelp:
	cmp	ecx, ebx
	jbe	short .gp4linelp_b
	sub	ecx, ebx
.gp4linelp_a:
	lodsw
	and	ax, byte 15
	jz	short .gp4lineskip_a
	mov	ax, [GrphPal + eax*2]
	mov	[edx], ax
.gp4lineskip_a:
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4linelp_a
	sub	esi, 400h
.gp4linelp_b:
	lodsw
	and	ax, 15
	jz	short .gp4lineskip_b
	mov	ax, [GrphPal + eax*2]
	mov	[edx], ax
.gp4lineskip_b:
	add	edx, byte 2
	loop	.gp4linelp_b
	pop	esi
	pop	ebx
	popf
	ret

	global	Grp_DrawLine4h
	align	16
Grp_DrawLine4h:
	push	ebx
	push	esi
	push	edi
	mov	esi, [esp + 12 + 4]
	mov	al, [CRTC_R20L]
	mov	edi, [GrphScroll]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp4hlinenotspecial
	add	esi, esi
.gp4hlinenotspecial:
	add	esi, edi
	mov	cl, 0
	test	esi, 512 << 16
	je	short .p0
	add	cl, 8
.p0:	test	edi, 512
	je	short .p1
	add	cl, 4
.p1:	and	esi, 511 << 16
	and	edi, 511
	shr	esi, 16 - 10
	xor	edx, edx
	lea	esi, [esi + edi*2]

	xor	edi, 511
	inc	edi
	mov	ebx, [TextDotX]
.gp4hlinelp:
	movzx	eax, word [GVRAM + esi]
	shr	eax, cl
	and	eax, byte 15
	mov	ax, word [GrphPal + eax*2]
	mov	[Grp_LineBuf + edx], ax
	add	esi, byte 2
	add	edx, byte 2
	dec	edi
	jnz	short .gp4hline_cnt
	sub	esi, 0400h
	xor	cl, 4
	mov	edi, 512
.gp4hline_cnt:
	dec	ebx
	jnz	short .gp4hlinelp
	pop	edi
	pop	esi
	pop	ebx
	ret

	global	Grp_DrawLine16SP
	align	16
Grp_DrawLine16SP:
	push	ebx
	push	edi
	push	esi
	mov	edi, 0
	mov	esi, [esp + 12 + 4]
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp16splinenotspecial
	add	esi, esi
.gp16splinenotspecial:
	add	esi, ebx
	and	ebx, 511
	mov	ecx, [TextDotX]
	and	esi, 511 << 16
	xor	edx, edx
	shr	esi, 16 - 10
	lea	esi, [esi + ebx*2]
	xor	ebx, 511
	inc	ebx
.gp16splinelp:
	movzx	eax, byte [GVRAM + esi + 1]
	mov	dh, [Pal_Regs + eax*2 + 1]
	movzx	eax, byte [GVRAM + esi]
	mov	dl, [Pal_Regs + eax*2]
	test	al, 1
	jnz	short .gp16splinesp
	and	dx, 0fffeh
	mov	ax, [Pal16 + edx*2]
	mov	word [Grp_LineBufSP + edi], 0
	mov	[Grp_LineBufSP2 + edi], ax
	jmp	short .gp16splineskip
.gp16splinesp:
	and	dx, 0fffeh
	mov	ax, [Pal16 + edx*2]
	mov	word [Grp_LineBufSP2 + edi], 0
	mov	[Grp_LineBufSP+ edi], ax
.gp16splineskip:
	add	esi, byte 2
	add	edi, byte 2
	dec	ebx
	jnz	short .gp16spline_cnt
	sub	esi, 0400h
.gp16spline_cnt:
	loop	.gp16splinelp
	pop	esi
	pop	edi
	pop	ebx
	ret

	global	Grp_DrawLine8SP
	align	16
Grp_DrawLine8SP:
	push	esi
	push	edi
	mov	esi, [esp + 8 + 4]
	mov	ecx, [esp + 8 + 8]	; ecx = page
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*8 + 0]
	mov	edx, [GrphScroll + ecx*8 + 4]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp8splinenotspecial
	add	esi, esi
.gp8splinenotspecial:
	mov	edi, esi
	add	esi, ebx
	add	edi, edx
	and	esi, 511 << 16
	and	edi, 511 << 16
	shr	esi, 16 - 10
	shr	edi, 16 - 10
	and	ebx, 511
	and	edx, 511
	lea	esi, [esi + ebx*2]
	lea	edi, [edi + edx*2]
	add	esi, ecx
	add	edi, ecx
	xor	ebx, 511
	inc	ebx
	mov	ecx, [TextDotX]
	xor	edx, edx
	xor	eax, eax
.gp8osplinelp:
	mov	al, [GVRAM + esi]
	mov	ah, [GVRAM + edi]
	and	ah, 0f0h
	and	al, 0fh
	or	al, ah
; xor	ah, ah
	test	al, 1
	jnz	short .gp8osplinesp
	and	eax, 0feh
	jz	short .gp8onotzero2
	mov	ax, [GrphPal + eax*2]
.gp8onotzero2:
	mov	word [Grp_LineBufSP + edx], 0
	mov	[Grp_LineBufSP2 + edx], ax
	jmp	short .gp8osplineskip
.gp8osplinesp:
	and	eax, 0feh
	jz	short .gp8onotzero	; ついんびー。Palette0以外の$0000は特殊Priでは透明じゃなく黒扱いらしい
	mov	ax, [GrphPal + eax*2]
	or	ax, [Ibit]		; Palette0以外はIbit立ててごまかそー
.gp8onotzero:				; 半透明が変になる時は、半透明と特殊Priを別ルーチンにしなきゃ…
	mov	[Grp_LineBufSP + edx], ax
	mov	word [Grp_LineBufSP2 + edx], 0
.gp8osplineskip:
	add	esi, byte 2
	add	edi, byte 2
	test	di, 03feh
	jnz	short .gp8spnotxend
	sub	edi, 0400h
.gp8spnotxend:
	add	edx, byte 2
	dec	ebx
	jnz	short .gp8ospline_cnt
	sub	esi, 0400h
.gp8ospline_cnt:
	loop	.gp8osplinelp
	pop	edi
	pop	esi
	ret

	global	Grp_DrawLine4SP
	align	16
Grp_DrawLine4SP:
	push	ebx
	push	esi
	mov	esi, [esp + 8 + 4]
	mov	ecx, [esp + 8 + 8]
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*8]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp4splinenotspecial
	add	esi, esi
.gp4splinenotspecial:
	add	esi, ebx
	and	ebx, 511
	and	esi, 511 << 16
	xor	edx, edx
	shr	esi, 16 - 10
	shr	ecx, 1
	lea	esi, [esi + ebx*2]

	jnc	short .Page02

	add	esi, ecx
	xor	ebx, 511
	inc	ebx
	mov	ecx, [TextDotX]
.gp4osplinelp2:
	mov	al, [GVRAM + esi]
	shr	al, 4
	test	al, 1
	jnz	short .gp4o2splinesp
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	word [Grp_LineBufSP + edx], 0
	mov	[Grp_LineBufSP2 + edx], ax
	jmp	short .gp4o2splineskip
.gp4o2splinesp:
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	[Grp_LineBufSP + edx], ax
	mov	word [Grp_LineBufSP2 + edx], 0
.gp4o2splineskip:
	add	esi, byte 2
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4ospline_cnt2
	sub	esi, 0400h
.gp4ospline_cnt2:
	loop	.gp4osplinelp2
	pop	esi
	pop	ebx
	ret

.Page02:
	add	esi, ecx
	xor	ebx, 511
	inc	ebx
	mov	ecx, [TextDotX]
.gp4osplinelp:
	mov	al, byte [GVRAM + esi]
	test	al, 1
	jnz	short .gp4osplinesp
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	word [Grp_LineBufSP + edx], 0
	mov	[Grp_LineBufSP2 + edx], ax
	jmp	short .gp4osplineskip
.gp4osplinesp:
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	[Grp_LineBufSP + edx], ax
	mov	word [Grp_LineBufSP2 + edx], 0
.gp4osplineskip:
	add	esi, byte 2
	add	edx, byte 2
	dec	ebx
	jnz	short .gp4ospline_cnt
	sub	esi, 0400h
.gp4ospline_cnt:
	loop	.gp4osplinelp
	pop	esi
	pop	ebx
	ret

	global	Grp_DrawLine4hSP
	align	16
Grp_DrawLine4hSP:
	push	ebx
	push	esi
	push	edi
	mov	esi, [esp + 12 + 4]
	mov	al, [CRTC_R20L]
	mov	edi, [GrphScroll]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp4hsplinenotspecial
	add	esi, esi
.gp4hsplinenotspecial:
	add	esi, edi
	mov	cl, 0
	test	esi, 512 << 16
	je	short .p0
	add	cl, 8
.p0:	test	edi, 512
	je	short .p1
	add	cl, 4
.p1:	and	esi, 511 << 16
	and	edi, 511
	shr	esi, 16 - 10
	xor	edx, edx
	lea	esi, [esi + edi*2]

	xor	edi, 511
	inc	edi
	mov	ebx, [TextDotX]
.gp4hsplinelp:
	movzx	eax, word [GVRAM + esi]
	shr	eax, cl
	test	eax, 1
	jnz	short .gp4hsplinesp
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	word [Grp_LineBufSP + edx], 0
	mov	[Grp_LineBufSP2 + edx], ax
	jmp	short .gp4hsplineskip
.gp4hsplinesp:
	and	eax, byte 14
	mov	ax, [GrphPal + eax*2]
	mov	[Grp_LineBufSP + edx], ax
	mov	word [Grp_LineBufSP2 + edx], 0
.gp4hsplineskip:
	add	esi, byte 2
	add	edx, byte 2
	dec	edi
	jnz	short .gp4hspline_cnt
	sub	esi, 0800h
.gp4hspline_cnt:
	dec	ebx
	jnz	short .gp4hsplinelp	;	loop	gp4hsplinelp
	pop	edi
	pop	esi
	pop	ebx
	ret

	global	Grp_DrawLine8TR
	align	16
Grp_DrawLine8TR:
	cmp	byte [esp + 12], 0
	jz	near .opaq_zero
	push	ebx
	push	esi
	push	edi
	mov	esi, [esp + 12 + 4]
	mov	ecx, [esp + 12 + 8]		; ecx = page
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*8]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp8trlinenotspecial
	add	esi, esi
.gp8trlinenotspecial:
	add	esi, ebx
	and	ebx, 511
	and	esi, 511 << 16
	mov	edx, [TextDotX]
	shr	esi, 16 - 10
	xor	edi, edi
	add	esi, ecx
.gp8otrlinelp:
	movzx	eax, word [Grp_LineBufSP + edi]
	movzx	ecx, byte [GVRAM + esi + ebx*2]
	or	ax, ax
	jnz	short .gp8otrlinetr
	mov	cx, [GrphPal + ecx*2]
	jmp	short .gp8otrlinenorm
.gp8otrlinetr:
	jcxz	.gp8otrlinenorm			; けろぴー…
	mov	cx, [GrphPal + ecx*2]
	jcxz	.gp8otrlinenorm			; けろぴー…
	and	ax, [Pal_HalfMask]
	test	cx, [Ibit]
	jz	short .gp8otrlinetrI
	or	ax, [Pal_Ix2]
.gp8otrlinetrI:
	and	cx, [Pal_HalfMask]
	add	cx, ax			; 17bit計算中
	rcr	cx, 1			; 17bit計算中
.gp8otrlinenorm:
	mov	[Grp_LineBuf + edi], cx
	inc	bx
	and	bh, 1			; and	bx, 511
	add	edi, byte 2
	dec	edx
	jnz	short .gp8otrlinelp
	pop	edi
	pop	esi
	pop	ebx
.opaq_zero:
	ret

	global	Grp_DrawLine4TR
	align	16
Grp_DrawLine4TR:
	push	ebx
	push	esi
	push	edi

	mov	esi, [esp + 12 + 4]
	mov	ecx, [esp + 12 + 8]
	mov	al, [CRTC_R20L]
	mov	ebx, [GrphScroll + ecx*4]
	and	al, 1ch
	shl	esi, 16
	cmp	al, 1ch
	jne	short .gp4trlinenotspecial
	add	esi, esi
.gp4trlinenotspecial:
	add	esi, ebx
	and	ebx, 511
	and	esi, 511 << 16
	xor	edi, edi
	shr	esi, 16 - 10

	shr	ecx, 1
	jnc	near .pagebit0eq0		; jmp (page 0 or 2)

	add	esi, ecx			; ecx = page/2
	cmp	byte [esp + 12 + 12], 0
	je	short .gp4trline2page0

	mov	edx, [TextDotX]
.gp4otrlinelp2:
	movzx	eax, word [Grp_LineBufSP + edi]
	movzx	ecx, byte [GVRAM + esi + ebx*2]
	shr	cl, 4
	or	ax, ax
	jnz	short .gp4otrlinetr2
	mov	cx, [GrphPal + ecx*2]
	jmp	short .gp4otrlinenorm2
.gp4otrlinetr2:
	jcxz	.gp4otrlinenorm2		; けろぴー
	mov	cx, [GrphPal + ecx*2]
	jcxz	.gp4otrlinenorm2		; けろぴー
	and	ax, [Pal_HalfMask]
	test	cx, [Ibit]
	jz	short .gp4otrlinetr2I
	or	ax, [Pal_Ix2]
.gp4otrlinetr2I:
	and	cx, [Pal_HalfMask]
	add	cx, ax			; 17bit計算中
	rcr	cx, 1			; 17bit計算中
.gp4otrlinenorm2:
	mov	[Grp_LineBuf + edi], cx
	inc	bx
	and	bh, 1			; and	bx, 511
	add	edi, byte 2
	dec	edx
	jnz	short .gp4otrlinelp2
	pop	edi
	pop	esi
	pop	ebx
	ret

.gp4trline2page0:
	mov	edx, [TextDotX]
.gp4trlinelp2:
	movzx	eax, word [Grp_LineBufSP + edi]
	or	ax, ax
	jnz	short .gp4trlinetr2
	movzx	ecx, byte [GVRAM + esi + ebx*2]
	shr	cl, 4
	jcxz	.gp4trlineskip2
	mov	cx, [GrphPal + ecx*2]
	jmp	short .gp4trlinenorm2
.gp4trlinetr2:
	movzx	ecx, byte [GVRAM + esi + ebx*2]
	shr	cl, 4
	jcxz	.gp4trlinenorm2		; けろぴー
	mov	cx, [GrphPal + ecx*2]
	jcxz	.gp4trlineskip2		; けろぴー
	and	ax, [Pal_HalfMask]
	test	cx, [Ibit]
	jz	short .gp4trlinetr2I
	or	ax, [Pal_Ix2]
.gp4trlinetr2I:
	and	cx, [Pal_HalfMask]
	add	cx, ax			; 17bit計算中
	rcr	cx, 1			; 17bit計算中
.gp4trlinenorm2:
	mov	[Grp_LineBuf + edi], cx
.gp4trlineskip2:
	inc	bx
	and	bh, 1			; and	bx, 511
	add	edi, byte 2
	dec	edx
	jnz	short .gp4trlinelp2
	pop	edi
	pop	esi
	pop	ebx
	ret

.pagebit0eq0:
	add	esi, ecx		; ecx = page/2
	cmp	byte [esp + 12 + 12], 0
	je	short .gp4trlinepage0

	mov	edx, [TextDotX]
.gp4otrlinelp:
	mov	cl, [GVRAM + esi + ebx*2]
	and	ecx, byte 15
	movzx	eax, word [Grp_LineBufSP + edi]
	or	ax, ax
	jnz	short .gp4otrlinetr
	mov	cx, [GrphPal + ecx*2]
	jmp	short .gp4otrlinenorm
.gp4otrlinetr:
	jcxz	.gp4otrlinenorm		; けろぴー
	mov	cx, [GrphPal + ecx*2]
	jcxz	.gp4otrlinenorm		; けろぴー
	and	ax, [Pal_HalfMask]
	test	cx, [Ibit]
	jz	short .gp4otrlinetrI
	or	ax, [Pal_Ix2]
.gp4otrlinetrI:
	and	cx, [Pal_HalfMask]
	add	cx, ax			; 17bit計算中
	rcr	cx, 1			; 17bit計算中
.gp4otrlinenorm:
	mov	[Grp_LineBuf + edi], cx
	inc	bx
	and	bh, 1			; and	bx, 511
	add	edi, byte 2
	dec	edx
	jnz	short .gp4otrlinelp
	pop	edi
	pop	esi
	pop	ebx
	ret

.gp4trlinepage0:
	mov	edx, [TextDotX]
.gp4trlinelp:
	mov	cl, [GVRAM + esi + ebx*2]
	and	ecx, byte 15
	movzx	eax, word [Grp_LineBufSP + edi]
	or	ax, ax
	jnz	short .gp4trlinetr

	jcxz	.gp4trlineskip
	mov	cx, [GrphPal + ecx*2]
	jmp	short .gp4trlinenorm

.gp4trlinetr:
	jcxz	.gp4trlinenorm		; けろぴー
	mov	cx, [GrphPal + ecx*2]
	jcxz	.gp4trlineskip		; けろぴー
	and	ax, [Pal_HalfMask]
	test	cx, [Ibit]
	jz	short .gp4trlinetrI
	or	ax, [Pal_Ix2]
.gp4trlinetrI:
	and	cx, [Pal_HalfMask]
	add	cx, ax			; 17bit計算中
	rcr	cx, 1			; 17bit計算中
.gp4trlinenorm:
	mov	[Grp_LineBuf + edi], cx
.gp4trlineskip:
	inc	bx
	and	bh, 1			; and	bx, 511
	add	edi, byte 2
	dec	edx
	jnz	short .gp4trlinelp
	pop	edi
	pop	esi
	pop	ebx
	ret
