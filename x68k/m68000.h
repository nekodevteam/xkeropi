﻿#ifndef _winx68k_m68000
#define _winx68k_m68000

#ifdef USE_68KEM

#include "68kem.h"

#define M68000_GETPC		regs.pc
#define M68000_SETPC(v)		do { regs.pc = (v); cpu_setOPbase24(regs.pc); } while (0 /*CONSTCOND*/)
#define M68000_GETD(n)		regs.d[n]
#define M68000_SETD(n, v)	regs.d[n] = (v)
#define M68000_GETA(n)		regs.a[n]
#define M68000_GETCR		regs.ccr
#define M68000_GETSR		regs.sr_high

#else	// USE_68KEM

#include "../m68000/m68000.h"

#define M68000_GETPC		m68000_get_reg(M68K_PC)
#define M68000_SETPC(v)		m68000_set_reg(M68K_PC, (v))
#define M68000_GETD(n)		m68000_get_reg(M68K_D0 + n)
#define M68000_SETD(n, v)	m68000_set_reg(M68K_D0 + n, (v))
#define M68000_GETA(n)		m68000_get_reg(M68K_A0 + n)
#define M68000_GETCR		m68000_get_reg(M68K_SR)
#define M68000_GETSR		m68000_get_reg(M68K_SR)

#endif	// USE_68KEM

#endif //_winx68k_m68000
