﻿// ---------------------------------------------------------------------------------------
//  SCC.C - Z8530 SCC（マウスのみ）
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "scc.h"
#include "irqh.h"
#include "mouse.h"

signed char MouseX = 0;
signed char MouseY = 0;
BYTE MouseSt = 0;

struct tagScc
{
	uint8_t RegsA[16];
	uint8_t RegNumA;
	uint8_t RegSetA;
	uint8_t RegsB[16];
	uint8_t RegNumB;
	uint8_t RegSetB;
	uint8_t Vector;
	uint8_t Dat[3];
	uint8_t DatNum;
};
static struct tagScc SCC;

// -----------------------------------------------------------------------
//   わりこみ
// -----------------------------------------------------------------------
static int32_t FASTCALL SCC_Int(uint_fast8_t irq)
{
	int32_t ret = -1;
	IRQH_IRQCallBack(irq);
	if ( (irq==5)&&(!(SCC.RegsB[9]&2)) )
	{
		if (SCC.RegsB[9]&1)
		{
			if (SCC.RegsB[9]&0x10)
				ret = (SCC.Vector&0x8f)+0x20;
			else
				ret = (SCC.Vector&0xf1)+4;
		}
		else
			ret = SCC.Vector;
	}

	return ret;
}


// -----------------------------------------------------------------------
//   割り込みのチェック
// -----------------------------------------------------------------------
void SCC_IntCheck(void)
{
	if ( (SCC.DatNum) && ((SCC.RegsB[1]&0x18)==0x10) && (SCC.RegsB[9]&0x08) )
	{
		IRQH_Int(5, &SCC_Int);
	}
	else if ( (SCC.DatNum==3) && ((SCC.RegsB[1]&0x18)==0x08) && (SCC.RegsB[9]&0x08) )
	{
		IRQH_Int(5, &SCC_Int);
	}
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void SCC_Init(void)
{
	MouseX = 0;
	MouseY = 0;
	MouseSt = 0;
	SCC.RegNumA = 0;
	SCC.RegSetA = 0;
	SCC.RegNumB = 0;
	SCC.RegSetB = 0;
	SCC.Vector = 0;
	SCC.DatNum = 0;
}


// -----------------------------------------------------------------------
//   I/O Write
// -----------------------------------------------------------------------
void FASTCALL SCC_Write(DWORD adr, IOBUS8 data)
{
	if (adr>=0xe98008) return;

	if ((adr&7) == 1)
	{
		if (SCC.RegSetB)
		{
			if (SCC.RegNumB == 5)
			{
				if ( (!(SCC.RegsB[5]&2))&&(data&2)&&(SCC.RegsB[3]&1)&&(!SCC.DatNum) )	// データが無い時だけにしやう（闇の血族）
				{			// マウスデータ生成
					Mouse_SetData();
					SCC.DatNum = 3;
					SCC.Dat[2] = MouseSt;
					SCC.Dat[1] = MouseX;
					SCC.Dat[0] = MouseY;
				}
			}
			else if (SCC.RegNumB == 2) SCC.Vector = data;
			SCC.RegSetB = 0;
			SCC.RegsB[SCC.RegNumB] = data;
/*{
FILE *fp;
fp=fopen("_scc.txt", "a");
fprintf(fp, "SCC  Reg[%d] = $%02X  @ $%08X\n", SCC_RegNumB, data, M68000_GETPC);
fclose(fp);
}*/
			SCC.RegNumB = 0;
		}
		else
		{
			if (!(data&0xf0))
			{
				data &= 15;
				SCC.RegSetB = 1;
				SCC.RegNumB = data;
			}
			else
			{
				SCC.RegSetB = 0;
				SCC.RegNumB = 0;
			}
		}
	}
	else if ((adr&7) == 5)
	{
		if (SCC.RegSetA)
		{
			SCC.RegSetA = 0;
			switch (SCC.RegNumA)
			{
			case 2:
				SCC.RegsB[2] = data;
				SCC.Vector = data;
				break;
			case 9:
				SCC.RegsB[9] = data;
				break;
			}
		}
		else
		{
			data &= 15;
			if (data)
			{
				SCC.RegSetA = 1;
				SCC.RegNumA = data;
			}
			else
			{
				SCC.RegSetA = 0;
				SCC.RegNumA = 0;
			}
		}
	}
	else if ((adr&7) == 3)
	{
	}
	else if ((adr&7) == 7)
	{
	}
}


// -----------------------------------------------------------------------
//   I/O Read
// -----------------------------------------------------------------------
IOBUS8 FASTCALL SCC_Read(DWORD adr)
{
	IOBUS8 ret = 0;

	if (adr>=0xe98008) return ret;

	if ((adr&7) == 1)
	{
		if (!SCC.RegNumB)
			ret = ((SCC.DatNum)?1:0);
		SCC.RegNumB = 0;
		SCC.RegSetB = 0;
	}
	else if ((adr&7) == 3)
	{
		if (SCC.DatNum)
		{
			SCC.DatNum--;
			ret = SCC.Dat[SCC.DatNum];
		}
	}
	else if ((adr&7) == 5)
	{
		switch(SCC.RegNumA)
		{
		case 0:
			ret = 4;			// 送信バッファ空（Xna）
			break;
		case 3:
			ret = ((SCC.DatNum)?4:0);
			break;
		}
		SCC.RegNumA = 0;
		SCC.RegSetA = 0;
	}
	return ret;
}
