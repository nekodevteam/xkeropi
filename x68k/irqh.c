﻿// ---------------------------------------------------------------------------------------
//  IRQH.C - IRQ Handler (架空のデバイスにょ)
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "irqh.h"
#include "m68000.h"

static	BYTE		IRQH_IRQ[8];
static	DMAHANDLER	IRQH_CallBack[8];

// -----------------------------------------------------------------------
//   デフォルトのベクタを返す（これが起こったら変だお）
// -----------------------------------------------------------------------
static int32_t FASTCALL IRQH_DefaultVector(uint_fast8_t irq)
{
	IRQH_IRQCallBack(irq);
	return -1;
}


// -----------------------------------------------------------------------
//   初期化
// -----------------------------------------------------------------------
void IRQH_Init(void)
{
	unsigned i;

	ZeroMemory(IRQH_IRQ, sizeof(IRQH_IRQ));
	for (i = 0; i < _countof(IRQH_CallBack); i++)
	{
		IRQH_CallBack[i] = IRQH_DefaultVector;
	}
}


// -----------------------------------------------------------------------
//   他の割り込みのチェック
//   各デバイスのベクタを返すルーチンから呼ばれます
// -----------------------------------------------------------------------
void IRQH_IRQCallBack(uint_fast8_t irq)
{
	int i;
	const unsigned level = irq & 7;
	IRQH_IRQ[level] = 0;
#if defined(USE_68KEM)
	regs.IRQ_level = 0;
#elif defined(CYCLONE)
	m68k.irq =0;
#else
	C68k_Set_IRQ(&C68K, 0);
#endif
	for (i=7; i>0; i--)
	{
		if (IRQH_IRQ[i])
		{
#if defined(USE_68KEM)
			regs.irq_callback = IRQH_CallBack[i];
			regs.IRQ_level = i;
			if ( m68000_ICount ) {					// 多重割り込み時（CARAT）
				m68000_ICountBk += m68000_ICount;		// 強制的に割り込みチェックをさせる
				m68000_ICount = 0;				// 苦肉の策 ^^;
			}
#elif defined(CYCLONE)
			m68k.irq = i;
#else
			C68k_Set_IRQ(&C68K, i);
#endif
			break;
		}
	}
}


// -----------------------------------------------------------------------
//   割り込み発生
// -----------------------------------------------------------------------
void IRQH_Int(uint_fast8_t irq, DMAHANDLER handler)
{
	int i;
	const unsigned level = irq & 7;
	IRQH_IRQ[level] = 1;
	if (handler == NULL)
		handler = &IRQH_DefaultVector;
	IRQH_CallBack[level] = handler;
	for (i=7; i>0; i--)
	{
		if (IRQH_IRQ[i])
		{
#if defined(USE_68KEM)
			regs.irq_callback = IRQH_CallBack[i];
			regs.IRQ_level = i;
			if ( m68000_ICount ) {					// 多重割り込み時（CARAT）
				m68000_ICountBk += m68000_ICount;		// 強制的に割り込みチェックをさせる
				m68000_ICount = 0;				// 苦肉の策 ^^;
			}
#elif defined(CYCLONE)
			m68k.irq = i;
#else
			C68k_Set_IRQ(&C68K, i);
#endif
			return;
		}
	}
}

#ifndef USE_68KEM
s32 FASTCALL my_irqh_callback(s32 level)
{
	DMAHANDLER func;
	int vect;
	int i;

	if (level == VECT_BUSERR)
	{
		return 2;
	}
	else if (level == VECT_ADRERR)
	{
		return 3;
	}

	level &= 7;
	func = IRQH_CallBack[level];
	vect = (*func)((uint_fast8_t)level);
	//printf("irq vect = %x line = %d\n", vect, level);

	for (i=7; i>0; i--)
	{
		if (IRQH_IRQ[i])
		{
#ifdef CYCLONE
			m68k.irq = i;
#else
			C68k_Set_IRQ(&C68K, i);
#endif
			break;
		}
	}

	return vect;
}
#endif	// !USE_68KEM
