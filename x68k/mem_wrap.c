﻿/*	$Id: mem_wrap.c,v 1.2 2003/12/05 18:07:19 nonaka Exp $	*/

// ---------------------------------------------------------------------------------------
//  mem_wrap.c - メモリ＆I/O管理
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "memory.h"
#include "m68000.h"

#ifdef USE_FETCHMEM
/*! バス */
uint8_t	FetchMem[0x1000000];
#else	// USE_FETCHMEM
BYTE	*MEM;
BYTE	GVRAM[0x80000];
BYTE	TVRAM[0x80000];
BYTE	SCSIIPL[0x2000];
BYTE	SRAM[0x4000];
BYTE	*IPL;
BYTE	*FONT;
#endif	// USE_FETCHMEM

BYTE BusErrFlag = 0;
DWORD BusErrAdr;

#ifdef _DEBUG
void FASTCALL Memory_ErrTrace(void)
{
#ifdef WIN68DEBUG
	FILE *fp;
	fp=fopen("_buserr.txt", "a");
	if (BusErrFlag==3)
		fprintf(fp, "BusErr - SetOP to $%08X  @ $%08X\n", BusErrAdr, M68000_GETPC);
	else if (BusErrFlag==2)
		fprintf(fp, "BusErr - Write to $%08X  @ $%08X\n", BusErrAdr, M68000_GETPC);
	else
		fprintf(fp, "BusErr - Read from $%08X  @ $%08X\n", BusErrAdr, M68000_GETPC);
	fclose(fp);
#endif
}


void FASTCALL Memory_IntErr(int i)
{
#ifdef WIN68DEBUG
	FILE *fp;
	fp=fopen("_interr.txt", "a");
	fprintf(fp, "IntErr - Int.No%d  @ $%08X\n", i, M68000_GETPC);
	fclose(fp);
#else
	(void)i;
#endif
}
#endif	// _DEBUG


void Memory_Init(void)
{
}

// ---------------------- メモリアクセス部 → MEMORY.ASM に移動 -----------------------
#ifndef USE_68KEM
#include <assert.h>
#include "adpcm.h"
#include "bg.h"
#include "crtc.h"
#include "dmac.h"
#include "fdc.h"
#include "gvram.h"
#include "mfp.h"
#include "ioc.h"
#include "irqh.h"
#include "palette.h"
#include "pia.h"
#include "rtc.h"
#include "sasi.h"
#include "scc.h"
#include "scsi.h"
#include "sram.h"
#include "sysport.h"
#include "tvram.h"
#include "../fmgen/fmg_wrap.h"

#ifdef NO_MERCURY
#define	Mcry_Read	rm_buserr
#define	Mcry_Write	wm_buserr
#else	// NO_MERCURY
#include "mercury.h"
#endif	// NO_MERCURY

#ifdef NO_MIDI
#define	MIDI_Read	rm_buserr
#define	MIDI_Write	wm_buserr
#else	// NO_MIDI
#include "midi.h"
#endif	// NO_MIDI

#ifdef NO_WINDRV
#define	WinDrv_Read	rm_nop
#define	WinDrv_Write	wm_nop
#else	// NO_WINDRV
#include "windrv.h"
#endif	// NO_WINDRV

static void FASTCALL wm_buserr(DWORD addr, IOBUS8 val);
static void FASTCALL wm_nop(DWORD addr, IOBUS8 val);

static IOBUS8 FASTCALL rm_font(DWORD addr);
static IOBUS8 FASTCALL rm_ipl(DWORD addr);
static IOBUS8 FASTCALL rm_nop(DWORD addr);
static IOBUS8 FASTCALL rm_buserr(DWORD addr);

static IOBUS8 (FASTCALL * MemReadTable[16 * 16])(DWORD) = {
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read, TVRAM_Read,
	CRTC_Read, VCtrl_Read, DMA_Read, rm_nop, MFP_Read, RTC_Read, rm_nop, SysPort_Read,
	OPM_Read, ADPCM_Read, FDC_Read, SASI_Read, SCC_Read, PIA_Read, IOC_Read, WinDrv_Read,
	SCSI_Read, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, MIDI_Read,
	BG_Read, BG_Read, BG_Read, BG_Read, BGRam_Read, BGRam_Read, BGRam_Read, BGRam_Read,
	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, Mcry_Read, rm_buserr,
	SRAM_Read, SRAM_Read, rm_nop, rm_nop, rm_nop, rm_nop, rm_nop, rm_nop, 
	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr,
	rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr, rm_buserr,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
	rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font, rm_font,
/* SCSI の場合は rm_buserr になる？ */
	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl,
	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl,
	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl,
	rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl, rm_ipl,
};

static void (FASTCALL * const MemWriteTable[16 * 16])(DWORD, IOBUS8) = {
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write, TVRAM_Write,
	CRTC_Write, VCtrl_Write, DMA_Write, wm_nop, MFP_Write, RTC_Write, wm_nop, SysPort_Write,
	OPM_Write, ADPCM_Write, FDC_Write, SASI_Write, SCC_Write, PIA_Write, IOC_Write, WinDrv_Write,
	SCSI_Write, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, MIDI_Write,
	BG_Write, wm_nop, wm_nop, wm_nop, BGRam_Write, BGRam_Write, BGRam_Write, BGRam_Write,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, Mcry_Write, wm_buserr,
	SRAM_Write, SRAM_Write, wm_nop, wm_nop, wm_nop, wm_nop, wm_nop, wm_nop,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
/* ROMエリアへの書きこみは全てバスエラー */
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
	wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr, wm_buserr,
};

// ---- word access

static IOBUS16 FASTCALL Bus16_Read(DWORD addr24)
{
	uint_fast16_t v;
	IOBUS8 (FASTCALL * func)(DWORD) = MemReadTable[(addr24 - 0xe00000) >> 13];
	v = (*func)(addr24) << 8;
	v += (*func)(addr24 + 1);
	return v;
}

static void FASTCALL Bus16_Write(DWORD addr24, IOBUS16 val16)
{
	void (FASTCALL * func)(DWORD, IOBUS8) = MemWriteTable[(addr24 - 0xe00000) >> 13];
	(*func)(addr24, (IOBUS8)(val16 >> 8));
	if (BusErrFlag == 0)
	{
		(*func)(addr24 + 1, (IOBUS8)(val16 & 0xff));
	}
}

static IOBUS16 FASTCALL rm16_font(DWORD addr)
{
	return (FONT[addr - 0xf00000] << 8) + FONT[addr + 1 - 0xf00000];
}

static IOBUS16 FASTCALL rm16_ipl(DWORD addr)
{
	return *(uint16_t *)(&IPL[addr - 0xfc0000]);
}

static IOBUS16 FASTCALL rm16_buserr(DWORD addr)
{
	SetBusErrFlag(1, addr);
	return 0;
}

static IOBUS16 FASTCALL rm16_nop(DWORD addr)
{
	return 0;
}

static void FASTCALL wm16_buserr(DWORD addr, IOBUS16 val)
{
	SetBusErrFlag(2, addr);
	(void)val;
}

static void FASTCALL wm16_nop(DWORD addr, IOBUS16 val)
{
}

static IOBUS16 (FASTCALL * const Mem16ReadTable[32 * 4])(DWORD) =
{
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e0
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e1
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e2
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e3
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e4
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e5
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e6
	TVRAM16_Read, TVRAM16_Read, TVRAM16_Read, TVRAM16_Read,		// e7
	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read,				// e8
	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read,				// e9
	Bus16_Read, rm16_buserr, rm16_buserr, Bus16_Read,			// ea
	BG16_Read, BG16_Read, BGRam16_Read, BGRam16_Read,			// eb
	rm16_buserr, rm16_buserr, rm16_buserr, Bus16_Read,			// ec
	SRAM16_Read, rm16_nop, rm16_nop, rm16_nop,					// ed
	rm16_buserr, rm16_buserr, rm16_buserr, rm16_buserr,			// ee
	rm16_buserr, rm16_buserr, rm16_buserr, rm16_buserr,			// ef
	rm16_font, rm16_font, rm16_font, rm16_font,					// f0
	rm16_font, rm16_font, rm16_font, rm16_font,					// f1
	rm16_font, rm16_font, rm16_font, rm16_font,					// f2
	rm16_font, rm16_font, rm16_font, rm16_font,					// f3
	rm16_font, rm16_font, rm16_font, rm16_font,					// f4
	rm16_font, rm16_font, rm16_font, rm16_font,					// f5
	rm16_font, rm16_font, rm16_font, rm16_font,					// f6
	rm16_font, rm16_font, rm16_font, rm16_font,					// f7
	rm16_font, rm16_font, rm16_font, rm16_font,					// f8
	rm16_font, rm16_font, rm16_font, rm16_font,					// f9
	rm16_font, rm16_font, rm16_font, rm16_font,					// fa
	rm16_font, rm16_font, rm16_font, rm16_font,					// fb
	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read, 			// fc
	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read, 			// fd
	rm16_ipl, rm16_ipl, rm16_ipl, rm16_ipl,						// fe
	rm16_ipl, rm16_ipl, rm16_ipl, rm16_ipl,						// ff
};

static void (FASTCALL * const Mem16WriteTable[32 * 4])(DWORD, IOBUS16) =
{
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e0
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e1
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e2
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e3
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e4
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e5
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e6
	TVRAM16_Write, TVRAM16_Write, TVRAM16_Write, TVRAM16_Write,	// e7
	CrtVC16_Write, Bus16_Write, Bus16_Write, Bus16_Write,		// e8
	Bus16_Write, Bus16_Write, Bus16_Write, Bus16_Write,			// e9
	Bus16_Write, wm16_buserr, wm16_buserr, Bus16_Write,			// ea
	BG16_Write, wm16_nop, BGRam16_Write, BGRam16_Write,			// eb
	wm16_buserr, wm16_buserr, wm16_buserr, Bus16_Write,			// ec
	SRAM16_Write, wm16_nop, wm16_nop, wm16_nop,					// ed
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// ee
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// ef
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f0
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f1
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f2
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f3
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f4
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f5
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f6
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f7
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f8
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// f9
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// fa
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// fb
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// fc
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// fd
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// fe
	wm16_buserr, wm16_buserr, wm16_buserr, wm16_buserr,			// ff
};

// ---- bus error

INLINE void
AdrError(DWORD addr)
{
	BusErrAdr = addr;
#ifdef CYCLONE
	m68k.irq = VECT_ADRERR;
#else	// CYCLONE
	C68k_Set_IRQ(&C68K, VECT_ADRERR);
#endif	// CYCLONE
}

INLINE void
BusError()
{
#ifdef _DEBUG
	Memory_ErrTrace();
#endif	// _DEBUG

#ifdef CYCLONE
	m68k.irq = VECT_BUSERR;
#else	// CYCLONE
	C68k_Set_IRQ(&C68K, VECT_BUSERR);
#endif	// CYCLONE
	BusErrFlag = 0;
}

/*
 * write function
 */
INLINE void FASTCALL
wm_ext(uint32_t addr24, uint_fast8_t val)
{
	if (addr24 < 0x00c00000)
	{
		wm_buserr(addr24, val);
	}
	else if (addr24 < 0x00e00000)
	{
		GVRAM_Write(addr24, val);
	}
	else
	{
		MemWriteTable[(addr24 - 0x00e00000) >> 13](addr24, val);
	}
}

INLINE void FASTCALL
wm_cnt(uint32_t addr, uint_fast8_t val)
{
	addr &= 0x00ffffff;
	if (addr < 0x00a00000)		// RAM 10MB
	{
		MEM[ADRSWAPW(addr)] = (uint8_t)val;
	}
	else
	{
		wm_ext(addr, val);
	}
}

INLINE void FASTCALL
wm_main(uint32_t addr, uint_fast8_t val)
{
	if (BusErrFlag == 0)
		wm_cnt(addr, val);
}

INLINE void FASTCALL
wm_ext_w(uint32_t addr24, uint_fast16_t val)
{
	if (addr24 < 0x00c00000)
	{
		wm_buserr(addr24, val);
	}
	else if (addr24 < 0x00e00000)
	{
		GVRAM16_Write(addr24, val);
	}
	else
	{
#if 1
		Mem16WriteTable[(addr24 - 0x00e00000) >> 14](addr24, LOW16(val));
#else	// 1
		void (FASTCALL * func)(DWORD, IOBUS8) = MemWriteTable[(addr24 - 0x00e00000) >> 13];
		(*func)(addr24, (uint8_t)(val >> 8));
		if (BusErrFlag == 0)
			(*func)(addr24 + 1, (uint8_t)val);
#endif	// 1
	}
}

static void FASTCALL
wm_cnt_w(uint32_t addr, uint_fast16_t val)
{
	addr &= 0x00ffffff;
	if (addr < 0x00a00000)		// RAM 10MB
	{
		*(uint16_t *)(&MEM[addr]) = (uint16_t)val;
	}
	else
	{
		wm_ext_w(addr, val);
	}
}

INLINE void FASTCALL
wm_main_w(uint32_t addr, uint_fast16_t val)
{
	if (BusErrFlag == 0)
		wm_cnt_w(addr, val);
}

void FASTCALL
dma_writemem24(uint32_t addr, uint_fast8_t val)
{
	wm_main(addr, val);
}

void FASTCALL
dma_writemem24_word(uint32_t addr, uint_fast16_t val)
{
	if (addr & 1) {
		SetBusErrFlag(4, addr);
		return;
	}

	wm_main_w(addr, val);
}

void FASTCALL
dma_writemem24_dword(uint32_t addr, uint32_t val)
{
	if (addr & 1) {
		SetBusErrFlag(4, addr);
		return;
	}

	wm_main_w(addr, (uint_fast16_t)(val >> 16));
	wm_main_w(addr + 2, (uint_fast16_t)(val));
}

EXTERNC void FASTCALL
cpu_writemem24(uint32_t addr, uint_fast8_t val)
{
	addr = LOW24(addr);
	if (addr < 0x00a00000)
	{
		MEM[ADRSWAPW(addr)] = val;
	}
	else
	{
		wm_ext(addr, val);
		if (BusErrFlag)
		{
			BusError();
		}
	}
}

EXTERNC void FASTCALL
cpu_writemem24_word(uint32_t addr, uint_fast16_t val)
{
	if (addr & 1) {
		AdrError(addr);
		return;
	}

	addr = LOW24(addr);
	if (addr < 0x00a00000)
	{
		*(uint16_t *)(&MEM[addr]) = val;
	}
	else
	{
		wm_ext_w(addr, val);
		if (BusErrFlag)
		{
			BusError();
		}
	}
}

EXTERNC void FASTCALL
cpu_writemem24_dword(uint32_t addr, uint32_t val)
{
	if (addr & 1) {
		AdrError(addr);
		return;
	}

	wm_cnt_w(addr, (uint_fast16_t)(val >> 16));
	wm_main_w(addr + 2, (uint_fast16_t)val);
	if (BusErrFlag) {
		BusError();
	}
}

static void FASTCALL
wm_buserr(DWORD addr, IOBUS8 val)
{
	SetBusErrFlag(2, addr);
	(void)val;
}

static void FASTCALL
wm_nop(DWORD addr, IOBUS8 val)
{
	/* Nothing to do */
	(void)addr;
	(void)val;
}

/*
 * read function
 */
INLINE uint_fast8_t FASTCALL
rm_ext(uint32_t addr24)
{
	if (addr24 < 0x00c00000) {
		return rm_buserr(addr24);
	} else if (addr24 < 0x00e00000) {
		return GVRAM_Read(addr24);
	} else {
		return MemReadTable[(addr24 - 0x00e00000) >> 13](addr24);
	}
}

/*
 * read function
 */
INLINE uint_fast8_t FASTCALL
rm_main(uint32_t addr)
{
	addr &= 0x00ffffff;
	if (addr < 0x00a00000)		// RAM 10MB
	{
		return MEM[ADRSWAPW(addr)];
	}
	else
	{
		return rm_ext(addr);
	}
}

INLINE uint_fast16_t FASTCALL
rm_ext_w(uint32_t addr24)
{
	if (addr24 < 0x00c00000)
	{
		return rm_buserr(addr24);
	}
	else if (addr24 < 0x00e00000)
	{
		return GVRAM16_Read(addr24);
	}
	else
	{
#if 1
		return Mem16ReadTable[(addr24 - 0x00e00000) >> 14](addr24);
#else	// 1
		IOBUS8 (FASTCALL * func)(DWORD) = MemReadTable[(addr24 - 0x00e00000) >> 13];
		uint_fast16_t v;
		v = (*func)(addr24) << 8;
		v += (*func)(addr24 + 1);
		return v;
#endif	// 1
	}
}

static uint_fast16_t FASTCALL
rm_main_w(uint32_t addr)
{
	addr &= 0x00ffffff;
	if (addr < 0x00a00000)		// RAM 10MB
	{
		return *(uint16_t *)(&MEM[addr]);
	}
	else
	{
		return rm_ext_w(addr);
	}
}

uint_fast8_t FASTCALL
dma_readmem24(uint32_t addr)
{
	return rm_main(addr);
}

uint_fast16_t FASTCALL
dma_readmem24_word(uint32_t addr)
{
	if (addr & 1) {
		SetBusErrFlag(3, addr);
		return 0;
	}

	return rm_main_w(addr);
}

uint32_t FASTCALL
dma_readmem24_dword(uint32_t addr)
{
	DWORD v;

	if (addr & 1) {
		SetBusErrFlag(3, addr);
		return 0;
	}

	v = rm_main_w(addr) << 16;
	v += rm_main_w(addr + 2);
	return v;
}

EXTERNC uint32_t FASTCALL
cpu_readmem24(uint32_t addr)
{
	addr = LOW24(addr);
	if (addr < 0x00a00000)
	{
		return MEM[ADRSWAPW(addr)];
	}
	else
	{
		const uint32_t v = rm_ext(addr);
		if (BusErrFlag)
		{
			BusError();
		}
		return v;
	}
}

EXTERNC uint32_t FASTCALL
cpu_readmem24_word(uint32_t addr)
{
	if (addr & 1) {
		AdrError(addr);
		return 0;
	}

	addr = LOW24(addr);
	if (addr < 0x00a00000)
	{
		return *(uint16_t *)(&MEM[addr]);
	}
	else
	{
		const uint32_t v = rm_ext_w(addr);
		if (BusErrFlag)
		{
			BusError();
		}
		return v;
	}
}

EXTERNC uint32_t FASTCALL
cpu_readmem24_dword(uint32_t addr)
{
	uint32_t v;

	if (addr & 1) {
		AdrError(addr);
		return 0;
	}

	v = rm_main_w(addr) << 16;
	v += rm_main_w(addr + 2);
	if (BusErrFlag) {
		BusError();
	}
	return v;
}

static IOBUS8 FASTCALL
rm_font(DWORD addr)
{
	return FONT[addr - 0xf00000];
}

static IOBUS8 FASTCALL
rm_ipl(DWORD addr)
{
	return IPL[ADRSWAPW(addr) - 0xfc0000];
}

static IOBUS8 FASTCALL
rm_nop(DWORD addr)
{
	(void)addr;
	return 0;
}

static IOBUS8 FASTCALL
rm_buserr(DWORD addr)
{
	SetBusErrFlag(1, addr);
	return 0;
}

/*
 * Memory misc
 */

void FASTCALL
Memory_SetSCSIMode(void)
{
	int i;

	for (i = 0xe0; i < 0xf0; i++) {
		MemReadTable[i] = rm_buserr;
	}
}

#endif	// !USE_68KEM
