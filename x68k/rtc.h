#ifndef _x68k_rtc
#define _x68k_rtc

#include "x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void RTC_Init(void);
IOBUS8 FASTCALL RTC_Read(DWORD adr);
void FASTCALL RTC_Write(DWORD adr, IOBUS8 data);
void RTC_Timer(int clock);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_x68k_rtc
