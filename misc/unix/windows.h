/**
 * @file	windows.h
 * @brief	definitions for generic international windows functions
 */

#pragma once

#include <memory.h>

#define __stdcall

// winnt.h
typedef unsigned char	BYTE;
typedef unsigned short	WORD;
typedef unsigned int	DWORD;

typedef signed int		INT;
typedef unsigned int	UINT;

typedef int				BOOL;

typedef char			TCHAR;
typedef char*			LPTSTR;
typedef const char*		LPCTSTR;
#define TEXT(x)			x

// winbase.h
#define lstrcat			strcat
#define lstrcmp			strcmp
#define lstrcpy			strcpy
#define lstrcpyn(a, b, c)	strcpy(a, b)
#define lstrlen			strlen
#define wsprintf		sprintf

#define ZeroMemory(a, b)	memset(a, 0, b)

#include "../winapi/privateprofile.h"

// windef.h
#define FALSE			0
#define TRUE			1
#define MAX_PATH		260

// tchar.h
#define _fgetts			fgets
#define _ftprintf		fprintf
#define _stscanf		scanf
#define _stprintf		sprintf
#define _tfopen			fopen
#define _tprintf		printf
#define _ttoi			atoi
#define _vstprintf		vsprintf

#include "../winapi/timeapi.h"
