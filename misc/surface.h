﻿/**
 * @file	surface.h
 * @brief	サーフェス クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief サーフェス クラス
 */
struct Surface
{
	int nWidth;				//!< size of the bitmap in pixels
	int nHeight;			//!< size of the bitmap in pixels
	int nBpp;				//!< bits of pixels
	int nYAlign;			//!< bytes of each line
	uint8_t* pBuffer;		//!< buffer
	uint32_t* pPalettes;	//!< palettes

#ifdef __cplusplus
	Surface();
	Surface(const Surface* parent, int x, int y, int width, int height);
	Surface(int width, int height, int bpp, int yalign, void* buffer, uint32_t* palettes = NULL);
	~Surface();
	void Bind(const Surface* bmp, int x, int y, int width, int height);
	void Bind(int width, int height, int bpp, int yalign, void* buffer, uint32_t* palettes = NULL);
	void Destroy();
	void* Ptr(int x, int y) const;
	unsigned int Get(int x, int y) const;
	uint32_t GetColor(int x, int y) const;
	void Set(int x, int y, unsigned int color);
	void Clear(unsigned int color);
	void Fill(int x, int y, int cx, int cy, unsigned int color);
	void Copy(int x, int y, int sx, int sy, int cx, int cy);
	int Width() const;
	int Height() const;
	int Bpp() const;					//!< bits of pixels
	int Align() const;					//!< bytes of each line
	unsigned int LineBytes() const;
	uint8_t* Line(int y) const;			//!< pointers to the start of each line
	void Put(const Surface& surface, int px, int py);
	void SetPat(const uint8_t* lpPattern, int x, int y, unsigned int nColor);
	void Text8(int x, int y, const char* lpString, unsigned int nColor, bool bPitch = false);
	void Text12(int x, int y, const char* lpString, unsigned int nColor, bool bPitch = false);
#endif	// __cplusplus
};

#ifdef __cplusplus
/**
 * 幅
 * @return 幅
 */
inline int Surface::Width() const
{
	return this->nWidth;
}

/**
 * 高さ
 * @return 高さ
 */
inline int Surface::Height() const
{
	return this->nHeight;
}

/**
 * BPP
 * @return BPP
 */
inline int Surface::Bpp() const
{
	return this->nBpp;
}

/**
 * Y アライン
 * @return Y アライン
 */
inline int Surface::Align() const
{
	return this->nYAlign;
}

/**
 * ライン バッファ
 * @param[in] y Y
 * @return バッファ
 */
inline uint8_t* Surface::Line(int y) const
{
	return this->pBuffer + (y * this->nYAlign);
}
#endif	// __cplusplus
