﻿/*!
 * @file	thread.h
 * @brief	スレッド基底クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <thread>

#undef Yield

/**
 * @brief スレッド基底クラス
 */
class CThread
{
public:
	/**
	 * @brief コンストラクタ
	 */
	CThread()
		: m_pThread(NULL)
	{
	}

	/**
	 * @brief デストラクタ
	 */
	virtual ~CThread()
	{
		Join();
	}

	/**
	 * @brief スレッド開始
	 * @retval true 成功
	 */
	bool Start()
	{
		if (m_pThread != NULL)
		{
			return false;
		}
		m_pThread = new std::thread(&CThread::ThreadProc, this);
		return true;
	}

	/**
	 * @brief スレッド終了
	 */
	void Join()
	{
		std::thread* pThread = m_pThread;
		m_pThread = NULL;
		if (pThread)
		{
			pThread->join();
			delete pThread;
		}
	}

	/**
	 * スレッド実行中?
	 * @retval true 実行中
	 * @retval false 実行していない
	 */
	bool IsRunning() const
	{
		return (m_pThread != NULL);
	}

	/**
	 * 現在のスレッドか?
	 * @retval true 現在のスレッド
	 * @retval false 別のスレッド
	 */
	bool IsCurrent() const
	{
		return (m_pThread) && (std::this_thread::get_id() == m_pThread->get_id());
	}

	/**
	 * スリープ
	 * @param[in] msec ミリ秒
	 */
	static void Delay(unsigned int msec)
	{
		std::chrono::milliseconds dura(msec);
		std::this_thread::sleep_for(dura);
	}

	/**
	 * イールド
	 */
	static void Yield()
	{
		std::this_thread::yield();
	}

	/**
	 * スレッド ネイティブ ハンドルを返す
	 * @return ネイティブ ハンドル
	 */
	void* NativeHandle()
	{
		if (m_pThread)
		{
			return reinterpret_cast<void*>(m_pThread->native_handle());
		}
		else
		{
			return NULL;
		}
	}

protected:
	virtual void Task() = 0;	/*!< スレッド */

private:
	std::thread* m_pThread;		/*!< スレッド */

	/**
	 * スレッド処理
	 * @param[in] self this ポインタ
	 */
	static void ThreadProc(CThread* self)
	{
		self->Task();
	}
};
