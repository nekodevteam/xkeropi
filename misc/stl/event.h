﻿/*!
 * @file	event.h
 * @brief	イベント クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <condition_variable>
#include <mutex>

/**
 * @brief イベント クラス
 */
class CEvent
{
public:
	/**
	 * コンストラクタ
	 * @param[in] bInitialState 初期ステート
	 */
	CEvent(bool bInitialState)
		: m_bFlag(bInitialState)
	{
	}

	/**
	 * シグナル
	 */
	void Signal()
	{
		m_mutex.lock();
		m_bFlag = true;
		m_mutex.unlock();
		m_condition.notify_all();
	}

	/**
	 * リセット
	 */
	void Reset()
	{
		m_mutex.lock();
		m_bFlag = false;
		m_mutex.unlock();
	}

	/**
	 * イベント待ち
	 */
	void Wait()
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_condition.wait(lock, [&]()->bool{ return m_bFlag; });
	}

	/**
	 * イベント待ち
	 * @param[in] msec タイムアウト
	 * @retval true イベントあり
	 * @retval false タイムアウトした
	 */
	bool Wait(unsigned int msec)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		return m_condition.wait_for(lock, std::chrono::milliseconds(msec), [&]()->bool{ return m_bFlag; });
	}

private:
	bool m_bFlag;							/*!< フラグ */
	std::mutex m_mutex;						/*!< guard */
	std::condition_variable m_condition;	/*!< コンディション */
};
