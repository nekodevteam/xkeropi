﻿/**
 * @file	endianutil.h
 * @brief	エンディアン クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#pragma pack(push, 1)

/**
 * @brief 24bit リトルエンディアン
 */
struct uint24le_t
{
	uint8_t b[3];		/*!< 値 */

#if defined(__cplusplus)
	/**
	 * 32ビット値取得
	 * @return 値
	 */
	operator unsigned int() const
	{
		return static_cast<unsigned int>(b[0] | (b[1] << 8) | (b[2] << 16));
	}

	/**
	 * 24ビット値設定
	 * @param[in] value 値
	 * @return 自分
	 */
	const uint24le_t& operator=(unsigned int value)
	{
		b[0] = static_cast<uint8_t>(value);
		b[1] = static_cast<uint8_t>(value >> 8);
		b[2] = static_cast<uint8_t>(value >> 16);
		return *this;
	}
#endif	/* defined(__cplusplus) */
};

#pragma pack(pop)
