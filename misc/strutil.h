﻿/**
 * @file	strutil.h
 * @brief	文字列ユーティリティのヘッダファイルです
 */

#pragma once

#include <algorithm>
#include <memory.h>
#include <stdarg.h>
#include <vector>
#include "tstring.h"

namespace StrUtil
{
	/**
	 * テキストを読む
	 * @param[in] fp ファイル
	 * @return テキスト
	 */
	static std::string ReadFile(FILE* fp)
	{
		fseek(fp, 0, SEEK_END);
		const size_t nSize = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		std::string ret;
		if (nSize)
		{
			ret.resize(nSize);
			const size_t length = fread(&ret.at(0), nSize, sizeof(char), fp);
			ret.resize(length);
		}
		return ret;
	}

	/**
	 * テキストを読む
	 * @param[in] lpFilename ファイル名
	 * @return テキスト
	 */
	static std::string ReadFile(const char* lpFilename)
	{
		std::string ret;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (fopen_s(&fp, lpFilename, "rb") == 0)
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp = fopen(lpFilename, "rb");
		if (fp)
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		{
			ret = ReadFile(fp);
			fclose(fp);
		}
		return ret;
	}

#ifdef _WIN32
	/**
	 * テキストを読む
	 * @param[in] lpFilename ファイル名
	 * @return テキスト
	 */
	static std::string ReadFile(const wchar_t* lpFilename)
	{
		std::string ret;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (_wfopen_s(&fp, lpFilename, L"rb") == 0)
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp = _wfopen(lpFilename, L"rb");
		if (fp)
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		{
			ret = ReadFile(fp);
			fclose(fp);
		}
		return ret;
	}
#endif	// _WIN32

	/**
	 * テキストを書く
	 * @param[in] fp ファイル
	 * @param[in] str テキスト
	 * @retval true 成功
	 * @retval false 失敗
	 */
	static bool WriteFile(FILE* fp, const std::string& str)
	{
		bool ret = false;
		const size_t n = str.length();
		if (n)
		{
			ret = (fwrite(str.data(), n, sizeof(char), fp) == n);
		}
		return ret;
	}

	/**
	 * テキストを書く
	 * @param[in] lpFilename ファイル名
	 * @param[in] str テキスト
	 * @retval true 成功
	 * @retval false 失敗
	 */
	static bool WriteFile(const char* lpFilename, const std::string& str)
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (fopen_s(&fp, lpFilename, "wb") == 0)
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp = fopen(lpFilename, "wb");
		if (fp)
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		{
			ret = WriteFile(fp, str);
			fclose(fp);
		}
		return ret;
	}

#ifdef _WIN32
	/**
	 * テキストを書く
	 * @param[in] lpFilename ファイル名
	 * @param[in] str テキスト
	 * @retval true 成功
	 * @retval false 失敗
	 */
	static bool WriteFile(const wchar_t* lpFilename, const std::string& str)
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (_wfopen_s(&fp, lpFilename, L"wb") == 0)
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp = _wfopen(lpFilename, L"wb");
		if (fp)
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		{
			ret = WriteFile(fp, str);
			fclose(fp);
		}
		return ret;
	}
#endif	// _WIN32

	/**
	 * UTF-16 から UTF-8 に変換
	 * @param[in] utf16 UTF-16 文字列
	 * @return UTF-8
	 */
	static std::string Utf16ToUtf8(const std::wstring& utf16)
	{
		std::string ret;
		for (std::wstring::const_iterator it = utf16.begin(); it != utf16.end(); ++it)
		{
			const unsigned int c = *it;
			if (c >= 0x800)
			{
				ret += static_cast<char>(0xe0 | (c >> 12));
				ret += static_cast<char>(0x80 | ((c >> 6) & 0x3f));
				ret += static_cast<char>(0x80 | (c & 0x3f));
			}
			else if (c >= 0x80)
			{
				ret += static_cast<char>(0xc0 | (c >> 6));
				ret += static_cast<char>(0x80 | (c & 0x3f));
			}
			else
			{
				ret += static_cast<char>(c);
			}
		}
		return ret;
	}

	/**
	 * UTF-8 から UTF-16 に変換
	 * @param[in] utf8 UTF-8 文字列
	 * @return UTF-16
	 */
	static std::wstring Utf8ToUtf16(const std::string& utf8)
	{
		std::wstring ret;
		std::string::const_iterator it = utf8.begin();
		while (it != utf8.end())
		{
			const char c = *it++;
			if ((c & 0x80) == 0)
			{
				ret += static_cast<wchar_t>(c);
			}
			else if (static_cast<unsigned char>(c) < 0xfe)
			{
				unsigned char bit = 0x40;
				while (c & bit)
				{
					bit >>= 1;
				}
				unsigned int u = c & (bit - 1);
				while (it != utf8.end())
				{
					const char c = *it;
					if ((c & 0xc0) != 0x80)
					{
						break;
					}
					u = (u << 6) | (c & 0x3f);
					++it;
				}
				ret += static_cast<wchar_t>(u);
			}
		}
		return ret;
	}

	/**
	 * フォーマット
	 * @param[in] lpFormat 書式
	 * @return 文字列
	 */
	static std::string Format(const char* lpFormat, ...)
	{
		va_list argptr;
		va_start(argptr, lpFormat);
		char tmp[1024];
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		vsprintf_s(tmp, lpFormat, argptr);
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		vsprintf(tmp, lpFormat, argptr);
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		va_end(argptr);
		return std::string(tmp);
	}

#ifdef _WIN32
	/**
	 * フォーマット
	 * @param[in] lpFormat 書式
	 * @return 文字列
	 */
	static std::wstring Format(const wchar_t* lpFormat, ...)
	{
		va_list argptr;
		va_start(argptr, lpFormat);
		wchar_t tmp[1024];
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		vswprintf_s(tmp, lpFormat, argptr);
#else	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		vswprintf(tmp, lpFormat, argptr);
#endif	// (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		va_end(argptr);
		return std::wstring(tmp);
	}
#endif	// _WIN32

	/**
	 * 文字列分割
	 * @param[in] str 文字列
	 * @param[in] t セパレータ
	 * @return 文字列配列
	 */
	template<class charT>
	static std::vector<std::basic_string<charT> > Split(const std::basic_string<charT>& str, charT t)
	{
		std::vector<std::basic_string<charT>> ret;
		typename std::basic_string<charT>::size_type index = 0;
		do
		{
			const typename std::basic_string<charT>::size_type pos = str.find(t, index);
			if (pos == std::basic_string<charT>::npos)
			{
				break;
			}
			ret.push_back(str.substr(index, pos - index));
			index = pos + 1;
		} while (true /*CONSTCOND*/);
		ret.push_back(str.substr(index));
		return ret;
	}

	/**
	 * 改行を得る
	 * @param[in] str 文字列
	 * @param[in] index インデックス
	 * @return インデックス
	 */
	static std::string::size_type EndOfLine(const std::string& str, std::string::size_type index = 0)
	{
		return str.find_first_of("\r\n", index);
	}

#ifdef _WIN32
	/**
	 * 改行を得る
	 * @param[in] str 文字列
	 * @param[in] index インデックス
	 * @return インデックス
	 */
	static std::wstring::size_type EndOfLine(const std::wstring& str, std::wstring::size_type index = 0)
	{
		return str.find_first_of(L"\r\n", index);
	}
#endif	// _WIN32

	/**
	 * 次行を得る
	 * @param[in] str 文字列
	 * @param[in] index インデックス
	 * @return インデックス
	 */
	static std::string::size_type NextLine(const std::string& str, std::string::size_type index = 0)
	{
		index = EndOfLine(str, index);
		if (index != std::string::npos)
		{
			const char c = str.at(index++);
			if ((c == '\r') && (index < str.size()) && (str.at(index) == '\n'))
			{
				index++;
			}
		}
		return index;
	}

	/**
	 * ホワイトスペースか?
	 * @param[in] c キャラクタ
	 * @retval true スペース
	 * @retval false 文字
	 */
	inline static bool IsWhiteSpace(int c)
	{
		return (c >= '\0') && (c <= ' ');
	}

	/**
	 * トリミング
	 * @param[in] str 文字列
	 * @return インデックス
	 */
	template<class T>
	static T Trim(const T& str)
	{
		typename T::size_type left = 0;
		typename T::size_type right = str.size();
		while ((left < right) && (IsWhiteSpace(str.at(left))))
		{
			left++;
		}
		while ((left > right) && (IsWhiteSpace(str.at(right - 1))))
		{
			right--;
		}
		return str.substr(left, right - left);
	}

	/**
	 * 文字長さ
	 * @param[in] str 文字列
	 * @return 長さ
	 */
	template<class charT>
	static typename std::basic_string<charT>::size_type Length(const charT* str)
	{
		typename std::basic_string<charT>::size_type ret = 0;
		if (str)
		{
			while (str[ret])
			{
				ret++;
			}
		}
		return ret;
	}

	/**
	 * 開始
	 * @param[in] str 文字列
	 * @param[in] t 文字列
	 * @retval true 一致
	 * @retval false 不一致
	 */
	template<class charT>
	static bool StartsWith(const std::basic_string<charT>& str, const charT* t)
	{
		const typename std::basic_string<charT>::size_type length = Length(t);
		const typename std::basic_string<charT>::size_type size = str.size();
		return (length) && (length <= size) && (memcmp(&str.at(0), t, length * sizeof(charT)) == 0);
	}

	/**
	 * 終了
	 * @param[in] str 文字列
	 * @param[in] t 文字列
	 * @retval true 一致
	 * @retval false 不一致
	 */
	template<class charT>
	static bool EndsWith(const std::basic_string<charT>& str, const charT* t)
	{
		const typename std::basic_string<charT>::size_type length = Length(t);
		const typename std::basic_string<charT>::size_type size = str.size();
		return (length) && (length <= size) && (memcmp(&str.at(size - length), t, length * sizeof(charT)) == 0);
	}

	/**
	 * 比較
	 * @param[in] c1 文字
	 * @param[in] c2 文字
	 * @param[in] f 大文字小文字を同一視する
	 * @return 結果
	 */
	inline static int Compare(int c1, int c2, bool f)
	{
		if (f)
		{
			c1 = tolower(c1);
			c2 = tolower(c2);
		}
		return c1 - c2;
	}

	/**
	 * 比較
	 * @param[in] str1 文字列
	 * @param[in] str2 文字列
	 * @param[in] f 大文字小文字を同一視する
	 * @return 結果
	 */
	template<class charT>
	static int Compare(const std::basic_string<charT>& str1, const charT* str2, bool f)
	{
		const typename std::basic_string<charT>::size_type size1 = str1.size();
		const typename std::basic_string<charT>::size_type size2 = Length(str2);
		const typename std::basic_string<charT>::size_type size = (std::min)(size1, size2);
		typename std::basic_string<charT>::size_type index = 0;
		while (index < size)
		{
			const int ret = Compare(str1.at(index), str2[index], f);
			if (ret)
			{
				return ret;
			}
			index++;
		}
		return static_cast<int>(size1 - size2);
	}

	/**
	 * 比較
	 * @param[in] str1 文字列
	 * @param[in] str2 文字列
	 * @param[in] f 大文字小文字を同一視する
	 * @return 結果
	 */
	template<class charT>
	inline static int Compare(const std::basic_string<charT>& str1, const std::basic_string<charT>& str2, bool f)
	{
		return Compare(str1, str2.c_str(), f);
	}

}	// namespace StrUtil
