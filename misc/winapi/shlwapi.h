﻿/**
 * @file	shlwapi.h
 * @brief	パス関係クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

LPTSTR PathAddBackslash(LPTSTR pszPath);
BOOL PathAddExtension(LPTSTR pszPath, LPCTSTR pszExt);
BOOL PathAppend(LPTSTR pszPath, LPCTSTR pszMore);
LPTSTR PathCombine(LPTSTR pszDest, LPCTSTR pszDir, LPCTSTR pszFile);
LPTSTR PathFindExtension(LPCTSTR pszPath);
LPTSTR PathFindFileName(LPCTSTR pszPath);
BOOL PathMatchSpec(LPCTSTR pszFile, LPCTSTR pszSpec, unsigned int dwFlags);
void PathRemoveExtension(LPTSTR pszPath);
BOOL PathRemoveFileSpec(LPTSTR pszPath);
BOOL PathRenameExtension(LPTSTR pszPath, LPCTSTR pszExt);

#ifdef __cplusplus
}
#endif	// __cplusplus
