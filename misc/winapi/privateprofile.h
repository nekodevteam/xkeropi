﻿/**
 * @file	privateprofile.h
 * @brief	初期化ファイル クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

UINT GetPrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, INT nDefault, LPCTSTR lpFileName);
DWORD GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault, LPTSTR lpReturnedString, DWORD nSize, LPCTSTR lpFileName);
BOOL WritePrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpString, LPCTSTR lpFileName);

#ifdef __cplusplus
}
#endif	// __cplusplus
