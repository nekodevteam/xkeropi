﻿/*!
 * @file	timeapi.h
 * @brief	時間クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

DWORD timeGetTime();

#ifdef __cplusplus
}
#endif	// __cplusplus
