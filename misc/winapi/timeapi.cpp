﻿/*!
 * @file	timeapi.cpp
 * @brief	時間クラスの動作の定義を行います
 */

#include "timeapi.h"
#include <time.h>

/**
 * ミリ秒を得る
 * @return ミリ秒
 */
DWORD timeGetTime()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (ts.tv_sec * 1000) + (ts.tv_nsec / 1000000);
}
