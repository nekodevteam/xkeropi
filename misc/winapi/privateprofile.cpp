﻿/**
 * @file	privateprofile.cpp
 * @brief	初期化ファイル クラスの動作の定義を行います
 */

#include "privateprofile.h"
#include "../inifile.h"

static IniFile s_inifile;			/*!< ini ファイル */

/**
 * ファイル名を設定
 * @param[in] lpFileName ファイル名
 * @retval true 成功
 * @retval false 失敗
 */
static bool ReloadFile(LPCTSTR lpFileName)
{
	if (lpFileName == NULL)
	{
		return false;
	}
	if (s_inifile.GetFileName() != lpFileName)
	{
		s_inifile.Read(lpFileName);
	}
	return true;
}

/**
 * マルチ バイト コードに変換する
 * @param[in] lpString 文字列
 * @return 文字列
 */
inline static std::string ToMbcs(LPCTSTR lpString)
{
#ifdef _UNICODE
	return StrUtil::Utf16ToUtf8(lpString);
#else	// _UNICODE
	return std::string(lpString);
#endif	// _UNICODE
}

/**
 * 結果を格納する
 * @param[out] lpReturnedString 取得した文字列を受け取るバッファへのポインタ
 * @param[in] nSize lpReturnedString パラメーターが指すバッファーのサイズ
 * @param[in] lpResult 結果の文字列
 * @param[in] nResult 結果の文字列長
 * @return バッファーにコピーされた文字数
 */
static DWORD SetResultString(LPTSTR lpReturnedString, DWORD nSize, LPCTSTR lpResult, DWORD nResult)
{
	unsigned nNull = 1;
	if ((nResult + nNull) > nSize)
	{
		if (nSize >= 2)
		{
			nResult = nSize - 2;
			nNull = 2;
		}
		else
		{
			nResult = 0;
			nNull = nSize;
		}
	}

	if (lpReturnedString)
	{
		if ((lpResult) && (nResult))
		{
			memcpy(lpReturnedString, lpResult, nResult * sizeof(TCHAR));
		}
		if (nNull)
		{
			memset(lpReturnedString + nResult, 0, nNull * sizeof(TCHAR));
		}
	}
	return nResult;
}

/**
 * 結果を格納する
 * @param[out] lpReturnedString 取得した文字列を受け取るバッファへのポインタ
 * @param[in] nSize lpReturnedString パラメーターが指すバッファーのサイズ
 * @param[in] lpResult 結果の文字列
 * @return バッファーにコピーされた文字数
 */
static DWORD SetResultString(LPTSTR lpReturnedString, DWORD nSize, LPCTSTR lpResult)
{
	const DWORD cchResult = (lpResult) ? lstrlen(lpResult) : 0;
	return SetResultString(lpReturnedString, nSize, lpResult, cchResult);
}

/**
 * 結果を格納する
 * @param[out] lpReturnedString 取得した文字列を受け取るバッファへのポインタ
 * @param[in] nSize lpReturnedString パラメーターが指すバッファーのサイズ
 * @param[in] result 結果の文字列
 * @return バッファーにコピーされた文字数
 */
static DWORD SetResultString(LPTSTR lpReturnedString, DWORD nSize, const std::string& result)
{
	if (!result.empty())
	{
#ifdef _UNICODE
		const std::wstring res = StrUtil::Utf8ToUtf16(result);
		return SetResultString(lpReturnedString, nSize, &res.at(0), static_cast<DWORD>(res.size()));
#else	// _UNICODE
		return SetResultString(lpReturnedString, nSize, &result.at(0), static_cast<DWORD>(result.size()));
#endif	// _UNICODE
	}
	else
	{
		return SetResultString(lpReturnedString, nSize, NULL, 0);
	}
}

/**
 * 結果を格納する
 * @param[out] lpReturnedString 取得した文字列を受け取るバッファへのポインタ
 * @param[in] nSize lpReturnedString パラメーターが指すバッファーのサイズ
 * @param[in] results 結果の文字列
 * @return バッファーにコピーされた文字数
 */
static DWORD SetResultString(LPTSTR lpReturnedString, DWORD nSize, const std::vector<std::string>& results)
{
	std::string result;
	for (std::vector<std::string>::const_iterator it = results.begin(); it != results.end(); ++it)
	{
		if (!result.empty())
		{
			result += '\0';
		}
		result += *it;
	}
	return SetResultString(lpReturnedString, nSize, result);
}

/**
 * 初期化ファイルの指定したセクションのキーに関連付けられている整数を取得します
 * @param[in] lpAppName 初期化ファイル内のセクションの名前
 * @param[in] lpKeyName 値を取得するキーの名前
 * @param[in] nDefault 初期化ファイルにキー名が見つからない場合に返される既定値
 * @param[in] lpFileName 初期化ファイルの名前
 * @return 整数
 */
UINT GetPrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, INT nDefault, LPCTSTR lpFileName)
{
	if ((lpAppName == NULL) || (lpKeyName == NULL) || (!ReloadFile(lpFileName)))
	{
		return nDefault;
	}

	const std::string app = ToMbcs(lpAppName);
	const std::string key = ToMbcs(lpKeyName);
	const std::string def = StrUtil::Format("%d", nDefault);
	const std::string ret = s_inifile.GetString(app, key, def);
	return atoi(ret.c_str());
}

/**
 * 初期化ファイル内の指定したセクションから文字列を取得します
 * @param[in] lpAppName キー名を含むセクションの名前
 * @param[in] lpKeyName 関連付けられた文字列を取得するキーの名前
 * @param[in] lpDefault 既定の文字列
 * @param[out] lpReturnedString 取得した文字列を受け取るバッファへのポインタ
 * @param[in] nSize lpReturnedString パラメータが指すバッファのサイズ
 * @param[in] lpFileName 初期化ファイルの名前
 * @return バッファにコピーされた文字数
 */
DWORD GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault, LPTSTR lpReturnedString, DWORD nSize, LPCTSTR lpFileName)
{
	if (!ReloadFile(lpFileName))
	{
		return SetResultString(lpReturnedString, nSize, lpDefault);
	}
	else if (lpAppName == NULL)
	{
		return SetResultString(lpReturnedString, nSize, s_inifile.GetSections());
	}
	else
	{
		const std::string app = ToMbcs(lpAppName);
		if (lpKeyName == NULL)
		{
			return SetResultString(lpReturnedString, nSize, s_inifile.GetKeys(app));
		}
		else
		{
			const std::string key = ToMbcs(lpKeyName);
			const std::string def = ToMbcs(lpDefault);
			return SetResultString(lpReturnedString, nSize, s_inifile.GetString(app, key, def));
		}
	}
}

/**
 * 初期化ファイルの指定したセクションに文字列をコピーします
 * @param[in] lpAppName 文字列のコピー先となるセクションの名前
 * @param[in] lpKeyName 文字列に関連付けるキーの名前
 * @param[in] lpString ファイルに書き込まれる null で終わる文字列
 * @param[in] lpFileName 初期化ファイルの名前
 * @retval true 成功
 * @retval false 失敗
 */
BOOL WritePrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpString, LPCTSTR lpFileName)
{
	if ((lpAppName == NULL) || (lpKeyName == NULL) || (lpString == NULL) || (!ReloadFile(lpFileName)))
	{
		return FALSE;
	}

	const std::string app = ToMbcs(lpAppName);
	const std::string key = ToMbcs(lpKeyName);
	const std::string str = ToMbcs(lpString);
	s_inifile.SetString(app, key, str);
	return TRUE;
}
