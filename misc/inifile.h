﻿/**
 * @file	inifile.h
 * @brief	ini ファイル クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "inidata.h"

/**
 * @brief INI ファイル クラス
 */
class IniFile : public IniData
{
public:
	/**
	 * コンストラクタ
	 */
	IniFile()
#ifdef _UNICODE
		: m_bUtf16(false)
#endif	// _UNICODE
	{
	}

	/**
	 * コンストラクタ
	 * @param[in] lpFileName ファイル名
	 */
	IniFile(LPCTSTR lpFileName)
#ifdef _UNICODE
		: m_bUtf16(false)
#endif	// _UNICODE
	{
		Read(lpFileName);
	}

	/**
	 * デストラクタ
	 */
	~IniFile()
	{
		Flush();
	}

	/**
	 * ファイル名を得る
	 * @return ファイル名
	 */
	const std::tstring& GetFileName() const
	{
		return m_filename;
	}

	/**
	 * クリア
	 */
	void Clear()
	{
		m_filename.erase();
		Set(std::string());
	}

	/**
	 * リード
	 * @param[in] lpFileName ファイル名
	 */
	void Read(LPCTSTR lpFileName)
	{
		Flush();
		if (lpFileName)
		{
			m_filename = lpFileName;
			Set(StrUtil::ReadFile(lpFileName));
		}
		else
		{
			Clear();
		}
	}

	/**
	 * フラッシュ
	 */
	void Flush()
	{
		if ((m_bModified) && (!m_filename.empty()))
		{
			m_bModified = !StrUtil::WriteFile(m_filename.c_str(), Data());
		}
	}

protected:
	std::tstring m_filename;		/*!< ファイル名 */
#ifdef _UNICODE
	bool m_bUtf16;					/*!< コンバート */
#endif	// _UNICODE
};
