/**
 * @file	console.cpp
 * @brief	コンソール クラスの動作の定義を行います
 */

#include "console.h"

/**
 * コンソールを開く
 * @retval TRUE 成功
 * @retval FALSE 失敗
 */
BOOL ConsoleOpen()
{
	return Console::Open() ? TRUE : FALSE;
}

/**
 * コンソールを閉じる
 */
void ConsoleClose()
{
	Console::Close();
}

/**
 * コンソール ウィンドウを得る
 * @return HWND
 */
HWND ConsoleGetHwnd()
{
	return Console::GetHwnd();
}

/**
 * コンソールが開いているか?
 * @retval true 開いている
 * @retval false 開いていない
 */
BOOL ConsoleIsOpened()
{
	return (Console::GetHwnd() != NULL) ? TRUE : FALSE;
}
