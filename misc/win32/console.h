/**
 * @file	console.h
 * @brief	コンソール クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <windows.h>

#ifdef __cplusplus

#include <locale.h>
#include <stdio.h>
#include <tchar.h>

/**
 * @brief コンソール クラス
 */
class Console
{
public:
	/**
	 * コンソールを開く
	 * @retval true 成功
	 * @retval false 失敗
	 */
	static bool Open()
	{
		// コンソール ウィンドウの表示
		if (!::AllocConsole())
		{
			return false;
		}

#if (_MSC_VER >= 1400)

		// 標準出力の割り当て (VS2005以降)
		FILE* fp = NULL;
		_tfreopen_s(&fp, TEXT("CON"), TEXT("w"), stdout);

#else	// (_MSC_VER >= 1400)

		// 標準出力の割り当て (VS2003以前)
		_tfreopen(TEXT("CON"), TEXT("w"), stdout);

#endif	// (_MSC_VER >= 1400)

		// ハンドラ追加
		::SetConsoleCtrlHandler(HandlerRoutine, TRUE);

		_tsetlocale(LC_ALL, TEXT(""));

		// 閉じるボタンを抑制する
		HWND hWnd = GetHwnd();
		if (hWnd != NULL)
		{
			HMENU hMenu = ::GetSystemMenu(hWnd, 0);
			::RemoveMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
		}
		return true;
	}

	/**
	 * コンソールを閉じる
	 */
	static void Close()
	{
		::FreeConsole();

		// ハンドラ削除
		::SetConsoleCtrlHandler(HandlerRoutine, FALSE);
	}

	/**
	 * コンソール ウィンドウを得る
	 * @return HWND
	 */
	static HWND GetHwnd()
	{
	#if (_MSC_VER < 1300)

		HMODULE kernel32 = ::GetModuleHandle(TEXT("kernel32.dll"));

		typedef HWND (WINAPI * GetConsoleWindowFn)(VOID);
		GetConsoleWindowFn fn = reinterpret_cast<GetConsoleWindowFn>(::GetProcAddress(kernel32, "GetConsoleWindow"));
		if (fn)
		{
			return (*fn)();
		}
		return NULL;

#else	// (_MSC_VER < 1300)

		return ::GetConsoleWindow();

#endif	// (_MSC_VER < 1300)
	}

	/**
	 * コンソールが開いているか?
	 * @retval true 開いている
	 * @retval false 開いていない
	 */
	static bool IsOpened()
	{
		return (GetHwnd() != NULL);
	}

private:
	/**
	 * HandlerRoutine は、アプリケーション定義関数名のプレースホルダです
	 * @param[in] dwCtrlType 受け取った制御信号の種類を示します
	 * @return コンソール信号を処理するときは、TRUE を返してください。FALSE を返すと、このプロセスのハンドラのリストに登録されている次のハンドラ関数が使われます
	 */
	static BOOL WINAPI HandlerRoutine(DWORD dwCtrlType)
	{
		return (dwCtrlType == CTRL_C_EVENT) ? TRUE : FALSE;
	}
};

extern "C"
{
#endif	// __cplusplus

BOOL ConsoleOpen();
void ConsoleClose();
HWND ConsoleGetHwnd();
BOOL ConsoleIsOpened();

#ifdef __cplusplus
}
#endif	// __cplusplus
