﻿/**
 * @file	vstmidievent.h
 * @brief	VST MIDI クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <vector>
#include <pluginterfaces/vst2.x/aeffectx.h>

/**
 * @brief VST MIDI クラス
 */
class CVstMidiEvent
{
public:
	CVstMidiEvent(bool bRealtime = false);
	~CVstMidiEvent();
	void Clear();
	void ShortMessage(VstInt32 nTick, unsigned int nMessage);
	void LongMessage(VstInt32 nTick, const void* lpMessage, unsigned int cbMessage);
	const VstEvents* GetEvents();

protected:

private:
	unsigned int m_nEvents;					/*!< イベント数 */
	bool m_bRealtime;						/*!< リアルタイムか? */
	std::vector<unsigned char> m_header;	/*!< ヘッダ */
	std::vector<unsigned char> m_event;		/*!< イベント */
	void Add(const VstEvent* pEvent, const void* lpMessage = NULL, unsigned int cbMessage = 0);
};
