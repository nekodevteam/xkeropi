﻿/**
 * @file	vsteditwndbase.h
 * @brief	VST edit ウィンドウ基底クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief VST edit ウィンドウ基底クラス
 */
class IVstEditWnd
{
public:
	/**
	 * リサイズ
	 * @param[in] nWidth 幅
	 * @param[in] nHeight 高さ
	 * @retval true 成功
	 * @retval false 失敗
	 */
	virtual bool OnResize(int nWidth, int nHeight) = 0;

	/**
	 * 再描画要求
	 * @retval true 成功
	 * @retval false 失敗
	 */
	virtual bool OnUpdateDisplay() = 0;
};
