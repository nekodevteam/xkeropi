﻿/**
 * @file	vstbuffer.h
 * @brief	VST バッファ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief VST バッファ クラス
 */
class CVstBuffer
{
public:
	CVstBuffer();
	CVstBuffer(unsigned int nChannels, unsigned int nSamples);
	~CVstBuffer();
	void Alloc(unsigned int nChannels, unsigned int nSamples);
	void Delloc();
	void ZeroFill();
	float** GetBuffer();
	void GetShort(short* lpBuffer) const;

private:
	unsigned int m_nChannels;	/*!< チャンネル数 */
	unsigned int m_nSamples;	/*!< サンプル数 */
	float** m_pBuffers;			/*!< バッファ */
};

/**
 * バッファを得る
 * @return バッファ
 */
inline float** CVstBuffer::GetBuffer()
{
	return m_pBuffers;
}
