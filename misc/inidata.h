﻿/**
 * @file	inidata.h
 * @brief	ini データ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <vector>
#include "strutil.h"

/**
 * @brief INI データ クラス
 */
class IniData
{
public:
	/**
	 * コンストラクタ
	 */
	IniData()
		: m_bModified(false)
	{
	}

	/**
	 * コンストラクタ
	 * @param[in] r 初期内容
	 */
	IniData(const std::string& r)
		: m_ini(r)
		, m_bModified(false)
	{
	}

	/**
	 * セット
	 * @param[in] r 初期内容
	 */
	void Set(const std::string& r)
	{
		m_ini = r;
		m_bModified = false;
	}

	/**
	 * データ
	 * @return データ
	 */
	const std::string& Data() const
	{
		return m_ini;
	}

	/**
	 * セクション リストを得る
	 * @return セクション リスト
	 */
	std::vector<std::string> GetSections() const
	{
		std::vector<std::string> ret;

		std::string::size_type index = 0;
		while (index != std::string::npos)
		{
			std::string line = GetLine(index, &index);

			std::string section;
			if (GetSection(line, &section))
			{
				ret.push_back(section);
			}
		}

		return ret;
	}

	/**
	 * キー リストを得る
	 * @param[in] section セクション名
	 * @return キー リスト
	 */
	std::vector<std::string> GetKeys(const char* section) const
	{
		std::vector<std::string> ret;

		std::string::size_type index = 0;
		bool inSection = false;
		while (index != std::string::npos)
		{
			std::string line = GetLine(index, &index);

			std::string currentSection;
			if (GetSection(line, &currentSection))
			{
				inSection = (StrUtil::Compare(currentSection, section, true) == 0);
			}
			else if (inSection)
			{
				const std::string key = GetKey(line);
				if (!key.empty())
				{
					ret.push_back(key);
				}
			}
		}

		return ret;
	}

	/**
	 * キー リストを得る
	 * @param[in] section セクション名
	 * @return キー リスト
	 */
	inline std::vector<std::string> GetKeys(const std::string& section) const
	{
		return GetKeys(section.c_str());
	}

	/**
	 * 値を得る
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] def デフォルト値
	 * @return 値
	 */
	int GetInt(const char* section, const char* key, const int def = 0) const
	{
		std::string::size_type index = FindSectionKey(section, key);
		if (index != std::string::npos)
		{
			std::string line = GetLine(index, &index);
			index = line.find('=');
			if (index != std::string::npos)
			{
				const std::string r = StrUtil::Trim(line.substr(index + 1));
				return atoi(r.c_str());
			}
		}
		return def;
	}

	/**
	 * 値を得る
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] def デフォルト値
	 * @return 値
	 */
	inline int GetInt(const std::string& section, const std::string& key, const int def = 0) const
	{
		return GetInt(section.c_str(), key.c_str(), def);
	}

	/**
	 * 値を得る
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] def デフォルト値
	 * @return 値
	 */
	std::string GetString(const char* section, const char* key, const char* def = NULL) const
	{
		std::string::size_type index = FindSectionKey(section, key);
		if (index != std::string::npos)
		{
			std::string line = GetLine(index, &index);
			index = line.find('=');
			if (index != std::string::npos)
			{
				return StrUtil::Trim(line.substr(index + 1));
			}
		}
		return (def) ? std::string(def) : std::string();
	}

	/**
	 * 値を得る
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] def デフォルト値
	 * @return 値
	 */
	inline std::string GetString(const std::string& section, const std::string& key, const std::string& def = std::string()) const
	{
		return GetString(section.c_str(), key.c_str(), def.c_str());
	}

	/**
	 * 値を設定
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] value 値
	 */
	void SetString(const char* section, const char* key, const char* value)
	{
		std::string::size_type sIndex = FindSection(section);
		if (sIndex == std::string::npos)
		{
			if (!StrUtil::EndsWith(m_ini, "\n"))
			{
				m_ini += "\r\n";
			}
			m_ini += StrUtil::Format("[%s]\r\n%s=%s\r\n", section, key, value);
		}
		else
		{
			std::string::size_type kIndex = FindKey(key, sIndex);
			if (kIndex != std::string::npos)
			{
				sIndex = kIndex;
				std::string::size_type eol = StrUtil::EndOfLine(m_ini, sIndex);
				std::string line = m_ini.substr(sIndex, eol - sIndex);
				kIndex = line.find('=');
				if (kIndex != std::string::npos)
				{
					kIndex = sIndex + kIndex + 1;
					m_ini.erase(kIndex, eol - kIndex);
					m_ini.insert(kIndex, value);
				}
			}
			if (kIndex == std::string::npos)
			{
				while (sIndex != std::string::npos)
				{
					std::string::size_type eol = StrUtil::EndOfLine(m_ini, sIndex);
					std::string line = m_ini.substr(sIndex, eol - sIndex);
					line = TrimLine(line);
					if (GetSection(line))
					{
						if (kIndex == std::string::npos)
						{
							kIndex = sIndex;
						}
						break;
					}
					sIndex = StrUtil::NextLine(m_ini, eol);
					if (line.find('=') != std::string::npos)
					{
						kIndex = sIndex;
					}
				}
				if (kIndex == std::string::npos)
				{
					if (!StrUtil::EndsWith(m_ini, "\n"))
					{
						m_ini += "\r\n";
					}
					kIndex = m_ini.size();
				}
				const std::string tmp = StrUtil::Format("%s=%s\r\n", key, value);
				m_ini.insert(kIndex, tmp);
			}
		}
		m_bModified = true;
	}

	/**
	 * 値を設定
	 * @param[in] section セクション名
	 * @param[in] key キー
	 * @param[in] value 値
	 */
	inline void SetString(const std::string& section, const std::string& key, const std::string& value)
	{
		SetString(section.c_str(), key.c_str(), value.c_str());
	}

protected:
	/**
	 * 一行得る
	 * @param[in] index インデックス
	 * @param[out] next 次の行
	 * @return 文字列
	 */
	std::string GetLine(std::string::size_type index, std::string::size_type* next = NULL) const
	{
		std::string ret;
		if (index != std::string::npos)
		{
			std::string::size_type eol = StrUtil::EndOfLine(m_ini, index);
			ret = m_ini.substr(index, eol - index);
			ret = TrimLine(ret);
			index = eol;
		}
		if (next)
		{
			*next = StrUtil::NextLine(m_ini, index);
		}
		return ret;
	}

	/**
	 * ラインをトリムする
	 * @param[in] line ライン
	 * @return 結果
	 */
	static std::string TrimLine(const std::string& line)
	{
		// コメントを無視
		std::string ret = line;
		std::string::size_type index = ret.find(';');
		if (index != std::string::npos)
		{
			ret.erase(index);
		}
		return StrUtil::Trim(ret);
	}

	/**
	 * セクション名を得る
	 * @param[in] line ライン
	 * @param[out] section セクション名
	 * @return 結果
	 */
	static bool GetSection(const std::string& line, std::string* section = NULL)
	{
		if ((StrUtil::StartsWith(line, "[")) && (StrUtil::EndsWith(line, "]")))
		{
			if (section)
			{
				*section = StrUtil::Trim(line.substr(1, line.size() - 2));
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * キー名を得る
	 * @param[in] line ライン
	 * @return 結果
	 */
	static std::string GetKey(const std::string& line)
	{
		std::string::size_type index = line.find('=');
		if (index != std::string::npos)
		{
			return StrUtil::Trim(line.substr(0, index));
		}
		else
		{
			return std::string();
		}
	}

	/**
	 * セクションを検索
	 * @param[in] section セクション名
	 * @return インデックス
	 */
	std::string::size_type FindSection(const char* section) const
	{
		std::string::size_type index = 0;
		while (index != std::string::npos)
		{
			std::string line = GetLine(index, &index);

			std::string currentSection;
			if (GetSection(line, &currentSection))
			{
				if (StrUtil::Compare(currentSection, section, true) == 0)
				{
					return index;
				}
			}
		}
		return std::string::npos;
	}

	/**
	 * キーを検索
	 * @param[in] key キー
	 * @param[in] index インデックス
	 * @return インデックス
	 */
	std::string::size_type FindKey(const char* key, std::string::size_type index) const
	{
		while (index != std::string::npos)
		{
			std::string::size_type next = std::string::npos;
			std::string line = GetLine(index, &next);

			std::string section;
			if (GetSection(line, &section))
			{
				break;
			}
			else
			{
				const std::string k = GetKey(line);
				if (StrUtil::Compare(k, key, true) == 0)
				{
					return index;
				}
			}
			index = next;
		}
		return std::string::npos;
	}

	/**
	 * 目的のインデックスを得る
	 * @param[in] section セクション名
	 * @param[in] key キー名
	 * @return インデックス
	 */
	std::string::size_type FindSectionKey(const char* section, const char* key) const
	{
		std::string::size_type index = FindSection(section);
		return FindKey(key, index);
	}

	std::string m_ini;		/*!< ファイル内容 */
	bool m_bModified;		/*!< 更新フラグ */
};
