﻿/**
 * @file	surface.cpp
 * @brief	サーフェス クラスの動作の定義を行います
 */

#include "common.h"
#include "surface.h"
#include <math.h>
#include "endianutil.h"
#include "font8.inl"
#include "font12.inl"

/**
 * コンストラクタ
 */
Surface::Surface()
{
	memset(this, 0, sizeof(*this));
}

/**
 * コンストラクタ
 * @param[in] parent 親
 * @param[in] x X
 * @param[in] y Y
 * @param[in] width 幅
 * @param[in] height 高さ
 */
Surface::Surface(const Surface* parent, int x, int y, int width, int height)
{
	memset(this, 0, sizeof(*this));
	Bind(parent, x, y, width, height);
}

/**
 * コンストラクタ
 * @param[in] width 幅
 * @param[in] height 高さ
 * @param[in] bpp 色数
 * @param[in] yalign Yアライメント
 * @param[in] buffer バッファ
 * @param[in] palettes パレット
 */
Surface::Surface(int width, int height, int bpp, int yalign, void* buffer, uint32_t* palettes)
{
	memset(this, 0, sizeof(*this));
	Bind(width, height, bpp, yalign, buffer, palettes);
}

/**
 * デストラクタ
 */
Surface::~Surface()
{
	Destroy();
}

/**
 * 設定
 * @param[in] parent 親
 * @param[in] x X
 * @param[in] y Y
 * @param[in] width 幅
 * @param[in] height 高さ
 */
void Surface::Bind(const Surface* parent, int x, int y, int width, int height)
{
	Destroy();

	if (parent == NULL)
	{
		return;
	}

	this->nWidth = width;
	this->nHeight = height;
	this->nBpp = parent->nBpp;
	this->nYAlign = parent->nYAlign;
	this->pBuffer = static_cast<uint8_t*>(parent->Ptr(x, y));
	this->pPalettes = parent->pPalettes;
}

/**
 * バインド
 * @param[in] width 幅
 * @param[in] height 高さ
 * @param[in] bpp 色数
 * @param[in] yalign Yアライメント
 * @param[in] buffer バッファ
 * @param[in] palettes パレット
 */
void Surface::Bind(int width, int height, int bpp, int yalign, void* buffer, uint32_t* palettes)
{
	this->nWidth = width;
	this->nHeight = height;
	this->nBpp = bpp;
	this->nYAlign = yalign;
	this->pBuffer = static_cast<uint8_t*>(buffer);
	this->pPalettes = palettes;
}

/**
 * 破棄
 */
void Surface::Destroy()
{
	memset(this, 0, sizeof(*this));
}

/**
 * ライン バイト数
 * @return ライン バイト数
 */
unsigned int Surface::LineBytes() const
{
	int nBpp = this->nBpp;
	if (nBpp == 15)
	{
		nBpp = 16;
	}
	return ((this->nWidth * nBpp) + 7) / 8;
}

/**
 * ポインタ
 * @param[in] x X
 * @param[in] y Y
 * @return ポインタ
 */
void* Surface::Ptr(int x, int y) const
{
	if ((x < 0) || (y < 0) || (x >= this->nWidth) || (y >= this->nHeight))
	{
		return NULL;
	}

	int nBpp = this->nBpp;
	if (nBpp == 15)
	{
		nBpp = 16;
	}
	return this->pBuffer + ((x * nBpp) >> 3) + (y * this->nYAlign);
}

/**
 * ゲット
 * @param[in] x X
 * @param[in] y Y
 * @return 色
 */
unsigned int Surface::Get(int x, int y) const
{
	void* ptr = Ptr(x, y);
	if (ptr == NULL)
	{
		return 0;
	}
	switch (this->nBpp)
	{
		case 1:
			{
				const uint8_t c = *(static_cast<uint8_t*>(ptr));
				return (c >> (7 - (x & 7))) & 1;
			}

		case 4:
			{
				const uint8_t c = *(static_cast<uint8_t*>(ptr));
				return (x & 1) ? (c & 15) : (c >> 4);
			}

		case 8:
			return *(static_cast<uint8_t*>(ptr));

		case 15:
		case 16:
			return *(static_cast<uint16_t*>(ptr));

		case 24:
			return *(static_cast<uint24le_t*>(ptr));

		case 32:
			return *(static_cast<uint32_t*>(ptr));
	}
	return 0;
}

/**
 * 色取得
 * @param[in] x X
 * @param[in] y Y
 * @return 色
 */
uint32_t Surface::GetColor(int x, int y) const
{
	const unsigned int c = Get(x, y);

	uint32_t rgb = 0;
	switch (this->nBpp)
	{
		case 1:
		case 4:
		case 8:
			if (this->pPalettes)
			{
				rgb = this->pPalettes[c];
			}
			break;

		case 15:
			rgb = ((c & 0x001f) << 19) + ((c & 0x03e0) << 6) + ((c & 0x7c00) >> 7);
			rgb += (rgb >> 5) & 0x070707;
			break;

		case 16:
			rgb = ((c & 0x001f) << 19) + ((c & 0xf800) >> 8);
			rgb += (rgb >> 5) & 0x070007;
			rgb += (c & 0x07e0) << 5;
			rgb += (c & 0x0600) >> 1;
			break;

		case 24:
		case 32:
			rgb = c;
			break;
	}
	return rgb;
}

/**
 * セット
 * @param[in] x X
 * @param[in] y Y
 * @param[in] color 色
 */
void Surface::Set(int x, int y, unsigned int color)
{
	void* ptr = Ptr(x, y);
	if (ptr == NULL)
	{
		return;
	}
	switch (this->nBpp)
	{
		case 1:
			if (color & 1)
			{
				*(static_cast<uint8_t*>(ptr)) |= (0x80 >> (x & 7));
			}
			else
			{
				*(static_cast<uint8_t*>(ptr)) &= ~(0x80 >> (x & 7));
			}
			break;

		case 4:
			if ((x & 1) == 0)
			{
				*(static_cast<uint8_t*>(ptr)) &= 0x0f;
				*(static_cast<uint8_t*>(ptr)) |= (color << 4);
			}
			else
			{
				*(static_cast<uint8_t*>(ptr)) &= 0xf0;
				*(static_cast<uint8_t*>(ptr)) |= (color & 15);
			}
			break;

		case 8:
			*(static_cast<uint8_t*>(ptr)) = static_cast<uint8_t>(color);
			break;

		case 15:
		case 16:
			*(static_cast<uint16_t*>(ptr)) = static_cast<uint16_t>(color);
			break;

		case 24:
			*(static_cast<uint24le_t*>(ptr)) = color;
			break;

		case 32:
			*(static_cast<uint32_t*>(ptr)) = color;
			break;
	}
}

/**
 * クリア
 * @param[in] color 色
 */
void Surface::Clear(unsigned int color)
{
	Fill(0, 0, Width(), Height(), color);
}

/**
 * Fill
 * @param[in] x X
 * @param[in] y Y
 * @param[in] cx 幅
 * @param[in] cy 高さ
 * @param[in] color 色
 */
void Surface::Fill(int x, int y, int cx, int cy, unsigned int color)
{
	for (int i = 0; i < cy; i++)
	{
		for (int j = 0; j < cx; j++)
		{
			Set(x + j, y + i, color);
		}
	}
}

/**
 * Copy
 * @param[in] x X
 * @param[in] y Y
 * @param[in] sx X
 * @param[in] sy Y
 * @param[in] cx 幅
 * @param[in] cy 高さ
 */
void Surface::Copy(int x, int y, int sx, int sy, int cx, int cy)
{
	for (int i = 0; i < cy; i++)
	{
		for (int j = 0; j < cx; j++)
		{
			Set(x + j, y + i, Get(sx + j, sy + i));
		}
	}
}

/**
 * 領域コピー
 * @param[in] surface サーフェス
 * @param[in] px X
 * @param[in] py Y
 */
void Surface::Put(const Surface& surface, int px, int py)
{
	for (int y = 0; y < surface.Height(); y++)
	{
		for (int x = 0; x < surface.Width(); x++)
		{
			const unsigned int c = surface.Get(x, y);
			Set(x + px, y + py, c);
		}
	}
}

/**
 * Set
 * @param[in] lpPattern パターン
 * @param[in] x X
 * @param[in] y Y
 * @param[in] nColor 色
 */
void Surface::SetPat(const uint8_t* lpPattern, int x, int y, unsigned int nColor)
{
	if (lpPattern == NULL)
	{
		return;
	}
	const uint8_t* p = lpPattern + 2;
	const unsigned int cy = lpPattern[1];
	for (unsigned int i = 0; i < cy; i++)
	{
		uint8_t bit = 0;
		uint8_t c = 0;
		const unsigned int cx = lpPattern[0];
		for (unsigned int j = 0; j < cx; j++)
		{
			if (!bit)
			{
				bit = 0x80;
				c = *p++;
			}
			if (c & bit)
			{
				Set(x + j, y + i, nColor);
			}
			bit >>= 1;
		}
	}
}

/**
 * Text
 * @param[in] x X
 * @param[in] y Y
 * @param[in] lpString 文字列
 * @param[in] nColor 色
 * @param[in] bPitch ピッチ
 */
void Surface::Text8(int x, int y, const char* lpString, unsigned int nColor, bool bPitch)
{
	if (lpString == NULL)
	{
		return;
	}

	const int orgx = x;
	uint8_t src[10];
	src[0] = 0;
	src[1] = 7;
	while (*lpString != '\0')
	{
		uint8_t c = *lpString++;
		const uint8_t* ptr = NULL;
		if ((c >= 0x20) && (c < 0x80))
		{
			ptr = s_font8[(c - 0x20)];
		}
		else if ((c >= 0xa0) && (c < 0xe0))
		{
			ptr = s_font8[(c - 0xa0 + 0x60)];
		}
		else if (c == '\n')
		{
			x = orgx;
			y += 8;
		}
		if (ptr == NULL)
		{
			continue;
		}
		int nWidth = ptr[0];
		src[0] = nWidth;
		CopyMemory(src + 2, ptr + 1, 7);

		int nPad = 0;
		if (!bPitch)
		{
			nPad = (5 - nWidth) / 2;
			nWidth = 5;
		}
		SetPat(src, x + nPad, y, nColor);
		x += nWidth + 1;
	}
}

/**
 * Text
 * @param[in] x X
 * @param[in] y Y
 * @param[in] lpString 文字列
 * @param[in] nColor 色
 * @param[in] bPitch ピッチ
 */
void Surface::Text12(int x, int y, const char* lpString, unsigned int nColor, bool bPitch)
{
	if (lpString == NULL)
	{
		return;
	}

	const int orgx = x;
	uint8_t src[13];
	src[0] = 0;
	src[1] = 11;
	while (*lpString != '\0')
	{
		uint8_t c = *lpString++;
		const uint8_t* ptr = NULL;
		if ((c >= 0x20) && (c < 0x80))
		{
			ptr = s_font12[(c - 0x20)];
		}
		else if (c == '\n')
		{
			x = orgx;
			y += 12;
		}
		if (ptr == NULL)
		{
			continue;
		}
		int nWidth = ptr[0];
		src[0] = nWidth;
		CopyMemory(src + 2, ptr + 1, 11);

		int nPad = 0;
		if (!bPitch)
		{
			nPad = (7 - nWidth) / 2;
			nWidth = 7;
		}
		SetPat(src, x + nPad, y, nColor);
		x += nWidth + 1;
	}
}
