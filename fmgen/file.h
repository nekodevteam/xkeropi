//	$fmgen-Id: file.h,v 1.6 1999/11/26 10:14:09 cisc Exp $

#pragma once

#include "fmgen_types.h"
#if defined(_WIN32)
#include <tchar.h>
#endif	// defined(_WIN32)

/**
 * @brief ファイル I/O クラス
 */
class FileIO
{
public:
	/**
	 * フラグ
	 */
	enum Flags
	{
		open		= 0x000001,
		readonly	= 0x000002
	};

	/**
	 * シーク メソッド
	 */
	enum SeekMethod
	{
		begin	= 0,
		current	= 1,
		end		= 2,
	};

	/**
	 * コンストラクタ
	 */
	FileIO()
		: m_fp(NULL)
		, m_flags(0)
	{
	}

	/**
	 * コンストラクタ
	 * @param[in] filename ファイル名
	 * @param[in] flags フラグ
	 */
	FileIO(LPCTSTR filename, uint flags = 0)
		: m_fp(NULL)
		, m_flags(0)
	{
		Open(filename, flags);
	}

	/**
	 * デストラクタ
	 */
	~FileIO()
	{
		Close();
	}

	/**
	 * ファイルを開く
	 * @param[in] filename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Open(LPCTSTR filename, uint = 0)
	{
		Close();

		m_flags = readonly;

#if defined(_WIN32)
		m_fp = _tfopen(filename, TEXT("rb"));
#else	// defined(_WIN32)
		m_fp = fopen(filename, "rb");
#endif	// defined(_WIN32)
		if (m_fp != NULL)
		{
			m_flags |= open;
		}

		return (m_fp != NULL);
	}

	/**
	 * ファイルを閉じる
	 */
	void Close()
	{
		if (m_fp != NULL)
		{
			fclose(m_fp);
			m_fp = NULL;
			m_flags = 0;
		}
	}

	/**
	 * ファイルからの読み出し
	 * @param[in] dest バッファ
	 * @param[in] len サイズ
	 * @return サイズ
	 */
	int32 Read(void* dest, int32 len)
	{
		if (m_fp != NULL)
		{
			return static_cast<int32>(fread(dest, 1, len, m_fp));
		}
		else
		{
			return -1;
		}
	}

	/**
	 * ファイルをシーク
	 * @param[in] fpos 位置
	 * @param[in] method メソッド
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Seek(int32 fpos, SeekMethod method)
	{
		if (m_fp != NULL)
		{
			int whence;
			switch (method)
			{
				case begin:
					whence = SEEK_SET;
					break;

				case current:
					whence = SEEK_CUR;
					break;

				case end:
					whence = SEEK_END;
					break;

				default:
					return false;
			}
			return (fseek(m_fp, fpos, whence) != -1);
		}
		else
		{
			return false;
		}
	}

private:
	FILE* m_fp;			/*!< ファイル */
	uint32 m_flags;		/*!< フラグ */

	/* コピー禁止 */
	FileIO(const FileIO&);
	const FileIO& operator=(const FileIO&);
};
