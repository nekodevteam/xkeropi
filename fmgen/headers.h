#ifndef WIN_HEADERS_H
#define WIN_HEADERS_H

#ifndef STRICT
#define STRICT
#endif
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#endif	// WIN_HEADERS_H
