#ifndef _win68_opm_fmgen
#define _win68_opm_fmgen

#include "../x68k/x68k.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

int OPM_Init(int clock, int rate);
void OPM_Cleanup(void);
void OPM_Reset(void);
void OPM_Update(short *buffer, int length);
void FASTCALL OPM_Write(DWORD r, IOBUS8 v);
IOBUS8 FASTCALL OPM_Read(DWORD a);
void FASTCALL OPM_Timer(DWORD step);
void OPM_SetVolume(BYTE vol);
void OPM_SetRate(int clock, int rate);
void OPM_RomeoOut(unsigned int delay);

#ifndef NO_MERCURY
int M288_Init(int clock, int rate, LPCTSTR path);
void M288_Cleanup(void);
void M288_Reset(void);
void M288_Update(short *buffer, int length);
void FASTCALL M288_Write(DWORD r, IOBUS8 v);
IOBUS8 FASTCALL M288_Read(DWORD a);
void FASTCALL M288_Timer(DWORD step);
void M288_SetVolume(BYTE vol);
void M288_SetRate(int clock, int rate);
void M288_RomeoOut(unsigned int delay);
#endif	// !NO_MERCURY

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_win68_opm_fmgen
