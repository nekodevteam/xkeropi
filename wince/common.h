#ifndef winx68k_common_h
#define winx68k_common_h

#include <windows.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>

#if (_MSC_VER < 1300 /*Visual Studio 2002*/)
#define DWORD_PTR	DWORD		/*!< DWORD_PTR */
#endif	// (_MSC_VER < 1300 /*Visual Studio 2002*/)

#define	TRUE		1
#define	FALSE		0

//#define WIN68DEBUG						// -DEBUG オプション用

#ifndef FASTCALL
#if defined(_MSC_VER) && !defined(_WIN32_WCE) && !defined(_WIN64)
#define FASTCALL	__fastcall
#else	// defined(_MSC_VER) && !defined(_WIN32_WCE) && !defined(_WIN64)
#define FASTCALL
#endif	// defined(_MSC_VER) && !defined(_WIN32_WCE) && !defined(_WIN64)
#endif	// !FASTCALL

#ifndef INLINE
#define	INLINE		__inline
#endif

#ifdef __cplusplus
#define EXTERNC extern "C"
#else	// __cplusplus
#define EXTERNC extern
#endif	// __cplusplus

#ifndef _countof
//! countof
#define _countof(x)		(sizeof((x)) / sizeof((x)[0]))
#endif	// _countof

#if defined(_MSC_VER) && (_MSC_VER < 1300)
#define for					if (0 /*NEVER*/) { /* no process */ } else for			/*!< for scope */
#endif	/* defined(_MSC_VER) && (_MSC_VER < 1300) */

#define __attribute(n)
#define __attribute__(n)

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void Error(const char* s);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //winx68k_common_h
