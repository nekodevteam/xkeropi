/**
 * @file	RemoveBom.cpp
 * @brief	このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
 */

#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include "misc/strutil.h"
#include "misc/win32/filecrawl.h"

typedef std::vector<std::tstring> StringArray;

//! BOM
static const char bom[] = "\357\273\277";

/**
 * BOM チェック
 * @param[in] lpPath パス
 * @param[in] arg ユーザ データ
 * @retval true 成功
 */
static bool CheckBom(LPCTSTR lpPath, LPVOID arg)
{
	std::string file = StrUtil::ReadFile(lpPath);
	if (StrUtil::StartsWith(file, bom))
	{
		(reinterpret_cast<StringArray*>(arg))->push_back(lpPath);
		_tprintf(TEXT("%s\n"), lpPath);
	}
	return true;
}

/**
 * Shift-JISに変換
 * @param[in] lpPath パス
 * @retval true 成功
 * @retval false 失敗
 */
static bool ToShiftJIS(LPCTSTR lpPath)
{
	// ごっそり読む
	const std::string str = StrUtil::ReadFile(lpPath);
	if (str.empty())
	{
		_tprintf(TEXT("Couldn't open %s\n"), lpPath);
		return false;
	}

	std::string ret(str);
	if (StrUtil::StartsWith(ret, bom))
	{
		ret.erase(0, 3);
		ret = StrUtil::Utf8ToShiftJis(ret);
	}
	if (str != ret)
	{
		StrUtil::WriteFile(lpPath, ret);
	}

	return true;
}

/**
 * メイン
 * @param[in] argc 引数
 * @param[in] argv 引数
 * @return リザルト コード
 */
int _tmain(int argc, TCHAR* argv[])
{
	TCHAR szPath[MAX_PATH];
	::GetModuleFileName(NULL, szPath, MAX_PATH);
	::PathRemoveFileSpec(szPath);

	if (argc >= 2)
	{
		::PathCombine(szPath, szPath, argv[1]);
	}

	StringArray res;
	if (!FileCrawl::Crawl(szPath, true, TEXT(".c;.cpp;.h;.hpp;.inl;ipp;.mcr"), CheckBom, &res))
	{
		_tprintf(TEXT("An error has occurred\n"));
		return 1;
	}

	if (res.empty())
	{
		_tprintf(TEXT("Not found files\n"));
		return 0;
	}

	_tprintf(TEXT("found %d files. Overwrite? (y/n)"), static_cast<int>(res.size()));
	char input = '\0';
	std::cin >> input;
	if (tolower(input) != 'y')
	{
		return 0;
	}

	for (StringArray::const_iterator it = res.begin(); it != res.end(); ++it)
	{
		ToShiftJIS(it->c_str());
	}
	return 0;
}

// プログラムの実行: Ctrl + F5 または [デバッグ] > [デバッグなしで開始] メニュー
// プログラムのデバッグ: F5 または [デバッグ] > [デバッグの開始] メニュー

// 作業を開始するためのヒント: 
//   1. ソリューション エクスプローラー ウィンドウを使用してファイルを追加/管理します 
//   2. チーム エクスプローラー ウィンドウを使用してソース管理に接続します
//   3. 出力ウィンドウを使用して、ビルド出力とその他のメッセージを表示します
//   4. エラー一覧ウィンドウを使用してエラーを表示します
//   5. [プロジェクト] > [新しい項目の追加] と移動して新しいコード ファイルを作成するか、[プロジェクト] > [既存の項目の追加] と移動して既存のコード ファイルをプロジェクトに追加します
//   6. 後ほどこのプロジェクトを再び開く場合、[ファイル] > [開く] > [プロジェクト] と移動して .sln ファイルを選択します
