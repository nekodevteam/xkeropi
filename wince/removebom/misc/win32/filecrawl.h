/**
 * @file	filecraw.h
 * @brief	このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
 */

#pragma once

#include <Shlwapi.h>

#pragma comment(lib, "Shlwapi.lib")

namespace FileCrawl
{

typedef bool (*FNONFILE)(LPCTSTR lpPath, LPVOID arg);

/**
 * 検索
 * @param[in] lpDirectory ディレクトリ
 * @param[in] lpFilterArray フィルタ
 * @param[in] fnOnFile コールバック
 * @param[in] arg 引数
 * @retval true 成功
 * @retval false 失敗
 */
static bool CrawlSub(LPCTSTR lpDirectory, bool bRescue, LPCTSTR lpFilterArray, FNONFILE fnOnFile, LPVOID arg)
{
	// ディレクトリ内のファイルを検索
	TCHAR szFind[MAX_PATH];
	::lstrcpy(szFind, lpDirectory);
	::PathAppend(szFind, TEXT("*.*"));

	bool r = true;

	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(szFind, &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			TCHAR szPath[MAX_PATH];
			::lstrcpy(szPath, lpDirectory);
			::PathAppend(szPath, fd.cFileName);
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if ((bRescue) && (fd.cFileName[0] != TEXT('.')))
				{
					r = CrawlSub(szPath, bRescue, lpFilterArray, fnOnFile, arg);
				}
			}
			else
			{
				PTSTR lpExt = ::PathFindExtension(szPath);
				LPCTSTR p = lpFilterArray;
				while ((p != NULL) && (*p != TEXT('\0')) && (::lstrcmpi(p, lpExt)))
				{
					p += ::lstrlen(p) + 1;
				}
				if ((p == NULL) || (*p != TEXT('\0')))
				{
					r = (*fnOnFile)(szPath, arg);
				}
			}
		} while ((r) && (::FindNextFile(hFind, &fd)));

		::FindClose(hFind);
	}
	return r;
}

/**
 * 検索
 * @param[in] lpDirectory ディレクトリ
 * @param[in] lpFilters フィルタ
 * @param[in] fnOnFile コールバック
 * @param[in] 引数
 * @retval true 成功
 * @retval false 失敗
 */
bool Crawl(LPCTSTR lpDirectory, bool bRescue, LPCTSTR lpFilters, FNONFILE fnOnFile, LPVOID arg = NULL)
{
	LPTSTR lpFilterArray = NULL;

	if (lpFilters)
	{
		const int size = ::lstrlen(lpFilters);

		lpFilterArray = new TCHAR [size + 2];

		LPCTSTR p = lpFilters;
		LPTSTR q = lpFilterArray;
		while (*p != TEXT('\0'))
		{
			if (*p == TEXT(';'))
			{
				p++;
				continue;
			}

			while ((*p != TEXT('\0')) && (*p != TEXT(';')))
			{
				*q++ = *p++;
			}
			*q++ = TEXT('\0');
		}
		*q++ = TEXT('\0');
	}

	const bool r = CrawlSub(lpDirectory, bRescue, lpFilterArray, fnOnFile, arg);

	delete[] lpFilterArray;

	return r;
}

}	// namespace FileCrawl
