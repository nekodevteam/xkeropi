// JOYSTICK.C - ジョイスティックサポート for WinX68k

#include "common.h"
#include "joystick.h"

static uint8_t JoyPortData[2] = {0, 0};

void Joystick_Init(void)
{
	JoyPortData[0] = 0;
	JoyPortData[1] = 0;
}

uint_fast8_t FASTCALL Joystick_Read(uint_fast8_t num)
{
	uint_fast8_t ret = 0;
	if (num < _countof(JoyPortData))
	{
		const uint_fast8_t ret0 = 0xff;
		const uint_fast8_t ret1 = 0xff;
		ret = ((~JoyPortData[num]) & ret0) | (JoyPortData[num] & ret1);
	}
	return ret;
}


void FASTCALL Joystick_Write(uint_fast8_t num, uint_fast8_t data)
{
	if (num < _countof(JoyPortData))
	{
		JoyPortData[num] = data;
	}
}
