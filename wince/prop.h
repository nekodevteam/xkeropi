/**
 * @file	prop.h
 * @brief	設定クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

typedef struct
{
	DWORD SampleRate;
	DWORD BufferSize;
	int OPM_VOL;
	int PCM_VOL;
#ifndef NO_MERCURY
	int MCR_VOL;
#endif	// !NO_MERCURY
	int JOY_BTN[2][8];
	int MouseSpeed;
	int DSAlert;
#ifndef NO_MIDI
	int MIDI_SW;
	int MIDI_Type;
	int MIDI_Reset;
	int ToneMap;
	TCHAR ToneMapFile[MAX_PATH];
	int MIDIDelay;
	int MIDIAutoDelay;
#endif	// !NO_MIDI
	int JoyKey;
	int JoyKeyReverse;
	int JoyKeyJoy2;
	int SRAMWarning;
	TCHAR HDImage[16][MAX_PATH];
	int XVIMode;
	int JoySwap;
#ifndef NO_WINDRV
	int LongFileName;
	int WinDrvFD;
#endif	// NO_WINDRV
	int Sound_LPF;
} Win68Conf;

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern Win68Conf Config;

void LoadConfig(void);
void SaveConfig(void);

#ifdef __cplusplus
}
#endif	// __cplusplus
