/**
 * @file	windraw.cpp
 * @brief	描画クラスの動作の定義を行います
 */

#include "common.h"
#include "windraw.h"
#include <algorithm>
#ifdef USE_GX
#include <gx.h>
#endif	// USE_GX
#include "winx68k.h"
#include "../misc/surface.h"
#include "../x68k/crtc.h"
#include "../x68k/palette.h"
#include "../x68k/tvram.h"

WinDraw WinDraw::sm_instance;	/*!< 唯一のインスタンスです */

/**
 * コンストラクタ
 */
WinDraw::WinDraw()
	: Draw_DrawFlag(1)
	, m_nFrameCount(0)
	, m_nYAlign(0)
	, m_nLastW(0)
	, m_nLastH(0)
#ifdef USES_ALLFLASH
	, RedrawLastV(0)
#endif	// USES_ALLFLASH
#ifdef USE_GX
	, m_pBuffer(NULL)
#else	// USE_GX
	, m_hBitmap(NULL)
	, m_pImage(NULL)
#endif	// USE_GX
{
	ZeroMemory(&m_szScreen, sizeof(m_szScreen));
}

/**
 * サイズ変更通知
 */
void WinDraw::ChangeSize()
{
	TextDotX = (std::min<DWORD>)(TextDotX, m_szScreen.cx);
	TextDotY = (std::min<DWORD>)(TextDotY, m_szScreen.cy);
}

/**
 * 初期化
 * @retval true 成功
 * @retval false 失敗
 */
bool WinDraw::Init()
{
#ifdef USE_GX
	GXDisplayProperties gxdp = GXGetDisplayProperties();
	m_szScreen.cx = gxdp.cxWidth;
	m_szScreen.cy = gxdp.cyHeight;
	m_nYAlign = gxdp.cbyPitch;
	if (GXOpenDisplay(hWndMain, GX_FULLSCREEN) == 0)
	{
		MessageBox(hWndMain, TEXT("Couldn't GAPI Object"), NULL, MB_OK | MB_ICONSTOP);
		return false;
	}
	delete[] m_pBuffer;
	m_pBuffer = new uint16_t [m_szScreen.cx * m_szScreen.cy];
	ZeroMemory(m_pBuffer, sizeof(uint16_t) * m_szScreen.cx * m_szScreen.cy);
#else	// USE_GX
	m_szScreen.cx = (std::min)(SCREEN_WIDTH, GetSystemMetrics(SM_CXSCREEN));
	m_szScreen.cy = (std::min)(SCREEN_HEIGHT, GetSystemMetrics(SM_CYSCREEN));

	BMPINFO bi;
	ZeroMemory(&bi, sizeof(bi));
	bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
	bi.bmiHeader.biWidth = m_szScreen.cx;
	bi.bmiHeader.biHeight = 0 - m_szScreen.cy;
	bi.bmiHeader.biPlanes = 1;
	bi.bmiHeader.biBitCount = 16;
	bi.bmiHeader.biCompression = BI_BITFIELDS;
	bi.bmiColors[0] = 0xf800;
	bi.bmiColors[1] = 0x07e0;
	bi.bmiColors[2] = 0x001f;
	HDC hdc = GetDC(NULL);
	m_hBitmap = CreateDIBSection(hdc, reinterpret_cast<BITMAPINFO*>(&bi), DIB_RGB_COLORS, &m_pImage, NULL, 0);
	ReleaseDC(NULL, hdc);
	if (m_hBitmap == NULL)
	{
		m_pImage = NULL;
		return false;
	}
	m_nYAlign = ((m_szScreen.cx * 2) + 3) & (~3);
	ZeroMemory(m_pImage, m_nYAlign * m_szScreen.cy);
#endif	// USE_GX

	Pal_SetColor();

	return true;
}

/**
 * 破棄
 */
void WinDraw::Cleanup()
{
#ifdef USE_GX
	GXCloseDisplay();
	delete[] m_pBuffer;
	m_pBuffer = NULL;
#else	// USE_GX
	if (m_hBitmap)
	{
		m_pImage = NULL;
		DeleteObject(m_hBitmap);
		m_hBitmap = NULL;
	}
#endif	// USE_GX
}

/**
 * 再描画
 */
void WinDraw::Redraw()
{
	Draw_DrawFlag = 1;
	TVRAM_SetAllDirty();
}

/**
 * 描画
 */
void WinDraw::Draw()
{
	m_nFrameCount++;

	if (!Draw_DrawFlag) return;
	Draw_DrawFlag = 0;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
		CRTC.AllFlash = 0;
		memset(TextDirtyLine, 1, RedrawLastV);
		RedrawLastV = 0;
	}
#endif	// USES_ALLFLASH

	RECT r;
	SetRect(&r, 0, 0, TextDotX, TextDotY);

#ifdef USE_GX
	void* ptr = GXBeginDraw();
	if (ptr)
	{
		if (m_pBuffer)
		{
			const uint16_t* p = m_pBuffer + (r.top * m_szScreen.cx);
			uint16_t* q =reinterpret_cast<uint16_t*>(reinterpret_cast<intptr_t>(ptr) + (r.top * m_nYAlign));
			for (int i = r.top; i < r.bottom; i++)
			{
#ifdef USE_NATIVECOLOR
				for (int x = r.left; x < r.right; x++)
				{
					const unsigned int c = p[x];
					q[x] = static_cast<uint16_t>(((c << 5) & 0xf800) + ((c >> 5) & 0x07c0) + ((c >> 1) & 0x001f));
				}
#else	// USE_NATIVECOLOR
				memcpy(q, p, w * 2);
#endif	// USE_NATIVECOLOR
				p += m_szScreen.cx;
				q = reinterpret_cast<uint16_t*>(reinterpret_cast<intptr_t>(q) + m_nYAlign);
			}
		}
		DrawSub(ptr, m_nYAlign, r);
		GXEndDraw();
	}
#else	// USE_GX
	if (m_pImage)
	{
		DrawSub(m_pImage, m_nYAlign, r);
	}

	HDC hdc = GetDC(hWndMain);
	HDC hdcMem = CreateCompatibleDC(hdc);
	HBITMAP hBitmap = static_cast<HBITMAP>(SelectObject(hdcMem, m_hBitmap));
	BitBlt(hdc, 0, r.top, r.right, r.bottom - r.top, hdcMem, 0, r.top, SRCCOPY);
	SelectObject(hdcMem, hBitmap);
	DeleteDC(hdcMem);
	ReleaseDC(hWndMain, hdc);
#endif	// USE_GX
}

void WinDraw::DrawSub(void* pDest, int nYAlign, RECT& r)
{
	const int nLastW = m_nLastW;
	const int nLastH = m_nLastH;
	m_nLastW = r.right;
	m_nLastH = r.bottom;
	if ((r.right < nLastW) || (r.bottom < nLastH))
	{
		Surface sur(m_szScreen.cx, m_szScreen.cy, 16, nYAlign, pDest);
		if (r.right < nLastW)
		{
			sur.Fill(r.right, r.top, nLastW - r.right, r.bottom - r.top, 0);
			r.right = nLastW;
		}
		if (r.bottom < nLastH)
		{
			sur.Fill(0, r.bottom, nLastW, nLastH - r.bottom, 0);
			r.bottom = nLastH;
		}
	}
}

/**
 * ライン描画
 */
void WinDraw::DrawLine(uint32_t v)
{
	if (v >= _countof(TextDirtyLine)) return;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
		RedrawLastV = v;
	}
	else
#endif	// USES_ALLFLASH
	if (!TextDirtyLine[v]) return;
	TextDirtyLine[v] = 0;

	if (static_cast<int>(v) >= m_szScreen.cy)
	{
		return;
	}

#ifdef USE_GX
	if (m_pBuffer)
	{
		CRTC_DrawLine(&m_pBuffer[v * m_szScreen.cx], v);
	}
#else	// USE_GX
	if (m_pImage)
	{
		uint16_t* p = reinterpret_cast<uint16_t*>(reinterpret_cast<intptr_t>(m_pImage) + (v * m_nYAlign));
		CRTC_DrawLine(p, v);
#ifdef USE_NATIVECOLOR
		for (int x = 0; x < m_szScreen.cx; x++)
		{
			const unsigned int c = p[x];
			p[x] = static_cast<uint16_t>(((c << 5) & 0xf800) + ((c >> 5) & 0x07c0) + ((c >> 1) & 0x001f));
		}
#endif	// USE_NATIVECOLOR
	}
#endif	// USE_GX

	Draw_DrawFlag = 1;
}

// ---- from C

void WinDraw_ChangeSize(void)
{
	WinDraw::GetInstance()->ChangeSize();
}
