/**
 * @file	winx68k.cpp
 * @brief	メイン クラスの動作の定義を行います
 */

#include "common.h"
#include "resource.h"
#include "winx68k.h"
#include <algorithm>
#include <shlwapi.h>
#include "dswin.hpp"
#include "fileio.h"
#include "joystick.h"
#include "keyboard.h"
#include "prop.h"
#include "status.h"
#include "windraw.h"
#include "../fmgen/fmg_wrap.h"
#ifndef _WIN32_WCE
#include "../misc/win32/console.h"
#endif	// !_WIN32_WCE
#include "../x68k/adpcm.h"
#include "../x68k/bg.h"
#include "../x68k/crtc.h"
#ifdef WIN68DEBUG
#include "../x68k/d68k.h"
#endif	// WIN68DEBUG
#include "../x68k/dmac.h"
#include "../x68k/fdc.h"
#include "../x68k/fdd.h"
#include "../x68k/gvram.h"
#include "../x68k/ioc.h"
#include "../x68k/irqh.h"
#include "../x68k/m68000.h"
#include "../x68k/memory.h"
#ifndef NO_MERCURY
#include "../x68k/mercury.h"
#endif	// !NO_MERCURY
#include "../x68k/mfp.h"
#ifndef NO_MIDI
#include "../x68k/midi.h"
#endif	// !NO_MIDI
#include "../x68k/palette.h"
#include "../x68k/pia.h"
#include "../x68k/rtc.h"
#include "../x68k/sasi.h"
#include "../x68k/scc.h"
#include "../x68k/scsi.h"
#include "../x68k/sram.h"
#include "../x68k/tvram.h"


// #define SUPPORT_XVI

static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

const TCHAR PrgName[]  = TEXT("Keropi");
const TCHAR PrgTitle[] = TEXT("Keropi");

TCHAR winx68k_dir[MAX_PATH];
TCHAR winx68k_ini[MAX_PATH];

HINSTANCE hInst;
HWND      hWndMain;

const bool NoWaitMode = false;

static DSound s_ds;

WinX68k WinX68k::sm_instance;

/**
 * コンストラクタ
 */
WinX68k::WinX68k()
	: FrameRate(0)
#ifdef WIN68DEBUG
	, traceflag(0)
#endif	// WIN68DEBUG
#ifdef SUPPORT_XVI
	: ClkUsed(0)
#endif	// SUPPORT_XVI
{
}

/**
 * デストラクタ
 */
WinX68k::~WinX68k()
{
}

/**
 * SCSI チェック
 */
void WinX68k::SCSICheck()
{
	static const BYTE SCSIIMG[] =
	{
		0x00, 0xfc, 0x00, 0x14,			// $fc0000 SCSI起動用のエントリアドレス
		0x00, 0xfc, 0x00, 0x16,			// $fc0004 IOCSベクタ設定のエントリアドレス(必ず"Human"の8バイト前)
		0x00, 0x00, 0x00, 0x00,			// $fc0008 ?
		0x48, 0x75, 0x6d, 0x61,			// $fc000c ↓
		0x6e, 0x36, 0x38, 0x6b,			// $fc0010 ID "Human68k"	(必ず起動エントリポイントの直前)
		0x4e, 0x75,				// $fc0014 "rts"		(起動エントリポイント)
		0x23, 0xfc, 0x00, 0xfc, 0x00, 0x2a,	// $fc0016 ↓		(IOCSベクタ設定エントリポイント)
		0x00, 0x00, 0x07, 0xd4,			// $fc001c "move.l #$fc002a, $7d4.l"
		0x74, 0xff,				// $fc0020 "moveq #-1, d2"
		0x4e, 0x75,				// $fc0022 "rts"
//		0x53, 0x43, 0x53, 0x49, 0x49, 0x4e,	// $fc0024 ID "SCSIIN"
// 内蔵SCSIをONにすると、SASIは自動的にOFFになっちゃうらしい…
// よって、IDはマッチしないようにしておく…
		0x44, 0x55, 0x4d, 0x4d, 0x59, 0x20,	// $fc0024 ID "DUMMY "
		0x70, 0xff,				// $fc002a "moveq #-1, d0"	(SCSI IOCSコールエントリポイント)
		0x4e, 0x75,				// $fc002c "rts"
	};

	bool scsi = 0;
	for (unsigned int i = 0x30600; i < 0x30c00; i += 2)
	{
		const uint16_t* p = reinterpret_cast<uint16_t*>(&IPL[i]);
		if ((p[0] == 0xfc00) && (p[1] == 0x0000))
		{
			scsi = 1;
		}
	}

	// SCSIモデルのとき
	if (scsi)
	{
		ZeroMemory(IPL, 0x2000);		// 本体は8kb
		memset(&IPL[0x2000], 0xff, 0x1e000);	// 残りは0xff
		memcpy(IPL, SCSIIMG, sizeof(SCSIIMG));	// インチキSCSI BIOS
	}
	else
	{
		// SASIモデルはIPLがそのまま見える
		memcpy(IPL, &IPL[0x20000], 0x20000);
	}
}

/**
 * BIOS を読み込む
 * @reval true 成功
 * @reval false 失敗
 */
bool WinX68k::LoadROMs()
{
	static const LPCTSTR BIOSFILE[] =
	{
		TEXT("iplrom.dat"), TEXT("iplrom30.dat"), TEXT("iplromco.dat"), TEXT("iplromxv.dat")
	};
	static const TCHAR FONTFILE[] = TEXT("cgrom.dat");
	static const TCHAR FONTFILETMP[] = TEXT("cgrom.tmp");

	FILEH fp = NULL;
	for (unsigned int i = 0; fp == 0 && i < _countof(BIOSFILE); ++i)
	{
		fp = File_OpenCurDir(BIOSFILE[i]);
	}
	if (fp == NULL)
	{
		Error("Not found BIOS ROM.");
		return false;
	}

	File_Read(fp, &IPL[0x20000], 0x20000);
	File_Close(fp);

	SCSICheck();	// SCSI IPLなら、$fc0000〜にSCSI BIOSを置く

	for (unsigned int i = 0; i < 0x40000; i += 2)
	{
		std::swap(IPL[i], IPL[i + 1]);
	}

	fp = File_OpenCurDir(FONTFILE);
	if (fp == NULL)
	{
		// cgrom.tmpがある？
		fp = File_OpenCurDir(FONTFILETMP);
	}
	if (fp == NULL)
	{
		Error("Not found FONT ROM.");
		return false;
	}
	File_Read(fp, FONT, 0xc0000);
	File_Close(fp);

	return true;
}

/**
 * リセット
 * @retval true 成功
 * @retval false 失敗
 */
bool WinX68k::Reset()
{
	OPM_Reset();

#ifdef USE_68KEM
	ZeroMemory(&regs, sizeof(m68k_regs));
	regs.a[7] = regs.isp = (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002];
	M68000_SETPC((IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
	regs.sr_high = 0x27;
	M68KRESET();
#else	// USE_68KEM
#ifdef CYCLONE
	m68000_reset();
	m68000_set_reg(M68K_A7, (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002]);
	m68000_set_reg(M68K_PC, (IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
#else	// CYCLONE
	C68k_Reset(&C68K);
	C68k_Set_AReg(&C68K, 7, (IPL[0x30001]<<24)|(IPL[0x30000]<<16)|(IPL[0x30003]<<8)|IPL[0x30002]);
	C68k_Set_PC(&C68K, (IPL[0x30005]<<24)|(IPL[0x30004]<<16)|(IPL[0x30007]<<8)|IPL[0x30006]);
#endif	// CYCLONE
#endif	// USE_68KEM

	Memory_Init();
	CRTC_Init();
	DMA_Init();
	MFP_Init();
	FDC_Init();
	FDD_Reset();
	SASI_Init();
	SCSI_Init();
	IOC_Init();
	SCC_Init();
	PIA_Init();
	RTC_Init();
	TVRAM_Init();
	GVRAM_Init();
	BG_Init();
	Pal_Init();
	IRQH_Init();
#ifndef NO_MIDI
	MIDI_Init();
#endif	// !NO_MIDI
#ifndef NO_WINDRV
	WinDrv_Init();
#endif	// !NO_WINDRV

#ifdef USE_68KEM
	m68000_ICount = 0;
	m68000_ICountBk = 0;
#endif	// USE_68KEM
	ICount = 0;

	DSound::GetInstance()->Stop();
	Keyboard::GetInstance()->Init();
	SRAM_VirusCheck();
//	CDROM_Init();
	DSound::GetInstance()->Play();

	return TRUE;
}

/**
 * 初期化
 * @retval true 成功
 * @retval false 失敗
 */
bool WinX68k::Init()
{
#ifndef USE_FETCHMEM
	IPL = (BYTE*)malloc(0x40000);
	MEM = (BYTE*)malloc(0xc00000);			// とりあえず12Mb
	FONT = (BYTE*)malloc(0xc0000);
#endif	// !USE_FETCHMEM

	ZeroMemory(MEM, 0xc00000);

#ifndef USE_68KEM
	m68000_init();
#endif	// !USE_68KEM
	return true;
}

/**
 * 解放
 */
void WinX68k::Cleanup()
{
#ifndef USE_FETCHMEM
	free(IPL);
	free(MEM);
	free(FONT);
#endif	// !USE_FETCHMEM
}

#define CLOCK_SLICE 200
/**
 * コアのめいんるーぷ
 */
void WinX68k::Exec(int bDrawFrame)
{
	vline = 0;
	int clk_count = -ICount;
	int clk_total = (CRTC_R20L & 0x10) ? VSYNC_HIGH : VSYNC_NORM;
#ifdef SUPPORT_XVI
	int clkdiv = 10;
	if (Config.XVIMode == 1)
	{
		clk_total = (clk_total*16)/10;
		clkdiv = 16;
	}
	else if (Config.XVIMode == 2)
	{
		clk_total = (clk_total*24)/10;
		clkdiv = 24;
	}
#endif	// SUPPORT_XVI
	ICount += clk_total;
	int clk_next = (clk_total/VLINE_TOTAL);
	bool hsync = 1;

	int clk_line = 0;
	int KeyIntCnt = 0, MouseIntCnt = 0;

	do
	{
		const int n = (ICount > CLOCK_SLICE) ? CLOCK_SLICE : ICount;
#ifdef USE_68KEM
		m68000_ICount = m68000_ICountBk = 0;			// 割り込み発生前に与えておかないとダメ（CARAT）
#endif	// USE_68KEM

		if (hsync)
		{
			hsync = 0;
			clk_line = 0;
			MFP_Int(0);
			if (vline == CRTC.IntLineRise)
			{
				MFP_Int(1);
			}
			if (vline == CRTC.IntLineVDisp)
			{
				MFP_Int(9);
			}
		}

		int usedclk;
#ifdef WIN68DEBUG
		if (traceflag/*&&fdctrace*/)
		{
			FILE *fp;
			static DWORD oldpc;
			int i;
			char buf[200];
			fp=fopen("_trace68.txt", "a");
			for (i=0; i<HSYNC_CLK; i++)
			{
				const DWORD pc = M68000_GETPC;
				m68k_disassemble(buf, pc);
//				if (MEM[0xa84c0]) tracing=1000;
//				if (pc==0x9d2a) tracing=5000;
//				if ((pc>=0x2000)&&((pc<=0x8e0e0))) tracing=50000;
//				if (pc<0x10000) tracing=1;
//				if ( (pc&1) )
//				fp=fopen("_trace68.txt", "a");
//				if ( (pc==0x7176) /*&& (Memory_ReadW(oldpc)==0xff1a)*/ ) tracing=100;
//				if ( (/*((pc>=0x27000) && (pc<=0x29000))||*/((pc>=0x27000) && (pc<=0x29000))) && (oldpc!=pc))
				if (/*fdctrace&&*/(oldpc!=pc))
				{
//					//tracing--;
					fprintf(fp, "D0:%08X D1:%08X D2:%08X D3:%08X D4:%08X D5:%08X D6:%08X D7:%08X CR:%04X\n", M68000_GETD(0), M68000_GETD(1), M68000_GETD(2), M68000_GETD(3), M68000_GETD(4), M68000_GETD(5), M68000_GETD(6), M68000_GETD(7), M68000_GETCR);
					fprintf(fp, "A0:%08X A1:%08X A2:%08X A3:%08X A4:%08X A5:%08X A6:%08X A7:%08X SR:%04X\n", M68000_GETA(0), M68000_GETA(1), M68000_GETA(2), M68000_GETA(3), M68000_GETA(4), M68000_GETA(5), M68000_GETA(6), M68000_GETA(7), M68000_GETSR);
					fprintf(fp, "<%04X> (%08X ->) %08X : %s\n", Memory_ReadW(pc), oldpc, pc, buf);
				}
				oldpc = pc;
#ifdef USE_68KEM
				m68000_ICount = 1;
				M68KRUN();
#else	// USE_68KEM
#ifdef CYCLONE
				m68000_execute(1);
#else	// CYCLONE
				C68k_Exec(&C68K, 1);
#endif	// CYCLONE
#endif	// USE_68KEM
			}
			fclose(fp);
			usedclk = clk_line = HSYNC_CLK;
			clk_count = clk_next;
		}
		else
#endif
		{
#ifdef USE_68KEM
			m68000_ICount = n;
			M68KRUN();
			const int m = (n-m68000_ICount-m68000_ICountBk);			// 経過クロック数
#else	// USE_68KEM
#ifdef CYCLONE
			const int m = m68000_execute(n);
#else	// CYCLONE
			const int m = C68k_Exec(&C68K, n);
#endif	// CYCLONE
#endif	// USE_68KEM
#ifdef SUPPORT_XVI
			ClkUsed += m*10;
			usedclk = ClkUsed/clkdiv;
			ClkUsed -= usedclk*clkdiv;
#else	// SUPPORT_XVI
			usedclk = m;
#endif	// SUPPORT_XVI
			clk_line += usedclk;
			ICount -= m;
			clk_count += m;
#ifdef USE_68KEM
			m68000_ICount = m68000_ICountBk = 0;
#endif	// USE_68KEM
		}

		MFP_Timer(usedclk);
		RTC_Timer(usedclk);
		DMA_Exec(0);
		DMA_Exec(1);
#ifndef NO_MERCURY
		DMA_Exec(2);
#endif	// !NO_MERCURY

		if (clk_count >= clk_next)
		{
//			OPM_RomeoOut(Config.BufferSize*5);
//			MIDI_DelayOut((Config.MIDIAutoDelay)?(Config.BufferSize*5):Config.MIDIDelay);
			if (vline == CRTC.VDispEvent)
			{
				MFP_VDispEvent();
			}
			if (vline == CRTC.IntLineFall)
			{
				MFP_Int(1);
			}
			if ((bDrawFrame) && (vline >= CRTC_VSTART) && (vline < CRTC_VEND))
			{
				const uint32_t v = ((vline - CRTC_VSTART) * CRTC_VStep) >> 1;
				if (CRTC_VStep == 1)
				{
					// HighReso 256dot（2度読み）
					if (vline % 2)
					{
						WinDraw::GetInstance()->DrawLine(v);
					}
				}
				else if (CRTC_VStep == 4)
				{
					// LowReso 512dot
					WinDraw::GetInstance()->DrawLine(v);			// 1走査線で2回描く（インターレース）
					WinDraw::GetInstance()->DrawLine(v + 1);
				}
				else
				{
					// High 512dot / Low 256dot
					WinDraw::GetInstance()->DrawLine(v);
				}
			}

			ADPCM_PreUpdate(clk_line);
			OPM_Timer(clk_line);
#ifndef NO_MIDI
			MIDI_Timer(clk_line);
#endif	// !NO_MIDI
#ifndef NO_MERCURY
			Mcry_PreUpdate(clk_line);
#endif	// !NO_MERCURY

			KeyIntCnt++;
			if (KeyIntCnt > (VLINE_TOTAL / 4))
			{
				KeyIntCnt = 0;
				Keyboard_Int();
			}
			MouseIntCnt++;
			if (MouseIntCnt > (VLINE_TOTAL / 8))
			{
				MouseIntCnt = 0;
				SCC_IntCheck();
			}
			DSound::GetInstance()->Send0(clk_line);			// Bufferがでかいとき用

			vline++;
			clk_next = (clk_total * (vline + 1)) / VLINE_TOTAL;
			hsync = 1;
		}
	} while (vline < VLINE_TOTAL);

	if (CRTC_Mode & 2)		// FastClrビットの調整（PITAPAT）
	{
		if (CRTC_FastClr)	// FastClr=1 且つ CRTC_Mode&2 なら 終了
		{
			CRTC_FastClr--;
			if (!CRTC_FastClr)
			{
				CRTC_Mode &= 0xfd;
			}
		}
		else				// FastClr開始
		{
			if (CRTC_R20L & 0x10)
			{
				CRTC_FastClr = 1;
			}
			else
			{
				CRTC_FastClr = 2;
			}
			TVRAM_SetAllDirty();
			GVRAM_FastClear();
		}
	}

//	Joystick_Update();
//	MidiOut_Update();
	DSound::GetInstance()->Send();
	FDD_SetFDInt();
	if (bDrawFrame)
	{
		WinDraw::GetInstance()->Draw();
	}
}

/**
 * メイン
 * @param[in] argc 引数
 * @param[in] argv 引数
 * @return リザルト コード
 */
int WinX68k::Main(HINSTANCE hInstance, HINSTANCE hPreInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	HWND hwnd;
	if ((hwnd = FindWindow(PrgName, NULL)) != NULL)
	{
#if !defined(WIN32_PLATFORM_PSPC)
		ShowWindow(hwnd, SW_RESTORE);
		SetForegroundWindow(hwnd);
#else
		ShowWindow(hwnd, SW_SHOW);
		SetForegroundWindow((HWND)((ULONG)hwnd | 1));
#endif
		return FALSE;
	}

	hInst = hInstance;

	GetModuleFileName(NULL, winx68k_dir, MAX_PATH);
	PathRemoveFileSpec(winx68k_dir);
	PathCombine(winx68k_ini, winx68k_dir, TEXT("winx68k.ini"));

//	if (!hPreInst)
	{
		WNDCLASS winx68k;
		winx68k.style = CS_HREDRAW | CS_VREDRAW;
		winx68k.lpfnWndProc = WndProc;
		winx68k.cbClsExtra = 0;
		winx68k.cbWndExtra = 0;
		winx68k.hInstance = hInstance;
		winx68k.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
		winx68k.hCursor = LoadCursor(NULL, IDC_ARROW);
		winx68k.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		winx68k.lpszMenuName = NULL;
		winx68k.lpszClassName = PrgName;
		if (!RegisterClass(&winx68k))
		{
			return FALSE;
		}
	}

#if !defined(_WIN32_WCE)
	hWndMain = CreateWindow(PrgName, PrgTitle,
			WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION |
			WS_MINIMIZEBOX,
			CW_USEDEFAULT, CW_USEDEFAULT,
			SCREEN_WIDTH, SCREEN_HEIGHT,
			NULL, NULL, hInstance, NULL);
	for (unsigned int i = 0; i < 2; i++)
	{
		RECT rectwindow;
		GetWindowRect(hWndMain, &rectwindow);
		RECT rectclient;
		GetClientRect(hWndMain, &rectclient);
		int cx = SCREEN_WIDTH;
		cx += rectwindow.right - rectwindow.left;
		cx -= rectclient.right - rectclient.left;
		int cy = SCREEN_HEIGHT;
		cy += rectwindow.bottom - rectwindow.top;
		cy -= rectclient.bottom - rectclient.top;
		MoveWindow(hWndMain, rectwindow.left, rectwindow.top, cx, cy, TRUE);
	}
#else	// !defined(_WIN32_WCE)
	hWndMain = CreateWindow(PrgName, PrgTitle,
			WS_VISIBLE,
			0, 0,
			GetSystemMetrics(SM_CXSCREEN),
			GetSystemMetrics(SM_CYSCREEN),
			NULL, NULL, hInstance, NULL);
#endif	// !defined(_WIN32_WCE)

	LoadConfig();

//	CheckCmdLine(lpszCmdLine);

	WinDraw::GetInstance()->ChangeSize();
//	WinDraw::GetInstance()->ChangeMode(false);

	ShowWindow(hWndMain, nCmdShow);
	UpdateWindow(hWndMain);

//	WinUI_Init();

	if (!WinDraw::GetInstance()->Init())
	{
		WinDraw::GetInstance()->Cleanup();
		Error("Failed initializing Gx.");
		return FALSE;
	}

	if (!Init())
	{
		Cleanup();
		WinDraw::GetInstance()->Cleanup();
		Error("Not enough memory.");
		return FALSE;
	}

	if (!LoadROMs())
	{
		Cleanup();
		WinDraw::GetInstance()->Cleanup();
		return FALSE;
	}

//	timeBeginPeriod(1);
	const uint32_t StartTime = GetTickCount();

	const uint32_t SoundSampleRate = Config.SampleRate;
	if (SoundSampleRate)
	{
		ADPCM_Init(SoundSampleRate);
		OPM_Init(4000000/*3579545*/, SoundSampleRate);
#ifndef NO_MERCURY
		Mcry_Init(SoundSampleRate, winx68k_dir);
#endif	// !NO_MERCURY
	}
	else
	{
		ADPCM_Init(100);
		OPM_Init(4000000/*3579545*/, 100);
#ifndef NO_MERCURY
		Mcry_Init(100, winx68k_dir);
#endif	// !NO_MERCURY
	}

//	Joystick_Init();
	SRAM_Init();
	Reset();
	m_timer.Init();

#ifndef NO_MIDI
	MIDI_Init();
	MIDI_SetMimpiMap(Config.ToneMapFile);	// 音色設定ファイル使用反映
	MIDI_EnableMimpiDef(Config.ToneMap);
#endif	// !NO_MIDI

	if (!DSound::GetInstance()->Init(Config.SampleRate, Config.BufferSize))
	{
		if ( Config.DSAlert ) Error("DirectSoundの初期化に失敗しました。\nサウンド無しで継続します。");
	}

	ADPCM_SetVolume((BYTE)Config.PCM_VOL);
	OPM_SetVolume((BYTE)Config.OPM_VOL);
	DSound::GetInstance()->Play();

//	SetCmdLineFD();			// コマンドラインでFD挿入を指示している場合、ここで入れる

	{
		TCHAR szPath[MAX_PATH];
		PathCombine(szPath, winx68k_dir, TEXT("A_DISK.DUP"));
		FDD_SetFD(0, szPath, 0);
		PathCombine(szPath, winx68k_dir, TEXT("B_DISK.DUP"));
		FDD_SetFD(1, szPath, 0);

		PathCombine(szPath, winx68k_dir, TEXT("vkmap.txt"));
		Keyboard::GetInstance()->LoadMap(szPath);
	}

	srand(GetTickCount());

	MSG msg;
	bool bDone = false;
	bool bDrawFrame = false;
	uint_fast8_t nDispFrame = 0;
	while (!bDone)
	{
		while (PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE))
		{
			if (!GetMessage(&msg, NULL, 0, 0))
			{
				bDone = true;
				break;
			}
			DispatchMessage(&msg);
		}

		if (NoWaitMode)
		{
			m_timer.Reset();
		}
		else
		{
			const int r = m_timer.Wait();
			if ((r) && (FrameRate == 0))
			{
				bDrawFrame = true;
			}
			if (r > 0)
			{
				Sleep(1);
				continue;
			}
		}

		nDispFrame++;
		if (FrameRate != 0)
		{
			bDrawFrame = (nDispFrame >= FrameRate);
		}
		else
		{
			if (!bDrawFrame)
			{
				if (nDispFrame >= LIMIT_AUTOFRAME)
				{
					bDrawFrame = true;
					m_timer.Reset();
				}
			}
		}
		Exec(bDrawFrame);
		if (bDrawFrame)
		{
			nDispFrame = 0;
			bDrawFrame = false;
		}
		m_timer.Advance();
	}

//	timeEndPeriod(1);

	Memory_WriteB(0xe8e00d, 0x31);														// SRAM書き込み許可
	Memory_WriteD(0xed0040, Memory_ReadD(0xed0040)+1+(GetTickCount()-StartTime)/60000);	// 積算稼働時間(min.)（ムーンクレスタで使ってるので、てきとーに足しておく）
	Memory_WriteD(0xed0044, Memory_ReadD(0xed0044)+1);									// 積算起動回数

	OPM_Cleanup();
#ifndef NO_MERCURY
	Mcry_Cleanup();
#endif	// !NO_MERCURY

	SRAM_Cleanup();
	FDD_Cleanup();
//	MIDI_Cleanup();
//	Joystick_Cleanup();
	DSound::GetInstance()->Cleanup();
	Cleanup();
	WinDraw::GetInstance()->Cleanup();

	SaveConfig();

	return static_cast<int>(msg.wParam);
}

static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CREATE:
			break;

		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				BeginPaint(hWnd, &ps);
				EndPaint(hWnd, &ps);
			}
			break;

		case WM_LBUTTONDOWN:
			DestroyWindow(hWnd);
			break;

		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			Keyboard::GetInstance()->KeyDown(wParam, lParam);
			break;

		case WM_KEYUP:
		case WM_SYSKEYUP:
			Keyboard::GetInstance()->KeyUp(wParam, lParam);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

#if defined(WAVEMNG_CBMAIN)
		case MM_WOM_DONE:
			DSound::GetInstance()->OnWomDone(reinterpret_cast<HWAVEOUT>(wParam), reinterpret_cast<WAVEHDR*>(lParam));
			break;
#endif	// defined(WAVEMNG_CBMAIN)

		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0L;
}

// --------------------------------------------------------------------------
//   お約束のメイン
// --------------------------------------------------------------------------

#if defined(__GNUC__)
#define _tWinMain	WinMain
#endif	// defined(__GUNC__)

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPreInst, LPTSTR lpszCmdLine, int nCmdShow)
{
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	CoInitialize(NULL);
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

#ifndef _WIN32_WCE
	Console::Open();
#endif	// !_WIN32_WCE

	const int r = WinX68k::GetInstance()->Main(hInstance, hPreInst, lpszCmdLine, nCmdShow);

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	CoUninitialize();
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	return r;
}
