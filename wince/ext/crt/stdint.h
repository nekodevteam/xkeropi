/**
 * @file	stdint.h
 */

#pragma once

#include <windows.h>

typedef signed char			int8_t;			/*!< signed 8bit */
typedef unsigned char		uint8_t;		/*!< unsigned 8bit */
typedef signed short		int16_t;		/*!< signed 16bit */
typedef unsigned short		uint16_t;		/*!< unsigned 16bit */
typedef signed int			int32_t;		/*!< signed 32bit */
typedef unsigned int		uint32_t;		/*!< unsigned 32bit */
typedef signed __int64		int64_t;		/*!< signed 64bit */
typedef unsigned __int64	uint64_t;		/*!< unsigned 64bit */

typedef signed int			int_fast8_t;	/*!< signed 8bit */
typedef unsigned int		uint_fast8_t;	/*!< unsigned 8bit */
typedef signed int			int_fast16_t;	/*!< signed 16bit */
typedef unsigned int		uint_fast16_t;	/*!< unsigned 16bit */

typedef INT_PTR				intptr_t;		/*!< pointer */
