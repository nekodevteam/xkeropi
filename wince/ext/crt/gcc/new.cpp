#include <stdio.h>
#include <memory.h>

extern "C"
{
	/**
	 * new
	 */
	void* _Znwj(unsigned s)
	{
		return malloc(s);
	}

	/**
	 * delete
	 */
	void _ZdlPvj(void* p, unsigned s)
	{
		free(p);
	}

	/**
	 * new[]
	 */
	void* _Znaj(unsigned s)
	{
		return malloc(s);
	}

	/**
	 * delete[]
	 */
	void _ZdaPv(void* p)
	{
		free(p);
	}

	/**
	 * delete[]
	 */
	void _ZdaPvj(void* p, unsigned s)
	{
		free(p);
	}
}
