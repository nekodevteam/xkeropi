/**
 * @file	status.h
 * @brief	ステータス バー クラスの動作の定義を行います
 */

#pragma once

INLINE static void StatBar_Show(int sw) { }
INLINE static void StatBar_UpdateTimer(void) { }
INLINE static void StatBar_SetFDD(int drv, LPCTSTR file) { }
INLINE static void StatBar_ParamFDD(int drv, int access, int insert, int blink) { }
INLINE static void StatBar_HDD(int sw) { }
