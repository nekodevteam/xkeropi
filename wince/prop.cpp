// ---------------------------------------------------------------------------------------
//  PROP.C - 各種設定用プロパティシートと設定値管理
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "winx68k.h"
#include "prop.h"

#define CFGLEN MAX_PATH

static const TCHAR ini_title[] = TEXT("WinX68k");
Win68Conf Config;

INLINE static UINT _GetPrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, INT nDefault, LPCTSTR lpFileName) { return nDefault; }
INLINE static DWORD _GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault, LPTSTR lpReturnedString, DWORD nSize, LPCTSTR lpFileName) { lstrcpy(lpReturnedString, lpDefault); return lstrlen(lpDefault); }
INLINE static BOOL _WritePrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpString, LPCTSTR lpFileName) { return TRUE; }

static LPCTSTR makeBOOL(uint_fast8_t value) {

	return (value) ? TEXT("true") : TEXT("false");
}

static BYTE Aacmp(LPCTSTR cmp, LPCTSTR str) {

	while(*str) {
		TCHAR p = *cmp++;
		if (!p) {
			break;
		}
		if (p >= 'a' && p <= 'z') {
			p -= 0x20;
		}
		if (p != *str++) {
			return(-1);
		}
	}
	return(0);
}

static uint_fast8_t solveBOOL(LPCTSTR str) {

	if ((!Aacmp(str, TEXT("TRUE"))) || (!Aacmp(str, TEXT("ON"))) ||
		(!Aacmp(str, TEXT("+"))) || (!Aacmp(str, TEXT("1"))) ||
		(!Aacmp(str, TEXT("ENABLE")))) {
		return(1);
	}
	return(0);
}

static uint_fast8_t _GetPrivateProfileBool(LPCTSTR lpAppName, LPCTSTR lpKeyName, uint_fast8_t bDefault, LPCTSTR lpFileName)
{
	TCHAR buf[CFGLEN];
	LPCTSTR lpDefault = (bDefault) ? TEXT("1") : TEXT("0");
	_GetPrivateProfileString(lpAppName, lpKeyName, lpDefault, buf, _countof(buf), lpFileName);
	return solveBOOL(buf);
}

static BOOL _WritePrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName)
{
	TCHAR buf[CFGLEN];
	wsprintf(buf, TEXT("%d"), nValue);
	return _WritePrivateProfileString(lpAppName, lpKeyName, buf, lpFileName);
}

void LoadConfig(void)
{
	WinX68k::GetInstance()->FrameRate = (BYTE)_GetPrivateProfileInt(ini_title, TEXT("FrameRate"), 0, winx68k_ini);

	Config.OPM_VOL = _GetPrivateProfileInt(ini_title, TEXT("OPM_Volume"), 12, winx68k_ini);
	Config.PCM_VOL = _GetPrivateProfileInt(ini_title, TEXT("PCM_Volume"), 15, winx68k_ini);
//	Config.MCR_VOL = _GetPrivateProfileInt(ini_title, TEXT("MCR_Volume"), 13, winx68k_ini);
	Config.SampleRate = _GetPrivateProfileInt(ini_title, TEXT("SampleRate"), 22050, winx68k_ini);
	Config.BufferSize = _GetPrivateProfileInt(ini_title, TEXT("BufferSize"), 50, winx68k_ini);

	Config.MouseSpeed = _GetPrivateProfileInt(ini_title, TEXT("MouseSpeed"), 10, winx68k_ini);

	Config.DSAlert = _GetPrivateProfileBool(ini_title, TEXT("DSAlert"), TRUE, winx68k_ini);
	Config.Sound_LPF = _GetPrivateProfileBool(ini_title, TEXT("SoundLPF"), TRUE, winx68k_ini);
#ifdef USE_ROMEO
	Config.SoundROMEO = _GetPrivateProfileBool(ini_title, TEXT("UseRomeo"), FALSE, winx68k_ini);
#endif	// USE_ROMEO

	Config.JoySwap = _GetPrivateProfileBool(ini_title, TEXT("JoySwap"), FALSE, winx68k_ini);

	Config.JoyKey = _GetPrivateProfileBool(ini_title, TEXT("JoyKey"), FALSE, winx68k_ini);
	Config.JoyKeyReverse = _GetPrivateProfileBool(ini_title, TEXT("JoyKeyReverse"), FALSE, winx68k_ini);
	Config.JoyKeyJoy2 = _GetPrivateProfileBool(ini_title, TEXT("JoyKeyJoy2"), FALSE, winx68k_ini);
	Config.SRAMWarning = _GetPrivateProfileBool(ini_title, TEXT("SRAMBootWarning"), TRUE, winx68k_ini);

	Config.XVIMode = (BYTE)_GetPrivateProfileInt(ini_title, TEXT("XVIMode"), 0, winx68k_ini);

	for (unsigned i = 0; i < _countof(Config.JOY_BTN); i++)
	{
		for (unsigned j = 0; j < _countof(Config.JOY_BTN[0]); j++)
		{
			TCHAR buf[CFGLEN];
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			Config.JOY_BTN[i][j] = _GetPrivateProfileInt(ini_title, buf, j, winx68k_ini);
		}
	}

	for (unsigned i = 0; i < _countof(Config.HDImage); i++)
	{
		TCHAR buf[CFGLEN];
		wsprintf(buf, TEXT("HDD%d"), i);
		_GetPrivateProfileString(ini_title, buf, TEXT(""), Config.HDImage[i], MAX_PATH, winx68k_ini);
	}
}


void SaveConfig(void)
{
	_WritePrivateProfileInt(ini_title, TEXT("FrameRate"), WinX68k::GetInstance()->FrameRate, winx68k_ini);

	_WritePrivateProfileInt(ini_title, TEXT("OPM_Volume"), Config.OPM_VOL, winx68k_ini);
	_WritePrivateProfileInt(ini_title, TEXT("PCM_Volume"), Config.PCM_VOL, winx68k_ini);
//	_WritePrivateProfileInt(ini_title, TEXT("MCR_Volume"), Config.MCR_VOL, winx68k_ini);
	_WritePrivateProfileInt(ini_title, TEXT("SampleRate"), Config.SampleRate, winx68k_ini);
	_WritePrivateProfileInt(ini_title, TEXT("BufferSize"), Config.BufferSize, winx68k_ini);

	_WritePrivateProfileInt(ini_title, TEXT("MouseSpeed"), Config.MouseSpeed, winx68k_ini);

	_WritePrivateProfileString(ini_title, TEXT("SoundLPF"), makeBOOL(Config.Sound_LPF), winx68k_ini);
#ifdef USE_ROMEO
	_WritePrivateProfileString(ini_title, TEXT("UseRomeo"), makeBOOL(Config.SoundROMEO), winx68k_ini);
#endif	// USE_ROMEO

	_WritePrivateProfileString(ini_title, TEXT("JoySwap"), makeBOOL(Config.JoySwap), winx68k_ini);

	_WritePrivateProfileString(ini_title, TEXT("JoyKey"), makeBOOL(Config.JoyKey), winx68k_ini);
	_WritePrivateProfileString(ini_title, TEXT("JoyKeyReverse"), makeBOOL(Config.JoyKeyReverse), winx68k_ini);
	_WritePrivateProfileString(ini_title, TEXT("JoyKeyJoy2"), makeBOOL(Config.JoyKeyJoy2), winx68k_ini);
	_WritePrivateProfileString(ini_title, TEXT("SRAMBootWarning"), makeBOOL(Config.SRAMWarning), winx68k_ini);

	_WritePrivateProfileInt(ini_title, TEXT("XVIMode"), Config.XVIMode, winx68k_ini);

	for (unsigned i = 0; i < _countof(Config.JOY_BTN); i++)
	{
		for (unsigned j = 0; j < _countof(Config.JOY_BTN[0]); j++)
		{
			TCHAR buf[CFGLEN];
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			_WritePrivateProfileInt(ini_title, buf, Config.JOY_BTN[i][j], winx68k_ini);
		}
	}

	for (unsigned i = 0; i < _countof(Config.HDImage); i++)
	{
		TCHAR buf[CFGLEN];
		wsprintf(buf, TEXT("HDD%d"), i);
		_WritePrivateProfileString(ini_title, buf, Config.HDImage[i], winx68k_ini);
	}
}
