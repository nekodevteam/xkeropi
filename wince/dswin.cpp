/**
 * @file	dswin.cpp
 * @brief	サウンド クラスの動作の定義を行います
 */

#include "common.h"
#include "dswin.hpp"
#include <algorithm>
#include "../fmgen/fmg_wrap.h"
#include "../x68k/adpcm.h"
#ifndef NO_MERCURY
#include "../x68k/mercury.h"
#endif	// !NO_MERCURY
#include "winx68k.h"

#ifndef _WIN32_WCE
#pragma comment(lib, "winmm.lib")
#endif	// !_WIN32_WCE

#define SOUNDBUFFERS	2		/*!< サウンド バッファ数 */

DSound DSound::sm_instance;		/*!< 唯一のインスタンスです */

/**
 * コンストラクタ
 */
DSound::DSound()
	: m_nRateBase(22050)
	, m_nSamples(0)
	, m_pBuffer(NULL)
	, m_nPreCounter(0)
	, m_hwo(NULL)
	, m_wh(NULL)
{
}

/**
 * デストラクタ
 */
DSound::~DSound()
{
}

/**
 * 初期化
 * @param[in] rate サンプリング レート
 * @param[in] buflen バッファ サイズ
 * @retval true 成功
 * @retval false 失敗
 */
bool DSound::Init(uint32_t rate, uint32_t buflen)
{
	// 初期化済みか？
	if (m_hwo != NULL)
	{
		return false;
	}

	DWORD dwFormat = 0;
	switch (rate)
	{
		case 0:
			return true;

		case 11025:
			dwFormat = WAVE_FORMAT_1S16;
			break;

		case 22050:
			dwFormat = WAVE_FORMAT_2S16;
			break;

		case 44100:
			dwFormat = WAVE_FORMAT_4S16;
			break;

		default:
			return false;
	}

	const int nDevices = waveOutGetNumDevs();
	int num = 0;
	while (num < nDevices)
	{
		WAVEOUTCAPS woc;
		if (waveOutGetDevCaps(num, &woc, sizeof(woc)) == MMSYSERR_NOERROR)
		{
			if (woc.dwFormats & dwFormat)
			{
				break;
			}
		}
		num++;
	}
	if (num >= nDevices)
	{
		return false;
	}

	m_nSamples = rate * buflen / 200;
	const unsigned nBufferBytes = m_nSamples * sizeof(int16_t) * 2;
	m_pBuffer = static_cast<uint8_t*>(malloc(nBufferBytes * SOUNDBUFFERS));
	if (m_pBuffer == NULL)
	{
		return false;
	}
	ZeroMemory(m_pBuffer, nBufferBytes * SOUNDBUFFERS);

	m_buffer.Allocate(nBufferBytes * 2);

	m_wh = static_cast<WAVEHDR*>(malloc(sizeof(WAVEHDR) * SOUNDBUFFERS));
	if (m_wh == NULL)
	{
		return false;
	}
	for (unsigned i = 0; i < SOUNDBUFFERS; i++)
	{
		ZeroMemory(&m_wh[i], sizeof(m_wh[i]));
		m_wh[i].lpData = reinterpret_cast<char*>(m_pBuffer + (nBufferBytes * i));
		m_wh[i].dwBufferLength = nBufferBytes;
		m_wh[i].dwUser = i;
	}

	WAVEFORMATEX wfex;
	wfex.wFormatTag = WAVE_FORMAT_PCM;
	wfex.nSamplesPerSec = rate;
	wfex.wBitsPerSample = 16;
	wfex.nChannels = 2;
	wfex.nBlockAlign = wfex.nChannels * (wfex.wBitsPerSample / 8);
	wfex.nAvgBytesPerSec = wfex.nSamplesPerSec * wfex.nBlockAlign;
#if defined(WAVEMNG_CBMAIN)
	const MMRESULT mr = waveOutOpen(&m_hwo, num, &wfex, reinterpret_cast<DWORD_PTR>(hWndMain), 0, CALLBACK_WINDOW);
#else	// defined(WAVEMNG_CBMAIN)
	const MMRESULT mr = waveOutOpen(&m_hwo, num, &wfex, reinterpret_cast<DWORD_PTR>(OnWaveOutCallback), reinterpret_cast<DWORD_PTR>(this), CALLBACK_FUNCTION);
#endif	// defined(WAVEMNG_CBMAIN)
	if (mr != MMSYSERR_NOERROR)
	{
		return false;
	}

	for (unsigned i = 0; i < SOUNDBUFFERS; i++)
	{
		waveOutPrepareHeader(m_hwo, &m_wh[i], sizeof(m_wh[i]));
		waveOutWrite(m_hwo, &m_wh[i], sizeof(m_wh[i]));
	}

	return true;
}

/**
 * 解放
 */
void DSound::Cleanup()
{
	if (m_hwo != NULL)
	{
#if !((defined(WIN32_PLATFORM_PSPC)) || (!defined(ARM)))
		waveOutReset(m_hwo);
#endif	// !((defined(WIN32_PLATFORM_PSPC)) || (!defined(ARM)))
		if (m_wh)
		{
			for (unsigned i = 0; i < SOUNDBUFFERS; i++)
			{
				waveOutUnprepareHeader(m_hwo, &m_wh[i], sizeof(m_wh[i]));
				m_wh[i].lpData = NULL;
			}
		}
#if (defined(WIN32_PLATFORM_PSPC)) || (!defined(ARM))
		waveOutPause(m_hwo);
		waveOutReset(m_hwo);
#endif	// (defined(WIN32_PLATFORM_PSPC)) || (!defined(ARM))

		unsigned nRetry = 5;
		do
		{
			if (waveOutClose(m_hwo) == MMSYSERR_NOERROR)
			{
				break;
			}
			Sleep(100);
		} while(--nRetry);
		m_hwo = NULL;
	}

	free(m_wh);
	m_wh = NULL;

	free(m_pBuffer);
	m_pBuffer = NULL;
}

/**
 * 再生
 */
void DSound::Play()
{
}

/**
 * 停止
 */
void DSound::Stop()
{
}

/**
 * サンプル更新
 * @param[in] nLength 長さ
 */
void DSound::Update(unsigned int nLength)
{
	while (nLength > 0)
	{
		unsigned int nSize;
		int16_t* p = static_cast<int16_t*>(m_buffer.Lock(nSize));
		if (p == NULL)
		{
			break;
		}

		nSize = (std::min)(nSize, nLength);
		const unsigned int nSamples = nSize / (sizeof(int16_t) * 2);

		ADPCM_Update(p, nSamples);
		OPM_Update(p, nSamples);
#ifndef NO_MERCURY
		Mcry_Update(p, nSamples);
#endif	// !NO_MERCURY
		m_buffer.Unlock(nSize);
		nLength -= nSize;
	}
}

/**
 * サウンド更新
 * @param[in] clock クロック
 */
void DSound::Send0(int clock)
{
	if (m_hwo != NULL)
	{
		m_nPreCounter += (m_nRateBase * clock);
		int length = 0;
		while (m_nPreCounter >= 10000000L)
		{
			length++;
			m_nPreCounter -= 10000000L;
		}
		m_guard.lock();
		Update(length * (sizeof(int16_t) * 2));
		m_guard.unlock();
	}
}

/**
 * サウンド更新
 */
void DSound::Send()
{
	// 本来は sdlaudio_callback内で足りないサンプルを補償したいが、
	// スレッドが異なるので、メインスレッドで埋めておく
	if (m_hwo != NULL)
	{
		m_guard.lock();
		const unsigned nAvail = m_buffer.Avail();
		const unsigned nBytes = m_nSamples * (sizeof(int16_t) * 2);
		if (nAvail < nBytes)
		{
			Update(nBytes - nAvail);
		}
		m_guard.unlock();
	}
}

/**
 * ストリーム取得
 * @param[in] stream ストリーム
 */
inline void DSound::GetStream(char* pStream)
{
	const unsigned nBytes = m_nSamples * (sizeof(int16_t) * 2);
	unsigned i = 0;

	m_guard.lock();
	while (i < nBytes)
	{
		const unsigned r = m_buffer.Read(pStream + i, nBytes - i);
		if (r == 0)
		{
			break;
		}
		i += r;
	}
	m_guard.unlock();

	if (i < nBytes)
	{
		ZeroMemory(pStream + i, nBytes - i);
	}
}

/**
 * MM_WOM_DONE メッセージ
 * @param[in] hwo WaveOut
 * @param[in] hw ヘッダ
 */
void DSound::OnWomDone(HWAVEOUT hwo, WAVEHDR* wh)
{
	waveOutUnprepareHeader(hwo, wh, sizeof(*wh));
	if (wh->lpData)
	{
		GetStream(wh->lpData);
		waveOutPrepareHeader(hwo, wh, sizeof(*wh));
		waveOutWrite(hwo, wh, sizeof(*wh));
	}
}

#if !defined(WAVEMNG_CBMAIN)
/**
 * コールバック
 * @param[in] hwo WaveOut
 * @param[in] uMsg メッセージ
 * @param[in] dwInstance インスタンス
 * @param[in] dwParam1 パラメタ
 * @param[in] dwParam2 パラメタ
 */
void CALLBACK DSound::OnWaveOutCallback(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2)
{
	if (uMsg == MM_WOM_DONE)
	{
		WAVEHDR* wh = reinterpret_cast<WAVEHDR*>(dwParam1);
		waveOutUnprepareHeader(hwo, wh, sizeof(*wh));
		if (wh->lpData)
		{
			(reinterpret_cast<DSound*>(dwInstance))->GetStream(wh->lpData);
			waveOutPrepareHeader(hwo, wh, sizeof(*wh));
			waveOutWrite(hwo, wh, sizeof(*wh));
		}
	}
}
#endif	// !defined(WAVEMNG_CBMAIN)
