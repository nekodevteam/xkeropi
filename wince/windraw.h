/**
 * @file	windraw.h
 * @brief	描画クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#define SCREEN_WIDTH		768		/*!< 幅 */
#define SCREEN_HEIGHT		512		/*!< 高さ */

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

#ifndef USE_NATIVECOLOR
static const WORD WinDraw_Pal16B = 0x001f;
static const WORD WinDraw_Pal16R = 0xf800;
static const WORD WinDraw_Pal16G = 0x07e0;
#endif	// !USE_NATIVECOLOR

void WinDraw_ChangeSize(void);

#ifdef __cplusplus
}

/**
 * @brief 描画クラス
 */
class WinDraw
{
public:
	static WinDraw* GetInstance();

	WinDraw();
	bool Init();
	void Cleanup();
	void Redraw(void);
	void Draw();
	void DrawLine(uint32_t v);
	void ChangeSize();

private:
	static WinDraw sm_instance;	/*!< 唯一のインスタンスです */

	uint8_t Draw_DrawFlag;
	uint32_t m_nFrameCount;		/*!< カウント */
	SIZE m_szScreen;			/*!< サーフェス サイズ */
	int m_nYAlign;				/*!< Y アライメント */
	int m_nLastW;
	int m_nLastH;

#ifdef USES_ALLFLASH
	uint32_t RedrawLastV;
#endif	// USES_ALLFLASH

#ifdef USE_GX
	uint16_t* m_pBuffer;		/*!< バッファ */
#else	// USE_GX
	/**
	 * @brief ビットマップ情報
	 */
	struct BMPINFO
	{
		BITMAPINFOHEADER bmiHeader;		/*!< ヘッダ */
		DWORD bmiColors[4];				/*!< 色 */
	};
	HBITMAP m_hBitmap;			/*!< ビットマップ ハンドル */
	void* m_pImage;				/*!< イメージ */
#endif	// USE_GX

	void DrawSub(void* pDest, int nYAlign, RECT& r);
};

/**
 * インスタンスを得る
 * @return インスタンス
 */
inline WinDraw* WinDraw::GetInstance()
{
	return &sm_instance;
}

#endif	// __cplusplus
