/**
 * @file	keyboard.h
 * @brief	キーボード入力クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

uint_fast8_t Keyboard_BufferEmpty(void);
uint_fast8_t Keyboard_Read(void);
#define Keyboard_Write(nData) do { } while (FALSE /*CONSTCOND*/)

#ifdef __cplusplus
}

/**
 * @brief キーボード
 */
class Keyboard
{
public:
	static Keyboard* GetInstance();

	Keyboard();
	void LoadMap(LPCTSTR lpMap);
	void Init();
	void KeyDown(WPARAM wParam, LPARAM lParam);
	void KeyUp(WPARAM wParam, LPARAM lParam);
	bool BufferEmpty() const;
	uint_fast8_t Read();

private:
	static Keyboard sm_instance;

	enum
	{
		KeyBufSize = (1 << 7)
	};

	uint8_t m_KeyBufWP;
	uint8_t m_KeyBufRP;
	uint8_t m_KeyBuf[KeyBufSize];		/*!< バッファ */
	uint8_t m_KeyTable[256];
};

/**
 * インスタンスを得る
 * @return インスタンス
 */
inline Keyboard* Keyboard::GetInstance()
{
	return &sm_instance;
}

/**
 * バッファは空か?
 * @retval true 空
 * @retval false データあり
 */
inline bool Keyboard::BufferEmpty() const
{
	return (m_KeyBufWP == m_KeyBufRP);
}

#endif	// __cplusplus
