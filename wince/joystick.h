
#pragma once

EXTERNC uint_fast8_t FASTCALL Joystick_Read(uint_fast8_t num);
EXTERNC void FASTCALL Joystick_Write(uint_fast8_t num, uint_fast8_t data);
