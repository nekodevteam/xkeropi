// ---------------------------------------------------------------------------------------
//  FILEIO.C - DOSIO.C中のファイル管理部だけ版
//  DOSIOほぼそのまま。
// ---------------------------------------------------------------------------------------
#include "common.h"
#include "fileio.h"
#include <shlwapi.h>
#include "winx68k.h"

FILEH File_Open(LPCTSTR filename) {

	FILEH	ret;

	if ((ret = CreateFile(filename, GENERIC_READ | GENERIC_WRITE,
						0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL))
											== INVALID_HANDLE_VALUE) {
		if ((ret = CreateFile(filename, GENERIC_READ,
						0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL))
											== INVALID_HANDLE_VALUE) {
			return ((FILEH)FALSE);
		}
	}
	return(ret);
}

FILEH File_Create(LPCTSTR filename) {

	FILEH	ret;

	if ((ret = CreateFile(filename, GENERIC_READ | GENERIC_WRITE,
						 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL))
									== INVALID_HANDLE_VALUE) {
		return ((FILEH)FALSE);
	}
	return(ret);
}

DWORD File_Seek(FILEH handle, long pointer, short mode) {

	return (SetFilePointer(handle, pointer, 0, mode));
}

DWORD File_Read(FILEH handle, void *data, DWORD length) {

	DWORD	readsize;

	if (!ReadFile(handle, data, length, &readsize, NULL)) {
		return(0);
	}
	return ((DWORD)readsize);
}

DWORD File_Write(FILEH handle, const void *data, DWORD length) {

	DWORD	writesize;

	if (!WriteFile(handle, data, length, &writesize, NULL)) {
		return (0);
	}
	return ((DWORD)writesize);
}

short File_Close(FILEH handle) {

	CloseHandle(handle);
	return (TRUE);
}

short File_Attr(LPCTSTR filename) {

	return ((short)GetFileAttributes(filename));
}


// EXEのあるDir中のファイルの操作

FILEH	File_OpenCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Open(buf));
}

FILEH	File_CreateCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Create(buf));
}

short	File_AttrCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Attr(buf));
}
