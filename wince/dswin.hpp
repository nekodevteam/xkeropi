/**
 * @file	dswin.hpp
 * @brief	サウンド クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "../misc/ringbuffer.hpp"
#include "../misc/win32/guard.h"

/**
 * @brief サウンド クラス
 */
class DSound
{
public:
	static DSound* GetInstance()
	{
		return &sm_instance;
	}

	DSound();
	~DSound();
	bool Init(uint32_t rate, uint32_t buflen);
	void Cleanup();
	void Play();
	void Stop();
	void Send0(int clock);
	void Send();
	void OnWomDone(HWAVEOUT hwo, WAVEHDR* wh);

private:
	static DSound sm_instance;	/*!< 唯一のインスタンスです */

	uint32_t m_nRateBase;		/*!< レート */
	uint32_t m_nSamples;		/*!< サンプル数 */
	uint8_t* m_pBuffer;			/*!< バッファ */
	int m_nPreCounter;			/*!< カウンタ */
	CGuard m_guard;				/*!< ガード */
	RingBuffer m_buffer;		/*!< バッファ */
	HWAVEOUT m_hwo;				/*!< waveout */
	WAVEHDR* m_wh;				/*!< wavehdr */

	void Update(unsigned int length);
	void GetStream(char* pStream);

#if !defined(WAVEMNG_CBMAIN)
	static void CALLBACK OnWaveOutCallback(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2);
#endif	// !defined(WAVEMNG_CBMAIN)
};
