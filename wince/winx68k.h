/**
 * @file	winx68k.h
 * @brief	メイン クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern const TCHAR PrgName[];
extern const TCHAR PrgTitle[];

extern TCHAR winx68k_dir[MAX_PATH];
extern TCHAR winx68k_ini[MAX_PATH];

extern HINSTANCE hInst;
extern HWND hWndMain;

int WinX68k_Reset(void);

#ifdef __cplusplus
}

#include "timer.hpp"

/**
 * @brief メイン クラス
 */
class WinX68k
{
public:
	uint8_t FrameRate;
#ifdef WIN68DEBUG
	uint8_t traceflag;
#endif	// WIN68DEBUG

	static WinX68k* GetInstance();

	WinX68k();
	~WinX68k();
	int Main(HINSTANCE hInstance, HINSTANCE hPreInst, LPTSTR lpszCmdLine, int nCmdShow);

private:
	static WinX68k sm_instance;

#ifdef SUPPORT_XVI
	int ClkUsed;
#endif	// SUPPORT_XVI

	Timer m_timer;

	bool Init();
	void Cleanup();
	bool Reset();
	void Exec(int bDrawFrame);
	static void SCSICheck();
	static bool LoadROMs();
};

/**
 * インスタンスを返す
 * @return インスタンス
 */
inline WinX68k* WinX68k::GetInstance()
{
	return &sm_instance;
}

#endif	// __cplusplus
