#ifndef winx68k_common_h
#define winx68k_common_h

#include "windows.h"
#include <stdio.h>
#include <string.h>

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else	// HAVE_STDINT_H
typedef signed char				int8_t;		/*!< signed 8bit */
typedef unsigned char			uint8_t;	/*!< unsigned 8bit */
typedef signed short			int16_t;	/*!< signed 16bit */
typedef unsigned short			uint16_t;	/*!< unsigned 16bit */
typedef signed int				int32_t;	/*!< signed 32bit */
typedef unsigned int			uint32_t;	/*!< unsigned 32bit */
typedef signed long long int	int64_t;	/*!< signed 64bit */
typedef unsigned long long int	uint64_t;	/*!< unsigned 64bit */
#define uint_fast8_t		uint8_t
#define uint_fast16_t		uint16_t
#endif	// HAVE_STDINT_H

#define	TRUE		1
#define	FALSE		0
#define	SUCCESS		0
#define	FAILURE		1

//#define WIN68DEBUG						// -DEBUG オプション用

#ifdef USE_68KEM
#define FASTCALL		__attribute__((fastcall))
#else	// USE_68KEM
#define FASTCALL
#endif	// USE_68KEM
#define STDCALL
#define	LABEL
#define	__stdcall

#ifdef __cplusplus
#define EXTERNC extern "C"
#else	// __cplusplus
#define EXTERNC extern
#endif	// __cplusplus

#ifndef _countof
//! countof
#define _countof(x)		(sizeof((x)) / sizeof((x)[0]))
#endif	// _countof

typedef union {
	struct {
		BYTE l;
		BYTE h;
	} b;
	WORD w;
} PAIR;

#ifdef __cplusplus
extern "C" {
#endif

void Error(const char* s);

#ifdef __cplusplus
}
#endif

#endif //winx68k_common_h
