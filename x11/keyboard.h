#ifndef _winx68k_keyboard
#define _winx68k_keyboard

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern BYTE KeyTable[512];
extern const BYTE KeyTableMaster[512];

void Keyboard_Init(void);
void Keyboard_KeyDown(DWORD vkcode);
void Keyboard_KeyUp(DWORD vkcode);
uint_fast8_t Keyboard_BufferEmpty(void);
uint_fast8_t Keyboard_Read(void);
void Keyboard_Write(uint_fast8_t nData);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_keyboard
