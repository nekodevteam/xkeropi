#ifndef winx1_timer_h
#define winx1_timer_h

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

void Timer_Init(void);
void Timer_Reset(void);
WORD Timer_GetCount(void);
void Timer_SetCount(WORD);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //winx1_timer_h
