/*	$Id: winui.h,v 1.1.1.1 2003/04/28 18:06:56 nonaka Exp $	*/

#ifndef _winx68k_winui_h
#define _winx68k_winui_h

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	DWORD	LastClock[4];

void WinUI_Init(void);
GtkWidget *create_menu(GtkWidget *w);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //winx68k_winui_h
