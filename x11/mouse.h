#ifndef _winx68k_mouse
#define _winx68k_mouse

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern	int	MousePosX;
extern	int	MousePosY;
extern	BYTE	MouseStat;
extern	BYTE	MouseSW;

void Mouse_Init(void);
void Mouse_Event(DWORD wparam, DWORD lparam);
void Mouse_SetData(void);
void Mouse_StartCapture(int flag);
void Mouse_ChangePos(void);

#ifdef __cplusplus
}
#endif	// __cplusplus

#endif //_winx68k_mouse
