# xkeropi
エックスけろぴー (WinX68k for X)

## 本レポジトリについて
- xkeropi をちょこちょこ修正したものです

## 参照ソースコード
- 本レポジトリは、下記のソースコードを利用させていただいています

### Winx68k
- けんじょさんによる Windows版 (オリジナル)
- [Web Page](http://retropc.net/usalin/)

### xkeropi
- nonakap さんによる X11 ポート
- [GitHub レポジトリ](https://github.com/nonakap/xkeropi)

### px68k
- ひっそりぃさんによる Android/iOS/PSP/RPI/MacOSX ポート
- [GitHub レポジトリ](https://github.com/hissorii/px68k)

### px68k-libretro
- r-type (not6) さんによる libretro ポート
- [GitHub レポジトリ](https://github.com/r-type/px68k-libretro)

### fmgen
- cisc さんによる OPM エンジン
- [Web Page](http://retropc.net/cisc/sound/)

### Cyclone 68000
- notaz さんによる ARM 32bit 向け MC68000 エンジン
- [Web Page](https://notaz.gp2x.de/cyclone.php)

## ビルド方法

### Windows 32bit
- winx68k.sln (Visual Studio 2008) をビルドしてください

### Windows 64bit
- keropi.sln (Visual Studio 2017) をビルドしてください

### X11
- 関連モジュール
~~~shell:インストール
$ sudo apt install build-essential
$ sudo apt install xutils-dev
$ sudo apt install libgtk2.0-dev
$ sudo apt install libsdl1.2-dev
~~~
- ビルド
~~~shell:make
$ xmkmf -a
$ make
~~~

----
Twitter: [@yuinejp](https://twitter.com/yuinejp)
