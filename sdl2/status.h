﻿/**
 * @file	status.h
 * @brief	ステータス バー クラスの動作の定義を行います
 */

#pragma once

INLINE void StatBar_Show(int sw) { }
INLINE void StatBar_UpdateTimer(void) { }
INLINE void StatBar_SetFDD(int drv, LPCTSTR file) { }
INLINE void StatBar_ParamFDD(int drv, int access, int insert, int blink) { }
INLINE void StatBar_HDD(int sw) { }
