﻿/**
 * @file	timer.h
 * @brief	タイマ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "../x68k/crtc.h"

#define LIMIT_AUTOFRAME 8

/**
 * @brief タイマ
 */
struct Timer
{
	uint32_t m_nTick;		/*!< Tick */

	/**
	 * コンストラクタ
	 */
	Timer()
	{
		Reset();
	}

	/**
	 * 初期化
	 */
	void Init()
	{
		Reset();
	}

	/**
	 * リセット
	 */
	void Reset()
	{
		m_nTick = timeGetTime() * 10000;
	}

	/**
	 * フレーム加算
	 */
	void Advance()
	{
		m_nTick += (CRTC_R20L & 0x10) ? VSYNC_HIGH : VSYNC_NORM;
	}

	/**
	 * ウェイト フラグを得る
	 * @return フラグ
	 */
	int Wait()
	{
		const uint32_t nNow = timeGetTime() * 10000;
		const int32_t nPast = nNow - m_nTick;

		const uint32_t nLimit = 1000 * 10000 * LIMIT_AUTOFRAME / 60;
		if (static_cast<uint32_t>(nPast + nLimit) >= (nLimit * 2))
		{
			m_nTick = nNow;
			return -1;
		}
		else
		{
			return (nPast < 0) ? 1 : 0;
		}
	}
};
