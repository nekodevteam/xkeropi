﻿/**
 * @file	common.h
 * @brief	共通クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef _WIN32
#include <tchar.h>
#endif	// _WIN32
#include <windows.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <SDL2/SDL.h>
#else	// _WIN32
#include <SDL.h>
#endif	// _WIN32

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else	// HAVE_STDINT_H
typedef signed char				int8_t;		/*!< signed 8bit */
typedef unsigned char			uint8_t;	/*!< unsigned 8bit */
typedef signed short			int16_t;	/*!< signed 16bit */
typedef unsigned short			uint16_t;	/*!< unsigned 16bit */
typedef signed int				int32_t;	/*!< signed 32bit */
typedef unsigned int			uint32_t;	/*!< unsigned 32bit */
typedef signed long long int	int64_t;	/*!< signed 64bit */
typedef unsigned long long int	uint64_t;	/*!< unsigned 64bit */
#define uint_fast8_t		uint8_t
#define uint_fast16_t		uint32_t
#endif	// HAVE_STDINT_H

#ifdef _WIN32
#define	FASTCALL	__fastcall
#else	// _WIN32
#ifdef USE_68KEM
#define FASTCALL	__attribute__((fastcall))
#else	// USE_68KEM
#define FASTCALL
#endif	// USE_68KEM
#endif	// _WIN32

#ifndef _WIN32
#define	__stdcall
#endif	// !_WIN32

#ifndef INLINE
#define	INLINE		__inline
#endif

#ifdef __cplusplus
#define EXTERNC extern "C"
#else	// __cplusplus
#define EXTERNC extern
#endif	// __cplusplus

#ifndef _countof
//! countof
#define _countof(x)		(sizeof((x)) / sizeof((x)[0]))
#endif	// _countof

#ifdef _WIN32
#define __attribute(n)
#define __attribute__(n)
#endif	// _WIN32

EXTERNC void Error(const char* s);
