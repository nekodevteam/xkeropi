﻿/**
 * @file	mouse.cpp
 * @brief	マウス入力クラスの動作の定義を行います
 */

#include "common.h"
#include "mouse.h"
#include "../x68k/scc.h"

/**
 * 初期化
 */
void Mouse_Init(void)
{
}

/**
 * Mouse Data send to SCC
 */
void Mouse_SetData(void)
{
	MouseSt = 0;
	MouseX = 0;
	MouseY = 0;
}
