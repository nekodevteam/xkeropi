﻿/**
 * @file	fileio.cpp
 * @brief	ファイル クラスの動作の定義を行います
 */

#include "common.h"
#include "fileio.h"
#include <shlwapi.h>
#include <sys/stat.h>
#include "winx68k.h"

#pragma comment(lib, "shlwapi.lib")

LPTSTR getFileName(LPCTSTR filename)
{
	return PathFindFileName(filename);
}

FILEH File_Open(LPCTSTR filename)
{
	FILEH ret = _tfopen(filename, TEXT("rb+"));
	if (ret == NULL)
	{
		ret = _tfopen(filename, TEXT("rb"));
	}
	return ret;
}

FILEH File_Create(LPCTSTR filename)
{
	return _tfopen(filename, TEXT("wb"));
}

uint32_t File_Seek(FILEH handle, int32_t pointer, short mode)
{
	fseek(handle, pointer, mode);
	return ftell(handle);
}

uint32_t File_Read(FILEH handle, void *data, uint32_t length)
{
	return static_cast<uint32_t>(fread(data, 1, length, handle));
}

uint32_t File_Write(FILEH handle, const void *data, uint32_t length)
{
	return static_cast<uint32_t>(fwrite(data, 1, length, handle));
}

short File_Close(FILEH handle)
{
	fclose(handle);
	return TRUE;
}

short File_Attr(LPCTSTR filename)
{
	short attr = -1;
#ifdef _WIN32
	struct _stat sb;
	if (_tstat(filename, &sb) == 0)
	{
		if (sb.st_mode & _S_IFDIR)
		{
			attr = FILEATTR_DIRECTORY;
		}
		else
		{
			attr = 0;
		}
		if (!(sb.st_mode & S_IWRITE))
		{
			attr |= FILEATTR_READONLY;
		}
	}
#else	// _WIN32
	struct stat sb;
	if (stat(filename, &sb) == 0)
	{
		if (S_ISDIR(sb.st_mode))
		{
			return(FILEATTR_DIRECTORY);
		}
		attr = 0;
		if (!(sb.st_mode & S_IWUSR))
		{
			attr |= FILEATTR_READONLY;
		}
	}
#endif	// _WIN32
	return attr;
}


// EXEのあるDir中のファイルの操作

FILEH	File_OpenCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Open(buf));
}

FILEH	File_CreateCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Create(buf));
}

short	File_AttrCurDir(LPCTSTR filename)
{
	TCHAR buf[MAX_PATH];
	PathCombine(buf, winx68k_dir, filename);
	return (File_Attr(buf));
}
