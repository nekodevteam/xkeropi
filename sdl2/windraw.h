﻿/**
 * @file	windraw.h
 * @brief	描画クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#define SCREEN_WIDTH		768		/*!< 幅 */
#define SCREEN_HEIGHT		512		/*!< 高さ */

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern WORD FrameCount;

#ifndef USE_NATIVEDRAW
extern WORD WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;
#endif	// !USE_NATIVEDRAW

void WinDraw_ChangeSize(void);

#ifdef __cplusplus
}

class WinDrawThread;

/**
 * @brief 描画クラス
 */
class WinDraw
{
public:
	static WinDraw* GetInstance();
	WinDraw();
	~WinDraw();
	bool Init();
	void Cleanup();
	static void Redraw();
	void Draw();
	void DrawLine(uint32_t v);
	void ChangeSize();

protected:
	static WinDraw sm_instance;			/*!< 唯一のインスタンスです */
#if (SDL_MAJOR_VERSION >= 2)
	SDL_Window* m_sdlWindow;			/*!< ウィンドウ */
	SDL_Renderer* m_renderer;			/*!< レンダラ */
	SDL_Texture* m_texture;				/*!< テクスチャ */
#else	/* (SDL_MAJOR_VERSION >= 2) */
	SDL_Surface* m_surface;
	unsigned int m_nLastW;
	unsigned int m_nLastH;
#endif	/* (SDL_MAJOR_VERSION >= 2) */

	uint8_t Draw_DrawFlag;
	bool m_bThread;						/*!< 裏で描画する? */
	WinDrawThread* m_pThread;			/*!< スレッド */

#ifdef USES_ALLFLASH
	uint32_t RedrawLastV;
#endif	// USES_ALLFLASH

	uint8_t m_dirty[SCREEN_HEIGHT];					/*!< ダーティ */
	uint16_t m_buf[SCREEN_WIDTH * SCREEN_HEIGHT];	/*!< バッファ */

private:
	void Update();

	friend WinDrawThread;
};

/**
 * インスタンスを返す
 * @return インスタンス
 */
inline WinDraw* WinDraw::GetInstance()
{
	return &sm_instance;
}

inline static void WinDraw_ChangeMode(int flag) { }

#endif	// __cplusplus
