﻿/**
 * @file	fileio.h
 * @brief	ファイル クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

typedef FILE *FILEH;

#define	FSEEK_SET	0
#define	FSEEK_CUR	1
#define	FSEEK_END	2

#define	FILEATTR_READONLY	0x01
#define	FILEATTR_DIRECTORY	0x10

LPTSTR getFileName(LPCTSTR filename);

FILEH File_Open(LPCTSTR filename);
FILEH File_Create(LPCTSTR filename);
uint32_t File_Seek(FILEH handle, int32_t pointer, short mode);
uint32_t File_Read(FILEH handle, void *data, uint32_t length);
uint32_t File_Write(FILEH handle, const void *data, uint32_t length);
short File_Close(FILEH handle);
short File_Attr(LPCTSTR filename);

void File_SetCurDir(LPCTSTR exename);
FILEH File_OpenCurDir(LPCTSTR filename);
FILEH File_CreateCurDir(LPCTSTR filename);
short File_AttrCurDir(LPCTSTR filename);

#ifndef _WIN32

#define FTYPE_SRAM 0

inline static FILEH File_CreateCurDir(LPCTSTR filename, int type)
{
	return File_CreateCurDir(filename);
}
#endif	// !_WIN32
