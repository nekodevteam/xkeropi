﻿/**
 * @file	common.cpp
 * @brief	共通クラスの動作の定義を行います
 */

#include "common.h"
#include <assert.h>

void Error(const char* s)
{
	printf("%s\n", s);
	assert(0);
}
