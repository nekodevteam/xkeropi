﻿/**
 * @file	windrawthread.h
 * @brief	描画クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "../misc/stl/event.h"
#include "../misc/stl/thread.h"
#include "windraw.h"

/**
 * @brief 描画スレッド
 */
class WinDrawThread : public CEvent, private CThread
{
public:
	/**
	 * コンストラクタ
	 * @param[in] parent 親
	 */
	WinDrawThread(WinDraw& parent)
		: CEvent(false)
		, m_parent(parent)
		, m_bAbort(false)
	{
		Start();
	}

	/**
	 * デストラクタ
	 */
	virtual ~WinDrawThread()
	{
		m_bAbort = true;
		Signal();
		Join();
	}

	/**
	 * スレッド
	 */
	virtual void Task()
	{
		do
		{
			if (Wait(200))
			{
				Reset();
				if (!m_bAbort)
				{
					m_parent.Update();
				}
			}
		} while (!m_bAbort);
	}

private:
	WinDraw& m_parent;			/*!< 親クラス */
	bool m_bAbort;				/*!< アボート */
};
