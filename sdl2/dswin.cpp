﻿/**
 * @file	dswin.cpp
 * @brief	サウンド クラスの動作の定義を行います
 */

#include "common.h"
#include "dswin.h"
#include <algorithm>
#include "../fmgen/fmg_wrap.h"
#include "../x68k/adpcm.h"
#ifndef NO_MERCURY
#include "../x68k/mercury.h"
#endif	// !NO_MERCURY

/**
 * コンストラクタ
 */
DSound::DSound()
	: m_pPcmBuffer(NULL)
	, m_nPcmBuffers(22050)
	, m_nPcmBufferMax(22050 * 3)
	, m_nPcmIndex(0)
	, m_nRateBase(22050)
	, DSound_PreCounter(0)
	, m_audio_fd(-1)
	, m_mutex(NULL)
{
}

/**
 * デストラクタ
 */
DSound::~DSound()
{
}

/**
 * 初期化
 * @param[in] rate サンプリング レート
 * @param[in] buflen バッファ サイズ
 * @retval true 成功
 * @retval false 失敗
 */
bool DSound::Init(uint32_t rate, uint32_t buflen)
{
	// 初期化済みか？
	if (m_audio_fd != -1)
	{
		return false;
	}

	// 無音指定か？
	if (rate == 0)
	{
		return true;
	}

	// pcmbuffermax = Dsoundバッファ3倍のサンプル数
	const uint32_t samples = calc_blocksize(rate * buflen / 200 / 4);
	m_nPcmBuffers = samples * 4;
	m_nPcmBufferMax = m_nPcmBuffers * 3;
	m_pPcmBuffer = new uint8_t[m_nPcmBufferMax];
	memset(m_pPcmBuffer, 0, m_nPcmBufferMax);
	m_nPcmIndex = 0;
	m_nRateBase = rate;

	// 初期化
	int rv = SDL_Init(SDL_INIT_AUDIO);
	if (rv < 0)
	{
		return false;
	}

	// サウンドバッファを確保するよ～
	SDL_AudioSpec fmt;
	memset(&fmt, 0, sizeof(fmt));
	fmt.freq = rate;
	fmt.format = AUDIO_S16SYS;
	fmt.channels = 2;
	fmt.samples = static_cast<Uint16>(samples);
	fmt.callback = sdlaudio_callback;
	fmt.userdata = this;
	m_audio_fd = SDL_OpenAudio(&fmt, NULL);
	if (m_audio_fd < 0)
	{
		return false;
	}

	m_mutex = SDL_CreateMutex();
	return true;
}

/**
 * 解放
 */
void DSound::Cleanup()
{
	if (m_audio_fd >= 0)
	{
		SDL_CloseAudio();
		m_audio_fd = -1;
	}
	if (m_mutex)
	{
		SDL_DestroyMutex(m_mutex);
		m_mutex = NULL;
	}
}

/**
 * 再生
 */
void DSound::Play()
{
	if (m_audio_fd >= 0)
	{
		SDL_PauseAudio(0);
	}
}

/**
 * 停止
 */
void DSound::Stop()
{
	if (m_audio_fd >= 0)
	{
		SDL_PauseAudio(1);
	}
}

/**
 * サンプル更新
 * @param[in] length 長さ
 */
void DSound::Update(int length)
{
	length = (std::min<int>)(length, (m_nPcmBufferMax - m_nPcmIndex) / (sizeof(int16_t) * 2));
	if (length > 0)
	{
		int16_t* p = reinterpret_cast<int16_t*>(m_pPcmBuffer + m_nPcmIndex);
		ADPCM_Update(p, length);
		OPM_Update(p, length);
#ifndef NO_MERCURY
		Mcry_Update(p, length);
#endif	// !NO_MERCURY
		m_nPcmIndex += length * sizeof(int16_t) * 2;
	}
}

/**
 * サウンド更新
 * @param[in] clock クロック
 */
void DSound::Send0(int clock)
{
	int length = 0;

	if (m_audio_fd >= 0) {
		DSound_PreCounter += (m_nRateBase * clock);
		if (SDL_LockMutex(m_mutex) == 0)
		{
			while (DSound_PreCounter >= 10000000L)
			{
				length++;
				DSound_PreCounter -= 10000000L;
			}
			Update(length);
			SDL_UnlockMutex(m_mutex);
		}
	}
}

/**
 * サウンド更新
 */
void DSound::Send()
{
	// 本来は sdlaudio_callback内で足りないサンプルを補償したいが、
	// スレッドが異なるので、メインスレッドで埋めておく
	if (m_audio_fd >= 0)
	{
		if (SDL_LockMutex(m_mutex) == 0)
		{
			if (m_nPcmIndex < m_nPcmBuffers)
			{
				Update((m_nPcmBuffers - m_nPcmIndex) / 4);
			}
			SDL_UnlockMutex(m_mutex);
		}
	}
}

/**
 * コールバック
 * @param[in] userdata ユーザ データ
 * @param[in] stream ストリーム
 * @paran[in] len 長さ
 */
void DSound::sdlaudio_callback(void* userdata, unsigned char* stream, int len)
{
	(static_cast<DSound*>(userdata))->OnCallback(stream, len);
}

/**
 * コールバック
 * @param[in] stream ストリーム
 * @paran[in] len 長さ
 */
void DSound::OnCallback(unsigned char* stream, int len)
{
	if (SDL_LockMutex(m_mutex) == 0)
	{
		len = (std::min)(len, m_nPcmIndex);
		if (len > 0)
		{
			SDL_memset(stream, 0, len);
			SDL_MixAudio(stream, m_pPcmBuffer, len, SDL_MIX_MAXVOLUME);

			const int remain = m_nPcmIndex - len;
			if (remain > 0)
			{
				memmove(m_pPcmBuffer, m_pPcmBuffer + len, remain);
			}
			m_nPcmIndex = remain;
		}
		SDL_UnlockMutex(m_mutex);
	}
}

/**
 * サイズ調整
 * @param[in] size サイズ
 * @return サイズ
 */
uint32_t DSound::calc_blocksize(uint32_t size)
{
	uint32_t s = size;
	if (s & (s - 1))
	{
		for (s = 32; s < size; s <<= 1)
		{
			continue;
		}
	}
	return s;
}
