﻿/**
 * @file	joystick.cpp
 * @brief	ジョイスティック クラスの動作の定義を行います
 */

#include "common.h"
#include "joystick.h"

#define	JOY_UP		0x01
#define	JOY_DOWN	0x02
#define	JOY_LEFT	0x04
#define	JOY_RIGHT	0x08
#define	JOY_TRG2	0x20
#define	JOY_TRG1	0x40

Joystick Joystick::sm_instance;		/*!< インスタンス */

/**
 * コンストラクタ
 */
Joystick::Joystick()
{
	memset(m_joystick, 0, sizeof(m_joystick));

	Init();
}

/**
 * デストラクタ
 */
Joystick::~Joystick()
{
	Cleanup();
}

/**
 * 初期化
 */
void Joystick::Init()
{
	m_stat[0] = 0xff;
	m_stat[1] = 0xff;
	m_data[0] = 0;
	m_data[1] = 0;

	SDL_Init(SDL_INIT_JOYSTICK);

	if (m_joystick[0] == NULL)
	{
		m_joystick[0] = SDL_JoystickOpen(0);
	}
}

/**
 * 解放
 */
void Joystick::Cleanup()
{
	for (unsigned int i = 0; i < _countof(m_joystick); i++)
	{
		if (m_joystick[i])
		{
			SDL_JoystickClose(m_joystick[i]);
			m_joystick[i] = NULL;
		}
	}
}

/**
 * アップデート
 * @param[in] ev イベント
 */
void Joystick::UpdateAxis(const SDL_JoyAxisEvent& ev)
{
	if (ev.which < 2)
	{
		uint8_t& s = m_stat[ev.which];
		switch (ev.axis)
		{
			case 0:
				s |= JOY_LEFT | JOY_RIGHT;
				if (ev.value < -16384)
				{
					s &= ~JOY_LEFT;
				}
				else if (ev.value > 16384)
				{
					s &= ~JOY_RIGHT;
				}
				break;

			case 1:
				s |= JOY_UP | JOY_DOWN;
				if (ev.value < -16384)
				{
					s &= ~JOY_UP;
				}
				else if (ev.value > 16384)
				{
					s &= ~JOY_DOWN;
				}
				break;
		}
	}
}

/**
 * アップデート
 * @param[in] ev イベント
 */
void Joystick::UpdateButton(const SDL_JoyButtonEvent& ev)
{
	uint8_t bit = 0;
	switch (ev.button)
	{
		case 0: case 2:
			bit = JOY_TRG1;
			break;

		case 1: case 3:
			bit = JOY_TRG2;
			break;
	}

	if ((bit) && (ev.which < 2))
	{
		uint8_t& s = m_stat[ev.which];
		switch (ev.state)
		{
			case SDL_RELEASED:
				s |= bit;
				break;

			case SDL_PRESSED:
				s &= ~bit;
				break;
		}
	}
}

/**
 * リード
 */
uint8_t Joystick::Read(uint8_t num)
{
	if (num < 2)
	{
		const uint8_t ret0 = m_stat[num];
		const uint8_t ret1 = 0xff;
		return ((~m_data[num]) & ret0) | (m_data[num] & ret1);
	}
	else
	{
		return 0xff;
	}
}

/**
 * ライト
 */
void Joystick::Write(uint8_t num, uint8_t data)
{
	if (num < 2)
	{
		m_data[num] = data;
	}
}
