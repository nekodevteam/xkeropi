﻿/**
 * @file	dswin.h
 * @brief	サウンド クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef _WIN32
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_mutex.h>
#else	// _WIN32
#include <SDL_audio.h>
#include <SDL_mutex.h>
#endif	// _WIN32

/**
 * @brief サウンド クラス
 */
class DSound
{
public:
	DSound();
	~DSound();
	bool Init(uint32_t rate, uint32_t buflen);
	void Cleanup();
	void Play();
	void Stop();
	void Send0(int clock);
	void Send();

private:
	uint8_t* m_pPcmBuffer;
	int m_nPcmBuffers;
	int m_nPcmBufferMax;
	int m_nPcmIndex;
	uint32_t m_nRateBase;
	int DSound_PreCounter;
	int m_audio_fd;
	SDL_mutex* m_mutex;

	static uint32_t calc_blocksize(uint32_t size);
	static void sdlaudio_callback(void* userdata, unsigned char* stream, int len);
	void OnCallback(unsigned char* stream, int len);
	void Update(int length);
};
