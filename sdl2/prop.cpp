﻿/**
 * @file	prop.cpp
 * @brief	設定クラスの動作の定義を行います
 */

#include "common.h"
#include "prop.h"
#include "windraw.h"
#include "winx68k.h"

static const TCHAR ini_title[] = TEXT("WinX68k");

Win68Conf Config;

#define CFGLEN MAX_PATH

static LPCTSTR makeBOOL(uint_fast8_t value)
{
	return (value) ? TEXT("true") : TEXT("false");
}

static int Aacmp(LPCTSTR cmp, LPCTSTR str)
{
	int c = 0;
	do
	{
		const TCHAR s = *str++;
		if (s == '\0')
		{
			break;
		}
		c = *cmp++;
		if (c == '\0')
		{
			break;
		}
		if ((c >= 'a') && (c <= 'z'))
		{
			c -= 0x20;
		}
		c -= s;
	} while (c == 0);
	return c;
}

static uint_fast8_t solveBOOL(LPCTSTR str)
{
	if ((!Aacmp(str, TEXT("TRUE"))) || (!Aacmp(str, TEXT("ON"))) ||
		(!Aacmp(str, TEXT("+"))) || (!Aacmp(str, TEXT("1"))) ||
		(!Aacmp(str, TEXT("ENABLE"))))
	{
		return 1;
	}
	return 0;
}

static uint_fast8_t GetPrivateProfileBool(LPCTSTR lpAppName, LPCTSTR lpKeyName, uint_fast8_t bDefault, LPCTSTR lpFileName)
{
	TCHAR buf[CFGLEN];
	GetPrivateProfileString(lpAppName, lpKeyName, makeBOOL(bDefault), buf, _countof(buf), lpFileName);
	return solveBOOL(buf);
}

static BOOL WritePrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName)
{
	TCHAR buf[CFGLEN];
	wsprintf(buf, TEXT("%d"), nValue);
	return WritePrivateProfileString(lpAppName, lpKeyName, buf, lpFileName);
}

void LoadConfig(void)
{
	FrameRate = (BYTE)GetPrivateProfileInt(ini_title, TEXT("FrameRate"), 0, winx68k_ini);

	Config.OPM_VOL = GetPrivateProfileInt(ini_title, TEXT("OPM_Volume"), 12, winx68k_ini);
	Config.PCM_VOL = GetPrivateProfileInt(ini_title, TEXT("PCM_Volume"), 15, winx68k_ini);
#ifndef NO_MERCURY
	Config.MCR_VOL = GetPrivateProfileInt(ini_title, TEXT("MCR_Volume"), 13, winx68k_ini);
#endif	// !NO_MERCURY
	Config.SampleRate = GetPrivateProfileInt(ini_title, TEXT("SampleRate"), 22050, winx68k_ini);
	Config.BufferSize = GetPrivateProfileInt(ini_title, TEXT("BufferSize"), 50, winx68k_ini);

	Config.MouseSpeed = GetPrivateProfileInt(ini_title, TEXT("MouseSpeed"), 10, winx68k_ini);

	Config.DSAlert = GetPrivateProfileBool(ini_title, TEXT("DSAlert"), TRUE, winx68k_ini);
	Config.Sound_LPF = GetPrivateProfileBool(ini_title, TEXT("SoundLPF"), TRUE, winx68k_ini);

#ifndef NO_MIDI
	Config.MIDI_SW = GetPrivateProfileBool(ini_title, TEXT("MIDI_SW"), TRUE, winx68k_ini);
	Config.MIDI_Reset = GetPrivateProfileBool(ini_title, TEXT("MIDI_Reset"), FALSE, winx68k_ini);
	Config.MIDI_Type = GetPrivateProfileInt(ini_title, TEXT("MIDI_Type"), 1, winx68k_ini);

	Config.ToneMap = GetPrivateProfileBool(ini_title, TEXT("ToneMapping"), FALSE, winx68k_ini);
	GetPrivateProfileString(ini_title, TEXT("ToneMapFile"), TEXT(""), Config.ToneMapFile, _countof(Config.ToneMapFile), winx68k_ini);

	Config.MIDIDelay = GetPrivateProfileInt(ini_title, TEXT("MIDIDelay"), Config.BufferSize*5, winx68k_ini);
	Config.MIDIAutoDelay = GetPrivateProfileInt(ini_title, TEXT("MIDIAutoDelay"), 1, winx68k_ini);
#endif	// NO_MIDI

	Config.JoySwap = GetPrivateProfileBool(ini_title, TEXT("JoySwap"), FALSE, winx68k_ini);

	Config.JoyKey = GetPrivateProfileBool(ini_title, TEXT("JoyKey"), FALSE, winx68k_ini);
	Config.JoyKeyReverse = GetPrivateProfileBool(ini_title, TEXT("JoyKeyReverse"), FALSE, winx68k_ini);
	Config.JoyKeyJoy2 = GetPrivateProfileBool(ini_title, TEXT("JoyKeyJoy2"), FALSE, winx68k_ini);
	Config.SRAMWarning = GetPrivateProfileBool(ini_title, TEXT("SRAMBootWarning"), TRUE, winx68k_ini);

#ifndef NO_WINDRV
	Config.LongFileName = GetPrivateProfileBool(ini_title, TEXT("WinDrvLFN"), TRUE, winx68k_ini);
	Config.WinDrvFD = GetPrivateProfileBOol(ini_title, TEXT("WinDrvFDD"), TRUE, winx68k_ini);
#endif	// !NO_WINDRV

	Config.XVIMode = (BYTE)GetPrivateProfileInt(ini_title, TEXT("XVIMode"), 0, winx68k_ini);

	for (unsigned i = 0; i < _countof(Config.JOY_BTN); i++)
	{
		for (unsigned j = 0; j < _countof(Config.JOY_BTN[0]); j++)
		{
			TCHAR buf[CFGLEN];
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			Config.JOY_BTN[i][j] = GetPrivateProfileInt(ini_title, buf, j, winx68k_ini);
		}
	}

	for (unsigned i = 0; i < _countof(Config.HDImage); i++)
	{
		TCHAR buf[CFGLEN];
		wsprintf(buf, TEXT("HDD%d"), i);
		GetPrivateProfileString(ini_title, buf, TEXT(""), Config.HDImage[i], MAX_PATH, winx68k_ini);
	}
}

void SaveConfig(void)
{
	WritePrivateProfileInt(ini_title, TEXT("FrameRate"), FrameRate, winx68k_ini);

	WritePrivateProfileInt(ini_title, TEXT("OPM_Volume"), Config.OPM_VOL, winx68k_ini);
	WritePrivateProfileInt(ini_title, TEXT("PCM_Volume"), Config.PCM_VOL, winx68k_ini);
#ifndef NO_MERCURY
	WritePrivateProfileInt(ini_title, TEXT("MCR_Volume"), Config.MCR_VOL, winx68k_ini);
#endif	// !NO_MERCURY
	WritePrivateProfileInt(ini_title, TEXT("SampleRate"), Config.SampleRate, winx68k_ini);
	WritePrivateProfileInt(ini_title, TEXT("BufferSize"), Config.BufferSize, winx68k_ini);

	WritePrivateProfileInt(ini_title, TEXT("MouseSpeed"), Config.MouseSpeed, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("DSAlert"), makeBOOL((BYTE)Config.DSAlert), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("SoundLPF"), makeBOOL((BYTE)Config.Sound_LPF), winx68k_ini);

#ifndef NO_MIDI
	WritePrivateProfileString(ini_title, TEXT("MIDI_SW"), makeBOOL((BYTE)Config.MIDI_SW), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("MIDI_Reset"), makeBOOL((BYTE)Config.MIDI_Reset), winx68k_ini);
	WritePrivateProfileInt(ini_title, TEXT("MIDI_Type"), Config.MIDI_Type, winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("ToneMapping"), makeBOOL((BYTE)Config.ToneMap), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("ToneMapFile"), Config.ToneMapFile, winx68k_ini);

	WritePrivateProfileInt(ini_title, TEXT("MIDIDelay"), Config.MIDIDelay, winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("MIDIAutoDelay"), makeBOOL((BYTE)Config.MIDIAutoDelay), winx68k_ini);

#endif	// !NO_MIDI

	WritePrivateProfileString(ini_title, TEXT("JoySwap"), makeBOOL((BYTE)Config.JoySwap), winx68k_ini);

	WritePrivateProfileString(ini_title, TEXT("JoyKey"), makeBOOL((BYTE)Config.JoyKey), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("JoyKeyReverse"), makeBOOL((BYTE)Config.JoyKeyReverse), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("JoyKeyJoy2"), makeBOOL((BYTE)Config.JoyKeyJoy2), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("SRAMBootWarning"), makeBOOL((BYTE)Config.SRAMWarning), winx68k_ini);

#ifndef NO_WINDRV
	WritePrivateProfileString(ini_title, TEXT("WinDrvLFN"), makeBOOL((BYTE)Config.LongFileName), winx68k_ini);
	WritePrivateProfileString(ini_title, TEXT("WinDrvFDD"), makeBOOL((BYTE)Config.WinDrvFD), winx68k_ini);
#endif	// NO_WINDRV

	WritePrivateProfileInt(ini_title, TEXT("XVIMode"), Config.XVIMode, winx68k_ini);

	for (unsigned i = 0; i < _countof(Config.JOY_BTN); i++)
	{
		for (unsigned j = 0; j < _countof(Config.JOY_BTN[0]); j++)
		{
			TCHAR buf[CFGLEN];
			wsprintf(buf, TEXT("Joy%dButton%d"), i+1, j+1);
			WritePrivateProfileInt(ini_title, buf, Config.JOY_BTN[i][j], winx68k_ini);
		}
	}

	for (unsigned i = 0; i < _countof(Config.HDImage); i++)
	{
		TCHAR buf[CFGLEN];
		wsprintf(buf, TEXT("HDD%d"), i);
		WritePrivateProfileString(ini_title, buf, Config.HDImage[i], winx68k_ini);
	}
}
