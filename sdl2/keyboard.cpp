﻿/**
 * @file	keyboard.cpp
 * @brief	キーボード入力クラスの動作の定義を行います
 */

#include "common.h"
#include "keyboard.h"

Keyboard Keyboard::sm_instance;

/**
 * @brief キー アイテム構造体
 */
struct KeyDefine
{
	uint8_t cKey;
	uint32_t wCode;
};

/**
 * キーマップ
 */
static const KeyDefine s_keydef[] =
{
#if (SDL_MAJOR_VERSION >= 2)
	{0x01,	SDL_SCANCODE_ESCAPE},			// ESC
	{0x02,	SDL_SCANCODE_1},				// 1
	{0x03,	SDL_SCANCODE_2},				// 2
	{0x04,	SDL_SCANCODE_3},				// 3
	{0x05,	SDL_SCANCODE_4},				// 4
	{0x06,	SDL_SCANCODE_5},				// 5
	{0x07,	SDL_SCANCODE_6},				// 6
	{0x08,	SDL_SCANCODE_7},				// 7
	{0x09,	SDL_SCANCODE_8},				// 8
	{0x0a,	SDL_SCANCODE_9},				// 9
	{0x0b,	SDL_SCANCODE_0},				// 0
	{0x0c,	SDL_SCANCODE_MINUS},			// -=
	{0x0d,	SDL_SCANCODE_EQUALS},			// ^~
	{0x0e,	SDL_SCANCODE_BACKSLASH},		// \|
	{0x0f,	SDL_SCANCODE_BACKSPACE},		// BS

	{0x10,	SDL_SCANCODE_TAB},				// TAB
	{0x11,	SDL_SCANCODE_Q},				// Q
	{0x12,	SDL_SCANCODE_W},				// W
	{0x13,	SDL_SCANCODE_E},				// E
	{0x14,	SDL_SCANCODE_R},				// R
	{0x15,	SDL_SCANCODE_T},				// T
	{0x16,	SDL_SCANCODE_Y},				// Y
	{0x17,	SDL_SCANCODE_U},				// U
	{0x18,	SDL_SCANCODE_I},				// I
	{0x19,	SDL_SCANCODE_O},				// O
	{0x1a,	SDL_SCANCODE_P},				// P
	{0x1b,	0},								// @
	{0x1c,	SDL_SCANCODE_LEFTBRACKET},		// [(
	{0x1d,	SDL_SCANCODE_RETURN},			// ent
	{0x1e,	SDL_SCANCODE_A},				// A
	{0x1f,	SDL_SCANCODE_S},				// S

	{0x20,	SDL_SCANCODE_D},				// D
	{0x21,	SDL_SCANCODE_F},				// F
	{0x22,	SDL_SCANCODE_G},				// G
	{0x23,	SDL_SCANCODE_H},				// H
	{0x24,	SDL_SCANCODE_J},				// J
	{0x25,	SDL_SCANCODE_K},				// K
	{0x26,	SDL_SCANCODE_L},				// L
	{0x27,	SDL_SCANCODE_SEMICOLON},		// ;
	{0x28,	SDL_SCANCODE_APOSTROPHE},		// :
	{0x29,	SDL_SCANCODE_RIGHTBRACKET},		// ])
	{0x2a,	SDL_SCANCODE_Z},				// Z
	{0x2b,	SDL_SCANCODE_X},				// X
	{0x2c,	SDL_SCANCODE_C},				// C
	{0x2d,	SDL_SCANCODE_V},				// V
	{0x2e,	SDL_SCANCODE_B},				// B
	{0x2f,	SDL_SCANCODE_N},				// N

	{0x30,	SDL_SCANCODE_M},				// M
	{0x31,	SDL_SCANCODE_COMMA},			// ,<
	{0x32,	SDL_SCANCODE_PERIOD},			// .>
	{0x33,	SDL_SCANCODE_SLASH},			// /?
	{0x34,	SDL_SCANCODE_NONUSBACKSLASH},	// _
	{0x35,	SDL_SCANCODE_SPACE},			// SP
	{0x36,	SDL_SCANCODE_HOME},				// HOME
	{0x37,	SDL_SCANCODE_DELETE},			// DEL
	{0x38,	SDL_SCANCODE_PAGEUP},			// RUP
	{0x39,	SDL_SCANCODE_PAGEDOWN},			// RDWN
	{0x3a,	SDL_SCANCODE_UNDO},				// UNDO
	{0x3b,	SDL_SCANCODE_LEFT},				// LEFT
	{0x3c,	SDL_SCANCODE_UP},				// UP
	{0x3d,	SDL_SCANCODE_RIGHT},			// RIGHT
	{0x3e,	SDL_SCANCODE_DOWN},				// DOWN
	{0x3f,	SDL_SCANCODE_NUMLOCKCLEAR},		// [CLR]

	{0x40,	SDL_SCANCODE_KP_DIVIDE},		// [/]
	{0x41,	SDL_SCANCODE_KP_MULTIPLY},		// [*]
	{0x42,	SDL_SCANCODE_KP_MINUS},			// [-]
	{0x43,	SDL_SCANCODE_KP_7},				// [7]
	{0x44,	SDL_SCANCODE_KP_8},				// [8]
	{0x45,	SDL_SCANCODE_KP_9},				// [9]
	{0x46,	SDL_SCANCODE_KP_PLUS},			// [+]
	{0x47,	SDL_SCANCODE_KP_4},				// [4]
	{0x48,	SDL_SCANCODE_KP_5},				// [5]
	{0x49,	SDL_SCANCODE_KP_6},				// [6]
	{0x4a,	SDL_SCANCODE_KP_EQUALS},		// [=]
	{0x4b,	SDL_SCANCODE_KP_1},				// [1]
	{0x4c,	SDL_SCANCODE_KP_2},				// [2]
	{0x4d,	SDL_SCANCODE_KP_3},				// [3]
	{0x4e,	SDL_SCANCODE_KP_ENTER},			// [RET]
	{0x4f,	SDL_SCANCODE_KP_0},				// [0]

	{0x50,	SDL_SCANCODE_KP_COLON},			// [,]
	{0x51,	SDL_SCANCODE_KP_PERIOD},		// [.]

	{0x52,	0},								// 記号
	{0x53,	0},								// 登録
	{0x54,	SDL_SCANCODE_HELP},				// HELP
	{0x55,	0},								// XF1
	{0x56,	0},								// XF2
	{0x57,	0},								// XF3
	{0x58,	0},								// XF4
	{0x59,	0},								// XF5
	{0x5a,	0},								// かな
	{0x5b,	0},								// ローマ
	{0x5c,	0},								// コード
	{0x5d,	SDL_SCANCODE_CAPSLOCK},			// CAPS
	{0x5e,	SDL_SCANCODE_INSERT},			// INS
	{0x5f,	0},								// ひら

	{0x60,	0},								// 全角
	{0x61,	SDL_SCANCODE_STOP},				// BREAK
	{0x62,	SDL_SCANCODE_COPY},				// COPY
	{0x63,	SDL_SCANCODE_F1},				// F1
	{0x64,	SDL_SCANCODE_F2},				// F2
	{0x65,	SDL_SCANCODE_F3},				// F3
	{0x66,	SDL_SCANCODE_F4},				// F4
	{0x67,	SDL_SCANCODE_F5},				// F5
	{0x68,	SDL_SCANCODE_F6},				// F6
	{0x69,	SDL_SCANCODE_F7},				// F7
	{0x6a,	SDL_SCANCODE_F8},				// F8
	{0x6b,	SDL_SCANCODE_F9},				// F9
	{0x6c,	SDL_SCANCODE_F10},				// F10

	{0x70,	SDL_SCANCODE_LSHIFT},			// SHIFT
	{0x71,	SDL_SCANCODE_LCTRL},			// CTRL
	{0x72,	SDL_SCANCODE_F11},				// OPT1
	{0x73,	SDL_SCANCODE_F12},				// OPT2
#else	// (SDL_MAJOR_VERSION >= 2)
	{0x01,	SDLK_ESCAPE},			// ESC
	{0x02,	SDLK_1},				// 1
	{0x03,	SDLK_2},				// 2
	{0x04,	SDLK_3},				// 3
	{0x05,	SDLK_4},				// 4
	{0x06,	SDLK_5},				// 5
	{0x07,	SDLK_6},				// 6
	{0x08,	SDLK_7},				// 7
	{0x09,	SDLK_8},				// 8
	{0x0a,	SDLK_9},				// 9
	{0x0b,	SDLK_0},				// 0
	{0x0c,	SDLK_MINUS},			// -=
	{0x0d,	SDLK_EQUALS},			// ^~
	{0x0e,	SDLK_BACKSLASH},		// \|
	{0x0f,	SDLK_BACKSPACE},		// BS

	{0x10,	SDLK_TAB},				// TAB
	{0x11,	SDLK_q},				// Q
	{0x12,	SDLK_w},				// W
	{0x13,	SDLK_e},				// E
	{0x14,	SDLK_r},				// R
	{0x15,	SDLK_t},				// T
	{0x16,	SDLK_y},				// Y
	{0x17,	SDLK_u},				// U
	{0x18,	SDLK_i},				// I
	{0x19,	SDLK_o},				// O
	{0x1a,	SDLK_p},				// P
	{0x1b,	0},						// @
	{0x1c,	SDLK_LEFTBRACKET},		// [(
	{0x1d,	SDLK_RETURN},			// ent
	{0x1e,	SDLK_a},				// A
	{0x1f,	SDLK_s},				// S

	{0x20,	SDLK_d},				// D
	{0x21,	SDLK_f},				// F
	{0x22,	SDLK_g},				// G
	{0x23,	SDLK_h},				// H
	{0x24,	SDLK_j},				// J
	{0x25,	SDLK_k},				// K
	{0x26,	SDLK_l},				// L
	{0x27,	SDLK_SEMICOLON},		// ;
	{0x28,	SDLK_COLON},			// :
	{0x29,	SDLK_RIGHTBRACKET},		// ])
	{0x2a,	SDLK_z},				// Z
	{0x2b,	SDLK_x},				// X
	{0x2c,	SDLK_c},				// C
	{0x2d,	SDLK_v},				// V
	{0x2e,	SDLK_b},				// B
	{0x2f,	SDLK_n},				// N

	{0x30,	SDLK_m},				// M
	{0x31,	SDLK_COMMA},			// ,<
	{0x32,	SDLK_PERIOD},			// .>
	{0x33,	SDLK_SLASH},			// /?
	{0x34,	SDLK_UNDERSCORE},		// _
	{0x35,	SDLK_SPACE},			// SP
	{0x36,	SDLK_HOME},				// HOME
	{0x37,	SDLK_DELETE},			// DEL
	{0x38,	SDLK_PAGEUP},			// RUP
	{0x39,	SDLK_PAGEDOWN},			// RDWN
	{0x3a,	SDLK_UNDO},				// UNDO
	{0x3b,	SDLK_LEFT},				// LEFT
	{0x3c,	SDLK_UP},				// UP
	{0x3d,	SDLK_RIGHT},			// RIGHT
	{0x3e,	SDLK_DOWN},				// DOWN
	{0x3f,	SDLK_NUMLOCK},			// [CLR]

	{0x40,	SDLK_KP_DIVIDE},		// [/]
	{0x41,	SDLK_KP_MULTIPLY},		// [*]
	{0x42,	SDLK_KP_MINUS},			// [-]
	{0x43,	SDLK_KP7},				// [7]
	{0x44,	SDLK_KP8},				// [8]
	{0x45,	SDLK_KP9},				// [9]
	{0x46,	SDLK_KP_PLUS},			// [+]
	{0x47,	SDLK_KP4},				// [4]
	{0x48,	SDLK_KP5},				// [5]
	{0x49,	SDLK_KP6},				// [6]
	{0x4a,	SDLK_KP_EQUALS},		// [=]
	{0x4b,	SDLK_KP1},				// [1]
	{0x4c,	SDLK_KP2},				// [2]
	{0x4d,	SDLK_KP3},				// [3]
	{0x4e,	SDLK_KP_ENTER},			// [RET]
	{0x4f,	SDLK_KP0},				// [0]

	{0x50,	0},						// [,]
	{0x51,	SDLK_KP_PERIOD},		// [.]

	{0x52,	0},						// 記号
	{0x53,	0},						// 登録
	{0x54,	SDLK_END},				// HELP
	{0x55,	0},						// XF1
	{0x56,	0},						// XF2
	{0x57,	0},						// XF3
	{0x58,	0},						// XF4
	{0x59,	0},						// XF5
	{0x5a,	0},						// かな
	{0x5b,	0},						// ローマ
	{0x5c,	0},						// コード
	{0x5d,	SDLK_CAPSLOCK},			// CAPS
	{0x5e,	SDLK_INSERT},			// INS
	{0x5f,	0},						// ひら

	{0x60,	0},						// 全角
	{0x61,	SDLK_PAUSE},			// BREAK
	{0x62,	SDLK_PRINT},			// COPY
	{0x63,	SDLK_F1},				// F1
	{0x64,	SDLK_F2},				// F2
	{0x65,	SDLK_F3},				// F3
	{0x66,	SDLK_F4},				// F4
	{0x67,	SDLK_F5},				// F5
	{0x68,	SDLK_F6},				// F6
	{0x69,	SDLK_F7},				// F7
	{0x6a,	SDLK_F8},				// F8
	{0x6b,	SDLK_F9},				// F9
	{0x6c,	SDLK_F10},				// F10

	{0x70,	SDLK_LSHIFT},			// SHIFT
	{0x71,	SDLK_LCTRL},			// CTRL
	{0x72,	SDLK_F11},				// OPT1
	{0x73,	SDLK_F12},				// OPT2
#endif	// (SDL_MAJOR_VERSION >= 2)
};

/**
 * コンストラクタ
 */
Keyboard::Keyboard()
{
	for (unsigned int i = 0; i < _countof(s_keydef); i++)
	{
		const KeyDefine& key = s_keydef[i];
		if (key.wCode)
		{
#if (SDL_MAJOR_VERSION >= 2)
			m_map[static_cast<SDL_Scancode>(key.wCode)] = key.cKey;
#else	// (SDL_MAJOR_VERSION >= 2)
			m_map[static_cast<SDLKey>(key.wCode)] = key.cKey;
#endif	// (SDL_MAJOR_VERSION >= 2)
		}
	}
}

/**
 * 初期化
 */
void Keyboard::Init()
{
	KeyBufWP = 0;
	KeyBufRP = 0;
	ZeroMemory(KeyBuf, sizeof(KeyBuf));
}

/**
 * キー更新
 * @param[in] ev イベント
 */
void Keyboard::UpdateKey(const SDL_KeyboardEvent& ev)
{
#if (SDL_MAJOR_VERSION >= 2)
	KeyMap::const_iterator it = m_map.find(ev.keysym.scancode);
#else	// (SDL_MAJOR_VERSION >= 2)
	KeyMap::const_iterator it = m_map.find(ev.keysym.sym);
#endif	// (SDL_MAJOR_VERSION >= 2)
	if (it != m_map.end())
	{
		const uint8_t newwp = (KeyBufWP + 1) % _countof(KeyBuf);
		if (newwp != KeyBufRP)
		{
			KeyBuf[KeyBufWP] = it->second | ((ev.state == SDL_RELEASED) ? 0x80 : 0x00);
			KeyBufWP = newwp;
		}
	}
/*{
FILE *fp;
fp=fopen("_key.txt", "a");
fprintf(fp, "KeyDn  WP:%04X  LP:%08X  Key:%02X\n", wp, lp, code);
fclose(fp);
}*/		//ログ取り用にょ
}

/**
 * リード
 * @return データ
 */
uint_fast8_t Keyboard::Read()
{
	uint_fast8_t nRet = 0;
	if (KeyBufRP != KeyBufWP)
	{
		nRet = KeyBuf[KeyBufRP];
		KeyBufRP = (KeyBufRP + 1) % _countof(KeyBuf);
	}
	return nRet;
}

// ---- from C

uint_fast8_t Keyboard_BufferEmpty(void)
{
	return Keyboard::GetInstance()->BufferEmpty();
}

uint_fast8_t Keyboard_Read(void)
{
	return Keyboard::GetInstance()->Read();
}

void Keyboard_Write(uint_fast8_t nData)
{
}
