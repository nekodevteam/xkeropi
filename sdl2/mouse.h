﻿/**
 * @file	mouse.h
 * @brief	マウス入力クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

void Mouse_Init(void);
void Mouse_SetData(void);
