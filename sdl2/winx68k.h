﻿/**
 * @file	winx68k.h
 * @brief	メイン クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "timer.h"

class DSound;

#ifdef _WIN32
const HWND hWndMain = NULL;
#endif	// _WIN32

extern	const TCHAR	PrgName[];
extern	const TCHAR	PrgTitle[];

extern	TCHAR	winx68k_dir[MAX_PATH];
extern	TCHAR	winx68k_ini[MAX_PATH];

extern	BYTE	FrameRate;

extern	DWORD	TimerICount;

/**
 * @brief メイン クラス
 */
class WinX68k
{
public:
	static WinX68k* GetInstance();
	WinX68k();
	~WinX68k();
	int Main(int argc, TCHAR *argv[]);

private:
	static WinX68k sm_instance;

	bool NoWaitMode;
	bool traceflag;
	int ClkUsed;

	DSound* m_dsound;
	Timer m_timer;

	bool Init();
	void Cleanup();
	bool Reset();
	void Exec(int bDrawFrame);
	static void SCSICheck();
	static bool LoadROMs();
};

/**
 * インスタンスを返す
 * @return インスタンス
 */
inline WinX68k* WinX68k::GetInstance()
{
	return &sm_instance;
}
