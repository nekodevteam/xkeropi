﻿/**
 * @file	joystick.h
 * @brief	ジョイスティック クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief ジョイスティック クラス
 */
class Joystick
{
public:
	static Joystick* GetInstance();
	Joystick();
	~Joystick();
	void Init();
	void Cleanup();
	uint8_t Read(uint8_t num);
	void Write(uint8_t num, uint8_t data);
	void UpdateAxis(const SDL_JoyAxisEvent& ev);
	void UpdateButton(const SDL_JoyButtonEvent& ev);

protected:
	static Joystick sm_instance;	/*!< インスタンス */
	uint8_t m_stat[2];				/*!< ステート */
	uint8_t m_data[2];				/*!< データ */
	SDL_Joystick* m_joystick[2];	/*!< ジョイスティック */
};

/**
 * インスタンスを得る
 * @return インスタンス
 */
inline Joystick* Joystick::GetInstance()
{
	return &sm_instance;
}

inline uint8_t Joystick_Read(uint8_t num) { return Joystick::GetInstance()->Read(num); }
inline void Joystick_Write(uint8_t num, uint8_t data) { Joystick::GetInstance()->Write(num, data); }
