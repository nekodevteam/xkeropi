﻿/**
 * @file	keyboard.h
 * @brief	キーボード入力クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

uint_fast8_t Keyboard_BufferEmpty(void);
uint_fast8_t Keyboard_Read(void);
void Keyboard_Write(uint_fast8_t nData);

#ifdef __cplusplus
}

#include <map>

/**
 * @brief キーボード
 */
class Keyboard
{
public:
	static Keyboard* GetInstance();

	Keyboard();
	void Init(void);
	void UpdateKey(const SDL_KeyboardEvent& ev);
	void Int();
	bool BufferEmpty() const;
	uint_fast8_t Read();

private:
	static Keyboard sm_instance;

	uint8_t KeyBufWP;
	uint8_t KeyBufRP;
	uint8_t KeyBuf[128];

#if (SDL_MAJOR_VERSION >= 2)
	typedef std::map<SDL_Scancode, uint8_t> KeyMap;
#else	// (SDL_MAJOR_VERSION >= 2)
	typedef std::map<SDLKey, uint8_t> KeyMap;
#endif	// (SDL_MAJOR_VERSION >= 2)
	KeyMap m_map;
};

/**
 * インスタンスを得る
 * @return インスタンス
 */
inline Keyboard* Keyboard::GetInstance()
{
	return &sm_instance;
}

/**
 * バッファは空か?
 * @retval true 空
 * @retval false データあり
 */
inline bool Keyboard::BufferEmpty() const
{
	return (KeyBufWP == KeyBufRP);
}

#endif	// __cplusplus
