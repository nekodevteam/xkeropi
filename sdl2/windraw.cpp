﻿/**
 * @file	windraw.cpp
 * @brief	描画クラスの動作の定義を行います
 */

#include "common.h"
#include "windraw.h"
#include <algorithm>
#include "windrawthread.h"
#include "winx68k.h"
#include "../x68k/crtc.h"
#include "../x68k/palette.h"
#include "../x68k/tvram.h"

static const int WindowX = SCREEN_WIDTH;
static const int WindowY = SCREEN_HEIGHT;

static const bool Debug_Text = true;
static const bool Debug_Grp = true;
static const bool Debug_Sp = true;

	WORD	FrameCount = 0;
	WORD	WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;

WinDraw WinDraw::sm_instance;			/*!< 唯一のインスタンスです */

/**
 * コンストラクタ
 */
WinDraw::WinDraw()
#if (SDL_MAJOR_VERSION >= 2)
	: m_sdlWindow(NULL)
	, m_renderer(NULL)
	, m_texture(NULL)
	, Draw_DrawFlag(1)
	, m_bThread(true)
#else	// (SDL_MAJOR_VERSION >= 2)
	: m_surface(NULL)
	, m_nLastW(SCREEN_WIDTH)
	, m_nLastH(SCREEN_HEIGHT)
	, Draw_DrawFlag(1)
	, m_bThread(false)
#endif	// (SDL_MAJOR_VERSION >= 2)
	, m_pThread(NULL)
#ifdef USES_ALLFLASH
	, RedrawLastV(0)
#endif	// USES_ALLFLASH
{
	memset(m_dirty, 0, sizeof(m_dirty));
	memset(m_buf, 0, sizeof(m_buf));
}

/**
 * デストラクタ
 */
WinDraw::~WinDraw()
{
}

/**
 * サイズ変更通知
 */
void WinDraw::ChangeSize()
{
	TextDotX = std::min<DWORD>(TextDotX, SCREEN_WIDTH);
	TextDotY = std::min<DWORD>(TextDotY, SCREEN_HEIGHT);
}

/**
 * 初期化
 * @retval true 成功
 * @retval false 失敗
 */
bool WinDraw::Init()
{
	do
	{
		if (SDL_InitSubSystem(SDL_INIT_VIDEO) < 0)
		{
			fprintf(stderr, "Error: SDL_Init: %s\n", SDL_GetError());
			break;
		}

#if (SDL_MAJOR_VERSION >= 2)
		m_sdlWindow = SDL_CreateWindow("keropi", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowX, WindowY, 0);
		if (m_sdlWindow == NULL)
		{
			break;
		}

		m_renderer = SDL_CreateRenderer(m_sdlWindow, -1, 0);
		if (m_renderer == NULL)
		{
			break;
		}
		SDL_RenderSetLogicalSize(m_renderer, WindowX, WindowY);

		m_texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGB555, SDL_TEXTUREACCESS_STREAMING, WindowX, WindowY);
		if (m_texture == NULL)
		{
			break;
		}
#ifndef USE_NATIVECOLOR
		WinDraw_Pal16B = 0x001f;
		WinDraw_Pal16G = 0x03e0;
		WinDraw_Pal16R = 0x7c00;
		Pal_SetColor();
#endif	// !USE_NATIVECOLOR
#else	/* (SDL_MAJOR_VERSION >= 2) */
		SDL_WM_SetCaption("keropi", "keropi");
		const SDL_VideoInfo* vinfo = SDL_GetVideoInfo();
		if (vinfo == NULL)
		{
			fprintf(stderr, "Error: SDL_GetVideoInfo: %s\n", SDL_GetError());
			break;
		}
		m_surface = SDL_SetVideoMode(WindowX, WindowY, vinfo->vfmt->BitsPerPixel, SDL_HWSURFACE | SDL_ANYFORMAT | SDL_DOUBLEBUF);
		if (m_surface == NULL)
		{
			fprintf(stderr, "Error: SDL_SetVideoMode: %s\n", SDL_GetError());
			break;
		}
		if (m_surface->format->BitsPerPixel != 32)
		{
			fprintf(stderr, "Error: Invalide colors (32bpp only)\n");
			break;
		}
#ifndef USE_NATIVECOLOR
		WinDraw_Pal16B = 0x001f << 1;
		WinDraw_Pal16R = 0x03e0 << 1;
		WinDraw_Pal16G = 0x7c00 << 1;
		Pal_SetColor();
#endif	// !USE_NATIVECOLOR
#endif	/* (SDL_MAJOR_VERSION >= 2) */

		if (m_bThread)
		{
			m_pThread = new WinDrawThread(*this);
		}
		return true;
	} while (false /*CONSTCOND*/);

	Cleanup();
	return false;
}

/**
 * 破棄
 */
void WinDraw::Cleanup()
{
	delete m_pThread;
	m_pThread = NULL;

#if (SDL_MAJOR_VERSION >= 2)
	if (m_sdlWindow)
	{
		SDL_DestroyWindow(m_sdlWindow);
		m_sdlWindow = NULL;
	}
	if (m_renderer)
	{
		SDL_DestroyRenderer(m_renderer);
		m_renderer = NULL;
	}
	if (m_texture)
	{
		SDL_DestroyTexture(m_texture);
		m_texture = NULL;
	}
#else	/* (SDL_MAJOR_VERSION >= 2) */
	m_surface = NULL;
#endif	/* (SDL_MAJOR_VERSION >= 2) */
}

/**
 * 再描画
 */
void WinDraw::Redraw()
{
	TVRAM_SetAllDirty();
}

/**
 * 描画
 */
void WinDraw::Draw()
{
	FrameCount++;

	if (!Draw_DrawFlag) return;
	Draw_DrawFlag = 0;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
		CRTC.AllFlash = 0;
		memset(TextDirtyLine, 1, RedrawLastV);
		RedrawLastV = 0;
	}
#endif	// USES_ALLFLASH

	if (m_pThread)
	{
		m_pThread->Signal();
	}
	else
	{
		Update();
	}
}

/**
 * 555 → 888
 * @param[in] c RGB555
 * @return RGB888
 */
inline static uint32_t RGB555To888(uint32_t c)
{
	return ((c << 13) & 0xf80000) + (c & 0x00f800) + ((c << 2) & 0x0000f8);
}

/**
 * サーフェス更新
 */
void WinDraw::Update()
{
#if (SDL_MAJOR_VERSION >= 2)

	if (m_texture == NULL)
	{
		return;
	}

	SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = TextDotX;
	rect.h = TextDotY;

	void* pixels = NULL;
	int pitch = 0;
	if (SDL_LockTexture(m_texture, &rect, &pixels, &pitch) == 0)
	{
		for (unsigned int y = 0; y < TextDotY; y++)
		{
			if (m_dirty[y] == 0)
			{
				continue;
			}
			m_dirty[y] = 0;
			const uint16_t* p = &m_buf[y * SCREEN_WIDTH];
			uint16_t* q = reinterpret_cast<uint16_t*>(reinterpret_cast<intptr_t>(pixels) + (y * pitch));
#ifdef USE_NATIVECOLOR
			for (unsigned int x = 0; x < TextDotX; x++)
			{
				const unsigned int c = p[x];
				q[x] = static_cast<uint16_t>(((c << 4) & 0x7c00) + ((c >> 6) & 0x03e0) + ((c >> 1) & 0x001f));
			}
#else	// USE_NATIVECOLOR
			memcpy(q, p, TextDotX * 2);
#endif	// USE_NATIVECOLOR
		}
		SDL_UnlockTexture(m_texture);
	}

	SDL_RenderClear(m_renderer);
	SDL_RenderCopy(m_renderer, m_texture, &rect, NULL);
	SDL_RenderPresent(m_renderer);

#else	/* (SDL_MAJOR_VERSION >= 2) */

	if (m_surface == NULL)
	{
		return;
	}

	if (SDL_LockSurface(m_surface) == 0)
	{
		void* pixels = m_surface->pixels;
		const int pitch = m_surface->pitch;

		const unsigned int yalign = (TextDotY <= 256) ? 2 : 1;

		unsigned int xalign4 = ((TextDotX <= 384) ? 2 : 1) * 4;
		if (std::abs(static_cast<int>(TextDotX - TextDotY)) < 32)
		{
			xalign4 = (xalign4 * 5) / 4;
		}

		for (unsigned int y = 0; y < TextDotY; y++)
		{
			if (m_dirty[y] == 0)
			{
				continue;
			}
			m_dirty[y] = 0;

			const uint16_t* p = &m_buf[y * SCREEN_WIDTH];
			if (yalign < 2)
			{
				uint32_t* q = reinterpret_cast<uint32_t*>(reinterpret_cast<intptr_t>(pixels) + (y * pitch));
				switch (xalign4)
				{
					case 4:
						for (unsigned int x = 0; x < TextDotX; x++)
						{
							q[x] = RGB555To888(p[x]);
						}
						break;

					case 5:
						for (unsigned int x = 0; x < TextDotX; )
						{
							const uint32_t c = RGB555To888(p[x++]);
							*q++ = c;
							if ((x & 3) == 0)
							{
								*q++ = c;
							}
						}
						break;

					case 8:
						for (unsigned int x = 0; x < TextDotX; x++)
						{
							const uint32_t c = RGB555To888(p[x]);
							q[(x * 2) + 0] = c;
							q[(x * 2) + 1] = c;
						}
						break;

					case 10:
						for (unsigned int x = 0; x < TextDotX; )
						{
							const uint32_t c = RGB555To888(p[x++]);
							*q++ = c;
							*q++ = c;
							if ((x & 1) == 0)
							{
								*q++ = c;
							}
						}
						break;
				}
			}
			else
			{
				uint32_t* q = reinterpret_cast<uint32_t*>(reinterpret_cast<intptr_t>(pixels) + (y * 2 * pitch));
				uint32_t* r = reinterpret_cast<uint32_t*>(reinterpret_cast<intptr_t>(q) + pitch);
				switch (xalign4)
				{
					case 4:
						for (unsigned int x = 0; x < TextDotX; x++)
						{
							const uint32_t c = RGB555To888(p[x]);
							q[x] = c;
							r[x] = c;
						}
						break;

					case 5:
						for (unsigned int x = 0; x < TextDotX; )
						{
							const uint32_t c = RGB555To888(p[x++]);
							*q++ = c;
							*r++ = c;
							if ((x & 3) == 0)
							{
								*q++ = c;
								*r++ = c;
							}
						}
						break;

					case 8:
						for (unsigned int x = 0; x < TextDotX; x++)
						{
							const uint32_t c = RGB555To888(p[x]);
							q[(x * 2) + 0] = c;
							q[(x * 2) + 1] = c;
							r[(x * 2) + 0] = c;
							r[(x * 2) + 1] = c;
						}
						break;

					case 10:
						for (unsigned int x = 0; x < TextDotX; )
						{
							const uint32_t c = RGB555To888(p[x++]);
							*q++ = c;
							*q++ = c;
							*r++ = c;
							*r++ = c;
							if ((x & 1) == 0)
							{
								*q++ = c;
								*r++ = c;
							}
						}
						break;
				}
			}
		}

		SDL_UnlockSurface(m_surface);

		SDL_Flip(m_surface);
	}

#endif	/* (SDL_MAJOR_VERSION >= 2) */
}

/**
 * ライン描画
 */
void WinDraw::DrawLine(uint32_t v)
{
	if (v >= _countof(m_dirty)) return;

#ifdef USES_ALLFLASH
	if (CRTC.AllFlash)
	{
		RedrawLastV = v;
	}
	else
#endif	// USES_ALLFLASH
	if (!TextDirtyLine[v]) return;
	TextDirtyLine[v] = 0;

	CRTC_DrawLine(&m_buf[v * SCREEN_WIDTH], v);

	Draw_DrawFlag = 1;
	m_dirty[v] = 1;
}

// ---- from C

void WinDraw_ChangeSize(void)
{
	WinDraw::GetInstance()->ChangeSize();
}
