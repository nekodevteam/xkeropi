  .text
  .align 4

	.global cyclone_readmem24
	.global cyclone_readmem24_word
	.global cyclone_readmem24_dword
	.global cyclone_writemem24
	.global cyclone_writemem24_word
	.global cyclone_writemem24_dword
	.extern ExceptionBusError_r
	.extern ExceptionBusError_w
	.extern ExceptionAddressError_r_data
	.extern ExceptionAddressError_w_data
	.extern FetchMem
	.extern ADPCM_Read
	.extern ADPCM_Write
	.extern BG_Read
	.extern BG_Write
	.extern BG16_Read
	.extern BG16_Write
	.extern BGRam_Read
	.extern BGRam_Write
	.extern BGRam16_Read
	.extern BGRam16_Write
	.extern CRTC_Read
	.extern CRTC_Write
	.extern CrtVC16_Write
	.extern DMA_Read
	.extern DMA_Write
	.extern FDC_Read
	.extern FDC_Write
	.extern GVRAM_Read
	.extern GVRAM_Write
	.extern GVRAM16_Read
	.extern GVRAM16_Write
	.extern IOC_Read
	.extern IOC_Write
	.extern MFP_Read
	.extern MFP_Write
	.extern OPM_Read
	.extern OPM_Write
	.extern PIA_Read
	.extern PIA_Write
	.extern RTC_Read
	.extern RTC_Write
	.extern SASI_Read
	.extern SASI_Write
	.extern SCC_Read
	.extern SCC_Write
	.extern SCSI_Read
	.extern SCSI_Write
	.extern SCSI_Read
	.extern SCSI_Write
	.extern SRAM_Read
	.extern SRAM_Write
	.extern SRAM16_Read
	.extern SRAM16_Write
	.extern SysPort_Read
	.extern SysPort_Write
	.extern TVRAM_Read
	.extern TVRAM_Write
	.extern TVRAM16_Read
	.extern TVRAM16_Write
	.extern VCtrl_Read
	.extern VCtrl_Write

aFetchMem:	.word	FetchMem

; @ ---- Read

rm_nop:		mov	r0, #0
		mov	pc, lr

cyclone_readmem24:
		bic	r0, r0, #0xff000000
		ldr	r2, aFetchMem
		cmp	r0, #0x00a00000
		bcs	rm24ext

		eor	r3, r0, #1
		ldrb	r0, [r3, r2]
		mov	pc, lr

rm24ext:	and	r3, r0, #0x00f80000
		adr	r12, rm24exttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
rm24exttbl:	.word	rm_buserr,		rm_buserr		; @ a
		.word	rm_buserr,		rm_buserr		; @ b
		.word	GVRAM_Read,		GVRAM_Read		; @ c
		.word	GVRAM_Read,		GVRAM_Read		; @ d
		.word	TVRAM_Read,		rm24e8			; @ e
		.word	rm24font,		rm24ipl			; @ f

rm24ipl:	cmp	r0, #0x00fc0000
		eorcs	r0, r0, #1
rm24font:	ldrb	r0, [r0, r2]
		mov	pc, lr

rm24e8:		and	r3, r0, #0x0007e000
		adr	r12, rm24e8tbl
		ldr	pc, [r12, r3, lsr #(13 - 2)]
rm24e8tbl:	.word	CRTC_Read, VCtrl_Read, DMA_Read, rm_nop		; @ e8
		.word	MFP_Read, RTC_Read, rm_nop, SysPort_Read
		.word	OPM_Read, ADPCM_Read, FDC_Read, SASI_Read	; @ e9
		.word	SCC_Read, PIA_Read, IOC_Read, rm_nop
		.word	SCSI_Read, rm_buserr, rm_buserr, rm_buserr	; @ ea
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		.word	BG_Read, BG_Read, BG_Read, BG_Read		; @ eb
		.word	BGRam_Read, BGRam_Read, BGRam_Read, BGRam_Read
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ec
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		.word	SRAM_Read, SRAM_Read, rm_nop, rm_nop		; @ ed
		.word	rm_nop, rm_nop, rm_nop, rm_nop
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ee
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ef
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr

cyclone_readmem24_word:
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_r_data
		cmp	r0, #0x00a00000
		bcs	rm24wext

		ldrh	r0, [r0, r2]
		mov	pc, lr

rm24wext:	and	r3, r0, #0x00f80000
		adr	r12, rm24wexttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
rm24wexttbl:	.word	rm_buserr,		rm_buserr		; @ a
		.word	rm_buserr,		rm_buserr		; @ b
		.word	GVRAM16_Read,		GVRAM16_Read		; @ c
		.word	GVRAM16_Read,		GVRAM16_Read		; @ d
		.word	TVRAM16_Read,		rm24we8			; @ e
		.word	rm24wfont,		rm24wipl		; @ f

rm24wipl:	cmp	r0, #0x00fc0000
		ldrcsh	r0, [r0, r2]
		movcs	pc, lr
rm24wfont:	add	r3, r0, #1
		ldrb	r1, [r0, r2]
		ldrb	r12, [r3, r2]
		orr	r0, r12, r1, lsl #8
		mov	pc, lr

rm24we8:	and	r3, r0, #0x0007c000
		adr	r12, rm24we8tbl
		ldr	pc, [r12, r3, lsr #(14 - 2)]
rm24we8tbl:	.word	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read
		.word	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read
		.word	Bus16_Read, rm_buserr, rm_buserr, rm_buserr
		.word	BG16_Read, BG16_Read, BGRam16_Read, BGRam16_Read
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		.word	SRAM16_Read, rm_nop, rm_nop, rm_nop
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		.word	rm_buserr, rm_buserr, rm_buserr, rm_buserr

Bus16_Read:	stmdb	sp!, {r4 - r5, lr}
		add	r4, r0, #1
		bl	rm24e8
rm_buserr_ret1:	mov	r5, r0, lsl #8
		mov	r0, r4
		ldr	r2, aFetchMem
		bl	rm24e8
rm_buserr_ret2:	orr	r0, r0, r5
		ldmia	sp!, {r4 - r5, pc}

cyclone_readmem24_dword:
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_r_data
		cmp	r0, #0x00a00000
		bcs	rm24dext

		add	r3, r0, #2
		ldrh	r1, [r0, r2]
		cmp	r3, #0x00a00000
		ldrcch	r12, [r3, r2]
		movcs	r12, #0
		orr	r0, r12, r1, lsl #16
		mov	pc, lr

rm24dext:	stmdb	sp!, {r4 - r5, lr}
		add	r4, r0, #2
		bl	rm24wext
rm_buserr_ret3:	mov	r5, r0, lsl #16
		mov	r0, r4
		ldr	r2, aFetchMem
		bl	rm24wext
rm_buserr_ret4:	orr	r0, r0, r5
		ldmia	sp!, {r4 - r5, pc}

rm_buserr:	adr	r3, rm_buserr_ret1
		cmp	r3, lr
		adr	r3, rm_buserr_ret2
		cmpne	r3, lr
		ldmeqia	sp!, {r4 - r5, lr}
		adr	r3, rm_buserr_ret3
		cmp	r3, lr
		adr	r3, rm_buserr_ret4
		cmpne	r3, lr
		ldmeqia	sp!, {r4 - r5, lr}
		b	ExceptionBusError_r

; @ ---- Write

cyclone_writemem24:
		bic	r0, r0, #0xff000000
		ldr	r2, aFetchMem
		cmp	r0, #0x00a00000
		bcs	wm24ext

		eor	r3, r0, #1
		strb	r1, [r3, r2]
wm_nop:		mov	pc, lr

wm24ext:	and	r3, r0, #0x00f80000
		adr	r12, wm24exttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
wm24exttbl:	.word	wm_buserr,		wm_buserr		; @ a
		.word	wm_buserr,		wm_buserr		; @ b
		.word	GVRAM_Write,		GVRAM_Write		; @ c
		.word	GVRAM_Write,		GVRAM_Write		; @ d
		.word	TVRAM_Write,		wm24e8			; @ e
		.word	wm_buserr,		wm_buserr		; @ f

wm24e8:		and	r3, r0, #0x0007e000
		adr	r12, wm24e8tbl
		ldr	pc, [r12, r3, lsr #(13 - 2)]
wm24e8tbl:	.word	CRTC_Write, VCtrl_Write, DMA_Write, wm_nop	; @ e8
		.word	MFP_Write, RTC_Write, wm_nop, SysPort_Write
		.word	OPM_Write, ADPCM_Write, FDC_Write, SASI_Write	; @ e9
		.word	SCC_Write, PIA_Write, IOC_Write, wm_nop
		.word	SCSI_Write, wm_buserr, wm_buserr, wm_buserr	; @ ea
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		.word	BG_Write, wm_nop, wm_nop, wm_nop		; @ eb
		.word	BGRam_Write, BGRam_Write, BGRam_Write, BGRam_Write
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ec
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		.word	SRAM_Write, SRAM_Write, wm_nop, wm_nop		; @ ed
		.word	wm_nop, wm_nop, wm_nop, wm_nop
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ee
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ef
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr

cyclone_writemem24_word:
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_w_data
		cmp	r0, #0x00a00000
		bcs	wm24wext

		strh	r1, [r0, r2]
		mov	pc, lr

wm24wext:	and	r3, r0, #0x00f80000
		adr	r12, wm24wexttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
wm24wexttbl:	.word	wm_buserr,		wm_buserr		; @ a
		.word	wm_buserr,		wm_buserr		; @ b
		.word	GVRAM16_Write,		GVRAM16_Write		; @ c
		.word	GVRAM16_Write,		GVRAM16_Write		; @ d
		.word	TVRAM16_Write,		wm24we8			; @ e
		.word	wm_buserr,		wm_buserr		; @ f

wm24we8:	and	r3, r0, #0x0007c000
		adr	r12, wm24we8tbl
		ldr	pc, [r12, r3, lsr #(14 - 2)]
wm24we8tbl:	.word	CrtVC16_Write, Bus16_Write, Bus16_Write, Bus16_Write
		.word	Bus16_Write, Bus16_Write, Bus16_Write, Bus16_Write
		.word	Bus16_Write, wm_buserr, wm_buserr, wm_buserr
		.word	BG16_Write, wm_nop, BGRam16_Write, BGRam16_Write
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		.word	SRAM16_Write, wm_nop, wm_nop, wm_nop
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		.word	wm_buserr, wm_buserr, wm_buserr, wm_buserr

Bus16_Write:	stmdb	sp!, {r0 - r2, lr}
		mov	r1, r1, lsr #8
		bl	wm24e8
wm_buserr_ret1:	ldmia	sp!, {r0 - r2, lr}
		add	r0, r0, #1
		and	r1, r1, #0xff
		b	wm24e8

cyclone_writemem24_dword:
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_w_data
		cmp	r0, #0xa00000
		bcs	wm24dext

		mov	r3, r1, lsr #16
		add	r12, r0, #2
		strh	r3, [r0, r2]
		cmp	r12, #0x00a00000
		strcch	r1, [r12, r2]
		mov	pc, lr

wm24dext:	stmdb	sp!, {r0 - r2, lr}
		mov	r1, r1, lsr #16
		bl	wm24wext
wm_buserr_ret2:	ldmia	sp!, {r0 - r2, lr}
		mov	r1, r1, lsl #16
		add	r0, r0, #2
		mov	r1, r1, lsr #16
		b	wm24wext

wm_buserr:	adr	r3, wm_buserr_ret1
		cmp	r3, lr
		ldmeqia	sp!, {r0 - r2, lr}
		adr	r3, wm_buserr_ret2
		cmp	r3, lr
		ldmeqia	sp!, {r0 - r2, lr}
		b	ExceptionBusError_w
