
	AREA	.text, CODE, READONLY

	export cyclone_readmem24
	export cyclone_readmem24_word
	export cyclone_readmem24_dword
	export cyclone_writemem24
	export cyclone_writemem24_word
	export cyclone_writemem24_dword
	import ExceptionBusError_r
	import ExceptionBusError_w
	import ExceptionAddressError_r_data
	import ExceptionAddressError_w_data
	import FetchMem
	import ADPCM_Read
	import ADPCM_Write
	import BG_Read
	import BG_Write
	import BG16_Read
	import BG16_Write
	import BGRam_Read
	import BGRam_Write
	import BGRam16_Read
	import BGRam16_Write
	import CRTC_Read
	import CRTC_Write
	import CrtVC16_Write
	import DMA_Read
	import DMA_Write
	import FDC_Read
	import FDC_Write
	import GVRAM_Read
	import GVRAM_Write
	import GVRAM16_Read
	import GVRAM16_Write
	import IOC_Read
	import IOC_Write
	import MFP_Read
	import MFP_Write
	import OPM_Read
	import OPM_Write
	import PIA_Read
	import PIA_Write
	import RTC_Read
	import RTC_Write
	import SASI_Read
	import SASI_Write
	import SCC_Read
	import SCC_Write
	import SCSI_Read
	import SCSI_Write
	import SCSI_Read
	import SCSI_Write
	import SRAM_Read
	import SRAM_Write
	import SRAM16_Read
	import SRAM16_Write
	import SysPort_Read
	import SysPort_Write
	import TVRAM_Read
	import TVRAM_Write
	import TVRAM16_Read
	import TVRAM16_Write
	import VCtrl_Read
	import VCtrl_Write

aFetchMem	dcd	FetchMem

; @ ---- Read

rm_nop		mov	r0, #0
		mov	pc, lr

cyclone_readmem24
		bic	r0, r0, #0xff000000
		ldr	r2, aFetchMem
		cmp	r0, #0x00a00000
		bcs	rm24ext

		eor	r3, r0, #1
		ldrb	r0, [r3, r2]
		mov	pc, lr

rm24ext		and	r3, r0, #0x00f80000
		adr	r12, rm24exttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
rm24exttbl	dcd	rm_buserr,		rm_buserr		; @ a
		dcd	rm_buserr,		rm_buserr		; @ b
		dcd	GVRAM_Read,		GVRAM_Read		; @ c
		dcd	GVRAM_Read,		GVRAM_Read		; @ d
		dcd	TVRAM_Read,		rm24e8			; @ e
		dcd	rm24font,		rm24ipl			; @ f

rm24ipl		cmp	r0, #0x00fc0000
		eorcs	r0, r0, #1
rm24font	ldrb	r0, [r0, r2]
		mov	pc, lr

rm24e8		and	r3, r0, #0x0007e000
		adr	r12, rm24e8tbl
		ldr	pc, [r12, r3, lsr #(13 - 2)]
rm24e8tbl	dcd	CRTC_Read, VCtrl_Read, DMA_Read, rm_nop		; @ e8
		dcd	MFP_Read, RTC_Read, rm_nop, SysPort_Read
		dcd	OPM_Read, ADPCM_Read, FDC_Read, SASI_Read	; @ e9
		dcd	SCC_Read, PIA_Read, IOC_Read, rm_nop
		dcd	SCSI_Read, rm_buserr, rm_buserr, rm_buserr	; @ ea
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dcd	BG_Read, BG_Read, BG_Read, BG_Read		; @ eb
		dcd	BGRam_Read, BGRam_Read, BGRam_Read, BGRam_Read
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ec
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dcd	SRAM_Read, SRAM_Read, rm_nop, rm_nop		; @ ed
		dcd	rm_nop, rm_nop, rm_nop, rm_nop
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ee
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr	; @ ef
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr

cyclone_readmem24_word
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_r_data
		cmp	r0, #0x00a00000
		bcs	rm24wext

		ldrh	r0, [r0, r2]
		mov	pc, lr
rm24wext	and	r3, r0, #0x00f80000
		adr	r12, rm24wexttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]

rm24wexttbl	dcd	rm_buserr,		rm_buserr		; @ a
		dcd	rm_buserr,		rm_buserr		; @ b
		dcd	GVRAM16_Read,		GVRAM16_Read		; @ c
		dcd	GVRAM16_Read,		GVRAM16_Read		; @ d
		dcd	TVRAM16_Read,		rm24we8			; @ e
		dcd	rm24wfont,		rm24wipl		; @ f

rm24wipl	cmp	r0, #0x00fc0000
		ldrcsh	r0, [r0, r2]
		movcs	pc, lr
rm24wfont	add	r3, r0, #1
		ldrb	r1, [r0, r2]
		ldrb	r12, [r3, r2]
		orr	r0, r12, r1, lsl #8
		mov	pc, lr

rm24we8		and	r3, r0, #0x0007c000
		adr	r12, rm24we8tbl
		ldr	pc, [r12, r3, lsr #(14 - 2)]
rm24we8tbl	dcd	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read
		dcd	Bus16_Read, Bus16_Read, Bus16_Read, Bus16_Read
		dcd	Bus16_Read, rm_buserr, rm_buserr, rm_buserr
		dcd	BG16_Read, BG16_Read, BGRam16_Read, BGRam16_Read
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dcd	SRAM16_Read, rm_nop, rm_nop, rm_nop
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr
		dcd	rm_buserr, rm_buserr, rm_buserr, rm_buserr

Bus16_Read	stmdb	sp!, {r4 - r5, lr}
		add	r4, r0, #1
		bl	rm24e8
rm_buserr_ret1	mov	r5, r0, lsl #8
		mov	r0, r4
		ldr	r2, aFetchMem
		bl	rm24e8
rm_buserr_ret2	orr	r0, r0, r5
		ldmia	sp!, {r4 - r5, pc}

cyclone_readmem24_dword
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_r_data
		cmp	r0, #0x00a00000
		bcs	rm24dext

		add	r3, r0, #2
		ldrh	r1, [r0, r2]
		cmp	r3, #0x00a00000
		ldrcch	r12, [r3, r2]
		movcs	r12, #0
		orr	r0, r12, r1, lsl #16
		mov	pc, lr

rm24dext	stmdb	sp!, {r4 - r5, lr}
		add	r4, r0, #2
		bl	rm24wext
rm_buserr_ret3	mov	r5, r0, lsl #16
		mov	r0, r4
		ldr	r2, aFetchMem
		bl	rm24wext
rm_buserr_ret4	orr	r0, r0, r5
		ldmia	sp!, {r4 - r5, pc}

rm_buserr	adr	r3, rm_buserr_ret1
		cmp	r3, lr
		adr	r3, rm_buserr_ret2
		cmpne	r3, lr
		ldmeqia	sp!, {r4 - r5, lr}
		adr	r3, rm_buserr_ret3
		cmp	r3, lr
		adr	r3, rm_buserr_ret4
		cmpne	r3, lr
		ldmeqia	sp!, {r4 - r5, lr}
		b	ExceptionBusError_r

; @ ---- Write

cyclone_writemem24
		bic	r0, r0, #0xff000000
		ldr	r2, aFetchMem
		cmp	r0, #0x00a00000
		bcs	wm24ext

		eor	r3, r0, #1
		strb	r1, [r3, r2]
wm_nop		mov	pc, lr

wm24ext		and	r3, r0, #0x00f80000
		adr	r12, wm24exttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
wm24exttbl	dcd	wm_buserr,		wm_buserr		; @ a
		dcd	wm_buserr,		wm_buserr		; @ b
		dcd	GVRAM_Write,		GVRAM_Write		; @ c
		dcd	GVRAM_Write,		GVRAM_Write		; @ d
		dcd	TVRAM_Write,		wm24e8			; @ e
		dcd	wm_buserr,		wm_buserr		; @ f

wm24e8		and	r3, r0, #0x0007e000
		adr	r12, wm24e8tbl
		ldr	pc, [r12, r3, lsr #(13 - 2)]
wm24e8tbl	dcd	CRTC_Write, VCtrl_Write, DMA_Write, wm_nop	; @ e8
		dcd	MFP_Write, RTC_Write, wm_nop, SysPort_Write
		dcd	OPM_Write, ADPCM_Write, FDC_Write, SASI_Write	; @ e9
		dcd	SCC_Write, PIA_Write, IOC_Write, wm_nop
		dcd	SCSI_Write, wm_buserr, wm_buserr, wm_buserr	; @ ea
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dcd	BG_Write, wm_nop, wm_nop, wm_nop		; @ eb
		dcd	BGRam_Write, BGRam_Write, BGRam_Write, BGRam_Write
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ec
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dcd	SRAM_Write, SRAM_Write, wm_nop, wm_nop		; @ ed
		dcd	wm_nop, wm_nop, wm_nop, wm_nop
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ee
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr	; @ ef
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr

cyclone_writemem24_word
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_w_data
		cmp	r0, #0x00a00000
		bcs	wm24wext

		strh	r1, [r0, r2]
		mov	pc, lr

wm24wext	and	r3, r0, #0x00f80000
		adr	r12, wm24wexttbl - (0x00a00000 >> (19 - 2))
		ldr	pc, [r12, r3, lsr #(19 - 2)]
wm24wexttbl	dcd	wm_buserr,		wm_buserr		; @ a
		dcd	wm_buserr,		wm_buserr		; @ b
		dcd	GVRAM16_Write,		GVRAM16_Write		; @ c
		dcd	GVRAM16_Write,		GVRAM16_Write		; @ d
		dcd	TVRAM16_Write,		wm24we8			; @ e
		dcd	wm_buserr,		wm_buserr		; @ f

wm24we8		and	r3, r0, #0x0007c000
		adr	r12, wm24we8tbl
		ldr	pc, [r12, r3, lsr #(14 - 2)]
wm24we8tbl	dcd	CrtVC16_Write, Bus16_Write, Bus16_Write, Bus16_Write
		dcd	Bus16_Write, Bus16_Write, Bus16_Write, Bus16_Write
		dcd	Bus16_Write, wm_buserr, wm_buserr, wm_buserr
		dcd	BG16_Write, wm_nop, BGRam16_Write, BGRam16_Write
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dcd	SRAM16_Write, wm_nop, wm_nop, wm_nop
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr
		dcd	wm_buserr, wm_buserr, wm_buserr, wm_buserr

Bus16_Write	stmdb	sp!, {r0 - r2, lr}
		mov	r1, r1, lsr #8
		bl	wm24e8
wm_buserr_ret1	ldmia	sp!, {r0 - r2, lr}
		add	r0, r0, #1
		and	r1, r1, #0xff
		b	wm24e8

cyclone_writemem24_dword
		tst	r0, #1
		ldr	r2, aFetchMem
		bic	r0, r0, #0xff000000
		bne	ExceptionAddressError_w_data
		cmp	r0, #0xa00000
		bcs	wm24dext

		mov	r3, r1, lsr #16
		add	r12, r0, #2
		strh	r3, [r0, r2]
		cmp	r12, #0x00a00000
		strcch	r1, [r12, r2]
		mov	pc, lr

wm24dext	stmdb	sp!, {r0 - r2, lr}
		mov	r1, r1, lsr #16
		bl	wm24wext
wm_buserr_ret2	ldmia	sp!, {r0 - r2, lr}
		mov	r1, r1, lsl #16
		add	r0, r0, #2
		mov	r1, r1, lsr #16
		b	wm24wext

wm_buserr	adr	r3, wm_buserr_ret1
		cmp	r3, lr
		ldmeqia	sp!, {r0 - r2, lr}
		adr	r3, wm_buserr_ret2
		cmp	r3, lr
		ldmeqia	sp!, {r0 - r2, lr}
		b	ExceptionBusError_w

	END
