﻿/******************************************************************************

	m68000.c

	M68000 CPUインタフェース関数

******************************************************************************/

#include "common.h"
#include "m68000.h"

#ifndef CYCLONE
#include "c68k/c68k.h"
#else
struct Cyclone m68k;
#endif

#include "../x68k/gvram.h"
#include "../x68k/memory.h"
#include "../x68k/scsi.h"
#include "../x68k/sram.h"
#include "../x68k/tvram.h"

int ICount;

EXTERNC uint32_t FASTCALL cpu_readmem24(uint32_t adr);
EXTERNC uint32_t FASTCALL cpu_readmem24_word(uint32_t adr);
EXTERNC uint32_t FASTCALL cpu_readmem24_dword(uint32_t adr);
EXTERNC void FASTCALL cpu_writemem24(uint32_t adr, uint_fast8_t data);
EXTERNC void FASTCALL cpu_writemem24_word(uint32_t adr, uint_fast16_t data);
EXTERNC void FASTCALL cpu_writemem24_dword(uint32_t adr, uint32_t data);

#ifdef CYCLONE

EXTERNC uint32_t cyclone_readmem24(uint32_t adr);
EXTERNC uint32_t cyclone_readmem24_word(uint32_t adr);
EXTERNC uint32_t cyclone_readmem24_dword(uint32_t adr);
EXTERNC void cyclone_writemem24(uint32_t adr, uint8_t data);
EXTERNC void cyclone_writemem24_word(uint32_t adr, uint16_t data);
EXTERNC void cyclone_writemem24_dword(uint32_t adr, uint32_t data);

static unsigned int MyCheckPc(unsigned int pc)
{
	const intptr_t a = pc - m68k.membase;		// Get the real program counter

#ifdef USE_FETCHMEM
	intptr_t base = (intptr_t)MEM;
#else	// USE_FETCHMEM
	intptr_t base;
	const intptr_t a24 = a & 0xffffff;
	if (a24 < 0xc00000)									base = (intptr_t)MEM;
	else if (a24 >= 0xfc0000)							base = (intptr_t)IPL - 0xfc0000;
	else if ((a24 >= 0xc00000) && (a24 <= 0xc80000))	base = (intptr_t)GVRAM - 0xc00000;
	else if ((a24 >= 0xe00000) && (a24 <= 0xe80000))	base = (intptr_t)TVRAM - 0xe00000;
	else if ((a24 >= 0xea0000) && (a24 <= 0xea2000))	base = (intptr_t)SCSIIPL - 0xea0000;
	else if ((a24 >= 0xed0000) && (a24 <= 0xed4000))	base = (intptr_t)SRAM - 0xed0000;
	else if ((a24 >= 0xf00000) && (a24 <= 0xfc0000))	base = (intptr_t)FONT - 0xf00000;
	else												return pc;
#endif	// USE_FETCHMEM

	base -= a & 0xff000000;
	m68k.membase = base;
	return base + a;			// New program counter
}

#endif	// CYCLONE

/******************************************************************************
	M68000インタフェース関数
******************************************************************************/

/*--------------------------------------------------------
	CPU初期化
--------------------------------------------------------*/
s32 FASTCALL my_irqh_callback(s32 level);

void m68000_init(void)
{
#ifdef CYCLONE

	m68k.membase = (intptr_t)MEM;
	m68k.checkpc = MyCheckPc;

	m68k.read8  = cyclone_readmem24;
	m68k.read16 = cyclone_readmem24_word;
	m68k.read32 = cyclone_readmem24_dword;

	m68k.write8  = cyclone_writemem24;
	m68k.write16 = cyclone_writemem24_word;
	m68k.write32 = cyclone_writemem24_dword;

	m68k.fetch8  = m68k.read8;
	m68k.fetch16 = m68k.read16;
	m68k.fetch32 = m68k.read32;

	m68k.IrqCallback = my_irqh_callback;

	CycloneInit();

#else	// CYCLONE

    C68k_Init(&C68K, my_irqh_callback);
    C68k_Set_ReadB(&C68K, (C68K_READ *)cpu_readmem24);
    C68k_Set_ReadW(&C68K, (C68K_READ *)cpu_readmem24_word);
    C68k_Set_ReadD(&C68K, (C68K_READ *)cpu_readmem24_dword);
    C68k_Set_WriteB(&C68K, (C68K_WRITE *)cpu_writemem24);
    C68k_Set_WriteW(&C68K, (C68K_WRITE *)cpu_writemem24_word);
    C68k_Set_WriteD(&C68K, (C68K_WRITE *)cpu_writemem24_dword);

        C68k_Set_Fetch(&C68K, 0x000000, 0xbfffff, (pointer)MEM);
        C68k_Set_Fetch(&C68K, 0xc00000, 0xc7ffff, (pointer)GVRAM);
        C68k_Set_Fetch(&C68K, 0xe00000, 0xe7ffff, (pointer)TVRAM);
        C68k_Set_Fetch(&C68K, 0xea0000, 0xea1fff, (pointer)SCSIIPL);
        C68k_Set_Fetch(&C68K, 0xed0000, 0xed3fff, (pointer)SRAM);
        C68k_Set_Fetch(&C68K, 0xf00000, 0xfbffff, (pointer)FONT);
        C68k_Set_Fetch(&C68K, 0xfc0000, 0xffffff, (pointer)IPL);

#endif	// CYCLONE
}


/*--------------------------------------------------------
	CPUリセット
--------------------------------------------------------*/

void m68000_reset(void)
{
#ifdef CYCLONE
	m68k.membase = (intptr_t)MEM;
	CycloneReset(&m68k);
	m68k.state_flags = 0; // Go to default state (not stopped, halted, etc.)
	m68k.srh = 0x27; // Set supervisor mode
#else	// CYCLONE
	C68k_Reset(&C68K);
#endif	// CYCLONE
}


/*--------------------------------------------------------
	CPU停止
--------------------------------------------------------*/

void m68000_exit(void)
{
}


/*--------------------------------------------------------
	CPU実行
--------------------------------------------------------*/

int m68000_execute(int cycles)
{
#ifdef CYCLONE
	m68k.cycles = cycles;
	CycloneRun(&m68k);
	return cycles - m68k.cycles;
#else
	return C68k_Exec(&C68K, cycles);
#endif
}


/*--------------------------------------------------------
	割り込み処理
--------------------------------------------------------*/

void m68000_set_irq_line(int irqline, int state)
{
#ifdef CYCLONE
	m68k.irq = irqline;
#else
	C68k_Set_IRQ(&C68K, irqline);
#endif	// CYCLONE
}


/*--------------------------------------------------------
	割り込みコールバック関数設定
--------------------------------------------------------*/

void m68000_set_irq_callback(int (*callback)(int line))
{
//	C68k_Set_IRQ_Callback(&C68K, callback);
}


/*--------------------------------------------------------
	レジスタ取得
--------------------------------------------------------*/

uint32_t m68000_get_reg(int regnum)
{
	switch (regnum)
	{
#ifdef CYCLONE
		case M68K_PC: return m68k.pc - m68k.membase;
		case M68K_SR: return CycloneGetSr(&m68k);
		case M68K_D0: return m68k.d[0];
		case M68K_D1: return m68k.d[1];
		case M68K_D2: return m68k.d[2];
		case M68K_D3: return m68k.d[3];
		case M68K_D4: return m68k.d[4];
		case M68K_D5: return m68k.d[5];
		case M68K_D6: return m68k.d[6];
		case M68K_D7: return m68k.d[7];
		case M68K_A0: return m68k.a[0];
		case M68K_A1: return m68k.a[1];
		case M68K_A2: return m68k.a[2];
		case M68K_A3: return m68k.a[3];
		case M68K_A4: return m68k.a[4];
		case M68K_A5: return m68k.a[5];
		case M68K_A6: return m68k.a[6];
		case M68K_A7: return m68k.a[7];
#else	// CYCLONE
		case M68K_PC:  return C68k_Get_PC(&C68K);
		case M68K_USP: return C68k_Get_USP(&C68K);
		case M68K_MSP: return C68k_Get_MSP(&C68K);
		case M68K_SR:  return C68k_Get_SR(&C68K);
		case M68K_D0:  return C68k_Get_DReg(&C68K, 0);
		case M68K_D1:  return C68k_Get_DReg(&C68K, 1);
		case M68K_D2:  return C68k_Get_DReg(&C68K, 2);
		case M68K_D3:  return C68k_Get_DReg(&C68K, 3);
		case M68K_D4:  return C68k_Get_DReg(&C68K, 4);
		case M68K_D5:  return C68k_Get_DReg(&C68K, 5);
		case M68K_D6:  return C68k_Get_DReg(&C68K, 6);
		case M68K_D7:  return C68k_Get_DReg(&C68K, 7);
		case M68K_A0:  return C68k_Get_AReg(&C68K, 0);
		case M68K_A1:  return C68k_Get_AReg(&C68K, 1);
		case M68K_A2:  return C68k_Get_AReg(&C68K, 2);
		case M68K_A3:  return C68k_Get_AReg(&C68K, 3);
		case M68K_A4:  return C68k_Get_AReg(&C68K, 4);
		case M68K_A5:  return C68k_Get_AReg(&C68K, 5);
		case M68K_A6:  return C68k_Get_AReg(&C68K, 6);
		case M68K_A7:  return C68k_Get_AReg(&C68K, 7);
#endif	// CYCLONE
		default: return 0;
	}
}


/*--------------------------------------------------------
	レジスタ設定
--------------------------------------------------------*/

void m68000_set_reg(int regnum, uint32_t val)
{
	switch (regnum)
	{
#ifdef CYCLONE
		case M68K_PC:
		  	m68k.pc = m68k.checkpc(val+m68k.membase);
			break;
		case M68K_SR:
		  	CycloneSetSr(&m68k, val);
			break;
		case M68K_D0: m68k.d[0] = val; break;
		case M68K_D1: m68k.d[1] = val; break;
		case M68K_D2: m68k.d[2] = val; break;
		case M68K_D3: m68k.d[3] = val; break;
		case M68K_D4: m68k.d[4] = val; break;
		case M68K_D5: m68k.d[5] = val; break;
		case M68K_D6: m68k.d[6] = val; break;
		case M68K_D7: m68k.d[7] = val; break;
		case M68K_A0: m68k.a[0] = val; break;
		case M68K_A1: m68k.a[1] = val; break;
		case M68K_A2: m68k.a[2] = val; break;
		case M68K_A3: m68k.a[3] = val; break;
		case M68K_A4: m68k.a[4] = val; break;
		case M68K_A5: m68k.a[5] = val; break;
		case M68K_A6: m68k.a[6] = val; break;
		case M68K_A7: m68k.a[7] = val; break;
#else	// CYCLONE
		case M68K_PC:  C68k_Set_PC(&C68K, val); break;
		case M68K_USP: C68k_Set_USP(&C68K, val); break;
		case M68K_MSP: C68k_Set_MSP(&C68K, val); break;
		case M68K_SR:  C68k_Set_SR(&C68K, val); break;
		case M68K_D0:  C68k_Set_DReg(&C68K, 0, val); break;
		case M68K_D1:  C68k_Set_DReg(&C68K, 1, val); break;
		case M68K_D2:  C68k_Set_DReg(&C68K, 2, val); break;
		case M68K_D3:  C68k_Set_DReg(&C68K, 3, val); break;
		case M68K_D4:  C68k_Set_DReg(&C68K, 4, val); break;
		case M68K_D5:  C68k_Set_DReg(&C68K, 5, val); break;
		case M68K_D6:  C68k_Set_DReg(&C68K, 6, val); break;
		case M68K_D7:  C68k_Set_DReg(&C68K, 7, val); break;
		case M68K_A0:  C68k_Set_AReg(&C68K, 0, val); break;
		case M68K_A1:  C68k_Set_AReg(&C68K, 1, val); break;
		case M68K_A2:  C68k_Set_AReg(&C68K, 2, val); break;
		case M68K_A3:  C68k_Set_AReg(&C68K, 3, val); break;
		case M68K_A4:  C68k_Set_AReg(&C68K, 4, val); break;
		case M68K_A5:  C68k_Set_AReg(&C68K, 5, val); break;
		case M68K_A6:  C68k_Set_AReg(&C68K, 6, val); break;
		case M68K_A7:  C68k_Set_AReg(&C68K, 7, val); break;
#endif	// CYCLONE
	default: break;
	}
}


/*------------------------------------------------------
	セーブ/ロード ステート
------------------------------------------------------*/

#ifdef SAVE_STATE

STATE_SAVE( m68000 )
{
	#ifdef CYCLONE
	
	#else

	int i;
	uint32_t pc = C68k_Get_Reg(&C68K, C68K_PC);

	for (i = 0; i < 8; i++)
		state_save_long(&C68K.D[i], 1);
	for (i = 0; i < 8; i++)
		state_save_long(&C68K.A[i], 1);

	state_save_long(&C68K.flag_C, 1);
	state_save_long(&C68K.flag_V, 1);
	state_save_long(&C68K.flag_Z, 1);
	state_save_long(&C68K.flag_N, 1);
	state_save_long(&C68K.flag_X, 1);
	state_save_long(&C68K.flag_I, 1);
	state_save_long(&C68K.flag_S, 1);
	state_save_long(&C68K.USP, 1);
	state_save_long(&pc, 1);
	state_save_long(&C68K.HaltState, 1);
	state_save_long(&C68K.IRQLine, 1);
	state_save_long(&C68K.IRQState, 1);
	#endif
}

STATE_LOAD( m68000 )
{
	#ifdef CYCLONE
	
	#else

	int i;
	uint32_t pc;

	for (i = 0; i < 8; i++)
		state_load_long(&C68K.D[i], 1);
	for (i = 0; i < 8; i++)
		state_load_long(&C68K.A[i], 1);

	state_load_long(&C68K.flag_C, 1);
	state_load_long(&C68K.flag_V, 1);
	state_load_long(&C68K.flag_Z, 1);
	state_load_long(&C68K.flag_N, 1);
	state_load_long(&C68K.flag_X, 1);
	state_load_long(&C68K.flag_I, 1);
	state_load_long(&C68K.flag_S, 1);
	state_load_long(&C68K.USP, 1);
	state_load_long(&pc, 1);
	state_load_long(&C68K.HaltState, 1);
	state_load_long(&C68K.IRQLine, 1);
	state_load_long(&C68K.IRQState, 1);

	C68k_Set_Reg(&C68K, C68K_PC, pc);
	#endif
}

#endif /* SAVE_STATE */
