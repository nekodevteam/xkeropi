/**
 * @file	shlwapi.cpp
 * @brief	パス関係クラスの動作の定義を行います
 */

#include "windows.h"
#include "shlwapi.h"
#include <algorithm>

/**
 * セパレータか?
 * @param[in] c キャラクタ
 * @retval true セパレータである
 * @retval false セパレータでない
 */
inline static bool IsSeparator(TCHAR c)
{
	return (c == TEXT('/'));
}

/**
 * 長さを得る
 * @param[in] pszPath パス
 * @return 長さ
 */
static int Length(LPCTSTR pszPath)
{
	int nIndex = 0;
	if (pszPath)
	{
		while (pszPath[nIndex] != TEXT('\0'))
		{
			nIndex++;
		}
	}
	return nIndex;
}

/**
 * 追加
 * @param[in,out] pszPath パス
 * @param[in] cchPath パスサイズ
 * @param[in] pszAdd 追加
 * @retval true すべてコピーした
 * @retval false オーバーフローした
 */
static bool Cat(LPTSTR pszPath, int cchPath, LPCTSTR pszAdd)
{
	bool r = true;
	int nSize = Length(pszAdd);
	if (nSize > 0)
	{
		const int nStart = Length(pszPath);
		const int nLimit = cchPath - nStart - 1;
		if (nSize > nLimit)
		{
			r = false;
			nSize = nLimit;
		}
		if (nSize > 0)
		{
			memcpy(pszPath + nStart, pszAdd, nSize * sizeof(TCHAR));
			pszPath[nStart + nSize] = TEXT('\0');
		}
	}
	return r;
}

/**
 * コピー
 * @param[in,out] pszPath パス
 * @param[in] cchPath パスサイズ
 * @param[in] pszAdd 追加
 */
static void Cpy(LPTSTR pszPath, int cchPath, LPCTSTR pszAdd)
{
	if (cchPath > 0)
	{
		int nSize = Length(pszAdd);
		nSize = (std::min)(nSize, cchPath - 1);
		if (nSize > 0)
		{
			memcpy(pszPath, pszAdd, nSize * sizeof(TCHAR));
		}
		pszPath[nSize] = TEXT('\0');
	}
}

/**
 * Adds a backslash to the end of a string to create the correct syntax for a path.
 * @param[in, out] pszPath A pointer to a buffer with a string that represents a path
 * @return A pointer
 */
LPTSTR PathAddBackslash(LPTSTR pszPath)
{
	int nIndex = Length(pszPath);
	if ((nIndex > 0) && (!IsSeparator(pszPath[nIndex - 1])) && ((nIndex + 2) < MAX_PATH))
	{
		pszPath[nIndex] = TEXT('/');
		nIndex++;
		pszPath[nIndex] = TEXT('\0');
	}
	return pszPath + nIndex;
}

/**
 * Appends one path to the end of another
 * @param[in,out] pszPath A pointer to a null-terminated string to which the path specified in pszMore is appended
 * @param[in] pszMore A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to be appended
 * @retval true if successful
 * @retval false Otherwise
 */
BOOL PathAppend(LPTSTR pszPath, LPCTSTR pszMore)
{
	PathAddBackslash(pszPath);
	const bool r = Cat(pszPath, MAX_PATH, pszMore);

	const int nIndex = Length(pszPath);
	if ((nIndex > 0) && (pszPath[nIndex - 1] == TEXT('.')))
	{
		pszPath[nIndex - 1] = TEXT('\0');
	}
	return (r) ? TRUE : FALSE;
}

/**
 * Concatenates two strings that represent properly formed paths into one path
 * @param[in,out] pszDest A pointer to a buffer that, when this function returns successfully, receives the combined path string
 * @param[in] pszDir A pointer to a null-terminated string of maximum length MAX_PATH that contains the first path
 * @param[in] pszFile A pointer to a null-terminated string of maximum length MAX_PATH that contains the second path
 * @return A pointer to a buffer
 */
LPTSTR PathCombine(LPTSTR pszDest, LPCTSTR pszDir, LPCTSTR pszFile)
{
	if (pszDest != pszDir)
	{
		Cpy(pszDest, MAX_PATH, pszDir);
	}
	if (pszFile)
	{
		PathAppend(pszDest, pszFile);
	}
	return pszDest;
}

/**
 * Searches a path for an extension
 * @param[in] pszPath A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to search, including the extension being searched for
 * @return The address of the "."
 */
LPTSTR PathFindExtension(LPCTSTR pszPath)
{
	if (pszPath == NULL)
	{
		return NULL;
	}

	LPCTSTR ret = NULL;
	while (1 /*CONSTCOND*/)
	{
		TCHAR c = *pszPath;
		if (c == TEXT('\0'))
		{
			if (ret == NULL)
			{
				ret = pszPath;
			}
			break;
		}
		else if (IsSeparator(c))
		{
			ret = NULL;
		}
		else if (c == TEXT('.'))
		{
			ret = pszPath;
		}
		pszPath++;
	}
	return const_cast<LPTSTR>(ret);
}

/**
 * Searches a path for a file name
 * @param[in] pszPath A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to search
 * @return A pointer to the address
 */
LPTSTR PathFindFileName(LPCTSTR pszPath)
{
	if (pszPath == NULL)
	{
		return NULL;
	}

	LPCTSTR ret = pszPath;
	while (1 /*CONSTCOND*/)
	{
		TCHAR c = *pszPath++;
		if (c == TEXT('\0'))
		{
			break;
		}
		else if (IsSeparator(c))
		{
			ret = pszPath;
		}
	}
	return const_cast<LPTSTR>(ret);
}

